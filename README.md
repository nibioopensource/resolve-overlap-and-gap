
# resolve-overlap-and-gap

PostgreSQL procedure to resolve overlaps and gaps in simple feature
layers representing planar topologies using PostGIS Topology.

Input requirements:
- Must be of Polygon type
- Must have a single primary key
- If we have surfaces of simple feature  and `st_valid` attributes on
  the surfaces will not be used. (???)
- As output we only support degrees now 4258

# Runtime requirements:
- Postgres 15 or higher
- PostGIS Topology
- dblink (requirement of `postgres_execute_parallel`)

# postgis 3.5.0 master  (we see big errors related to postgis topology on postgis 3.5 also, so you need to checkout out commit 4fcfd0fd64b9b42c8120aa2cd9a4f16733cae863 and compile your own code now. )

There is a fix https://trac.osgeo.org/postgis/ticket/5394 that will come postgis 3.3.4, 3.4.0 that fixes this bug https://gitlab.com/nibioopensource/resolve-overlap-and-gap/-/pipelines/922704533



Included code:
- https://github.com/larsop/content_balanced_grid
- https://gitlab.com/nibioopensource/postgres_execute_parallel.git
- https://github.com/larsop/find-overlap-and-gap
- https://gitlab.com/nibioopensource/pgtopo_update_sql


# To checkout code
```
git clone --recursive https://gitlab.com/nibioopensource/resolve-overlap-and-gap.git
cd resolve-overlap-and-gap
```

# To create the install scipts
make

# To run regress tests (may take some minuttes, you may skip this step if you only need the install scrips)
make check

# Last pipiline status on develop branch
[![pipeline status](https://gitlab.com/nibioopensource/resolve-overlap-and-gap/badges/develop/pipeline.svg)](https://gitlab.com/nibioopensource/resolve-overlap-and-gap/-/commits/develop)

# To load code and all deps into database (must run make first)

```
export DB=foss4g_2024

psql --set ON_ERROR_STOP=1 -c "CREATE DATABASE $DB template=template0;"

psql --set ON_ERROR_STOP=1 $DB -c "DROP schema iF exists topo_rog_static cascade"

psql --set ON_ERROR_STOP=1 -f install-runtime-deps.sql $DB

psql --set ON_ERROR_STOP=1 -f resolve_overlap_and_gap-static.sql $DB

psql --set ON_ERROR_STOP=1 -f src/test/foss4g_2024_test.sql $DB

```

If the above runs with any errors, we have an [wiki page](https://gitlab.com/nibioopensource/resolve-overlap-and-gap/-/wikis/Simple-Feature-and-Postgis-overlays) where we have more test samples.

# To check the result
1) QGIS
2) Load TopoViewer from DB Manager in QGIS
3) Or just plain psql 'psql $DB'


- The following is implemented
- Load all lines into Postgis Topology
- Smooth lines if requested
- Collapse small/tiny surfaces if requested
- Collapse small/tiny gaps if requested
- Create new a new simple Feature layer
- Add attributes and assign values
- Support multiple tables/views in input
- Input only table
- - and we compute geoemetry and pk column in psql
- Input can have both Polygon and MulitiPolygon
- - if more than one geomtri column we take the first one
- Input can have different srids
- Simple feauture table view <schema>.multi_input_<num tables>_tables_result_v has a ref to all gaps and overlaps in the input tables
- - after running foss4g_2023_test.sql we get test_topo_ar5_test3.multi_input_2_tables_result
- Simple feauture result view <schema>.multi_input_<num tables>_tables_result_v has a ref to all attribute columns
- - after running foss4g_2023_test.sql we get test_topo_ar5_test3.multi_input_2_tables_result_v

