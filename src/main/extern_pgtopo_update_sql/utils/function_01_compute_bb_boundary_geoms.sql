/**
 * Compute boundary geoms for given bbox
 */


CREATE OR REPLACE FUNCTION topo_update.compute_bb_boundary_geoms(

_bb geometry, -- The bb box to compute for

_utm boolean, -- We need to know if UTM or degrees are used

_zero_topology_info_topology_snap_tolerance float
-- We get this as separate variable to handle 0 values
-- This will used when securing border line spacing between grid cell when topology_snap_tolerance  is zero


)
  RETURNS rog_bb_boundary_geoms
  AS $$
DECLARE
   -- This a area where do not touch or move any points
  boundary_geom geometry;

  -- this is the inner part of boundary_geom
  inner_boundary_geom geometry;

  -- this is the outer part of boundary_geom
  outer_boundary_geom geometry;


  -- the result to return
  result rog_bb_boundary_geoms;


BEGIN

  RAISE NOTICE 'Compute rog_bb_boundary_geoms for _bb %, _utm %, _zero_topology_info_topology_snap_tolerance  %',
  _bb, _utm, _zero_topology_info_topology_snap_tolerance;


  IF _utm = false THEN
    inner_boundary_geom := ST_Buffer(_bb, (-2.0*_zero_topology_info_topology_snap_tolerance));
    outer_boundary_geom := ST_Buffer(_bb, (+2.0*_zero_topology_info_topology_snap_tolerance));
  ELSE
    inner_boundary_geom := ST_Buffer(_bb, (-8.0*_zero_topology_info_topology_snap_tolerance));
    outer_boundary_geom := ST_Buffer(_bb, (+8.0*_zero_topology_info_topology_snap_tolerance));
  END IF;

  boundary_geom := ST_MakePolygon ((
      SELECT ST_ExteriorRing (outer_boundary_geom) AS outer_ring), ARRAY (
       SELECT ST_ExteriorRing (inner_boundary_geom) AS inner_rings));

  result = (_bb,boundary_geom,inner_boundary_geom,outer_boundary_geom);

  RETURN result;
END;

$$
LANGUAGE plpgsql
STABLE;

