CREATE OR REPLACE FUNCTION topo_update.do_find_edges_to_heal (_atopology varchar, _bb geometry, _outer_cell_boundary_lines geometry default null)
  RETURNS integer[][]
  LANGUAGE 'plpgsql'
  AS $function$
DECLARE
  command_string text;
  loop_nr int = 0;
  max_loops int = 15;

  edges_to_fix  integer[][];
  edges integer[];
  edge_ids_found int;

  heal_result int;
  edges_fixed int;
  edges_mising int;

  start_time_delta_job timestamp WITH time zone;


BEGIN

    start_time_delta_job := Clock_timestamp();

    command_string := Format('SELECT ARRAY( SELECT ARRAY[r.edge_to_live, r.edge_to_eat]
    FROM (
        SELECT DISTINCT ON (r.edge_to_eat) r.edge_to_eat, r.edge_to_live
        from (
        SELECT
        r.edge_to_live,
        r.edge_to_eat
        FROM
        (
            SELECT e1.geom as edge_to_eat_geom, e1.edge_id as edge_to_eat, e2.edge_id as edge_to_live
            FROM
            %1$s.edge_data e1,
            %1$s.edge_data e2,
        %1$s.node n1,
            (
                select r.node_id as node_id
                from (
                    select count(n1.node_id) num_edges_end_here, n1.node_id as node_id
                    from
                    %1$s.node n1,
                    %1$s.edge_data e1
                    where e1.geom &&  %2$L and
            n1.geom &&  %2$L and
            ST_intersects(e1.geom,%2$L) and
                    (ST_Disjoint(e1.geom,%3$L) OR %3$L is null)
                    and
                    (e1.start_node = n1.node_id or e1.end_node = n1.node_id)
                    group by n1.node_id
                ) as r
                where r.num_edges_end_here = 2
            ) as r
            where (e1.start_node = r.node_id or e1.end_node = r.node_id) and
            (e2.start_node = r.node_id or e2.end_node = r.node_id) and
        r.node_id = n1.node_id and
            e1.start_node != e1.end_node and
            e2.start_node != e2.end_node and --to avoid a closed surface ending in a line

            -- we have to add this because we have big face that covers very man polygons
            -- and then an edge should be healded even if two face on the sane are not equal
            --(
            --  (e2.left_face = e1.left_face and e2.right_face = e1.right_face ) or
            --  (e2.left_face = e2.right_face or e1.left_face = e1.right_face)
            --) and
            e2.left_face = e1.left_face and e2.right_face = e1.right_face and
            ST_CoveredBy(n1.geom,%2$L) and
            (ST_Disjoint(e1.geom,%3$L) OR %3$L is null) and (ST_Disjoint(e2.geom,%3$L) OR %3$L is null)
        ) as r
        ) as r
        order by edge_to_eat
    ) as r
    where r.edge_to_live != r.edge_to_eat)', _atopology, _bb, _outer_cell_boundary_lines);
    EXECUTE command_string into edges_to_fix;

    RAISE NOTICE 'Did find % edes to heal lines for topo % and bb % at % used_time %',
    (Array_length(edges_to_fix, 1)), _atopology, _bb, Clock_timestamp(), (Extract(EPOCH FROM (Clock_timestamp() - start_time_delta_job)));



  RETURN edges_to_fix;
END
$function$;



