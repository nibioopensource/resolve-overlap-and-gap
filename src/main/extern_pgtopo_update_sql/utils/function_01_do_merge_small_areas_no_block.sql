/**
 * If possible we here merge small areas in to bigger areas by remoing one or more egdes
 *
 */


CREATE OR REPLACE PROCEDURE topo_update.do_merge_small_areas_no_block (
_atopology varchar,
_table_name_result_prefix varchar,
_min_area float,
_min_st_is_empty_buffer_value float,
_bb geometry,
_utm boolean,
_cell_job_type int,
INOUT _more_edges_to_check_for_removel boolean,
_fixed_point_set geometry default null,
face_edges_to_remove integer[] default null,
table_input_order int default 0
)
LANGUAGE plpgsql
AS $$
DECLARE
	command_string_find text;
	command_string_update_result text;
	command_string text;
	num_rows int = 0;
	num_failed_remove_rows int = 0;
	num_not_valid_to_remove int = 0;
	-- Based on testing and it's not accurate at all
	min_mbr_area float = _min_area * 1000;
	edge_id_list_to_remove integer[];
	edge_id_list_removed_ok integer[];
	edge_id_list_not_ok_remove integer[];

	edge_id_tmp integer;
	remove_edge integer;
	v_state text;
	v_msg text;
	v_detail text;
	v_hint text;
	v_context text;
	tbl_name_edges_to_remove text;

	topo_info_not_removed text;
	this_more_info text;

	min_face_id int = -1;

	this_left_face_area real;
	this_right_face_area real;

	this_left_face_geom geometry;
	this_right_face_geom geometry;

	start_time timestamp WITH time zone;
	used_time real;
	min_st_is_empty_buffer_value float = NULL;

	max_edges_to_delete int;
BEGIN

	start_time := Clock_timestamp();


	IF _min_st_is_empty_buffer_value IS NOT NULL AND _min_st_is_empty_buffer_value > 0 THEN
		min_st_is_empty_buffer_value = - (_min_st_is_empty_buffer_value) ;
	END IF;

	-- RAISE NOTICE 'do_merge_small_areas_no_block _min_st_is_empty_buffer_value %   min_st_is_empty_buffer_value %', _min_st_is_empty_buffer_value, min_st_is_empty_buffer_value;

	-- Set default value false and set true of read from table by limit rows
	_more_edges_to_check_for_removel = FALSE;


	IF face_edges_to_remove IS NOT NULL THEN
		-- get edges to remove for temp topologies
		edge_id_list_to_remove = face_edges_to_remove;

		tbl_name_edges_to_remove  = NULL;

	ELSE
		IF _cell_job_type = 3 THEN
			-- just test a number here for now, when 3.500 we got big problems
			max_edges_to_delete = 100;
			RAISE NOTICE 'In do_merge_small_areas_no_block for cell_job_type % from % using table % set max_edges_to_delete to % and st_area of _bb % ',
			_cell_job_type, _atopology||'.face' , tbl_name_edges_to_remove, max_edges_to_delete, ST_Area(_bb,true);

		END IF;



		tbl_name_edges_to_remove  = _table_name_result_prefix||'_edges_to_remove';

		-- get edges to remove
		command_string_find := format(
		$fmt$
			SELECT ARRAY(
				SELECT ARRAY[g.edge_id]
				FROM
				( SELECT DISTINCT edge_id, table_input_order , g.geom
						FROM
						( SELECT e.edge_id, e.geom, er.table_input_order
					FROM
					%1$s.edge e,
					%1$s.face lf,
					%1$s.face rf,
					%4$s er
					WHERE is_handled_at IS NULL AND
					e.left_face = lf.face_id AND
					e.right_face = rf.face_id AND
					e.left_face > %5$s AND e.right_face > %5$s AND
					e.left_face != e.right_face AND

					(
						%2$L IS NULL
						OR
						(
							ST_Intersects(e.geom,%2$L)
							-- This two tests are needed when working on final toplogies or else we may ende wihh Topology validate errors
							AND (e.left_face = 0 OR ST_CoveredBy(lf.mbr,%2$L))
							AND (e.left_face = 0 OR ST_CoveredBy(rf.mbr,%2$L))
						)
					) AND
					e.edge_id = er.edge_id
				) g WHERE (ST_Disjoint(g.geom,%3$L) OR %3$L is null)
		) g ORDER BY table_input_order desc ,  ST_X(ST_Centroid(g.geom)), ST_Y(ST_Centroid(g.geom)), ST_Length(g.geom,true) desc limit %6$L
			)
		$fmt$,
		_atopology, --01
		_bb, --02
		_fixed_point_set, --03
		tbl_name_edges_to_remove, --04
		min_face_id, --05
		max_edges_to_delete --06
		);

		-- find egdes to remove
		EXECUTE command_string_find INTO edge_id_list_to_remove;

		IF Array_length(edge_id_list_to_remove, 1) = max_edges_to_delete THEN
			_more_edges_to_check_for_removel = true;
		END IF;

		RAISE NOTICE 'In do_merge_small_areas_no_block for cell_job_type % and min_face_id % , found % number of edges for tiny faces tiny polygons from % using table % used _fixed_point_set size % and st_area of _bb % ',
		_cell_job_type, min_face_id, Array_length(edge_id_list_to_remove, 1), _atopology||'.face' , tbl_name_edges_to_remove,  ST_NPoints(_fixed_point_set), ST_Area(_bb,true);

	END IF;


--  RAISE NOTICE 'In do_merge_small_areas_no_block for cell_job_type % and min_face_id % , found % edges for tiny faces tiny polygons from % using % used _fixed_point_set size % and st_area of _bb % ',
--  _cell_job_type, min_face_id, Array_length(edge_id_list_to_remove, 1), _atopology||'.face' , tbl_name_edges_to_remove,  ST_NPoints(_fixed_point_set), ST_Area(_bb,true);

	IF edge_id_list_to_remove IS NOT NULL AND (Array_length(edge_id_list_to_remove, 1)) IS NOT NULL THEN

		FOREACH edge_id_tmp IN ARRAY edge_id_list_to_remove
				LOOP

					command_string_find := format(
						$fmt$
							SELECT
							CASE WHEN e.left_face > 0 THEN topology.ST_GetFaceGeometry(%1$L, e.left_face)
							ELSE NULL
							END AS left_face_geom,
							CASE WHEN e.right_face > 0 THEN topology.ST_GetFaceGeometry(%1$L, e.right_face)
							ELSE NULL
							END AS right_face_geom
							FROM
					%1$I.edge e
					WHERE e.edge_id = %2$s  AND e.left_face != e.right_face
					$fmt$,
					_atopology,
					edge_id_tmp,
					_min_area,
					tbl_name_edges_to_remove
					);

					EXECUTE command_string_find INTO this_left_face_geom, this_right_face_geom;

					IF this_left_face_geom IS NOT NULL THEN
						this_left_face_area = ST_Area(this_left_face_geom,true);
					ELSE
						this_left_face_area = 0;
					END IF;

					IF this_right_face_geom IS NOT NULL THEN
						this_right_face_area = ST_Area(this_right_face_geom,true);
					ELSE
						this_right_face_area = 0;
					END IF;


					this_more_info := format(
						$fmt$ cell_job_type:%1$s edge:%2$s num: %3$s min_face_id: %4$s left_face_area: %5$s right_face_area: %6$s  %7$s $fmt$,
						_cell_job_type,
				edge_id_tmp,
				to_char(num_rows , '00000') ,
				min_face_id,
				this_left_face_area,
				this_right_face_area,
				ST_AsEWKT(ST_PointOnSurface(_bb))
			);


					--this test fails because area has changed
					--IF ( this_left_face_area <= this_face_area )
					--OR ( this_right_face_area <= this_face_area ) THEN

					--this test fails because area has changed
					--IF ( (this_left_face_area < _min_area OR this_left_face_area <= this_face_area OR ST_IsEmpty(ST_Buffer(this_left_face_geom,-0.00002)) )  AND this_left_face_area > 0)
					--OR ( (this_right_face_area < _min_area OR this_right_face_area <= this_face_area OR ST_IsEmpty(ST_Buffer(this_right_face_geom,-0.00002)) ) AND this_right_face_area > 0) THEN

					-- Time: 1433177.267 ms (23:53.177) to Time: 3053134.528 ms (50:53.135)
					--IF ( (this_left_face_area < _min_area OR
					--        (ST_Area(this_left_face_geom,true) < (_min_area*10.0) AND ST_IsEmpty(ST_Buffer(this_left_face_geom,-0.00001)))
					--     )
					--     AND this_left_face_area > 0
					--   )
					--OR ( (this_right_face_area < _min_area OR
					--       (ST_Area(this_right_face_geom,true) < (_min_area*10.0) AND ST_IsEmpty(ST_Buffer(this_right_face_geom,-0.00001)))
					--     )
					--     AND this_right_face_area > 0
					--   ) THEN


					-- Time: 3053134.528 ms (50:53.135 Time: 1433177.267 ms (23:53.177)
					--IF ( (this_left_face_area < _min_area )  AND this_left_face_area > 0)
					--OR ( (this_right_face_area < _min_area ) AND this_right_face_area > 0) THEN

					IF (
							(
								this_left_face_area < _min_area OR
								( min_st_is_empty_buffer_value IS NOT NULL AND ST_IsEmpty(ST_Buffer(this_left_face_geom,min_st_is_empty_buffer_value)))
							) AND
							this_left_face_area > 0
						)
					OR
						(
							(
								this_right_face_area < _min_area OR
								( min_st_is_empty_buffer_value IS NOT NULL AND ST_IsEmpty(ST_Buffer(this_right_face_geom,min_st_is_empty_buffer_value)))
							) AND
							this_right_face_area > 0
						) THEN

							BEGIN
								-- RAISE NOTICE 'REMOVE edge % for this_face_area % this_left_face_area % this_right_face_area %', edge_id_tmp, this_face_area, this_left_face_area, this_right_face_area;

								PERFORM topology.ST_RemEdgeModFace (_atopology, edge_id_tmp);

								num_rows := num_rows + 1;
								edge_id_list_removed_ok := array_append(edge_id_list_removed_ok, edge_id_tmp);


								EXCEPTION
								WHEN OTHERS THEN
									num_failed_remove_rows = num_failed_remove_rows + 1;

									GET STACKED DIAGNOSTICS v_state = RETURNED_SQLSTATE, v_msg = MESSAGE_TEXT, v_detail = PG_EXCEPTION_DETAIL, v_hint = PG_EXCEPTION_HINT, v_context = PG_EXCEPTION_CONTEXT;
									RAISE NOTICE 'ERROR failed to remove tiny face % for % state  : %  message: % detail : % hint   : % context: %',
									edge_id_tmp, _atopology, v_state, v_msg, v_detail, v_hint, v_context;

									topo_info_not_removed = 'ERROR failed v_state:'||v_state||' v_msg:'v_msg;

									IF face_edges_to_remove IS NULL THEN
										-- this does not often we just update the database when it happens
										command_string_update_result := format(
										$fmt$
											UPDATE %1$s de SET is_handled_at = statement_timestamp() , not_removed_reasonn = $2
											WHERE edge_id = $1
											$fmt$,
											tbl_name_edges_to_remove
										);
										EXECUTE command_string_update_result
										USING edge_id_tmp,
										quote_literal(topo_info_not_removed);
									END IF;


							END;


					ELSE

						edge_id_list_not_ok_remove := array_append(edge_id_list_not_ok_remove, edge_id_tmp);
						num_not_valid_to_remove = num_not_valid_to_remove + 1;

					END IF;


					IF mod(num_rows,500) = 0 AND num_rows > 0 THEN
						RAISE NOTICE 'CURRENT WORK (NO COMMIT), In do_merge_small_areas_no_block, for cell_job_type % and min_face_id % , removed % edges (failed/not removed edges % / %) for tiny faces tiny polygons from % using % used _fixed_point_set size % and st_area of _bb % ',
						_cell_job_type, min_face_id, num_rows, num_failed_remove_rows, num_not_valid_to_remove, _atopology||'.face' , tbl_name_edges_to_remove,  ST_NPoints(_fixed_point_set), ST_Area(_bb,true);
						-- When dropping commit here, we seems to get less problems SubtransControlLock
						-- COMMIT;
					END IF;


					END LOOP;

	END IF;

	IF face_edges_to_remove IS NULL THEN
		-- update the all edges that are removed OK with a timestampe
		command_string_update_result := format(
		$fmt$
			UPDATE %1$s de
			SET is_handled_at = statement_timestamp()
			WHERE edge_id = ANY($1)
			$fmt$,
			tbl_name_edges_to_remove
		);
		EXECUTE command_string_update_result USING edge_id_list_removed_ok;

		-- update the all edges that are NOT removed OK with a timestamp and message
		command_string_update_result := format(
		$fmt$
			UPDATE %1$s de SET is_handled_at = statement_timestamp() , not_removed_reasonn = $2
			WHERE edge_id = ANY($1)
			$fmt$,
			tbl_name_edges_to_remove
		);
		EXECUTE command_string_update_result
		USING edge_id_list_not_ok_remove,
		'Not valid edge to remove for instance like wrong area, left face or right face is 0,....';
	END IF;


	-- stop the clock
	used_time := (Extract(EPOCH FROM (Clock_timestamp() - start_time)));

	IF face_edges_to_remove IS NULL THEN

		RAISE NOTICE 'In do_merge_small_areas_no_block, for cell_job_type % with used_time % and min_face_id % , removed % edges (failed/not removed edges % / %) for tiny faces tiny polygons from % using % used _fixed_point_set size % and st_area of _bb % edge_id_list_removed_ok % ',
		_cell_job_type,
		used_time,
		min_face_id,
		num_rows,
		num_failed_remove_rows,
		num_not_valid_to_remove, _atopology||'.face' ,
		tbl_name_edges_to_remove,
		ST_NPoints(_fixed_point_set), ST_Area(_bb,true),
		edge_id_list_removed_ok;
	END IF;

END
$$;

-- not removed from tmp_resultat_1804_t4


/**


CALL resolve_overlap_gap_single_cell(
	E'{"(,id,geo,org_klimatilpasning.myr_1804,id,geo,4258,f,\\"id integer\\",id,\\"atil integer,myr integer,id integer\\",\\"atil,myr,id\\")","(,,,org_klimatilpasning.ssb_1804_v2,id,geo,25833,t,,,,)","(,,,org_klimatilpasning.sr16_1804,id,geo,4258,f,,,,)","(,,,org_klimatilpasning.ar5_1804_v2,id,geo,4258,f,,,,)"}','{"(1,org_klimatilpasning.myr_1804,id,integer,,geo,,4258,f)","(2,org_klimatilpasning.ssb_1804_v2,id,integer,,geo,,25833,t)","(3,org_klimatilpasning.sr16_1804,id,integer,,geo,,4258,f)","(4,org_klimatilpasning.ar5_1804_v2,id,integer,,geo,,4258,f)"}','(tmp_resultat_1804_t4,4258,f,0,t,1,2,t,t)',1e-07,'tmp_resultat_1804_t4.multi_input_4_tables',
	'(200,,0,0,0,0,0,0,0,0,)',
	'tmp_resultat_1804_t4.multi_input_4_tables_job_list','tmp_resultat_1804_t4.multi_input_4_grid','0103000020A21000000100000005000000039F5EBCBE752940ACBA0A5CEABA5040039F5EBCBE752940EF4EAD8FC6EC5040B81838E2A6BC2E40EF4EAD8FC6EC5040B81838E2A6BC2E40ACBA0A5CEABA5040039F5EBCBE752940ACBA0A5CEABA5040',3,5);


*/