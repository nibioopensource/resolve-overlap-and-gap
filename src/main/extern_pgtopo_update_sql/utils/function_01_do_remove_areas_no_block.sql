/**
 * Here main focus is remove small areas to to get off white spikes sliver areas
 *
 * The inpout here is list if edges and main rule be to drop the face with smallest area when below giveb limit.
 *
 */

CREATE OR REPLACE PROCEDURE topo_update.do_remove_small_areas_no_block (
_atopology varchar,
_min_area float,
_min_st_is_empty_buffer_value float,
_bb geometry,
_utm boolean,
_cell_job_type int,
_fixed_point_set geometry,
max_time_in_secs_to int, -- if this is bigger than 0 , then the code will return more this time ise used.
INOUT _face_edges_to_remove_after_simplify add_border_lines_faces_to_remove[] default null
)
LANGUAGE plpgsql
AS $$
DECLARE
	command_string_find text;
	command_string_update_result text;
	command_string text;
	num_rows int = 0;
	num_failed_remove_rows int = 0;
	num_not_valid_to_remove int = 0;
	-- Based on testing and it's not accurate at all
	min_mbr_area float = _min_area * 1000;
	edge_id_list_to_remove integer[];
	edge_id_list_removed_ok integer[];
	edge_id_list_not_ok_remove integer[];

	edge_id_abs integer;
	remove_edge integer;
	v_state text;
	v_msg text;
	v_detail text;
	v_hint text;
	v_context text;
	tbl_name_edges_to_remove text;

	topo_info_not_removed text;
	this_more_info text;

	min_face_id int = -1;

	this_left_face_area real;
	this_right_face_area real;
	this_face_area real;

	this_left_face_geom geometry;
	this_right_face_geom geometry;

	this_left_face_id int;
	this_right_face_id int;

	start_time timestamp WITH time zone;
	used_time real;

	remove_face_id int;
	remove_edge_id int;

	-- TODO find a way to remove smal area
	edge_id_tmp int;
	remove_edge_id_list int[];
	face_edges_to_remove integer[];
	face_edge_to_remove_after_simplify_tmp add_border_lines_faces_to_remove;

	face_edges_to_remove_after_simplify add_border_lines_faces_to_remove[];

	return_to_caller boolean = false;


BEGIN

	start_time := Clock_timestamp();



	IF _face_edges_to_remove_after_simplify IS NOT NULL AND (Array_length(_face_edges_to_remove_after_simplify, 1)) > 0 THEN
		-- get edges to remove for temp topologies
		FOREACH face_edge_to_remove_after_simplify_tmp IN ARRAY _face_edges_to_remove_after_simplify
		LOOP

			IF return_to_caller THEN
				-- if return to caller add rest of to lines_to_add_to_next_batch
				face_edges_to_remove_after_simplify = face_edges_to_remove_after_simplify||face_edge_to_remove_after_simplify_tmp;
			ELSE
				edge_id_list_to_remove = (face_edge_to_remove_after_simplify_tmp).edge_ids;
	--			RAISE NOTICE 'In do_remove_small_areas_no_block _atopology % table_input_orderv % with _min_area % at % with edge_id_list_to_remove %',
	--			_atopology, (face_edge_to_remove_after_simplify_tmp).table_input_order,_min_area , start_time, edge_id_list_to_remove;

				FOREACH edge_id_tmp IN ARRAY edge_id_list_to_remove
				LOOP
					edge_id_abs = abs(edge_id_tmp);
					remove_face_id = null;
					this_left_face_area = 0;
					this_right_face_area = 0;
					this_left_face_id = null;
					this_right_face_id = null;

					command_string_find := format(
					$fmt$
						SELECT
						CASE WHEN e.left_face > 0 THEN e.left_face
						ELSE NULL
						END AS left_face_id,

						CASE WHEN e.left_face > 0 THEN topology.ST_GetFaceGeometry(%1$L, e.left_face)
						ELSE NULL
						END AS left_face_geom,

						CASE WHEN e.right_face > 0 THEN e.right_face
						ELSE NULL
						END AS right_face_id,

						CASE WHEN e.right_face > 0 THEN topology.ST_GetFaceGeometry(%1$L, e.right_face)
						ELSE NULL
						END AS right_face_geom

						FROM
						%1$I.edge e
						WHERE e.edge_id = %2$s  AND e.left_face != e.right_face
					$fmt$,
					_atopology,
					edge_id_abs
					);

					EXECUTE command_string_find INTO this_left_face_id, this_left_face_geom, this_right_face_id, this_right_face_geom;

					IF this_left_face_geom IS NOT NULL THEN
						this_left_face_area = ST_Area(this_left_face_geom,true);
						IF _min_st_is_empty_buffer_value IS NOT NULL AND this_left_face_area > _min_area AND
							ST_IsEmpty(ST_Buffer(this_left_face_geom,_min_st_is_empty_buffer_value)) THEN
							this_left_face_area = _min_area/2.0;
						END IF;
					END IF;

					IF this_right_face_geom IS NOT NULL THEN
						this_right_face_area = ST_Area(this_right_face_geom,true);
						IF _min_st_is_empty_buffer_value IS NOT NULL AND this_right_face_area > _min_area AND
							ST_IsEmpty(ST_Buffer(this_right_face_geom,_min_st_is_empty_buffer_value)) THEN
							this_right_face_area = _min_area/2.0;
						END IF;
					END IF;

					IF this_left_face_area < _min_area AND this_left_face_area < this_right_face_area AND this_left_face_area > 0.0 THEN
						remove_face_id = this_left_face_id;
						this_face_area = this_left_face_area;
					ELSIF this_right_face_area < _min_area AND this_right_face_area < this_left_face_area AND this_right_face_area > 0.0 THEN
						remove_face_id = this_right_face_id;
						this_face_area = this_right_face_area;
					ELSIF this_left_face_area < _min_area AND this_left_face_area < _min_area AND this_left_face_area > 0.0 THEN
						remove_face_id = this_left_face_id;
						this_face_area = this_left_face_area;
					ELSIF this_right_face_area < _min_area AND this_right_face_area < _min_area AND this_right_face_area > 0 THEN
						remove_face_id = this_right_face_id;
						this_face_area = this_right_face_area;
					END IF;

					IF remove_face_id IS NOT NULL THEN
						BEGIN

--							RAISE NOTICE 'In do_remove_small_areas_no_block try remove_face_id % edge_id_abs % this_left_face_area % this_right_face_area %',
--							remove_face_id, edge_id_abs, this_left_face_area, this_right_face_area;

							SELECT ARRAY_AGG(abs(fe.edge))
							FROM
							topology.ST_GetFaceEdges(_atopology,remove_face_id) fe
							INTO remove_edge_id_list;

							command_string := format(
							$fmt$
							--SELECT topology.ST_RemEdgeNewFace(%1$L,e.edge_id)
							SELECT e.edge_id,
							CASE WHEN ei.edge_id IS NULL THEN 0
							ELSE %4$s END AS sort_order
							FROM
							(SELECT unnest(%2$L::int[]) AS edge_id) r
							LEFT JOIN %1$I.edge e ON e.edge_id = r.edge_id
							LEFT JOIN (SELECT unnest(%3$L::int[]) AS edge_id) ei ON (r.edge_id = ei.edge_id)
							--ORDER BY ST_length(e.geom,true) DESC
							ORDER BY sort_order
							LIMIT 1
							$fmt$,
							_atopology,
							remove_edge_id_list,
							edge_id_list_to_remove,
							(face_edge_to_remove_after_simplify_tmp).table_input_order
							);

							execute command_string INTO remove_edge_id;

--							remove_edge_id = edge_id_abs;

							IF remove_edge_id IS NOT NULL THEN
								PERFORM topology.ST_RemEdgeNewFace(_atopology, remove_edge_id);
								-- RAISE NOTICE 'REMOVE remove_face_id % remove_edge_id % (%) (based on added edge_id %) for this_face_area %', remove_face_id, remove_edge_id, remove_edge_id_list , edge_id_abs, this_face_area;
							END IF;


							num_rows := num_rows + 1;
							edge_id_list_removed_ok := array_append(edge_id_list_removed_ok, edge_id_abs);


							EXCEPTION
							WHEN OTHERS THEN
								num_failed_remove_rows = num_failed_remove_rows + 1;

								GET STACKED DIAGNOSTICS v_state = RETURNED_SQLSTATE, v_msg = MESSAGE_TEXT, v_detail = PG_EXCEPTION_DETAIL, v_hint = PG_EXCEPTION_HINT, v_context = PG_EXCEPTION_CONTEXT;
								RAISE NOTICE 'ERROR failed to merge tiny spike with swl % face % for % state  : %  message: % detail : % hint   : % context: %',
								command_string, edge_id_abs, _atopology, v_state, v_msg, v_detail, v_hint, v_context;

								topo_info_not_removed = 'ERROR failed v_state:'||v_state||' v_msg:'v_msg;
						END;

					ELSE
						edge_id_list_not_ok_remove := array_append(edge_id_list_not_ok_remove, edge_id_abs);
						num_not_valid_to_remove = num_not_valid_to_remove + 1;
						--RAISE NOTICE 'In do_remove_small_areas_no_block not remove_face_id % edge_id_abs % this_left_face_area % this_right_face_area %',
						--remove_face_id, edge_id_abs, this_left_face_area, this_right_face_area;
					END IF;
				END LOOP;
			END IF;

			IF max_time_in_secs_to IS NOT NULL THEN
				used_time := (Extract(EPOCH FROM (Clock_timestamp() - start_time)));
				IF used_time >= max_time_in_secs_to THEN
					return_to_caller = true;
				END IF;
			END IF;

		END LOOP;

		-- set return value

		_face_edges_to_remove_after_simplify = face_edges_to_remove_after_simplify;

		used_time := (Extract(EPOCH FROM (Clock_timestamp() - start_time)));

		RAISE NOTICE 'In do_remove_small_areas_no_block, for cell_job_type % with used_time % removed % edges (num_failed_remove_rows %) left egdes to remove is % for tiny faces tiny polygons from % using % used _fixed_point_set size % and st_area of _bb % edge_id_list_removed_ok % ',
		_cell_job_type,
		used_time,
		num_rows,
		num_failed_remove_rows,
		Array_length(face_edges_to_remove_after_simplify,1),
		_atopology||'.face' ,
		tbl_name_edges_to_remove,
		ST_NPoints(_fixed_point_set), ST_Area(_bb,true),
		edge_id_list_removed_ok;

	END IF;



END
$$;

-- not removed from tmp_resultat_1804_t4



