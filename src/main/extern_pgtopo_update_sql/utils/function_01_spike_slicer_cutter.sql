DROP FUNCTION IF EXISTS topo_update._find_slice_cutter_lines (_atopology varchar, _bb geometry, _outer_cell_boundary_lines geometry );

DROP FUNCTION IF EXISTS topo_update.spike_slicer_cutter (_atopology varchar, _bb geometry, _outer_cell_boundary_lines geometry );


CREATE OR REPLACE PROCEDURE topo_update.spike_slicer_cutter (
_atopology varchar,
_bb geometry,
_outer_cell_boundary_lines geometry,
INOUT res_spike_remove_lines rog_input_line_order[]
)
LANGUAGE plpgsql
AS $$
DECLARE
  command_string text;
  edge_ids_found int;

  start_time_delta_job timestamp WITH time zone;

  -- holdes lines and input table order
  tmp_lines_to_add rog_input_line_order[];

  use_geography boolean = true;
  p1 geometry;
  p2 geometry;


BEGIN

  -- a hack to work around to check for geography support
  p1 = ST_pointOnSurface(_bb);
  p2 = ST_Project(p1::geometry , 0.1,  radians(15.0));
  IF ST_length(ST_MakeLine(ARRAY[p1,p2]),true) < 1 THEN
    use_geography = FALSE;
  END IF;





 command_string = FORMAT(
  $fmt$
  SELECT
  -- r.sp
  ARRAY_AGG (r.sp )
  FROM
  (
    WITH edges AS (
      SELECT
        e1.geom, e1.edge_id
      FROM
      %1$s.edge_data e1
      WHERE ST_intersects(e1.geom,%2$L) and
      (ST_Disjoint(e1.geom,%3$L) OR %3$L is null)
    ),
    edges_touches AS (
      SELECT e1.geom, e1.edge_id
      FROM edges e1
    ),
    edges_points AS (
      SELECT (ST_DumpPoints(e1.geom)).geom AS points
      FROM edges e1
    ),
    edges_points_collect AS (
      SELECT ST_collect(e1.points) all_points
      FROM edges_points e1
    ),
    result AS (
      SELECT topo_update.spike_slicer_cutter(e1.geom,e2.all_points, ST_collect(et.geom),%4$L) AS sp
      FROM
      edges e1
      LEFT JOIN edges_touches et ON ST_Touches(e1.geom,et.geom) OR et.edge_id = e1.edge_id OR e1.geom && et.geom
      JOIN edges_points_collect e2 ON TRUE
        --AND e1.edge_id = 100
        --AND e1.edge_id = 61
        -- AND e1.edge_id = 51
        -- AND e1.edge_id = 60
        --AND e1.edge_id = 63
        --AND e1.e1.edge_id = 77
        --AND e1.edge_id IN (66,63)
        --AND e1.edge_id IN (92)
      GROUP BY e1.geom,e2.all_points
    )
    SELECT unnest(r.sp::rog_input_line_order[]) AS sp
    FROM result r
    WHERE r.sp <> '{}'
  ) AS r
  $fmt$,
  _atopology,
  _bb,
  _outer_cell_boundary_lines,
  use_geography
  );



  -- RAISE NOTICE 'command_string %', command_string;

  EXECUTE command_string into res_spike_remove_lines;


  --RETURN tmp_lines_to_add;

END
$$;



CREATE OR REPLACE FUNCTION topo_update.spike_slicer_cutter (_geom geometry, all_points geometry, touching_edges geometry,_use_geography boolean)
returns rog_input_line_order[]
  AS $$
DECLARE
  res rog_input_line_order[] = '{}'::rog_input_line_order[];
  sub_res_01 rog_find_slice_cutter_lines_internal_type;
  sub_res_02 rog_find_slice_cutter_lines_internal_type;
  sub_res_03 rog_find_slice_cutter_lines_internal_type;

  all_points geometry;
BEGIN

all_points = all_points;

sub_res_01 = topo_update._find_slice_cutter_lines (ST_Multi(_geom), all_points, touching_edges, false, _use_geography);

--sub_res_02 = topo_update._find_slice_cutter_lines (ST_Reverse(ST_Multi(_geom)), all_points, touching_edges, true, _use_geography);
sub_res_02 = topo_update._find_slice_cutter_lines (ST_Reverse(sub_res_01.inputline_left), all_points, touching_edges, true, _use_geography);

--sub_res_03 = topo_update._find_spike_cutter_lines(ST_Multi(_geom));
sub_res_03 = topo_update._find_spike_cutter_lines(sub_res_02.inputline_left);


/**
RAISE NOTICE ' sub_res_01.inputline_left %',  sub_res_01.inputline_left;
RAISE NOTICE ' sub_res_01.spike_lines %',  sub_res_01.spike_lines;

RAISE NOTICE ' sub_res_02.inputline_left %',  sub_res_02.inputline_left;
RAISE NOTICE ' sub_res_02.spike_lines %',  sub_res_02.spike_lines;

RAISE NOTICE ' sub_res_03.inputline_left %',  sub_res_03.inputline_left;
RAISE NOTICE ' sub_res_03.spike_lines %',  sub_res_03.spike_lines;
*/

res = sub_res_01.spike_lines||sub_res_02.spike_lines||sub_res_03.spike_lines;

-- RAISE NOTICE '  res sub_res_03.spike_lines %',  res;

RETURN res;

END;
$$
LANGUAGE plpgsql
IMMUTABLE
;





CREATE OR REPLACE FUNCTION topo_update._find_slice_cutter_lines (_geom geometry, _all_points geometry, touching_edges geometry, _reverse boolean, _use_geography boolean )
returns rog_find_slice_cutter_lines_internal_type

  AS $$
DECLARE
  sub_res rog_find_slice_cutter_lines_internal_type;

  command_string text;

  p_listdump geometry[];
  p_listsize int;
  i int;

  i_lag int;
  i_lead int;

  p1_lag geometry;
  p1_lead geometry;
  p2_lag geometry;
  p2_lead geometry;

  p1_i int;
  p2_i int;



  angle_1 float;
  angle_2 float;

  tmp_line geometry;
  tmp_line_before geometry;
  tmp_line_after geometry;

  last_new_line geometry;

  center geometry;
  center_2 geometry;
  lead geometry;
  lag geometry;

  pclose geometry;
  azimuth_l1 float;
  azimuth_l2 float;

  lag_tmp_p geometry;

  ibuffer_value float = 0.00001;
  max_linelength float = 10.00;

  min_linelength_addtion_angle float = 4.0;

  min_linelength float;

  max_angle_constructed_spike int = 240;
  min_angle_constructed_spike int = 90;

  counter int;

  master_degrees_angle_01 float;

  true_spike boolean ;
  constructed_spike boolean ;

  lag_line_length float;
  lead_line_length float;

  check_points_all geometry;

  check_point_01 geometry;
  check_point_02 geometry;
  check_area geometry;
  check_points geometry[];

  check_i integer;

  check_buffer_value float = 0.000001;
  check_buffer_value_temp float;

  check_result_geomtery geometry;

  i2 integer;
  i2_geometry geometry;
  i2_area float;
  i2_minarea float = 10000.0;

  end_node boolean;
  start_node boolean;

  exclude_points geometry;

  n1 int = 0;
  n2 int;
  n1p geometry;
  nl geometry;
  next_l_index int;
  next_l_geom geometry;
  all_points geometry;


BEGIN

--CREATE TABLE IF NOT EXISTS constructed_spike_log_lines(id serial primary key, i int,added_add_at timestamp default now(), lag_line geometry, check_point_01 geometry, check_point_02 geometry,  check_area geometry, check_points geometry);

--CREATE TABLE IF NOT EXISTS checkpoint(id serial primary key, i int,added_add_at timestamp default now(), check_point_01 geometry, check_area geometry);


---- RAISE NOTICE 'input edge _________ %', _geom;

sub_res.spike_lines='{}'::rog_input_line_order[];

FOR next_l_index IN SELECT generate_series(1, ST_NumGeometries(_geom)) LOOP

  next_l_geom := ST_GeometryN(_geom, next_l_index);


  p_listdump = ARRAY_AGG((dp).geom ORDER BY (dp).path[1] ) FROM ( SELECT ST_DumpPoints (next_l_geom) AS dp ) r ;

  --all_points = _all_points;
  all_points = ST_Collect(p_listdump);

  p_listsize = ARRAY_LENGTH(p_listdump,1);

  check_points_all =  ST_Collect(p_listdump);

  i = 0;

  WHILE i < (p_listsize) LOOP
    i = i + 1;
    i_lag = i;
    i_lead = i;

    end_node = false;
    start_node = false;
    lag = NULL;
    center = NULL;



    IF i = 1 THEN
      center = p_listdump[i];
      lag = p_listdump[i+1];
      lead = p_listdump[i+1];
      start_node = true;
      exclude_points = ST_Collect(p_listdump[i:i+1]);
      i_lag  = i_lag - 1;
      i_lead  = i_lead + 1;

      -- RAISE NOTICE 'start loop i % lag % center % ', i, lag, center;

    ELSEIF i = (p_listsize) THEN
      center = p_listdump[i];
      lag = p_listdump[i-1];
      lead = p_listdump[i-1];
      end_node = true;
      exclude_points = ST_Collect(p_listdump[i-1:i]);

      i_lag  = i_lag - 1;

      -- RAISE NOTICE 'end loop i % lag % center % ', i, lag, center;
    ELSE
      lag = p_listdump[i-1];
      center = p_listdump[i];
      lead = p_listdump[i+1];
      azimuth_l1 = ST_Azimuth (center, lead);
      azimuth_l2 = ST_Azimuth (center, lag);
      master_degrees_angle_01 = Abs(Degrees(azimuth_l1 - azimuth_l2));

      exclude_points = ST_Collect(p_listdump[i-1:i+1]);

      i_lead  = i_lead + 1;

      -- RAISE NOTICE 'mid loop i % lag % center % master_degrees_angle_01 %', i, lag, center, master_degrees_angle_01;

      -- psql:src/main/extern_pgtopo_update_sql/utils/function_01_spike_slicer_cutter.sql:648: NOTICE:  00000: mid loop i 8 lag 0101000020A2100000B2F709EE6CE12540C4047C3D8EEB4F40 center 0101000020A2100000AD5FDF516DE12540401542378EEB4F40 master_degrees_angle_01 246.1479551782305

    END IF;


    -- RAISE NOTICE 'single inputline_left loop i:%  i_lag:% i_lead:% n1 % n2 % n1p % nit uses lines % last_new_line %', i, i_lag, i_lead, n1, n2, n1p, nl, last_new_line;


    true_spike = false;
    constructed_spike = false;


    IF (
        end_node OR start_node OR
        master_degrees_angle_01 > max_angle_constructed_spike OR
        master_degrees_angle_01 < min_angle_constructed_spike
       )  THEN


      IF _use_geography THEN
        IF _reverse THEN
          check_point_01 = ST_Project(center::geography , (min_linelength_addtion_angle),  (ST_Azimuth (lead,center) + radians(15.0) ) );
          check_point_02 = ST_Project(center::geography , (min_linelength_addtion_angle),  (ST_Azimuth (lead,center) - radians(30.0) ) );
        ELSE
          check_point_01 = ST_Project(center::geography , (min_linelength_addtion_angle),  (ST_Azimuth (lag,center) + radians(15.0) ) );
          check_point_02 = ST_Project(center::geography , (min_linelength_addtion_angle),  (ST_Azimuth (lag,center) - radians(30.0) ) );
        END IF;
      ELSE
        IF _reverse THEN
          check_point_01 = ST_Project(center, (min_linelength_addtion_angle),  (ST_Azimuth (lead,center) + radians(15.0) ) );
          check_point_02 = ST_Project(center, (min_linelength_addtion_angle),  (ST_Azimuth (lead,center) - radians(30.0) ) );
        ELSE
          check_point_01 = ST_Project(center, (min_linelength_addtion_angle),  (ST_Azimuth (lag,center) + radians(15.0) ) );
          check_point_02 = ST_Project(center, (min_linelength_addtion_angle),  (ST_Azimuth (lag,center) - radians(30.0) ) );
        END IF;
      END IF;

      -- postgis 3.4.0 check_point_01 = ST_Project(lag::geography, center::geography, CAST ((min_linelength_addtion_angle*0.5),FLOAT) );

   --   check_point_01 = center;

      --check_area = ST_Buffer(check_point_01::geography,min_linelength_addtion_angle,'quad_segs=2');

      check_area = ST_MakePolygon(ST_MakeLine(
      ARRAY[
      center,
      check_point_01, check_point_02,
      center
      ]::geometry[] ));


      --check_points = ST_Multi(ST_difference(ST_intersection(check_points_all,check_area),ST_Collect(p_listdump[i-1:i+1])));

      check_points = ARRAY_AGG((dp).geom ORDER BY ST_Distance((dp).geom,center,true) ASC) FROM ( SELECT ST_DumpPoints
      (ST_difference(
                  ST_intersection(all_points,check_area),
                  exclude_points)) AS dp ) r ;

      -- IF now nodes crate a new one
      IF ARRAY_LENGTH(check_points,1) = 0 OR check_points IS NULL THEN

          IF _use_geography THEN
            check_area = ST_difference(check_area,ST_Buffer(center::geography,(min_linelength_addtion_angle/1000.0))::geometry);
          ELSE
            check_area = ST_difference(check_area,center);
          END IF;


          check_points = ARRAY_AGG((dp).geom) FROM ( SELECT ST_DumpPoints
      (ST_difference(
                  ST_ClosestPoint(
                  ST_Intersection(check_area,touching_edges),center),
                  exclude_points)) AS dp ) r ;
           -- RAISE NOTICE 'make new center %  cloest_points % check_area %' , center, ST_ClosestPoint(ST_Intersection(check_area,touching_edges),center), check_area;

      END IF;


      -- RAISE NOTICE 'check_points %  exclude_points % num all_points %', check_points, exclude_points, ST_NPoints(all_points);


      -- debug ----------------------
  --    INSERT INTO constructed_spike_log_lines( i, lag_line , check_point_01 , check_point_02 ,  check_area, check_points)
  --    VALUES (i, ST_MakeLine(lag,center),  check_point_01 , check_point_02 ,  check_area, ST_collect(check_points));

      check_i = 0;

      WHILE (check_i) < ARRAY_LENGTH(check_points,1) LOOP

        check_i = check_i + 1;
        lead = check_points[check_i];

        -- RAISE NOTICE 'check_i %  lead % ', check_i, lead;

        last_new_line = ST_MakeLine(center,lead);

        check_buffer_value_temp = check_buffer_value;


        IF (ST_length(last_new_line)/100.0) < check_buffer_value_temp THEN
              check_buffer_value_temp = ST_length(last_new_line)/100.0;
        END IF;


        IF ST_intersects(check_area,lead)
           --AND ST_length(ST_intersection(touching_edges,ST_buffer(last_new_line::geography,check_buffer_value)),true) < (ST_length(last_new_line,true)*2.0)

           --AND ST_length(ST_intersection(touching_edges,ST_buffer(ST_LineInterpolatePoint(last_new_line,0.5)::geography,check_buffer_value_temp))::geometry,true) = 0
  /**
           AND
           ST_length(
           ST_intersection(
           touching_edges,
           ST_buffer(ST_LineInterpolatePoint(last_new_line,0.5),check_buffer_value_temp)::geometry
           ),
           true
           ) = 0.0
  */
           --AND ST_length(ST_intersection(touching_edges,ST_buffer(last_new_line::geography,check_buffer_value)),true) < (check_buffer_value*2.0)


           --AND ST_length(ST_intersection(touching_edges,last_new_line),true) < ST_length(last_new_line,true)


           THEN

           /**
               INSERT INTO constructed_spike_log_lines( i, lag_line , check_point_01 , check_point_02 ,  check_area, check_points)
      VALUES (i, ST_MakeLine(lag,center),  check_point_01 , check_point_02 ,
      ST_buffer(ST_LineInterpolatePoint(last_new_line,0.5),check_buffer_value_temp)::geometry,
      ST_collect(check_points));
      */

           IF
             ST_length(
             ST_intersection(
             touching_edges,
             ST_buffer(ST_LineInterpolatePoint(last_new_line,0.5),check_buffer_value_temp)::geometry
             ),
             true
             ) > 0.0 THEN
             last_new_line = NULL;
             EXIT;
           END IF;


           check_result_geomtery = ST_Multi(ST_Polygonize(ST_Union(next_l_geom,last_new_line))) ;

           FOR i2 IN 1..ST_NumGeometries(check_result_geomtery) LOOP
             i2_geometry = ST_GeometryN(check_result_geomtery, i2);
             i2_area = ST_area(i2_geometry,true);
             IF i2_area < i2_minarea THEN
               EXIT;
             END IF;
           END LOOP;


           IF i2_area IS NULL OR i2_area < i2_minarea THEN
              -- RAISE NOTICE 'ok at i % with inter length % new_line constructed_spike %', i, ST_length(ST_intersection(_geom,ST_buffer(last_new_line::geography,check_buffer_value)),true), last_new_line;
              sub_res.spike_lines = sub_res.spike_lines||(last_new_line,0,NULL)::rog_input_line_order;
              tmp_line = last_new_line;

              n1p = ST_PointN(last_new_line,2);

              IF _reverse THEN
                n2 = i_lead;
                IF start_node THEN
                  n2 = i_lead;
                  -- RAISE NOTICE 'single inputline_left start_node % n2: %', start_node, n2;
                  WHILE (n2) < p_listsize LOOP
                    n2 = n2 + 1;
                    IF ST_Equals(n1p,p_listdump[n2]) THEN
                      EXIT;
                    END IF;
                  END LOOP;
                  n1 = n2;
                ELSE
                  n2 = i_lead;
                  -- RAISE NOTICE 'single inputline_left % n2: %', start_node, n2;
                  WHILE (n2) > 0 LOOP
                    n2 = n2 - 1;
                    IF ST_Equals(n1p,p_listdump[n2]) THEN
                      EXIT;
                    END IF;
                  END LOOP;

                END IF;


              ELSE
                n2 = i_lag;
              END IF;


              IF end_node THEN
                n2 = i_lag;
                -- RAISE NOTICE 'single inputline_left end_node % n2: %', end_node, n2;
                WHILE (n2) > 0 LOOP
                  n2 = n2 - 1;
                  IF ST_Equals(n1p,p_listdump[n2]) THEN
                    EXIT;
                  END IF;
                END LOOP;
              END IF;



              IF n1 < n2 THEN
                nl = ST_MakeLine((p_listdump[n1:n2]));
              END IF;


              sub_res.inputline_left = ST_Collect(sub_res.inputline_left, nl );
              -- RAISE NOTICE 'single inputline_left i:%  i_lag:% i_lead:% n1 % n2 % n1p % nit uses lines % last_new_line %', i, i_lag, i_lead, n1, n2, n1p, nl, last_new_line;


              IF end_node = FALSE AND start_node = FALSE  THEN
                IF _reverse THEN
                  WHILE (i) <= (p_listsize) LOOP
                    -- RAISE NOTICE 'single inputline_left spike _reverse = true i % type constructed_spike lag %, lead % ', i, ST_AsText(lag), ST_AsText(lead);

                    i = i + 1;
                    IF ST_Equals(lag,p_listdump[i]) OR NOT ST_Intersects(lag,next_l_geom) THEN
                      i_lag = i+1;
                      i_lead = i;
                      EXIT;
                    END IF;

                  END LOOP;
                ELSE
                  WHILE (i) <= (p_listsize) LOOP
                    -- RAISE NOTICE 'single inputline_left spike _reverse = false i % type constructed_spike lag %, lead % ', i, ST_AsText(lag), ST_AsText(lead);

                    i = i + 1;
                    IF ST_Equals(lead,p_listdump[i]) OR NOT ST_Intersects(lead,next_l_geom) THEN
                      i_lag = i-1;
                      i_lead = i;
                      EXIT;
                    END IF;

                  END LOOP;
                END IF;

              END IF;

              IF _reverse THEN
                 n1 = n2;
              ELSE
                IF NOT start_node THEN
                  n1 = i;
                  n2 = i_lag;
                END IF;
              END IF;


            END IF;
            EXIT;
        END IF;


      END LOOP;
    END IF;

  END LOOP;

  IF _reverse THEN
    n1 = i_lag;
    n2 = i;
    nl = ST_MakeLine((p_listdump[n1:n2]));
  ELSE
    n2 = i_lag;
    nl = ST_MakeLine((n1p||p_listdump[n1:n2]));
  END IF;
  sub_res.inputline_left = ST_Collect(sub_res.inputline_left, nl );
  -- RAISE NOTICE 'single inputline_left i:%  i_lag:% i_lead:% n1 % n2 % n1p % nit uses lines % last_new_line %', i, i_lag, i_lead, n1, n2, n1p, nl, last_new_line;
  sub_res.inputline_left = ST_Multi(ST_CollectionExtract(sub_res.inputline_left,2));

END LOOP;


IF sub_res.spike_lines = '{}' THEN
    sub_res.inputline_left = _geom;
END IF;

RETURN sub_res;

END;
$$
LANGUAGE plpgsql
IMMUTABLE
;


CREATE OR REPLACE FUNCTION topo_update._find_spike_cutter_lines (
_geom geometry)
returns rog_find_slice_cutter_lines_internal_type
  AS $$
DECLARE
  sub_res rog_find_slice_cutter_lines_internal_type;

  command_string text;
  split_lines geometry[];

  p_listdump geometry[];
  p_dump geometry;
  p_listsize int;
  i int;

  i_lag int;
  i_lead int;

  p1_lag geometry;
  p1_lead geometry;
  p2_lag geometry;
  p2_lead geometry;

  p1_i int;
  p2_i int;



  angle_1 float;
  angle_2 float;

  tmp_line geometry;
  tmp_line_before geometry;
  tmp_line_after geometry;

  last_new_line geometry;

  center geometry;
  lead geometry;
  lag geometry;

  pclose geometry;
  azimuth_l1 float;
  azimuth_l2 float;

  lag_tmp_p geometry;

  ibuffer_value float = 0.00001;
  max_linelength float = 10.00;

  min_linelength_addtion_angle float = 1.0;

  min_linelength float;

  max_angle_true_spike int = 330;
  min_angle_true_spike int = 30;

  counter int;

  master_degrees_angle_01 float;
  master_degrees_angle_02 float;

  true_spike boolean ;
  constructed_spike boolean ;

  lag_line_length float;
  lead_line_length float;

  check_point geometry;
  check_area geometry;


  n1 int = 0;
  n2 int;
  n1p geometry;
  nl geometry;

  next_l_index int;
  next_l_geom geometry;


BEGIN
  sub_res.spike_lines= '{}'::rog_input_line_order[];


--CREATE TABLE IF NOT EXISTS constructed_spike_log_lines(id serial primary key, i int,added_add_at timestamp default now(), lag_line geometry, check_point geometry,  check_area geometry);

-- RAISE NOTICE 'input edge _________ %', _geom;

FOR next_l_index IN SELECT generate_series(1, ST_NumGeometries(_geom)) LOOP

  next_l_geom := ST_GeometryN(_geom, next_l_index);


  p_listdump = ARRAY_AGG((dp).geom ORDER BY (dp).path[1] ) FROM ( SELECT ST_DumpPoints (next_l_geom) AS dp ) r ;

  p_listsize = ARRAY_LENGTH(p_listdump,1);

  i = 2;

  WHILE i <= (p_listsize) LOOP
    i = i + 1;

    p_dump = p_listdump[i];

    lag = p_listdump[i-1];
    center = p_listdump[i];
    lead = p_listdump[i+1];
    azimuth_l1 = ST_Azimuth (center, lead);
    azimuth_l2 = ST_Azimuth (center, lag);
    master_degrees_angle_01 = Abs(Degrees(azimuth_l1 - azimuth_l2));
    master_degrees_angle_02 = null;

    true_spike = false;
    constructed_spike = false;


    IF (master_degrees_angle_01 > max_angle_true_spike OR master_degrees_angle_01 < min_angle_true_spike) THEN

      counter = 0;
      i_lag = i;
      i_lead = i;



      WHILE TRUE  loop

        counter = counter + 1;
        p1_lag = p_listdump[i_lag];
        p1_lead = p_listdump[i_lead];

        IF counter = 1 THEN
          i_lag  = i_lag - 1;
          p2_lag = p_listdump[i_lag];
          tmp_line_before = ST_MakeLine(p1_lag,p2_lag);
          lag_line_length = ST_Length(tmp_line_before,true);

          i_lead  = i_lead + 1;
          p2_lead = p_listdump[i_lead];
          tmp_line_after = ST_MakeLine(p1_lead,p2_lead);
          lead_line_length = ST_Length(tmp_line_after,true);
        ELSE
          IF lag_line_length < lead_line_length THEN
            azimuth_l1 = ST_Azimuth (p_listdump[i_lag], p_listdump[i_lag-1]);
            azimuth_l2 = ST_Azimuth (p_listdump[i_lag], p_listdump[i_lag+1]);
            master_degrees_angle_02 = Abs(Degrees(azimuth_l1 - azimuth_l2));
            IF (master_degrees_angle_02 > max_angle_true_spike OR master_degrees_angle_02 < min_angle_true_spike) THEN
              EXIT;
            END IF;

            i_lag  = i_lag - 1;
            p2_lag = p_listdump[i_lag];
            tmp_line_before = ST_MakeLine(p1_lag,p2_lag);
            lag_line_length = lag_line_length + ST_Length(tmp_line_before,true);



          ELSE
            azimuth_l1 = ST_Azimuth (p_listdump[i_lead], p_listdump[i_lead-1]);
            azimuth_l2 = ST_Azimuth (p_listdump[i_lead], p_listdump[i_lead+1]);
            master_degrees_angle_02 = Abs(Degrees(azimuth_l1 - azimuth_l2));
            IF (master_degrees_angle_02 > max_angle_true_spike OR master_degrees_angle_02 < min_angle_true_spike) THEN
              EXIT;
            END IF;

            i_lead  = i_lead + 1;
            p2_lead = p_listdump[i_lead];
            tmp_line_after = ST_MakeLine(p1_lead,p2_lead);
            lead_line_length = lead_line_length + ST_Length(tmp_line_after,true);
          END IF;
        END IF;

        IF lag_line_length < lead_line_length THEN
          -- find a point on tmp_line_after that is close to p2_lag
          p2_lag = p_listdump[i_lag];
          p2_lead = ST_ClosestPoint(tmp_line_after,p2_lag);
          -- RAISE NOTICE 'find next p2_lead % = ST_ClosestPoint(%,%);', p2_lead, tmp_line_after, p2_lag  ;

        ELSE
            -- find a point on tmp_line_before that is close to p2_lead
          p2_lead = p_listdump[i_lead];
          p2_lag = ST_ClosestPoint(tmp_line_before,p2_lead);
          -- RAISE NOTICE 'find next p2_lag % = ST_ClosestPoint(%,%);', p2_lag, tmp_line_before, p2_lead  ;

        END If;



        tmp_line = ST_MakeLine(p2_lead,p2_lag);

        -- RAISE NOTICE 'test split line % , length %, last_new_line %', tmp_line, ST_Length(tmp_line,true), last_new_line  ;

        IF tmp_line IS NULL THEN
          EXIT;
        END IF;


        IF ST_Length(tmp_line,true) > max_linelength THEN
          IF last_new_line IS NOT NULL THEN
            --RAISE NOTICE 'ok new_line %', last_new_line;
            sub_res.spike_lines = sub_res.spike_lines||(last_new_line,0,NULL)::rog_input_line_order;

            n2 = i_lag+1;
            nl = ST_MakeLine((n1p||p_listdump[n1:n2]));
            sub_res.inputline_left = ST_Collect(sub_res.inputline_left, nl );
            --RAISE NOTICE 'single inputline_left i:%  i_lag:% i_lead:% n1 % n2 % n1p % nit uses lines % last_new_line %', i, i_lag, i_lead, n1, n2, n1p, nl, last_new_line;
            n1 = i_lead;
            n1p = ST_PointN(last_new_line,1);

          END IF;

          EXIT;
        END IF;

        IF ST_Length(tmp_line,true) < max_linelength THEN
            last_new_line = tmp_line;
        END IF;


       END LOOP;

  --     IF tmp_line IS NULL THEN
  --       i = i_lead - counter + 1;
  --     END IF;

       end if;

  END LOOP;
  n2 = i;
  nl = ST_MakeLine((n1p||p_listdump[n1:n2]));
  sub_res.inputline_left = ST_Collect(sub_res.inputline_left, nl );
  --RAISE NOTICE 'single inputline_left i:%  i_lag:% i_lead:% n1 % n2 % n1p % nit uses lines % last_new_line %', i, i_lag, i_lead, n1, n2, n1p, nl, last_new_line;

  sub_res.inputline_left = ST_CollectionExtract(sub_res.inputline_left,2);
END LOOP;

IF sub_res.spike_lines = '{}' THEN
  sub_res.inputline_left = _geom;
END IF;


RETURN sub_res;

END;
$$
LANGUAGE plpgsql
IMMUTABLE;
