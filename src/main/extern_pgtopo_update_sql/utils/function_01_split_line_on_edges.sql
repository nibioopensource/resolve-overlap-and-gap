-- topo_update

CREATE OR REPLACE FUNCTION topo_update.split_line_on_edges(_topology_name varchar, _geom geometry)
RETURNS setof geometry AS $$
DECLARE

split_points geometry;
num_points int;
command_string varchar;

BEGIN

  command_string := Format(
  $fmt$
    SELECT ST_Collect(geom) FROM (
    SELECT (ST_Dump(ST_Intersection(%1$L,e.geom))).geom
    FROM
    %2$s.edge_data e
    WHERE e.geom && %1$L
    ) AS r
    WHERE ST_geometryType(geom) = 'ST_Point'
  $fmt$,
  _geom,
  _topology_name
  );

  EXECUTE command_string INTO split_points;

  IF split_points IS NOT NULL THEN
    RETURN QUERY SELECT (ST_Dump(ST_CollectionExtract(ST_Split(_geom, split_points),2))).geom;
  ELSE
    RETURN QUERY SELECT (ST_Dump(ST_Multi(_geom))).geom;
  END IF;


END;
$$ LANGUAGE plpgsql STABLE;

