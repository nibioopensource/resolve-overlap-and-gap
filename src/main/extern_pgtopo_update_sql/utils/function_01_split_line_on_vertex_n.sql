
--SplitLineStringOnVertexN

CREATE OR REPLACE FUNCTION topo_update.split_line_on_vertex_n(
_geom geometry,
_max_vertexes_pr_line int)
RETURNS geometry[] AS $$

DECLARE

line_points geometry[];
num_points int;
result geometry[] = '{}'::geometry[];
i int = 1;
inext int;

BEGIN

  num_points = ST_NPoints(_geom);
  IF _max_vertexes_pr_line < 2 THEN
    _max_vertexes_pr_line = 2;
  END IF;

  IF num_points > _max_vertexes_pr_line THEN
    SELECT Array_agg(r.geom) pr
      FROM (
      SELECT (ST_DumpPoints(_geom)).geom AS geom
    ) as r INTO line_points;

    WHILE i < num_points LOOP
      inext = i+_max_vertexes_pr_line;

      IF inext + 1 = num_points THEN
        inext = num_points;
      END IF;

      result = result||ARRAY[ST_MakeLine(line_points[i:inext])];
      i = inext;
    END LOOP;

    RETURN result;
  ELSE
    RETURN ARRAY[_geom];
  END IF;


END;
$$ LANGUAGE plpgsql IMMUTABLE;



--SELECT topo_update.split_line_on_vertex_n('LINESTRING(-9.681914765965978 57.7590052828386,-9.681914765965978 63.57625834769352,1.410658737933064 63.57625834769352,1.410658737933064 57.7590052828386,-9.681914765965978 57.7590052828386)',1);



