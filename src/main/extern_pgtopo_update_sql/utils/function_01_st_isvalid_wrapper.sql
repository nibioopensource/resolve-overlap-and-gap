DROP FUNCTION IF EXISTS topo_update.st_isValid_wrapper(_geom geometry);

-- A wrapper function testing on ST_IsValid thats catches EXCEPTION's

CREATE OR REPLACE FUNCTION topo_update.st_isValid_wrapper(_geom geometry)
RETURNS boolean
AS $$DECLARE

BEGIN


   BEGIN
      IF ST_IsValid(_geom) THEN
        RETURN TRUE;
      ELSE
        RETURN FALSE;
      END IF;

   EXCEPTION WHEN OTHERS THEN
        -- log and return false
         RAISE NOTICE 'Failed to run ST_IsValid(%)',ST_asBinary(_geom) ;
        RETURN FALSE;

   END;

END;
$$ LANGUAGE plpgsql IMMUTABLE;

-- Grant som all can use it
GRANT EXECUTE ON FUNCTION topo_update.st_isValid_wrapper(_geom geometry) to PUBLIC;

