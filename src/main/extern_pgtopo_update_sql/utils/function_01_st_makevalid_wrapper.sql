DROP FUNCTION IF EXISTS topo_update.st_makevalid_wrapper(_geom geometry);
DROP FUNCTION IF EXISTS topo_update.st_makevalid_wrapper(_geom geometry, _org_geo geometry);

-- A wrapper function testing on ST_IsValid thats catches EXCEPTION's

CREATE OR REPLACE FUNCTION topo_update.st_makevalid_wrapper(_geom geometry, _org_geo geometry)
RETURNS geometry
AS $$DECLARE
   tmp_geom_input geometry = _geom;
   tmp_geom_new geometry;
   invalid_reason text = '';
   pn text = 'st_makevalid_wrapper';
   v_state text;
   v_msg text;
   v_detail text;
   v_hint text;
   v_context text;
BEGIN

   BEGIN


     BEGIN
       invalid_reason  = ST_IsValidReason(tmp_geom_input);
     EXCEPTION WHEN OTHERS THEN
       -- log and return input geom, with error
       GET STACKED DIAGNOSTICS v_state = RETURNED_SQLSTATE, v_msg = MESSAGE_TEXT, v_detail = PG_EXCEPTION_DETAIL, v_hint = PG_EXCEPTION_HINT, v_context = PG_EXCEPTION_CONTEXT;
       invalid_reason = 'exception when checking error by using ST_IsValidReason:'||v_msg;
     END;


--     RAISE NOTICE 'In %: Will try to fix invalid (%) geo by using ST_MakeValid(%), _org_geo was % ',
--     pn, invalid_reason, tmp_geom_input, _org_geo ;

     -- Will try to remove repeated points based on the info from Sandro https://trac.osgeo.org/postgis/ticket/5528
     -- tmp_geom_new = ST_RemoveRepeatedPoints(tmp_geom_input);

     IF topo_update.st_isValid_wrapper(tmp_geom_new) THEN
       RAISE NOTICE 'In %:Fixed by using ST_RemoveRepeatedPoints with polygon % points fix invalid (%)',
       pn, ST_NPoints(tmp_geom_input), invalid_reason ;
     ELSE
       RAISE NOTICE 'In %:Not fixed by using ST_RemoveRepeatedPoints, but try a final ST_MakeValidwith polygon % points fix invalid (%) ',
       pn, ST_NPoints(tmp_geom_input), invalid_reason ;
       tmp_geom_new = ST_MakeValid(tmp_geom_new);
     END IF;

   EXCEPTION WHEN OTHERS THEN

     -- log and return input geom, with error
     RAISE WARNING 'In %:Failed to fix this by using ST_RemoveRepeatedPoints and ST_MakeValid(%) ', pn, _geom ;

     tmp_geom_new = _geom;


   END;

   RETURN tmp_geom_new;

END;
$$ LANGUAGE plpgsql IMMUTABLE;

-- Grant som all can use it
GRANT EXECUTE ON FUNCTION topo_update.st_makevalid_wrapper(_geom geometry, _org_geo geometry) to PUBLIC;

