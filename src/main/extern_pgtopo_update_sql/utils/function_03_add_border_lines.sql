drop function if exists
topo_update._add_border_lines (_topology_name character varying,
_new_line_raw geometry,
_snap_tolerance float,
_table_name_result_prefix varchar,
_do_retry_add boolean,
_previously_failed_line boolean );

CREATE OR REPLACE FUNCTION topo_update._add_border_lines (_topology_name character varying,
_new_line_raw geometry,
_snap_tolerance float,
_table_name_result_prefix varchar,
_do_retry_add boolean,
_previously_failed_line boolean )
	RETURNS integer[]
	AS $$
DECLARE
	new_line geometry;
	new_egde_geom geometry;
	tmp_egde_geom geometry;
	tmp_edge_geom2 geometry;
	single_line_geo geometry;
	single_line_geo2 geometry;
	command_string text;
	v_state text;
	v_msg text;
	v_detail text;
	v_hint text;
	v_context text;
	i INT DEFAULT 0;
	dim INT DEFAULT 0;
	i2 INT DEFAULT 0;
	dim2 INT DEFAULT 0;
	no_cutline_filename varchar;
	crosses_edge int;
	crosses_edge_num int;
	done_ok boolean;
	num_done_ok int;
	num_not_done_ok int = 0;
	max_num_not_done_ok int = 4;
	lost_data boolean;
	-- returns a set of edge identifiers forming it up
	edges_added integer[];

	tolerance_retry_num int;
	tolerance_retry_diff real;
	tolerance_retry_value real;

	deadlock_detected int;


BEGIN

	no_cutline_filename = _table_name_result_prefix || '_no_cut_line_failed';
	BEGIN
		IF _previously_failed_line THEN
		new_line := _new_line_raw;
	ELSE
		new_line := ST_RemoveRepeatedPoints (_new_line_raw, _snap_tolerance);
	END IF;


		command_string := Format('SELECT ARRAY(SELECT abs(topogeo_addlinestring) FROM topology.TopoGeo_addLinestring(%L,%L,%s))', _topology_name, new_line, _snap_tolerance);
		EXECUTE command_string into edges_added;

		EXCEPTION
		WHEN OTHERS THEN
		GET STACKED DIAGNOSTICS v_state = RETURNED_SQLSTATE, v_msg = MESSAGE_TEXT, v_detail = PG_EXCEPTION_DETAIL, v_hint = PG_EXCEPTION_HINT,
		v_context = PG_EXCEPTION_CONTEXT;
		RAISE NOTICE 'First error at % : % message: % detail : % hint : % context: %', Clock_timestamp(), v_state, v_msg, v_detail, v_hint, v_context;

		-- return if not retry
		IF (_do_retry_add = false) THEN
			RAISE NOTICE '_do_retry_add is false, just log  : % message: % detail : % hint   : % context: %', v_state, v_msg, v_detail, v_hint, v_context;
			EXECUTE Format('INSERT INTO %s(line_geo_lost, error_info, d_state, d_msg, d_detail, d_hint, d_context, geo)
											VALUES(%L, %L, %L, %L, %L, %L, %L, %L)',
											no_cutline_filename, TRUE, 'Will not do retry because _do_retry_add is false ', v_state, v_msg, v_detail, v_hint, v_context, new_line);
			RETURN edges_added;
		END IF;


		-- return if deadlock
		SELECT Position('deadlock detected' IN v_msg) INTO deadlock_detected;
		IF (deadlock_detected > 0 ) THEN
			EXECUTE Format('INSERT INTO %s(line_geo_lost, error_info, d_state, d_msg, d_detail, d_hint, d_context, geo)
			VALUES(%L, %L, %L, %L, %L, %L, %L, %L)',
			no_cutline_filename, TRUE, 'Will not do retry because deadlock detected ', v_state, v_msg, v_detail, v_hint, v_context, new_line);
			-- Why do we raise an error here
			RAISE EXCEPTION 'failed: state deadlock detected : % message: % detail : % hint   : % context: %', v_state, v_msg, v_detail, v_hint, v_context;
		END IF;

		SELECT Position('geometry crosses edge' IN v_msg) INTO crosses_edge;
		-- If crosse egde error try to solve without
		IF (crosses_edge > 0) THEN
			crosses_edge_num := Trim(Substring(v_msg FROM (crosses_edge + Char_length('geometry crosses edge'))))::Int;
			RAISE NOTICE 'crosses_edge % state % message: % detail : % hint   : % context: %', crosses_edge_num, v_state, v_msg, v_detail, v_hint, v_context;

			-- try with only linemerge
			BEGIN
				command_string := Format('SELECT ARRAY(SELECT abs(topogeo_addlinestring) FROM topology.TopoGeo_addLinestring(%3$L,line,%1$s) from
					(
					select distinct (ST_Dump(ST_LineMerge(ST_Union(e.geom)))).geom as line
					from (
						select geom from %3$s.edge e where e.edge_id = %4$s
						union
						select %2$L as geom
						) as e
					) as e
				)',
				_snap_tolerance, _new_line_raw, _topology_name, crosses_edge_num);
				RAISE NOTICE 'command_string %', command_string;
				EXECUTE command_string into edges_added;

				EXECUTE Format('INSERT INTO %s(line_geo_lost, error_info, d_state, d_msg, d_detail, d_hint, d_context, geo) VALUES(%L, %L, %L, %L, %L, %L, %L, %L)',
				no_cutline_filename, FALSE, 'ok to handle crosses_edge with line_merge only for crossing edge, topo_update._add_border_lines ',
				null, null, null, null, null, new_line);

				RETURN edges_added;
			EXCEPTION
				WHEN OTHERS THEN
					GET STACKED DIAGNOSTICS v_state = RETURNED_SQLSTATE, v_msg = MESSAGE_TEXT, v_detail = PG_EXCEPTION_DETAIL, v_hint = PG_EXCEPTION_HINT,
					v_context = PG_EXCEPTION_CONTEXT;
				RAISE NOTICE 'failed to handle crosses_edge with line_merge only for crossing edge % :% message: % detail : % hint   : % context: %',
				crosses_edge_num, v_state, v_msg, v_detail, v_hint, v_context;
				EXECUTE Format('INSERT INTO %s(line_geo_lost, error_info, d_state, d_msg, d_detail, d_hint, d_context, geo) VALUES(%L, %L, %L, %L, %L, %L, %L, %L)',
				no_cutline_filename, FALSE, 'failed to handle crosses_edge with line_merge only for crossing edge, topo_update._add_border_lines ', v_state, v_msg, v_detail, v_hint, v_context, new_line);
			END;

			-- try with delete and the line merge
			BEGIN
				CREATE TEMP table temp_table_fix_topo_crosses_edge_delete_delete(line geometry);
				command_string := Format('INSERT into temp_table_fix_topo_crosses_edge_delete_delete(line)
				select distinct (ST_Dump(ST_LineMerge(ST_Union(e.geom)))).geom as line
				from (
					select geom from %3$s.edge e where e.edge_id = %4$s
					union
					select %2$L as geom
				) as e', _snap_tolerance, _new_line_raw, _topology_name, crosses_edge_num);

				RAISE NOTICE 'command_string %', command_string;
				EXECUTE command_string;

				command_string := Format('select topology.ST_RemEdgeModFace(%1$L, %2$s )'
				, _topology_name, crosses_edge_num);
				EXECUTE command_string;

				command_string := Format('SELECT ARRAY(SELECT abs(topogeo_addlinestring) FROM topology.TopoGeo_addLinestring(%L,line,%s)
				from temp_table_fix_topo_crosses_edge_delete_delete )'
				,_topology_name , _snap_tolerance);

				RAISE NOTICE 'will try to % lines, by command_string %',
				(select count(*) from temp_table_fix_topo_crosses_edge_delete_delete)::int, command_string;

				EXECUTE command_string into edges_added;

				EXECUTE Format('INSERT INTO %s(line_geo_lost, error_info, d_state, d_msg, d_detail, d_hint, d_context, geo) VALUES(%L, %L, %L, %L, %L, %L, %L, %L)',
				no_cutline_filename, FALSE, 'ok to handle crosses_edge with delete crossing edge, topo_update._add_border_lines ',
				null, null, null, null, null, new_line);

				drop table if exists temp_table_fix_topo_crosses_edge_delete_delete;
				RETURN edges_added;
			EXCEPTION
				WHEN OTHERS THEN
					GET STACKED DIAGNOSTICS v_state = RETURNED_SQLSTATE, v_msg = MESSAGE_TEXT, v_detail = PG_EXCEPTION_DETAIL, v_hint = PG_EXCEPTION_HINT,
					v_context = PG_EXCEPTION_CONTEXT;
				RAISE NOTICE 'failed to handle crosses_edge with delete crossing edge % :% message: % detail : % hint   : % context: %',
				crosses_edge_num, v_state, v_msg, v_detail, v_hint, v_context;
				EXECUTE Format('INSERT INTO %s(line_geo_lost, error_info, d_state, d_msg, d_detail, d_hint, d_context, geo) VALUES(%L, %L, %L, %L, %L, %L, %L, %L)',
				no_cutline_filename, FALSE, 'failed to handle crosses_edge with delete crossing edge, topo_update._add_border_lines ',
				v_state, v_msg, v_detail, v_hint, v_context, new_line);
				drop table if exists temp_table_fix_topo_crosses_edge_delete_delete;

			END;
		END IF;

		IF (_snap_tolerance = 0)
		THEN
			RAISE NOTICE 'failed: state deadlock detected or _snap_tolerance = 0  : % message: % detail : % hint   : % context: %', v_state, v_msg, v_detail, v_hint, v_context;
			EXECUTE Format('INSERT INTO %s(line_geo_lost, error_info, d_state, d_msg, d_detail, d_hint, d_context, geo)
											VALUES(%L, %L, %L, %L, %L, %L, %L, %L)',
											no_cutline_filename, TRUE, 'Failed3, topo_update._add_border_lines ', v_state, v_msg, v_detail, v_hint, v_context, new_line);
		END IF;

		tolerance_retry_num := 1;
		tolerance_retry_diff := 0.75;
		tolerance_retry_value := _snap_tolerance*tolerance_retry_num*tolerance_retry_diff;

		EXECUTE Format('INSERT INTO %s(line_geo_lost, error_info, d_state, d_msg, d_detail, d_hint, d_context, geo) VALUES(%L, %L, %L, %L, %L, %L, %L, %L)', no_cutline_filename, FALSE,
		'Warn2 before loop, will do retry, topo_update._add_border_lines with tolerance :'||tolerance_retry_value||' and tolerance_retry_num '|| tolerance_retry_num ||'for topology '||_topology_name,
		v_state, v_msg, v_detail, v_hint, v_context, new_line);


	-- Try with different snap to

		LOOP

			tolerance_retry_value := _snap_tolerance*tolerance_retry_num*tolerance_retry_diff;
			BEGIN
			command_string := Format('SELECT ARRAY(SELECT abs(topogeo_addlinestring) FROM topology.TopoGeo_addLinestring(%L,%L,%s))',
			_topology_name, new_line, tolerance_retry_value);
				EXECUTE command_string into edges_added;
				RETURN edges_added;

			EXCEPTION
			WHEN OTHERS THEN
				GET STACKED DIAGNOSTICS v_state = RETURNED_SQLSTATE, v_msg = MESSAGE_TEXT, v_detail = PG_EXCEPTION_DETAIL, v_hint = PG_EXCEPTION_HINT,
				v_context = PG_EXCEPTION_CONTEXT;
				RAISE NOTICE 'failed after trying with tolerance % : % message: % detail : % hint   : % context: %',
				tolerance_retry_value, v_state, v_msg, v_detail, v_hint, v_context;

				SELECT Position('deadlock detected' IN v_msg) INTO deadlock_detected;
				IF (deadlock_detected > 0 )
				THEN
				RAISE EXCEPTION 'failed after trying with tolerance, got dead lock: state  : % message: % detail : % hint   : % context: %', v_state, v_msg, v_detail, v_hint, v_context;
				END IF;

			END;


			EXIT WHEN tolerance_retry_num > 8;
			tolerance_retry_num := tolerance_retry_num  + 1;

			EXECUTE Format('INSERT INTO %s(line_geo_lost, error_info, d_state, d_msg, d_detail, d_hint, d_context, geo) VALUES(%L, %L, %L, %L, %L, %L, %L, %L)', no_cutline_filename, FALSE,
			'Warn2 in loop, will do retry, topo_update._add_border_lines with tolerance :'||tolerance_retry_value||' and tolerance_retry_num '|| tolerance_retry_num ||'for topology '||_topology_name,
			v_state, v_msg, v_detail, v_hint, v_context, new_line);

		END LOOP;



			-- Try with break up in smaller parts

		BEGIN
			single_line_geo = ST_Multi (topo_update.get_single_lineparts (new_line));
			SELECT ST_NumGeometries (single_line_geo) INTO dim;
			-- Add eache single line
			WHILE i < dim LOOP
				i := i + 1;
				new_egde_geom := ST_GeometryN (single_line_geo, i);
				---
				BEGIN
					command_string := Format('select topology.TopoGeo_addLinestring(%s,%L,%s)', Quote_literal(_topology_name), new_egde_geom, _snap_tolerance);
					EXECUTE command_string;
					EXCEPTION
					WHEN OTHERS THEN
						RAISE NOTICE 'failed topo_update._add_border_lines ::::::::::::::::::::::::::::::::::::::::::::::::::: %', ST_GeometryType (new_egde_geom);
					GET STACKED DIAGNOSTICS v_state = RETURNED_SQLSTATE, v_msg = MESSAGE_TEXT, v_detail = PG_EXCEPTION_DETAIL, v_hint = PG_EXCEPTION_HINT,
					v_context = PG_EXCEPTION_CONTEXT;
					RAISE NOTICE 'failed: state  : % message: % detail : % hint   : % context: %', v_state, v_msg, v_detail, v_hint, v_context;
					EXECUTE Format('INSERT INTO %s(line_geo_lost, error_info, d_state, d_msg, d_detail, d_hint, d_context, geo) VALUES(%L, %L, %L, %L, %L, %L, %L, %L)', no_cutline_filename, FALSE,
					'Warn2, will do retry, topo_update._add_border_lines ' || 'for topology '||_topology_name,
					v_state, v_msg, v_detail, v_hint, v_context, new_egde_geom);
					--1
					BEGIN
						-- try extend line, that may help som time
						tmp_egde_geom := topo_update.extend_line (new_egde_geom, _snap_tolerance * 2);
						command_string := Format('select topology.TopoGeo_addLinestring(%s,%L,%s)', Quote_literal(_topology_name), tmp_egde_geom, _snap_tolerance);
						EXECUTE command_string;
						EXCEPTION
						WHEN OTHERS THEN
							RAISE NOTICE 'failed topo_update._add_border_lines ::::::::::::::::::::::::::::::::::::::::::::::::::: %', ST_GeometryType (new_egde_geom);
						GET STACKED DIAGNOSTICS v_state = RETURNED_SQLSTATE, v_msg = MESSAGE_TEXT, v_detail = PG_EXCEPTION_DETAIL, v_hint = PG_EXCEPTION_HINT,
						v_context = PG_EXCEPTION_CONTEXT;
						RAISE NOTICE 'failed: state  : % message: % detail : % hint   : % context: %', v_state, v_msg, v_detail, v_hint, v_context;
						EXECUTE Format('INSERT INTO %s(line_geo_lost, error_info, d_state, d_msg, d_detail, d_hint, d_context, geo) VALUES(%L, %L, %L, %L, %L, %L, %L, %L)', no_cutline_filename, FALSE,
						'Warn3, will do retry, topo_update._add_border_lines ', v_state, v_msg, v_detail, v_hint, v_context, tmp_egde_geom);
						-- 2
						BEGIN
							-- try remove old intersecting egdes and add them again
							-- Check if the last message is 'SQL/MM Spatial exception - geometry crosses edge ****'

						CREATE TEMP table temp_table_fix_topo(line geometry, edge_id int);

							LOOP
								BEGIN
							crosses_edge_num = - 1;
							done_ok := true;
								num_not_done_ok := num_not_done_ok + 1;
								i2 := 0;

								EXECUTE Format('INSERT INTO %s(line_geo_lost, error_info, d_state, d_msg, d_detail, d_hint, d_context, geo) VALUES(%L, %L, %L, %L, %L, %L, %L, %L)', no_cutline_filename, FALSE,
								'Warn4, will do retry, topo_update._add_border_lines ', v_state, v_msg, v_detail, v_hint, v_context, tmp_egde_geom);

								RAISE NOTICE 'num_not_done_ok rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr: %', num_not_done_ok;

								SELECT Position('geometry crosses edge' IN v_msg) INTO crosses_edge;
								tmp_egde_geom := new_egde_geom;
								IF (crosses_edge > 0) THEN
									crosses_edge_num := Trim(Substring(v_msg FROM (crosses_edge + Char_length('geometry crosses edge'))))::Int;
									command_string := Format('INSERT into temp_table_fix_topo(line,edge_id)
											select distinct (ST_Dump(ST_LineMerge(ST_Union(ST_SnapToGrid(e.geom,%1$s),ST_SnapToGrid(%2$L,%1$s))))).geom as line, e.edge_id
											from
											%3$s.edge e
											where e.edge_id = %4$L', _snap_tolerance, tmp_egde_geom, _topology_name, crosses_edge_num);
								ELSE
									command_string := Format('INSERT into temp_table_fix_topo(line,edge_id)
											select distinct (ST_Dump(ST_LineMerge(ST_Union(ST_SnapToGrid(e.geom,%1$s),ST_SnapToGrid(%2$L,%1$s))))).geom as line, e.edge_id
											from
											%3$s.edge e
											where e.geom && %2$L and ST_Intersects(e.geom,%2$L)
											', _snap_tolerance, tmp_egde_geom, _topology_name);
								END IF;
								EXECUTE command_string;
								command_string := Format('select topology.ST_RemEdgeModFace(%1$L,  l.edge_id) from
											(select distinct edge_id from temp_table_fix_topo) as l,
											%1$s.edge e
											where l.edge_id = e.edge_id', _topology_name);
								EXECUTE command_string;
								command_string := Format('select ST_CollectionExtract(ST_Collect(distinct line),2) from temp_table_fix_topo');
								EXECUTE command_string INTO single_line_geo2;
								-- start loop throug each
								SELECT ST_NumGeometries (single_line_geo2) INTO dim2;
								--1
								WHILE i2 < dim2 LOOP
									i2 := i2 + 1;
									tmp_edge_geom2 := ST_GeometryN (single_line_geo2, i2);
									BEGIN
								EXECUTE Format('INSERT INTO %s(line_geo_lost, error_info, d_state, d_msg, d_detail, d_hint, d_context, geo) VALUES(%L, %L, %L, %L, %L, %L, %L, %L)', no_cutline_filename, FALSE,
								'Warn5, will do retry, topo_update._add_border_lines i2:'||i2||' dim2:'||dim2||' num_not_done_ok:'||num_not_done_ok||' max_num_not_done_ok:'||max_num_not_done_ok,
								v_state, v_msg, v_detail, v_hint, v_context, tmp_edge_geom2);

										command_string := Format('select topology.TopoGeo_addLinestring(%s,%L,%s)', Quote_literal(_topology_name), tmp_edge_geom2, _snap_tolerance );
										EXECUTE command_string;
										EXCEPTION
										WHEN OTHERS THEN
										done_ok := false;

											RAISE NOTICE 'failed topo_update._add_border_lines ::::::::::::::::::::::::::::::::::::::::::::::::::: %', ST_GeometryType (tmp_edge_geom2);
										GET STACKED DIAGNOSTICS v_state = RETURNED_SQLSTATE, v_msg = MESSAGE_TEXT, v_detail = PG_EXCEPTION_DETAIL, v_hint = PG_EXCEPTION_HINT,
										v_context = PG_EXCEPTION_CONTEXT;
										RAISE NOTICE 'failed1: state  : % message: % detail : % hint   : % context: %', v_state, v_msg, v_detail, v_hint, v_context;
										IF  (max_num_not_done_ok = num_not_done_ok) THEN
											lost_data = true;
										ELSE
											lost_data = false;
										END IF;
										EXECUTE Format('INSERT INTO %s(line_geo_lost, error_info, d_state, d_msg, d_detail, d_hint, d_context, geo) VALUES(%L, %L, %L, %L, %L, %L, %L, %L)',
										no_cutline_filename, lost_data,
										'Failed1, at num ' || i2 || ' topo_update._add_border_lines where crosses_edge_num=' || crosses_edge_num || ' num_not_done_ok:' || num_not_done_ok,
										v_state, v_msg, v_detail, v_hint, v_context, tmp_edge_geom2);
									END;
								END LOOP;
								EXCEPTION
								WHEN OTHERS THEN
									RAISE NOTICE 'failed topo_update._add_border_lines ::::::::::::::::::::::::::::::::::::::::::::::::::: %', ST_GeometryType (tmp_egde_geom);
								GET STACKED DIAGNOSTICS v_state = RETURNED_SQLSTATE, v_msg = MESSAGE_TEXT, v_detail = PG_EXCEPTION_DETAIL, v_hint = PG_EXCEPTION_HINT,
								v_context = PG_EXCEPTION_CONTEXT;
								RAISE NOTICE 'failed2: state  : % message: % detail : % hint   : % context: %', v_state, v_msg, v_detail, v_hint, v_context;
								EXECUTE Format('INSERT INTO %s(line_geo_lost, error_info, d_state, d_msg, d_detail, d_hint, d_context, geo) VALUES(%L, %L, %L, %L, %L, %L, %L, %L)', no_cutline_filename, TRUE, 'Failed2, topo_update._add_border_lines', v_state, v_msg, v_detail, v_hint, v_context, new_egde_geom);
								-- 2
								END;
								EXIT WHEN num_not_done_ok > max_num_not_done_ok or done_ok = true;

							END LOOP;

							if (done_ok = false) THEN
								RAISE NOTICE 'failed2, done_ok = false state  : % message: % detail : % hint   : % context: %', v_state, v_msg, v_detail, v_hint, v_context;
								EXECUTE Format('INSERT INTO %s(line_geo_lost, error_info, d_state, d_msg, d_detail, d_hint, d_context, geo) VALUES(%L, %L, %L, %L, %L, %L, %L, %L)', no_cutline_filename, TRUE, 'Failed2, topo_update._add_border_lines', v_state, v_msg, v_detail, v_hint, v_context, new_egde_geom);
							END IF;

							-- done loop throug each
							DROP TABLE temp_table_fix_topo;
							--1
							END;
						END;
					END;
				---
			END LOOP;
			EXCEPTION
			WHEN OTHERS THEN
				GET STACKED DIAGNOSTICS v_state = RETURNED_SQLSTATE, v_msg = MESSAGE_TEXT, v_detail = PG_EXCEPTION_DETAIL, v_hint = PG_EXCEPTION_HINT,
				v_context = PG_EXCEPTION_CONTEXT;
			RAISE NOTICE 'failed: state  : % message: % detail : % hint   : % context: %', v_state, v_msg, v_detail, v_hint, v_context;
			EXECUTE Format('INSERT INTO %s(line_geo_lost, error_info, d_state, d_msg, d_detail, d_hint, d_context, geo) VALUES(%L, %L, %L, %L, %L, %L, %L, %L)', no_cutline_filename, TRUE, 'Failed3, topo_update._add_border_lines ', v_state, v_msg, v_detail, v_hint, v_context, new_line);
			END;
		END;
	RETURN edges_added;
END;

$$
LANGUAGE plpgsql VOLATILE;






drop function if exists
topo_update.add_border_lines (_topology_name character varying,
_new_line_raw geometry,
_snap_tolerance float,
_table_name_result_prefix varchar,
_do_retry_add boolean);

drop function if exists
topo_update.add_border_lines (_topology_name character varying,
_new_line_raw geometry,
_snap_tolerance float,
_table_name_result_prefix varchar,
_do_retry_add boolean,
_previously_failed_line boolean );

drop function if exists
topo_update.add_border_lines (_topology_name character varying,
_new_line_raw geometry,
_table_input_order integer,
_snap_tolerance float,
_table_name_result_prefix varchar,
_do_retry_add boolean,
_previously_failed_line boolean );

drop function if exists
topo_update.add_border_lines (_topology_name character varying,
_new_line_raw geometry,
_min_tolerated_face_area float8,
_table_input_order integer,
_snap_tolerance float,
_table_name_result_prefix varchar,
_do_retry_add boolean,
_previously_failed_line boolean );

drop function if exists
topo_update.add_border_lines (
_topology_name character varying,
_new_line_raw geometry,
_min_tolerated_face_area float8,
_min_st_is_empty_buffer_value float,
_table_input_order integer,
_snap_tolerance float,
_table_name_result_prefix varchar,
_do_retry_add boolean,
_previously_failed_line boolean,
_utm boolean
);


CREATE OR REPLACE FUNCTION topo_update.add_border_lines (
_topology_name character varying,
_new_line_raw geometry,
_min_tolerated_face_area float8,
_splite_line_at_vertex_n int, -- split all lines at given vertex num
_min_st_is_empty_buffer_value float,
_table_input_order integer,
_src_table_pk_column_value text,
_snap_tolerance float,
_table_name_result_prefix varchar,
_do_retry_add boolean,
_previously_failed_line boolean,
_utm boolean,
_log_failed_line_error_table boolean default TRUE, -- If true it will add new rows to _table_name_result_prefix || '_no_cut_line_failed';
_use_temp_edges_table_name_for_small_area boolean default FALSE, -- If true a local table name will be used
_check_geeomtry_after_added_line boolean default TRUE, -- By default we will check for min area values and sliver value in topology created.
_tables_ext_metainfo rog_overlay_source_table_metainfo_type[] default NULL
)
	RETURNS rog_overlay_add_border_lines_type
	AS $$
DECLARE
	new_line geometry;
	new_egde_geom geometry;
	tmp_egde_geom geometry;
	tmp_edge_geom2 geometry;
	single_line_geo geometry;
	single_line_geo2 geometry;
	command_string text;
	v_state text;
	v_msg text;
	v_detail text;
	v_hint text;
	v_context text;
	i INT DEFAULT 0;
	dim INT DEFAULT 0;
	i2 INT DEFAULT 0;
	dim2 INT DEFAULT 0;
	no_cutline_filename varchar;
	crosses_edge int;
	crosses_edge_num int;
	done_ok boolean;
	num_done_ok int;
	num_not_done_ok int = 0;
	max_num_not_done_ok int = 4;
	lost_data boolean;
	-- returns a set of edge identifiers forming it up
	inputLineEdgeIds integer[];

	tolerance_retry_num int;
	tolerance_retry_diff real;
	tolerance_retry_value real;

	deadlock_detected int;

	candidateInputLines GEOMETRY;
	blade_lines GEOMETRY;
	intersection_points GEOMETRY;

	candidateLine GEOMETRY;
	proc_name text = 'rog add_border_lines';
	sql text;
	_borderLayer_layer_id int;
	dropInputLines INT[];

	face_edges_to_remove_list add_border_lines_faces_to_remove[];
	face_edges_to_remove add_border_lines_faces_to_remove;

	rec RECORD;
	egde_to_small_area boolean = false;
	inputLineNodeIds INT[];

	no_usable_area int;

	rec_line RECORD;

	result rog_overlay_add_border_lines_type ;

	failed_line rog_overlay_add_border_failed_line;
	tbl_name_edges_to_remove text;

	min_st_is_empty_buffer_value float;

	line_edges_tmp integer[];

	splite_lines_result geometry[];

	rog_meta_info rog_overlay_source_table_metainfo_type;


	--Test with https://gitlab.com/nibioopensource/resolve-overlap-and-gap/uploads/352d900a4f5e66baccc00407a14d85e6/test_10_b.sql.gz
	--_splite_line_at_vertex_n int = 100;
	--0.22s user 0.26s system 0% cpu 2:35.26 total
	--output simple feature num polygons:716| num vertexes:554962

	--Test with https://gitlab.com/nibioopensource/resolve-overlap-and-gap/uploads/352d900a4f5e66baccc00407a14d85e6/test_10_b.sql.gz
	--_splite_line_at_vertex_n int;
	--0.22s user 0.25s system 0% cpu 22:20.13 total
	--output simple feature num polygons:716| num vertexes:554962

	-- TODO add as parameter

	-- sql used find faces added
	sql_faces_added text;

	-- number edges created by this input line
	edge_counter int;

	-- the selected mbr created by this value
	selcted_edge_mbr geometry;

	-- the selected face for this edge
	selected_edge_face_id int;

	row_ct int;


BEGIN

	no_cutline_filename = _table_name_result_prefix || '_no_cut_line_failed';
	new_line := _new_line_raw;

	tbl_name_edges_to_remove  = _table_name_result_prefix||'_edges_to_remove';

	IF _use_temp_edges_table_name_for_small_area THEN
		tbl_name_edges_to_remove  = _topology_name||'.edges_to_remove';
	END IF;

	IF _min_st_is_empty_buffer_value IS NOT NULL AND _min_st_is_empty_buffer_value > 0 THEN
		min_st_is_empty_buffer_value = - (_min_st_is_empty_buffer_value) ;
	END IF;
	-- RAISE NOTICE 'add_border_lines _min_st_is_empty_buffer_value %   min_st_is_empty_buffer_value %', _min_st_is_empty_buffer_value, min_st_is_empty_buffer_value;

	BEGIN

/**
		sql := format(
					$fmt$
						SELECT ST_Intersection(e.geom,%2$L)
						FROM %1$I.edge_data e
						WHERE e.geom && %2$L
					$fmt$,
					_topology_name,
					new_line);
		execute sql INTO intersection_points;

		RAISE NOTICE 'TRY add adding a line with length %, num points %, start % end % (but maybe retried at later stage) at  % with intersection_points %',
		ST_Length(new_line,true), ST_NPoints(new_line), ST_StartPoint(new_line), ST_EndPoint(new_line),
		Clock_timestamp(), ST_AsText(intersection_points);
*/

		-- Insert the inputLine to the topology
		-- and gather list of edges forming it
		IF _do_retry_add THEN
			-- This code is be removed topology is 100% robust
			SELECT topo_update._add_border_lines(_topology_name,new_line,_snap_tolerance,_table_name_result_prefix,_do_retry_add,_previously_failed_line)
			INTO inputLineEdgeIds;
		ELSE

			IF _splite_line_at_vertex_n > 1 THEN
				splite_lines_result = topo_update.split_line_on_vertex_n(new_line,_splite_line_at_vertex_n);

--				RAISE NOTICE 'topo_update.add_border_lines _splite_line_at_vertex_n % splite_lines_result %  a line with length %, num points %, start % end % at %',
--				_splite_line_at_vertex_n, ARRAY_Length(splite_lines_result,1), ST_Length(new_line,true), ST_NPoints(new_line), ST_StartPoint(new_line), ST_EndPoint(new_line), Clock_timestamp();

				command_string := Format(
				$fmt$
				SELECT ARRAY(
					SELECT abs(t.topogeo_addlinestring) FROM
					(
					SELECT topology.TopoGeo_addLinestring(%1$L,sl.line,%3$s)
					FROM (SELECT unnest(%2$L::geometry[]) line) sl
					) t
				)
				$fmt$,
				_topology_name,
				splite_lines_result,
				_snap_tolerance
				);
				EXECUTE command_string into inputLineEdgeIds;
			ELSE
--				RAISE NOTICE 'topo_update.add_border_lines no split a line with length %, num points %, start % end % at %',
--				ST_Length(new_line,true), ST_NPoints(new_line), ST_StartPoint(new_line), ST_EndPoint(new_line), Clock_timestamp();

				command_string := Format(
				$fmt$
				SELECT ARRAY(
					SELECT abs(topogeo_addlinestring) FROM topology.TopoGeo_addLinestring(%1$L,%2$L,%3$s)
				)
				$fmt$,
				_topology_name,
				new_line,
				_snap_tolerance
				);
				EXECUTE command_string into inputLineEdgeIds;
			END IF;


		END IF;


		-- When adding line from a temp geometry that are not connected there is no need check small area spikes and so on, because that is already checked
		IF _check_geeomtry_after_added_line THEN

			face_edges_to_remove_list := topo_update.check_added_updated_edge(
				_topology_name,
				inputLineEdgeIds,
				_min_tolerated_face_area,
				min_st_is_empty_buffer_value,
				_table_input_order,
				_table_name_result_prefix,
				_use_temp_edges_table_name_for_small_area
			)::add_border_lines_faces_to_remove[];


-- RAISE NOTICE 'NOT FFFAIL %;', command_string;

		END IF;

	EXCEPTION WHEN OTHERS THEN
		GET STACKED DIAGNOSTICS v_state = RETURNED_SQLSTATE, v_msg = MESSAGE_TEXT, v_detail = PG_EXCEPTION_DETAIL, v_hint = PG_EXCEPTION_HINT,
		v_context = PG_EXCEPTION_CONTEXT;

-- RAISE NOTICE 'IS FFFAIL %;', command_string;

		RAISE NOTICE 'FAIL adding to % using _splite_line_at_vertex_n % line ST_IsClosed % line with length %, num points %, start % end % (but maybe retried at later stage) at  % : % message: % detail : % hint : % context: %',
		_topology_name, _splite_line_at_vertex_n, ST_IsClosed(new_line), ST_Length(new_line,true), ST_NPoints(new_line), ST_StartPoint(new_line), ST_EndPoint(new_line),
		Clock_timestamp(), v_state, v_msg, v_detail, v_hint, v_context;

		failed_line = (new_line,v_state,v_msg,v_detail,v_hint,v_context);

		IF _log_failed_line_error_table THEN
			--RAISE NOTICE 'First error outer logged to table % : % message: % detail : % hint : % context: %', Clock_timestamp(), v_state, v_msg, v_detail, v_hint, v_context;
			EXECUTE Format('INSERT INTO %s(line_geo_lost, error_info, d_state, d_msg, d_detail, d_hint, d_context, geo)
			VALUES(%L, %L, %L, %L, %L, %L, %L, %L)',
			no_cutline_filename, TRUE, 'Will do retry in a later stage ', v_state, v_msg, v_detail, v_hint, v_context, new_line);
		--ELSE
			--RAISE NOTICE 'First error outer NOT logged to table % : % message: % detail : % hint : % context: %', Clock_timestamp(), v_state, v_msg, v_detail, v_hint, v_context;
		END IF;

	END;

	result = (inputLineEdgeIds,failed_line,face_edges_to_remove_list,_table_input_order,_src_table_pk_column_value,new_line,selected_edge_face_id);

	RETURN result;
END;

$$
LANGUAGE plpgsql VOLATILE;


/**

-- Test 1
-- this code is to slow and gives wrong resultfor some cases
-- Ok for rog_overlay_test_02.sql, fails for rog_overlay_test_14.sql

		sql_faces_added := format(
				$fmt$

				WITH newEdges AS (
						SELECT edge_id, left_face, right_face, geom
						FROM %1$I.edge_data
						WHERE edge_id = ANY($1)
						AND left_face != right_face
						--AND ST_Overlaps(%2$L,geom)
				), newBoundFaces AS (
						SELECT DISTINCT
						face_id,
						mbr
						FROM %1$I.face f
						WHERE face_id > 0
						AND face_id IN (
								SELECT left_face FROM newEdges
								UNION
								SELECT right_face FROM newEdges
						)
				)
				SELECT
				DISTINCT
				f.face_id,
				f.mbr
				FROM newBoundFaces f, newEdges e
				WHERE (f.face_id = e.left_face OR f.face_id = e.right_face)
				$fmt$,
				_topology_name,
				_new_line_raw

		);


		RAISE NOTICE 'added rec_line % ', _new_line_raw ;

		edge_counter = 0;
		FOR rec_line IN EXECUTE sql_faces_added USING inputLineEdgeIds
		LOOP
			edge_counter = edge_counter + 1;
			RAISE NOTICE 'for edge_counter % _src_table_pk_column_value % added rec_line % ',
			edge_counter, _src_table_pk_column_value, rec_line ;
			RAISE NOTICE '_tables_ext_metainfo % ', _tables_ext_metainfo ;



			selcted_edge_mbr = rec_line.mbr;
			selected_edge_face_id = rec_line.face_id;

			FOREACH rog_meta_info IN ARRAY _tables_ext_metainfo
			LOOP

			sql = format(
					$fmt$
						INSERT INTO %1$s (table_input_order, grid_cell_id, src_table_pk_column_value,face_mbr,face_id,pk_value_set)
						SELECT %2$L,%3$L,
						CAST (old.%9$s AS TEXT) AS src_table_pk_column_value,
						%5$L,%6$L,true
						FROM
						%8$s old
						WHERE -- NOT EXISTS (SELECT TRUE FROM %1$s ar WHERE ar.table_input_order = %2$L AND ar.src_table_pk_column_value = %4$s::text AND ar.face_id = %6$L) AND
						ST_Intersects( (ST_MaximumInscribedCircle(ST_GetFaceGeometry(%7$L,%6$s))).center,  ST_Transform(old.%10$s,4258))
						ON CONFLICT (table_input_order, face_id, src_table_pk_column_value) DO NOTHING;

					$fmt$,
					_table_name_result_prefix||'_org_pk_value_to_face_id', --01
					(rog_meta_info).table_input_order, --02
					NULL, -- maybe we need send as paramater --03
					_src_table_pk_column_value, --04
					selcted_edge_mbr, --05
					selected_edge_face_id, --06
					_topology_name, --07
					(rog_meta_info).table_name, --08
					(rog_meta_info).table_pk_colum ,--09
					(rog_meta_info).table_geo_column --10
			);

			RAISE NOTICE 'rec_line sql %', sql ;

			EXECUTE sql;

			GET DIAGNOSTICS row_ct := ROW_COUNT;
			RAISE NOTICE 'rec_line.face_id % row_ct %, sql %', rec_line.face_id, row_ct, sql ;

			END LOOP;

		END LOOP;

*/


/**

-- Test 2
-- this code is to slow and gives wrong result for some cases also
-- Ok for rog_overlay_test_02.sql, fails for rog_overlay_test_14.sql

		sql_faces_added := format(
				$fmt$

				WITH newEdges AS (
						SELECT edge_id, left_face, right_face, geom
						FROM %1$I.edge_data
						WHERE edge_id = ANY($1)
						AND left_face != right_face
						--AND ST_Overlaps(%2$L,geom)
				), newBoundFaces AS (
						SELECT DISTINCT
						face_id,
						mbr
						FROM %1$I.face f
						WHERE face_id > 0
						AND face_id IN (
								SELECT left_face FROM newEdges
								UNION
								SELECT right_face FROM newEdges
						)
				)
				SELECT
				DISTINCT
				f.face_id,
				f.mbr
				FROM newBoundFaces f, newEdges e
				WHERE (f.face_id = e.left_face OR f.face_id = e.right_face)
				$fmt$,
				_topology_name,
				_new_line_raw

		);

RAISE NOTICE 'added rec_line % ', _new_line_raw ;

		edge_counter = 0;
		FOR rec_line IN EXECUTE sql_faces_added USING inputLineEdgeIds
		LOOP
			edge_counter = edge_counter + 1;
			RAISE NOTICE 'for edge_counter % _src_table_pk_column_value % added rec_line % ',
			edge_counter, _src_table_pk_column_value, rec_line ;
			RAISE NOTICE '_tables_ext_metainfo % ', _tables_ext_metainfo ;



			selcted_edge_mbr = rec_line.mbr;
			selected_edge_face_id = rec_line.face_id;

			FOREACH rog_meta_info IN ARRAY _tables_ext_metainfo
			LOOP

			sql = format(
					$fmt$
						INSERT INTO %1$s (table_input_order, grid_cell_id, src_table_pk_column_value,face_mbr,face_id,pk_value_set)
						SELECT %2$L,%3$L,
						CAST (old.%9$s AS TEXT) AS src_table_pk_column_value,
						%5$L,%6$L,true
						FROM
						%8$s old
						WHERE -- NOT EXISTS (SELECT TRUE FROM %1$s ar WHERE ar.table_input_order = %2$L AND ar.src_table_pk_column_value = %4$s::text AND ar.face_id = %6$L) AND
						ST_Intersects( (ST_MaximumInscribedCircle(ST_GetFaceGeometry(%7$L,%6$s))).center,  ST_Transform(old.%10$s,4258))
						ON CONFLICT (table_input_order, face_id, src_table_pk_column_value) DO NOTHING;

					$fmt$,
					_table_name_result_prefix||'_org_pk_value_to_face_id', --01
					(rog_meta_info).table_input_order, --02
					NULL, -- maybe we need send as paramater --03
					_src_table_pk_column_value, --04
					selcted_edge_mbr, --05
					selected_edge_face_id, --06
					_topology_name, --07
					(rog_meta_info).table_name, --08
					(rog_meta_info).table_pk_colum ,--09
					(rog_meta_info).table_geo_column --10
			);

			RAISE NOTICE 'rec_line sql %', sql ;

			EXECUTE sql;

			GET DIAGNOSTICS row_ct := ROW_COUNT;
			RAISE NOTICE 'rec_line.face_id % row_ct %, sql %', rec_line.face_id, row_ct, sql ;

			END LOOP;
*/


