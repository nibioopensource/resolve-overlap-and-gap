/**

Check that new/changdes edge contributtes to a new valid face

*/

CREATE OR REPLACE FUNCTION topo_update.check_added_updated_edge (
		IN _topology_name character varying,
		IN _input_line_edge_ids integer[],
		IN _min_tolerated_face_area float8,
		IN _min_st_is_empty_buffer_value float,
		IN _table_input_order integer,
		IN _table_name_result_prefix varchar,
		IN _use_temp_edges_table_name_for_small_area boolean
)
RETURNS add_border_lines_faces_to_remove[]
AS $$
DECLARE
		dropInputLines INT[] := '{}';
		rec RECORD;
		face_edges_to_remove add_border_lines_faces_to_remove;
		face_edges_to_remove_list add_border_lines_faces_to_remove[];
		tbl_name_edges_to_remove text := _table_name_result_prefix || '_edges_to_remove';
		line_edges_tmp integer[];
		sql text;
BEGIN

IF _min_tolerated_face_area > 0 OR _min_st_is_empty_buffer_value > 0 THEN
	sql := format(
			$fmt$
			WITH newEdges AS (
					SELECT edge_id, left_face, right_face, geom
					FROM %1$I.edge_data
					WHERE edge_id = ANY($1)
			), newBoundFaces AS (
					SELECT
					face_id,
					CASE
					WHEN $2 > 0 AND ST_Area(f.mbr, true) < $2 THEN NULL -- if check for smallarea and bbox area is below that trow awat this
					ELSE topology.ST_GetFaceGeometry(%1$L, f.face_id)  -- else return geometry to check the actual area
					END AS face_geom
					FROM %1$I.face f
					WHERE face_id > 0
					AND face_id IN (
							SELECT left_face FROM newEdges
							UNION
							SELECT right_face FROM newEdges
					)
			)
			SELECT
			array_agg(e.edge_id) edge_ids,
			f.face_id,
			-1 face_area,
			ST_Collect(e.geom) edge_geoms
			FROM newBoundFaces f, newEdges e
			WHERE (f.face_id = e.left_face OR f.face_id = e.right_face)
			AND
			(
					face_geom IS NULL -- the bbox area is below or st_buffer removed the
					OR
					($2 > 0 AND ST_Area(face_geom, true) < $2) -- is areas constrains check area
					OR
					($3 > 0 AND ST_IsEmpty(ST_Buffer(face_geom, $3))) -- if sliver constraint check with st_buffer
			)
			GROUP BY f.face_id, f.face_geom
			$fmt$,
			_topology_name
	);

	FOR rec IN EXECUTE sql USING _input_line_edge_ids, _min_tolerated_face_area, _min_st_is_empty_buffer_value
	LOOP
			line_edges_tmp := (
					SELECT array_agg(distinct e)
					FROM unnest(rec.edge_ids) e
					WHERE NOT dropInputLines @> rec.edge_ids
			);

			IF line_edges_tmp IS NOT NULL THEN
				dropInputLines := array_cat(dropInputLines, line_edges_tmp);
				IF _use_temp_edges_table_name_for_small_area = FALSE THEN
						EXECUTE format(
								$fmt$
								INSERT INTO %1$s(edge_id, table_input_order, geo, face_area, face_id)
								SELECT e.edge_id, %2$L AS table_input_order, e.geom AS geo, %5$L AS face_area, %6$L AS face_id
								FROM
								%3$I.edge e,
								(SELECT unnest(%4$L::INT[]) edge_id) de
								WHERE de.edge_id = e.edge_id
								ON CONFLICT (edge_id)
								DO
								UPDATE SET table_input_order = EXCLUDED.table_input_order, geo = EXCLUDED.geo, face_area = EXCLUDED.face_area, face_id = EXCLUDED.face_id
								$fmt$,
								tbl_name_edges_to_remove,
								_table_input_order,
								_topology_name,
								rec.edge_ids,
								rec.face_area,
								rec.face_id
						);
				ELSE
						face_edges_to_remove := (rec.edge_ids, rec.face_area, rec.face_id, _table_input_order, rec.edge_geoms);
						face_edges_to_remove_list := array_append(face_edges_to_remove_list, face_edges_to_remove);
				END IF;
			END IF;
	END LOOP;
END IF;

/**

		RAISE NOTICE 'Done checking % edges and dropInputLines number % and return face_edges_to_remove_list %',
				ARRAY_Length(_input_line_edge_ids,1) ,
				ARRAY_Length(dropInputLines,1),
				ARRAY_Length(face_edges_to_remove_list,1);

*/
RETURN face_edges_to_remove_list;
END;
$$
LANGUAGE plpgsql VOLATILE;
