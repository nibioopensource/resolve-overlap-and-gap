
-- Add border line parts not adde to connect lines crossing cell borders and remove any area below min surface size

CREATE OR REPLACE PROCEDURE topo_update.add_cellborder_lines (
  _topology_info resolve_overlap_data_topology_type,
  ---(_topology_info).topology_name varchar, -- The topology schema name where we store store sufaces and lines from the simple feature dataset and th efinal result
  -- NB. Any exting data will related to topology_name will be deleted
  --(_topology_info).topology_srid
  --(_topology_info).topology_snap_tolerance float, -- this is tolerance used as base when creating the the postgis topolayer
  --(_topology_info).create_topology_attrbute_tables boolean -- if this is true and we value for line_table_name we create attribute tables refferances to
  -- this tables will have atrbuttes equal to the simple feauture tables for lines and feautures
  -- a flag for testing the diff using a temporary tempology or not or insert directly into final source topology
  -- this is a test related to performance and testing on that, the final results should be pretty much the same.
  -- (_topology_info).use_temp_topology boolean

  _clean_info resolve_overlap_data_clean_type, -- different parameters used if need to clean up your data
  --(_clean_info)._min_area_to_keep float default 0, -- if this a polygon  is below this limit it will merge into a neighbour polygon. The area is sqare meter.
  --(_clean_info).split_input_lines_at_vertex_n int default NULL, -- split all lines at given vertex num
  --(_clean_info)._simplify_tolerance float default 0, -- is this is more than zero simply will called with
  --(_clean_info)._simplify_max_average_vertex_length int default 0, -- in meter both for utm and deegrees, this used to avoid running ST_simplifyPreserveTopology for long lines lines with few points
  --(_clean_info)._chaikins_nIterations int default 0, -- IF 0 NO CHAKINS WILL BE DONE,  A big value here make no sense because the number of points will increaes exponential )
  --(_clean_info)._chaikins_max_length int default 0, --edge that are longer than this value will not be touched by _chaikins_min_degrees and _chaikins_max_degrees
  --(_clean_info)._chaikins_min_degrees int default 0, -- The angle has to be less this given value, This is used to avoid to touch all angles.
  --(_clean_info)._chaikins_max_degrees int default 0, -- OR the angle has to be greather than this given value, This is used to avoid to touch all angles
  --(_clean_info)._chaikins_min_steep_angle_degrees int default 0, -- The angle has to be less this given value, This is used to avoid to touch all angles.
  --(_clean_info)._chaikins_max_steep_angle_degrees int default 0-- OR The angle has to be greather than this given value, This is used to avoid to touch all angles

    _table_name_result_prefix text,

    _this_topology_info_topology_snap_tolerance float,

    -- when working sub cells we need know the bouding box
    -- when null all bords not adde will be add
    _bb geometry,

    -- if too long time is used the code will return and the caller has to retry
    _return_if_long_execute_time boolean,

    _tables_ext_metainfo rog_overlay_source_table_metainfo_type[], -- A list input tables meta info

    INOUT _done_add_all_lines boolean
)
LANGUAGE plpgsql
AS $$
DECLARE

  result text;
  rec_new_line RECORD;
  rec_new_temp_edges int[];
  rec_new_edges_since_anlyze int = 0;
  rec_new_edges_total int = 0;
  rec_new_edges_analyze int = 500;
  rec_new_counter int = 0;
  rog_added_border_line rog_overlay_add_border_lines_type;

  start_time_delta_job timestamp WITH time zone;
  start_time timestamp WITH time zone;
  used_time real;

  rec_new_edges_to_remove int = 0;
  lines_to_retry rog_overlay_add_border_failed_line[] = '{}'::rog_overlay_add_border_failed_line[];
  tmp_line_to_retry rog_overlay_add_border_failed_line;

  command_string text;

  set_added_to_master int = 0;

  row_count int;
  total_num_border_crossing_line int;

  bb_buffer geometry = _bb;

  lines_added geometry[] = '{}'::geometry[];
  update_master_line geometry;

  -- holdes lines and input table order
  tmp_lines_to_add rog_input_line_cell_crossing_order[];

  line_segments_id_add int[];


  -- running with 1e-07 we found this problem https://gitlab.com/nibioopensource/resolve-overlap-and-gap/-/issues/90
  add_border_snap_tolerance float = 0;


BEGIN

      -- We have to buffeer in or else ST_CoveredBy _bb can return lines that are paralell with the exterior line.
      -- Then we end up with line may make connection to a node out _bb and locks like this
      -- topo_arealregnskap_v04_001.node | relation | 1523458304 | 1908080705 | [NULL] | [NULL] | [NULL]     |        [NULL] |  [NULL] | [NULL] |   [NULL] | 31/283312          | 1710299 | ShareUpdateExclusiveLock | f       | f
      -- ..
      -- (24 rows)

      _done_add_all_lines = true;

      start_time =  Clock_timestamp();

      IF _bb IS NOT NULL THEN
        bb_buffer = ST_Buffer(_bb,-_this_topology_info_topology_snap_tolerance);
      END IF;




-- Here we try add lines closes any surfaces first and that li

      command_string := Format($fmt$
      WITH all_cell_crossing_borders AS (
      SELECT * FROM (
          SELECT DISTINCT ON (r.id) r.id,
          r.geom, r.table_input_order
          FROM %1$s r
          WHERE r.added_to_master = FALSE AND
          (%3$L IS NULL OR
          ST_CoveredBy(r.geom, %3$L))
        ) r
      ),
      coverby_by_existing_edges  AS (
      SELECT * FROM (
          SELECT DISTINCT ON (r.id) r.id,
          r.geom, r.table_input_order
          FROM %1$s r
          JOIN %2$s.edge_data e ON ST_Touches(ST_StartPoint(r.geom),e.geom) AND ST_Touches(ST_EndPoint(r.geom),e.geom) AND ST_MaxDistance(r.geom,e.geom) <= %4$s
          WHERE r.added_to_master = FALSE AND
          (%3$L IS NULL OR
          ST_CoveredBy(r.geom, %3$L))
        ) r
      ),
      cell_crossing_borders_to_add AS (
        SELECT * FROM all_cell_crossing_borders a WHERE NOT EXISTS (SELECT 1 FROM coverby_by_existing_edges e WHERE a.id = e.id)
      ),
      easy_to_add_lines AS (
        SELECT r.id, r.table_input_order, r.geom FROM (
          SELECT r.id, r.table_input_order, r.geom, ST_StartPoint(r.geom) start_point, ST_EndPoint(r.geom) end_point,
          CASE WHEN start_left_face = end_left_face THEN TRUE ELSE FALSE END AS left_face_eq,
          CASE WHEN start_left_face = end_left_face THEN TRUE ELSE FALSE END AS right_face_eq,
          start_id,
          end_id
          FROM (
            SELECT DISTINCT ON (r.id) r.id,
            r.geom, r.table_input_order,
            start_p.edge_id AS start_id, start_p.left_face AS start_left_face, start_p.right_face AS start_right_face,
            end_p.edge_id AS end_id, end_p.right_face AS end_left_face, end_p.right_face AS end_right_face
            FROM cell_crossing_borders_to_add r
            LEFT JOIN %2$s.edge_data start_p ON ST_Touches(ST_StartPoint(r.geom),start_p.geom) AND start_p.left_face = start_p.right_face
            LEFT JOIN %2$s.edge_data end_p ON ST_Touches(ST_EndPoint(r.geom),end_p.geom) AND end_p.left_face = end_p.right_face
            WHERE start_p.edge_id IS NOT NULL AND
            end_p.edge_id IS NOT NULL
          ) r
        ) r
      ),
      rest_off_lines AS (
        SELECT r.id, r.table_input_order, r.geom FROM (
          SELECT a.* FROM cell_crossing_borders_to_add a WHERE NOT EXISTS (SELECT 1 FROM easy_to_add_lines e WHERE a.id = e.id)
        ) r
      ),
      result AS (
        SELECT e.id, e.table_input_order, e.geom, 0 AS hard_to_add FROM easy_to_add_lines e
        UNION ALL
        SELECT h.id, h.table_input_order, h.geom, 1 AS hard_to_add FROM rest_off_lines h
      ),
      result_simple AS (
        SELECT r.id, r.table_input_order, r.geom
        FROM result r
        ORDER BY r.table_input_order,  ST_IsClosed(geom) desc, hard_to_add, ST_X(ST_StartPoint(r.geom)), ST_Y(ST_StartPoint(r.geom))
      )
      SELECT ARRAY_agg((r.id,r.geom,r.table_input_order))
      FROM result_simple r
    $fmt$,
    _table_name_result_prefix||'_border_line_segments_final',
    (_topology_info).topology_name,
    bb_buffer,
    _this_topology_info_topology_snap_tolerance
    ) r;

    --- RAISE NOTICE 'command_string % ', command_string;


    EXECUTE command_string into tmp_lines_to_add;

      total_num_border_crossing_line  = Array_length(tmp_lines_to_add, 1);

      -- create a sql to loop through all input lines and add them to then temp topology
      -- faling lines will be adde to lines_to_retry array
      command_string := Format('SELECT r.id, r.geom,r.table_input_order
      FROM (select DISTINCT id, geo AS geom, input_order AS table_input_order
      FROM unnest(%1$L::rog_input_line_cell_crossing_order[])) AS r',
      tmp_lines_to_add
      );



      start_time_delta_job := Clock_timestamp();


      FOR rec_new_line IN EXECUTE command_string LOOP
         -- Do analyze based on number of edges
        IF rec_new_counter = 0 THEN
          RAISE NOTICE '	 start to add %  topology % at % for bbox area % centroid % bb_buffer % ',
          total_num_border_crossing_line, (_topology_info).topology_name, Clock_timestamp(), ST_Area(bb_buffer,true), ST_Centroid(bb_buffer), bb_buffer;
        END IF ;


        -- RAISE NOTICE 'Try to add % at % .', rec_new_line.geom, Clock_timestamp();

        rog_added_border_line = topo_update.add_border_lines(
        (_topology_info).topology_name,
        rec_new_line.geom,
        (_clean_info).min_area_to_keep,
        0, -- there is no need splitt lines crossing borders are already splitt (_clean_info).split_input_lines_at_vertex_n, -- split all lines at given vertex num
        (_clean_info).min_st_is_empty_buffer_value,
        rec_new_line.table_input_order,
        ''::text, -- src_table_pk_column_value
        add_border_snap_tolerance,
        _table_name_result_prefix,
        FALSE,
        FALSE,
        (_topology_info).topology_is_utm,
        TRUE , -- _log_failed_line_error_table If true it will add new rows to _table_name_result_prefix || '_no_cut_line_failed';
        FALSE, -- _use_temp_edges_table_name_for_small_area boolean default FALSE, -- If true a local table name will be used
        TRUE, --_check_geeomtry_after_added_line boolean default TRUE, -- By default we will check for min area values and sliver value in topology created.
        _tables_ext_metainfo


        );

        --RAISE NOTICE 'Done add %, % at % .', rec_new_counter, rec_new_line.geom, Clock_timestamp();

        line_segments_id_add = line_segments_id_add||rec_new_line.id;

        -- Get lines to retry that failed
        IF (rog_added_border_line).failed_line IS NOT NULL THEN
          lines_to_retry = array_append(lines_to_retry,(rog_added_border_line).failed_line);
        ELSE

          rec_new_edges_since_anlyze = rec_new_edges_since_anlyze + Array_length((rog_added_border_line).ok_input_line_edges, 1);
          rec_new_edges_total = rec_new_edges_total + Array_length((rog_added_border_line).ok_input_line_edges, 1);

          lines_added = array_append(lines_added,rec_new_line.geom);

          set_added_to_master = set_added_to_master + 1;

        END IF;

        -- Get/Check numbers for running analyze
        rec_new_counter = rec_new_counter + 1;

        IF (rog_added_border_line).face_edges_to_remove IS NOT NULL THEN
          rec_new_edges_to_remove = rec_new_edges_to_remove + Array_length((rog_added_border_line).face_edges_to_remove, 1);
        END IF;


         -- Do analyze based on number of edges
        IF rec_new_edges_since_anlyze > rec_new_edges_analyze THEN
          used_time := (Extract(EPOCH FROM (Clock_timestamp() - start_time_delta_job)));
          IF used_time > (rec_new_edges_since_anlyze/2) THEN
            RAISE NOTICE 'In add_cellborder_lines, Used long time added border % lines in this loop  (total all loops %) (total to add %) used_time % secs. (edges created %) for % bbox area % centroid % .',
            rec_new_edges_since_anlyze, rec_new_counter, total_num_border_crossing_line, used_time, rec_new_edges_total, _topology_info.topology_name, ST_Area(bb_buffer,true), ST_Centroid(bb_buffer);

            -- TODO This should be moved to outer caller to avoid many threads doing this and we blocking calls
            -- I kust keep it for now since it does seem to casue any problems, but when testing it does not seem to increase performance
            -- I do run vaccum analyze i outer caller for each loop
            _done_add_all_lines = FALSE;

            EXECUTE Format($fmt$
            UPDATE %1$s f SET added_to_master = TRUE
            WHERE id = ANY(%2$L)
            $fmt$,
             _table_name_result_prefix||'_border_line_segments_final', --01
             line_segments_id_add
            );

            IF _return_if_long_execute_time THEN
              RETURN;
            END IF;
          ELSE
            RAISE NOTICE 'In add_cellborder_lines, added border % lines in this loop  (total all loops %) (total to add %) used_time % secs. (edges created %) for % bbox area % centroid % .',
            rec_new_edges_since_anlyze, rec_new_counter, total_num_border_crossing_line, used_time, rec_new_edges_total, _topology_info.topology_name, ST_Area(bb_buffer,true), ST_Centroid(bb_buffer);
          END IF;
          rec_new_edges_since_anlyze = 0;
          start_time_delta_job := Clock_timestamp();
        END IF;

    END LOOP;


    used_time := (Extract(EPOCH FROM (Clock_timestamp() - start_time)));

    RAISE NOTICE 'In add_cellborder_lines, done with %  using _this_topology_info_topology_snap_tolerance % (set added_master true %) number border lines for crossing grid lines  (number of failed lines %), number edges to remove %, phase 0 for topology % at % used_time: % for bb_buffer %',
    rec_new_counter,
    _this_topology_info_topology_snap_tolerance,
    set_added_to_master,
    Array_length(lines_to_retry, 1),
    (_topology_info).topology_name,
    rec_new_edges_to_remove,
    Clock_timestamp(),
    used_time,
    bb_buffer;

    -- Retry adding failed lines
    IF lines_to_retry IS NOT NULL AND (Array_length(lines_to_retry, 1)) > 0 THEN
    start_time_delta_job := Clock_timestamp();

      FOREACH tmp_line_to_retry IN ARRAY lines_to_retry
        LOOP

            start_time_delta_job := Clock_timestamp();

          --RAISE NOTICE 'In add_cellborder_lines, start try failed add_border_lines (%) first time, phase 0 for topology % at % line_length: % for bb_buffer %',
          --Array_length(lines_to_retry, 1),_topology_info.topology_name, Clock_timestamp(), ST_Length(tmp_line_to_retry.error_line,true) , bb_buffer;

          rog_added_border_line = topo_update.add_border_lines(
          (_topology_info).topology_name,
          tmp_line_to_retry.error_line,
          (_clean_info).min_area_to_keep,
          (_clean_info).split_input_lines_at_vertex_n, -- split all lines at given vertex num
          (_clean_info).min_st_is_empty_buffer_value,
          rec_new_line.table_input_order,
          ''::text, -- src_table_pk_column_value
          add_border_snap_tolerance,
          _table_name_result_prefix,
          TRUE,
          FALSE,
        (_topology_info).topology_is_utm,
        TRUE , -- _log_failed_line_error_table If true it will add new rows to _table_name_result_prefix || '_no_cut_line_failed';
        FALSE, -- _use_temp_edges_table_name_for_small_area boolean default FALSE, -- If true a local table name will be used
        TRUE, --_check_geeomtry_after_added_line boolean default TRUE, -- By default we will check for min area values and sliver value in topology created.
          _tables_ext_metainfo
          );

          rec_new_temp_edges = (rog_added_border_line).ok_input_line_edges;

          IF (rog_added_border_line).failed_line IS NULL THEN
            lines_added = array_append(lines_added,tmp_line_to_retry.error_line);
          END IF;

          used_time := (Extract(EPOCH FROM (Clock_timestamp() - start_time_delta_job)));

          --RAISE NOTICE 'In add_cellborder_lines, done failed add_border_lines (%) first time, phase 0 for topology % at % used_time: % for bb_buffer %',
          --Array_length(lines_to_retry, 1),_topology_info.topology_name, Clock_timestamp(), used_time, bb_buffer;

        END LOOP;


    END IF;


    -- update addded lines
    IF lines_added IS NOT NULL AND (Array_length(lines_added, 1)) > 0 THEN
      start_time_delta_job := Clock_timestamp();
      set_added_to_master = 0;

      FOREACH update_master_line IN ARRAY lines_added
        LOOP

          EXECUTE Format($fmt$
              UPDATE %1$s f SET added_to_master = TRUE
              WHERE added_to_master = FALSE
              AND (
                ST_Buffer(ST_Envelope(%2$L::geometry),%3$s) && f.geom
                AND
                ST_Distance(%2$L,f.geom,true) <= %3$s
              )
              AND (
                %4$L IS NULL
                OR
                ST_CoveredBy(f.geom, %4$L)
              )
            $fmt$,
          _table_name_result_prefix||'_border_line_segments_final', --01
          update_master_line,
          _this_topology_info_topology_snap_tolerance,
          bb_buffer
          );

          GET DIAGNOSTICS row_count = ROW_COUNT;

          set_added_to_master = set_added_to_master + row_count;

      END LOOP;

      used_time := (Extract(EPOCH FROM (Clock_timestamp() - start_time_delta_job)));
      RAISE NOTICE 'In add_cellborder_lines, set added_to_master (%) for topology % at % used_time: % for bb_buffer %',
      set_added_to_master ,_topology_info.topology_name, Clock_timestamp(), used_time, bb_buffer;

    END IF;

END;
$$;

