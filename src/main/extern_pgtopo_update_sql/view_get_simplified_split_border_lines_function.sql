/**

Cut lines at border and let Postgis set the edges to gether

This contains bugs

*/

CREATE OR REPLACE PROCEDURE topo_update.get_simplified_split_border_lines (
_tables_ext_metainfo rog_overlay_source_table_metainfo_type[], -- the set of input tables to use

_topology_info resolve_overlap_data_topology_type,
---(_topology_info).topology_name varchar, -- The topology schema name where we store store sufaces and lines from the simple feature dataset and th efinal result
-- NB. Any exting data will related to topology_name will be deleted
--(_topology_info).topology_srid
--(_topology_info).topology_snap_tolerance float, -- this is tolerance used as base when creating the the postgis topolayer
--(_topology_info).create_topology_attrbute_tables boolean -- if this is true and we value for line_table_name we create attribute tables refferances to
-- this tables will have atrbuttes equal to the simple feauture tables for lines and feautures

_clean_info resolve_overlap_data_clean_type, -- different parameters used if need to clean up your data
--(_clean_info)._min_area_to_keep float default 0, -- if this a polygon  is below this limit it will merge into a neighbour polygon. The area is sqare meter.
--(_clean_info).split_input_lines_at_vertex_n int, -- split all lines at given vertex num

-- if the evenlope of an input polygons is bigger than the content based grid cell used the code will break it up at the cell border.
--(_clean_info).break_up_big_polygons boolean,

--(_clean_info)._simplify_tolerance float default 0, -- is this is more than zero simply will called with
--(_clean_info)._simplify_max_average_vertex_length int default 0, -- in meter both for utm and deegrees, this used to avoid running ST_simplifyPreserveTopology for long lines lines with few points
--(_clean_info)._chaikins_nIterations int default 0, -- IF 0 NO CHAKINS WILL BE DONE,  A big value here make no sense because the number of points will increaes exponential )
--(_clean_info)._chaikins_max_length int default 0, --edge that are longer than this value will not be touched by _chaikins_min_degrees and _chaikins_max_degrees
--(_clean_info)._chaikins_min_degrees int default 0, -- The angle has to be less this given value, This is used to avoid to touch all angles.
--(_clean_info)._chaikins_max_degrees int default 0, -- OR the angle has to be greather than this given value, This is used to avoid to touch all angles
--(_clean_info)._chaikins_min_steep_angle_degrees int default 0, -- The angle has to be less this given value, This is used to avoid to touch all angles.
--(_clean_info)._chaikins_max_steep_angle_degrees int default 0-- OR The angle has to be greather than this given value, This is used to avoid to touch all angles

-- To limit and set as master area. This must be in the same coordsys as _new_sf_table_srid and must be polygon or a multipolgyon,
-- This will added as the first geometry into postgis topology. Data outside the master area will not be used.
-- If you send in a null geometry here this parameter will not have any effect.
--(_clean_info).input_boundary geometry

bb_boundary_geoms rog_bb_boundary_geoms,

-- cell/bix id
_cell_id int ,
_table_name_result_prefix varchar, -- The topology schema name where we store store result sufaces and lines from the simple feature dataset,
INOUT res rog_get_simplified_split_border_lines_result
)
LANGUAGE plpgsql
AS $$
DECLARE
  command_string text;

  tmp_table_name varchar = 'tmp_data_all_lines' || Md5(ST_AsBinary ((bb_boundary_geoms).input_bb));
  tmp_table_name_raw varchar = tmp_table_name||'_raw';



  -- this the name the geometry column in the content based grid to be used
  -- this has to be the same and not depend on columns name of the input
  -- TODO this should input config settings
  cell_geo_column_name text = 'cell_bbox';

  -- this is master input area from (_clean_info).input_boundary if (_clean_info).input_boundary IS NOT NULL;
  -- input_boundary_boandary_area IS NULL all area is used
  input_boundary_boandary_area geometry;

  -- holdes lines and input table order
  tmp_lines_to_add rog_input_line_order[];

  -- holdes lines that are valid and input table order
  -- this lines needs to be handle in speiceial way
  tmp_invalid_lines_to_add rog_input_line_order[];

  -- holdes the point not be toched any operation before all lines added to final toplogy
  tmp_fixed_point_set geometry;

  num_rows int;

  -- Array of spatially equal rows to delete
  ctid_delete_list tid[];

  grid_snap_tolerance float = 0;
  input_boundary_box3d box3d ;

BEGIN


  IF (_clean_info).input_boundary IS NOT NULL THEN
    --grid_snap_tolerance = 1e-15; -- tests are not affted, but we get to manuy errors
    grid_snap_tolerance = 1e-08; -- one test is affected but the results are much better
    input_boundary_box3d = ST_3DExtent((_clean_info).input_boundary);
  END IF;

  -- insert debug info about  cell
  /**

  NOT needed but may a flag for this

  EXECUTE Format('INSERT INTO %1$s (_cell_id,log_key,geo) VALUES(%2$s,%3$L,%4$L)',
  _table_name_result_prefix||'_grid_log_info',
  _cell_id,
  '(bb_boundary_geoms).boundary_geom',
  (bb_boundary_geoms).boundary_geom);

  EXECUTE Format('INSERT INTO %1$s (_cell_id,log_key,geo) VALUES(%2$s,%3$L,%4$L)',
  _table_name_result_prefix||'_grid_log_info',
  _cell_id,
  '(bb_boundary_geoms).inner_boundary_geom',
  (bb_boundary_geoms).inner_boundary_geom);

  */

  RAISE NOTICE 'enter topo_update.get_simplified_split_border_lines with (bb_boundary_geoms).input_bb % ',
  ST_AsText((bb_boundary_geoms).input_bb);

  -- Create a temp table with all rows based on (bb_boundary_geoms).input_bb
  command_string := Format(
     $fmt$
     CREATE TEMP TABLE %1$s ON COMMIT DROP AS WITH
     --CREATE TABLE %1$s AS WITH
     lines_from_cell AS ( -- Get all polygons with holes as linestrings
        SELECT * FROM (
         SELECT
         ST_ExteriorRing((ST_DumpRings((st_dump(geo)).geom)).geom) AS geom,
         r.table_input_order,
         r.src_table_pk_column_value,
         r.input_geo_is_valid
         FROM topo_rog_static.rog_create_read_data_function(
         %2$L::rog_overlay_source_table_metainfo_type[],
         %3$L::geometry,
         %4$s,
         %5$L::boolean, -- if the evenlope of an input polygons is bigger than the content based grid cell used the code will break it up at the cell border.
         true, -- IF true ST_Validate will done and the result will saved in input_geo_is_valid
         true, -- IF true ST_MakeValid will done and the result will be used and column geo_tried_fix_invalid will be set to true.
         true -- IF true ST_MakeValid will done and the result will be used and column geo_tried_fix_invalid will be set to true.
          ) r
        ) as v
     )
     SELECT
     CASE WHEN %7$s = 0 THEN geom
     ELSE ST_SnapToGrid(geom, ST_XMin(%6$L), ST_YMin(%6$L), %7$s, %7$s)
     END AS geom ,
     table_input_order,src_table_pk_column_value,input_geo_is_valid FROM lines_from_cell
     $fmt$,
     tmp_table_name_raw, -- 01
     _tables_ext_metainfo, --02
     (bb_boundary_geoms).input_bb, --03
     (_topology_info).topology_srid, --04
     (_clean_info).break_up_big_polygons, --05 if the evenlope of an input polygons is bigger than the content based grid cell used the code will break it up at the cell border.
     input_boundary_box3d,
     grid_snap_tolerance

  );
  EXECUTE command_string;

  GET DIAGNOSTICS num_rows = ROW_COUNT;
  RAISE NOTICE 'topo_update.get_simplified_split_border_lines with (bb_boundary_geoms).input_bb created table % and num_rows %',
  tmp_table_name_raw,
  num_rows;


--  RAISE NOTICE 'topo_update.get_simplified_split_border_lines with (bb_boundary_geoms).input_bb created table % and (bb_boundary_geoms).input_bb %',
--  tmp_table_name_raw,
--  (bb_boundary_geoms).input_bb;


  IF num_rows = 0 THEN
    RETURN;
  END IF;

  -- Create a spatial index
  command_string := Format(
    $fmt$
    CREATE INDEX ON %1$s USING gist (geom)
    $fmt$,
    tmp_table_name_raw
  );
  EXECUTE command_string;

  -- Find spatilay equal rows
  /**
  command_string := Format(
    $fmt$
    SELECT ARRAY_AGG(DISTINCT ctid_to_remove) FROM (
      SELECT
      CASE WHEN t1.ctid > t2.ctid THEN t1.ctid
      ELSE NULL
      END AS ctid_to_remove
      FROM
      %1$s t1,
      %1$s t2
      WHERE t1.ctid != t2.ctid
      AND t1.geom && t2.geom
      AND ST_Equals(t1.geom,t2.geom)
    ) r WHERE ctid_to_remove IS NOT NULL
    $fmt$,
    tmp_table_name_raw
  );
  EXECUTE command_string INTO ctid_delete_list;


  IF ctid_delete_list IS NOT NULL THEN
    RAISE NOTICE 'topo_update.get_simplified_split_border_lines with (bb_boundary_geoms).input_bb created table % and found duplicate spatial % rows of num_rows % ',
    tmp_table_name_raw,
    ARRAY_LENGTH(ctid_delete_list,1),
    num_rows;
  END IF;
  */

  -- Split input lines based boandry and inner polygon
  command_string := Format(
     $fmt$
     CREATE TEMP TABLE %01$s ON COMMIT DROP AS WITH
     --CREATE TABLE %01$s AS WITH
      lines_checked_input_boundary AS (
        -- Get all polygons with holes as linestrings
        -- If with have global intersection area (_clean_info).input_boundary, do intersection againt area also
        -- Also add (_clean_info).input_boundary as a line to be sure that they are used
        SELECT * FROM (
         SELECT
         CASE WHEN %02$L IS NULL THEN l.geom
         ELSE ST_Intersection(%02$L,l.geom)
         END AS geom,
         l.table_input_order,
         l.src_table_pk_column_value,
         l.input_geo_is_valid
         FROM %03$s l
         WHERE %06$L IS NULL OR NOT (l.ctid = ANY (%06$L))
         UNION
         SELECT
         ST_ExteriorRing((ST_DumpRings((st_dump(%02$L)).geom)).geom),
         0 AS table_input_order,
         '' AS src_table_pk_column_value,
         TRUE AS input_geo_is_valid
        ) as v
      ),
      line_parts_intersects_cell_wall AS (
        SELECT
        (ST_Dump(ST_Multi(ST_Intersection(la1.geom,%04$L)))).geom,
        la1.table_input_order,
        la1.src_table_pk_column_value,
        la1.input_geo_is_valid,
        FALSE AS inner_line
        FROM
        lines_checked_input_boundary la1 WHERE ST_Intersects(la1.geom,%04$L)
      ),
      line_parts_inside_cell_wall AS (
        SELECT
        (ST_Dump(ST_Multi(ST_Intersection(la2.geom,%05$L)))).geom,
        la2.table_input_order,
        la2.src_table_pk_column_value,
        la2.input_geo_is_valid,
        TRUE AS inner_line
        FROM
        lines_checked_input_boundary la2 WHERE ST_Intersects(la2.geom,%05$L)
      ),
      line_parts AS (
        SELECT * FROM line_parts_intersects_cell_wall
        UNION ALL
        SELECT * FROM line_parts_inside_cell_wall
      )
      SELECT
      geom,
      table_input_order,
      src_table_pk_column_value,
      input_geo_is_valid,
      inner_line
      FROM line_parts
     $fmt$,
     tmp_table_name, -- 01
     (_clean_info).input_boundary, --02
     tmp_table_name_raw, --03
     (bb_boundary_geoms).boundary_geom, --04
     (bb_boundary_geoms).inner_boundary_geom, --05
     ctid_delete_list
     );

     EXECUTE command_string;



     command_string := Format('create index on %2$s using gist(geom)',
      'idxtmp_data_all_lines_geom_temp'|| Md5(ST_AsBinary ((bb_boundary_geoms).input_bb)),
      tmp_table_name);
     EXECUTE command_string;





    -- log other_geom that are not lines for instance a points after st_intersection
    -- this data will not be used any more, just logged to keep track off
    EXECUTE Format('WITH error_lines AS
    (DELETE FROM %4$s r WHERE ST_GeometryType(r.geom) != %5$L RETURNING geom)
    INSERT INTO %2$s (error_info, other_geom)
    SELECT %3$L AS error_info, r.geom as other_geom
    FROM error_lines r',
    _cell_id, --01
    _table_name_result_prefix||'_no_cut_line_failed', --02
    'This is a not line, so we can not use it', ---03
    tmp_table_name, --04
    'ST_LineString' --05
    );

    -- This is lines that are not valid so we will track of in a separate list
    -- We also add to log table for failed lines but NOT with status lost, because the caller will do a retry.
    command_string = Format('WITH error_lines AS
        (DELETE FROM %4$s r where ST_IsValid(r.geom) = false RETURNING r.*),
        insert_error_lines AS (INSERT INTO %2$s (line_geo_lost, error_info, table_input_order, input_geo_is_valid, geo)
        SELECT FALSE AS line_geo_lost, %3$L AS error_info, r.table_input_order, r.input_geo_is_valid, r.geom as geo
        FROM error_lines r
        RETURNING geo, table_input_order)
        SELECT ARRAY_agg((l.geo,l.table_input_order)) FROM insert_error_lines l
        ',
        _cell_id,
        _table_name_result_prefix||'_no_cut_line_failed',
        'Failed to make valid input border from input for tmp_boundary_lines_merged',
       tmp_table_name );

    EXECUTE command_string INTO tmp_invalid_lines_to_add;

    -- We just add this for logging purpose and so check at the final result how this affected the values for the surfaces
    EXECUTE Format('WITH problem_lines AS
        (SELECT r.* FROM %4$s r WHERE r.input_geo_is_valid = FALSE )
        INSERT INTO %2$s (line_geo_lost, error_info, table_input_order, input_geo_is_valid, geo)
        SELECT FALSE AS line_geo_lost, %3$L AS error_info, r.table_input_order, r.input_geo_is_valid, r.geom as geo
        FROM problem_lines r',
        _cell_id,
        _table_name_result_prefix||'_no_cut_line_failed',
        'Input border Is not valid, but this line part is valid so it will be added ',
    tmp_table_name );

    --EXECUTE Format('create table %1$s AS table %2$s', tmp_table_name||'_all_lines',tmp_table_name);

    -- make line part for outer box, that contains the line parts will be added add the final stage when all the cell are done.
    command_string := Format(
    $fmt$
      WITH border_lines_deleted AS  (
        DELETE FROM %3$s r where inner_line = FALSE
        RETURNING geom, table_input_order, src_table_pk_column_value)
      ,
      border_lines_inserted AS (
        INSERT INTO %1$s (geom, table_input_order,src_table_pk_column_value)
        SELECT distinct (ST_dump(geom)).geom AS geom, table_input_order, src_table_pk_column_value
        FROM border_lines_deleted
        RETURNING ST_Union(ST_StartPoint(geom), ST_EndPoint(geom)) AS points
      )
      SELECT ST_Union(points) FROM border_lines_inserted


    $fmt$,
    _table_name_result_prefix||'_border_line_segments_short',
    (bb_boundary_geoms).boundary_geom,
    tmp_table_name
    );

    EXECUTE command_string INTO tmp_fixed_point_set;

    -- RAISE NOTICE 'done with command_string%s  and int fixed_point_set %s' , command_string, tmp_fixed_point_set;

    -- make the result of inner geos to handled imediatly
    command_string := Format('SELECT
      ARRAY_agg((l.geom,l.table_input_order,l.src_table_pk_column_value))
      FROM %1$s l ',
      tmp_table_name
      );
    EXECUTE command_string into tmp_lines_to_add;

  RAISE NOTICE 'topo_update.get_simplified_split_border_lines with (bb_boundary_geoms).input_bb created table % and tmp_fixed_point_set size %',
  tmp_table_name_raw,
  ST_NPoints(tmp_fixed_point_set);

    -- make the final return result
    res = (tmp_lines_to_add,tmp_invalid_lines_to_add,tmp_fixed_point_set);

END
$$;

