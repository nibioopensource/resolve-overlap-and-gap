CREATE OR REPLACE FUNCTION topo_update.lock_area(
	_topology_name TEXT,
	_lock_table_name TEXT,
	_cell_geo_column_name TEXT,
	_bbox_area_to_lock GEOMETRY,
	_max_lock_try int -- If max lock try is one only test time and return if there if there are other working the area
)
RETURNS BOOLEAN AS
$BODY$
DECLARE
	num_rows_intersecting int := 0;
	error_text text;

	counter int = 0;
	max_counter int = 100;
	grid_id_list int[];
	id_to_lock int;
	grid_id_list_locked int[] = '{}';

		topology_id int = 0;
	my_pid int;
	pg_advisory_lock_status boolean;

	lock_try_loop_sleep_time int = 1; -- this is only used if _max_lock_try is bigger than one


	lock_result BOOLEAN := false;
	pg_advisory_unlock_status BOOLEAN;
	sql TEXT;

BEGIN

	SELECT pg_backend_pid() INTO my_pid;

		sql := format(
			$$
				SELECT ARRAY(SELECT gr.id
			FROM %1$s gr
			WHERE ST_Intersects(gr.%2$s,%3$L) AND ST_area(ST_Intersection(gr.%2$s,%3$L),true) > 0.0)
			$$,
			_lock_table_name,
			_cell_geo_column_name,
			_bbox_area_to_lock
		);

	EXECUTE sql INTO grid_id_list;

		sql := format(
			$$
			SELECT id FROM topology.topology
			WHERE name = %1$L
			$$,
			_topology_name
		);
		EXECUTE sql INTO topology_id;

--	RAISE NOTICE 'start lock_area for topology_id %  and % ids in grid_id_list % needed for bbox % for pid % ',
--	topology_id, array_length(grid_id_list,1), grid_id_list, _bbox_area_to_lock, my_pid;

	-- DEFAULT GRID ID is 0 (make global value for default value)
	IF array_length(grid_id_list, 1) IS NULL THEN
		grid_id_list = '{0}';
	END IF;





	FOREACH id_to_lock IN ARRAY grid_id_list
	LOOP
		counter = 1;

		WHILE counter <= _max_lock_try loop
			--this maybe cause some random issue about what thread that wins that may change the result of the tests but I don understand way ???
			--like here https://gitlab.com/nibioopensource/pgtopo_update_sql/-/jobs/3164377299
			--The pipe line failing if not using locks so mI thinks it's random issuse

			--We also have the same error in this run here but here we use dblink and not advisory locks
			--https://gitlab.com/nibioopensource/pgtopo_update_sql/-/jobs/3105132270



--			RAISE NOTICE 'CHECK/WAITS FOR LOCK for id_to_lock % in loop % of max % loops ', id_to_lock, counter, _max_lock_try;

			-- Just to test to use pg_advisory_lock instead off pg_try_advisory_lock and then not give up and this loop always run only one time.
			-- EXECUTE pg_advisory_lock(topology_id,id_to_lock);
			-- When we get here we know that have a valid lock.
			-- pg_advisory_lock_status = true;
			-- pg_advisory_lock can not be used

			SELECT pg_try_advisory_lock(topology_id,id_to_lock) INTO pg_advisory_lock_status;

			-- If true exit
			IF pg_advisory_lock_status THEN
				EXIT;
			END IF;

			-- update loop counter
			counter = counter + 1;
			execute pg_sleep(lock_try_loop_sleep_time);
		END loop;

		-- if not a valid lock do not check other and exit main loop
		IF (pg_advisory_lock_status = FALSE) THEN
			lock_result = false;
			EXIT;
		ELSE
			lock_result = true;
			grid_id_list_locked = array_append(grid_id_list_locked, id_to_lock);
		END IF;
	END LOOP;

	IF lock_result = false THEN
		FOREACH id_to_lock IN ARRAY grid_id_list_locked
		LOOP
			SELECT pg_advisory_unlock(topology_id,id_to_lock) INTO pg_advisory_unlock_status;
		END LOOP;
		RAISE NOTICE 'No lock for for topology_id % and id_to_lock %, is % done lock_area % locks for grid_id_list % needed for bbox % for pid % with _max_lock_try %',
		topology_id,id_to_lock,lock_result, counter, grid_id_list, _bbox_area_to_lock, my_pid, _max_lock_try;
--	ELSE
--		RAISE NOTICE 'OK status for topology_id % and id_to_lock %, is % done lock_area % locks for grid_id_list % needed for bbox % for pid % with _max_lock_try % ',
--		topology_id,id_to_lock,lock_result, counter, grid_id_list, _bbox_area_to_lock, my_pid, _max_lock_try;
	END IF;



	RETURN lock_result;



END;
$BODY$ LANGUAGE 'plpgsql'; --}


CREATE OR REPLACE FUNCTION topo_update.release_lock_area(
	_topology_name TEXT,
	_lock_table_name TEXT,
	_cell_geo_column_name TEXT,
	_bbox_area_to_release geometry
)
RETURNS void AS
$BODY$
DECLARE
	num_rows_intersecting int := 0;
	error_text text;

	counter int = 0;
	max_counter int = 100;
	grid_id_list int[];
	id_to_release int;
	pg_advisory_unlock_status boolean = false;

		topology_id int = 0;
	my_pid int;
	num_pid_locks int;
	sql TEXT;


BEGIN

	select pg_backend_pid() INTO my_pid;

		sql := format(
			$$
				SELECT ARRAY(SELECT gr.id
			FROM %1$s gr
			WHERE ST_Intersects(gr.%2$s,%3$L) AND ST_area(ST_Intersection(gr.%2$s,%3$L),true) > 0.0)
			$$,
			_lock_table_name,
			_cell_geo_column_name,
			_bbox_area_to_release

		);

	EXECUTE sql INTO grid_id_list;

		sql := format(
			$$
			SELECT id FROM topology.topology
			WHERE name = %1$L
			$$,
			_topology_name
		);
		EXECUTE sql INTO topology_id;


	-- DEFAULT GRID ID is 0 (make global value for default value)
	IF array_length(grid_id_list, 1) IS NULL THEN
		grid_id_list = '{0}';
--	ELSE
--		RAISE NOTICE 'start release_lock_area for topology_id %  and for % ids grid_id_list % needed for bbox % for pid % ',
--		topology_id, array_length(grid_id_list,1), grid_id_list, _bbox_area_to_release, my_pid;
	END IF;


	FOREACH id_to_release IN ARRAY grid_id_list
	LOOP

		num_pid_locks = 1;

		while num_pid_locks > 0 loop

			SELECT pg_advisory_unlock(topology_id,id_to_release) INTO pg_advisory_unlock_status;
			IF pg_advisory_unlock_status THEN
				counter = counter + 1;
			END IF;

			--perform pg_sleep(1);

			SELECT count(*)
			FROM  pg_locks
			WHERE pid = my_pid AND objid = id_to_release AND granted = true AND locktype = 'advisory'
			INTO num_pid_locks;


		end loop;

	END LOOP;


--	RAISE NOTICE 'done release_lock_area for topology_id % and id_to_release %, for % locks for grid_id_list % needed for bbox % for pid % ',
---	topology_id, id_to_release, counter, grid_id_list, _bbox_area_to_release, my_pid;




END;
$BODY$ LANGUAGE 'plpgsql'; --}
