DO $BODY$
DECLARE
  sp text;
  sql text;
BEGIN
  SELECT reset_val
  INTO sp
  FROM pg_catalog.pg_settings
  WHERE name = 'search_path';

  IF sp NOT LIKE '%topo_rog_static%' THEN
  RAISE NOTICE 'Adding topo_rog_static to search_path';
    sp := sp || ', topo_rog_static';
    sql := format('SET search_path = %s', sp);
    EXECUTE format(
      'ALTER DATABASE %I %s',
      pg_catalog.current_database(),
      sql
    );
    EXECUTE sql;
  END IF;

  SELECT reset_val
  INTO sp
  FROM pg_catalog.pg_settings
  WHERE name = 'search_path';

  IF sp NOT LIKE '%topology%' THEN
  RAISE NOTICE 'Adding topo_rog_static to search_path';
    sp := sp || ', topology';
    sql := format('SET search_path = %s', sp);
    EXECUTE format(
      'ALTER DATABASE %I %s',
      pg_catalog.current_database(),
      sql
    );
    EXECUTE sql;
  END IF;

END
$BODY$ LANGUAGE 'plpgsql';
