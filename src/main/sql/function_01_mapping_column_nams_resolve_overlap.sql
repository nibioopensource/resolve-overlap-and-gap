/**

This is a set of functions used decide names of columns based alias input values and _topology_result_schema_name

*/

-- Compute alias for table name
CREATE OR REPLACE FUNCTION ros_table_metainfo_pk_list_name(_rog_source_table_metainfo_type rog_overlay_source_table_metainfo_type)
RETURNS text
  AS $$
DECLARE
  name text;
BEGIN

  IF _rog_source_table_metainfo_type.table_selected_alias IS NULL THEN
  	name=_rog_source_table_metainfo_type.table_input_order||'-pk_list-'||_rog_source_table_metainfo_type.table_pk_colum||'-'||_rog_source_table_metainfo_type.table_name;
  ELSE
  	name=_rog_source_table_metainfo_type.table_selected_alias||'__other_pks';
  END IF;
  return name;
END;
$$
LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION ros_table_metainfo_pk_name(_rog_source_table_metainfo_type rog_overlay_source_table_metainfo_type)
RETURNS text
  AS $$
DECLARE
  name text;
BEGIN
  IF _rog_source_table_metainfo_type.table_selected_alias IS NULL THEN
    name=_rog_source_table_metainfo_type.table_input_order||'-pk-'||_rog_source_table_metainfo_type.table_pk_colum||'-'||_rog_source_table_metainfo_type.table_name;
  ELSE
  	name=_rog_source_table_metainfo_type.table_selected_alias||'__selected_pk';
  END IF;

  return name;
END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION ros_table_metainfo_column_prefix(_rog_source_table_metainfo_type rog_overlay_source_table_metainfo_type)
RETURNS text
  AS $$
DECLARE
  name text;
BEGIN
  IF _rog_source_table_metainfo_type.table_selected_alias IS NULL THEN
    name='t'||TO_CHAR(_rog_source_table_metainfo_type.table_input_order, 'fm00');
  ELSE
  	name=_rog_source_table_metainfo_type.table_selected_alias;
  END IF;

  return name;
END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION ros_view_sf_result_schema_name(
_topology_result_schema_name varchar -- the result schema and name from input, maybe only a schema or a name
)
RETURNS text
  AS $$
DECLARE
  pos integer;
  schema_name text;

BEGIN
  -- find table name including schema
  pos = position ('.' IN _topology_result_schema_name);
  IF pos > 1 THEN
    schema_name := split_part(_topology_result_schema_name, '.', 1);
  ELSE
    schema_name = _topology_result_schema_name;
  END IF;

  return schema_name;
END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION ros_view_sf_result_topo_name(
_topology_result_schema_name varchar, -- the result schema and name from input, maybe only a schema or a name
_tmp_rog_input_boundary rog_input_boundary -- info the boundary, since we split topos in smaller topos
)
RETURNS text
  AS $$
DECLARE
  topology_result_schema_name text;
  pos integer;
  schema_name text;

BEGIN
  -- find table name including schema
  pos = position ('.' IN _topology_result_schema_name);
  IF pos > 1 THEN
    schema_name := split_part(_topology_result_schema_name, '.', 1);
  ELSE
    schema_name = _topology_result_schema_name;
  END IF;

  topology_result_schema_name = schema_name||'_'||(_tmp_rog_input_boundary).name;

  return topology_result_schema_name;
END;
$$
LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION ros_view_sf_result_base_name(
_topology_result_schema_name varchar, -- the result schema and name from input, maybe only a schema or a name
_input_tables text[] -- List of table with including schema name on this format ARRAY['sk_grl.n5_forv_fylke2021_flate','sk_grl.n2000_komm_flate']
)
RETURNS text
  AS $$
DECLARE
BEGIN
  RETURN ros_view_sf_result_base_name(_topology_result_schema_name,(ARRAY_LENGTH(_input_tables,1)));
END;
$$
LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION ros_view_sf_result_base_name(
_topology_result_schema_name varchar, -- the result schema and name from input, maybe only a schema or a name
num_input_tables int -- List of table with including schema name on this format ARRAY['sk_grl.n5_forv_fylke2021_flate','sk_grl.n2000_komm_flate']
)
RETURNS text
  AS $$
DECLARE
  name text;
  pos integer;
BEGIN

  pos = position ('.' IN _topology_result_schema_name);
  IF pos > 1 THEN
    name := split_part(_topology_result_schema_name, '.', 2);
  ELSE
    name := 'multi_input_'||num_input_tables;
  END IF;


  return name;
END;
$$
LANGUAGE plpgsql;





CREATE OR REPLACE FUNCTION ros_table_metainfo_gaps_name(_rog_source_table_metainfo_type rog_overlay_source_table_metainfo_type)
RETURNS text
  AS $$
DECLARE
  name text;
BEGIN
  IF _rog_source_table_metainfo_type.table_selected_alias IS NULL THEN
    name=ros_table_metainfo_column_prefix(_rog_source_table_metainfo_type)||'_gaps'||'_'||_rog_source_table_metainfo_type.table_name;
  ELSE
  	name=_rog_source_table_metainfo_type.table_selected_alias||'__gaps';
  END IF;

  return name;
END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION ros_table_metainfo_overlaps_name(_rog_source_table_metainfo_type rog_overlay_source_table_metainfo_type)
RETURNS text
  AS $$
DECLARE
  name text;
BEGIN
  IF _rog_source_table_metainfo_type.table_selected_alias IS NULL THEN
    name=ros_table_metainfo_column_prefix(_rog_source_table_metainfo_type)||'_overlaps'||'_'||_rog_source_table_metainfo_type.table_name;
  ELSE
  	name=_rog_source_table_metainfo_type.table_selected_alias||'__overlaps';
  END IF;

  return name;
END;
$$
LANGUAGE plpgsql;
