/**

Here we add functions used to map from one datatype to another

*/

/**

A mapper frunction from resolve_overlap_data_input_type[] to rog_overlay_source_table_metainfo_type[]

*/
CREATE OR REPLACE FUNCTION rog_map_2_rog_overlay_source_table_metainfo_type(
_input_data_array resolve_overlap_data_input_type[]
)
-- We may need to reformat the return data
RETURNS rog_overlay_source_table_metainfo_type[]
AS $func$
DECLARE

  tables_ext_metainfo rog_overlay_source_table_metainfo_type[];
  this_table_input_order smallint = 0;
  tmp_rdit resolve_overlap_data_input_type;
  tmp_ext_metainfo rog_overlay_source_table_metainfo_type;

BEGIN


  -- Create data structur that can be used for rog_create_read_data_function
  FOREACH tmp_rdit IN ARRAY _input_data_array
  LOOP
    -- a counter to be used a ref neeed to access to all tabe
       this_table_input_order = this_table_input_order + 1;
         tmp_ext_metainfo.table_input_order = this_table_input_order;

    -- The table to resolv, imcluding schema name
         tmp_ext_metainfo.table_name = (tmp_rdit).polygon_table_name;

         tmp_ext_metainfo.table_selected_alias = (tmp_rdit).polygon_table_alias;


    --The primary of the input table
         tmp_ext_metainfo.table_pk_colum = (tmp_rdit).polygon_table_pk_column;

       -- find primary key type is missing, because this is not part of resolve_overlap_data_input_type
       tmp_ext_metainfo.table_pk_type = vsr_get_data_type(tmp_ext_metainfo.table_name,tmp_ext_metainfo.table_pk_colum);

    -- the name of geometry column on the table to analyze
         tmp_ext_metainfo.table_geo_column = (tmp_rdit).polygon_table_geo_collumn;

    -- the srid for the given geo column on the table analyze
    tmp_ext_metainfo.table_geo_srid = (tmp_rdit).table_srid;

    -- What kind geometry column this is may not used for now in rog_create_read_data_function
         -- tmp_ext_metainfo.table_geo_type = missing in tmp_rdit

         -- if false use_spheroid=true will true when compting areas and so os
         tmp_ext_metainfo.table_geo_is_utm = (tmp_rdit).table_is_utm;


    tables_ext_metainfo = tables_ext_metainfo||tmp_ext_metainfo;


  END LOOP;

  RETURN tables_ext_metainfo;
END;
$func$ LANGUAGE 'plpgsql';

/**

A mapper frunction  rog_overlay_source_table_metainfo_type[] to resolve_overlap_data_input_type[] to

*/
CREATE OR REPLACE FUNCTION rog_map_2_resolve_overlap_data_input_type(
_input_data rog_overlay_source_table_metainfo_type[]
)
-- We may need to reformat the return data
RETURNS resolve_overlap_data_input_type[]
AS $func$
DECLARE

  tables_out resolve_overlap_data_input_type[];
  this_table_input_order smallint = 0;
  tmp_ext_metainfo rog_overlay_source_table_metainfo_type;
  tmp_out resolve_overlap_data_input_type;

BEGIN


  -- Create data structur that can be used for rog_create_read_data_function
  FOREACH tmp_ext_metainfo IN ARRAY _input_data
  LOOP

    -- The table to resolv, imcluding schema name
         tmp_out.polygon_table_name = (tmp_ext_metainfo).table_name;

   -- The user selted alias for table name
         tmp_out.polygon_table_alias = (tmp_ext_metainfo).table_selected_alias;


    --The primary of the input table
          tmp_out.polygon_table_pk_column = (tmp_ext_metainfo).table_pk_colum;

       -- this is not part of resolve_overlap_data_input_type
       -- (tmp_ext_metainfo).table_pk_type

    -- the name of geometry column on the table to analyze
         tmp_out.polygon_table_geo_collumn = (tmp_ext_metainfo).table_geo_column;

    -- the srid for the given geo column on the table analyze
     tmp_out.table_srid = (tmp_ext_metainfo).table_geo_srid;

    -- What kind geometry column this is may not used for now in rog_create_read_data_function
         --missing in tmp_out = (tmp_ext_metainfo).table_geo_type;

         -- if false use_spheroid=true will true when compting areas and so os
         tmp_out.table_is_utm = (tmp_ext_metainfo).table_geo_is_utm;

    tables_out = tables_out||tmp_out;


  END LOOP;

  RETURN tables_out;
END;
$func$ LANGUAGE 'plpgsql';


