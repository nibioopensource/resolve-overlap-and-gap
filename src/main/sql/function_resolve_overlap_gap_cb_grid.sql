/**

A function used to create

*/

CREATE OR REPLACE FUNCTION resolve_overlap_gap_cb_grid (

-- grid table name
_overlapgap_grid text,

-- this is the max number rows that intersects with box before it's split into 4 new boxes
_max_rows_in_each_cell int,

 -- A new type with more info abouth input tables, to replace _input_data_array resolve_overlap_data_input_type[] in the future
_tables_ext_metainfo rog_overlay_source_table_metainfo_type[],

-- The srid to be used for tables result
_new_sf_table_srid int,

-- column name grid cells to use
_cell_geo_column_name text,

-- this is master input area from (_clean_info).input_boundary if (_clean_info).input_boundary IS NOT NULL;
-- _input_boundary_boandary_area IS NULL all area is used
_input_boundary_boandary_area geometry
)
  RETURNS INTEGER
  AS $$
DECLARE
  -- used to run commands
  command_string text;
  -- the number of cells created in the grid
  num_cells_master_grid int;
  -- drop result tables

  -- Used to create content based grid
  cbg_tables text[];

  tmp_ext_metainfo rog_overlay_source_table_metainfo_type;

BEGIN


  -- for each craete a primary key value column tables_ext_metainfo
  FOREACH tmp_ext_metainfo IN ARRAY _tables_ext_metainfo
  LOOP
    command_string = FORMAT('%1$s %2$s',
    tmp_ext_metainfo.table_name,
    tmp_ext_metainfo.table_geo_column
    );
  cbg_tables=ARRAY_APPEND(cbg_tables,command_string);
  END LOOP;

  -- create a content based grid table for input data
  -- ----------------------------- STARt - Handle content based grid init


    RAISE NOTICE 'Create content based grid % ', _overlapgap_grid;

    -- create master table for content based grid
    EXECUTE Format('CREATE TABLE %s( id serial,
    numtables_intersects int, -- number of tables intersecting this cell
    numrows_intersects int, -- number of rows for all tables intersecting this cell
    has_cell_minsize boolean, -- if this cell is reached min size of cells
     %s geometry(Geometry,%s))',
    _overlapgap_grid, _cell_geo_column_name, _new_sf_table_srid);

	--SELECT (r.cell_row).cell_geom AS geo FROM (SELECT unnest(cbg_content_based_balanced_grid) cell_row from cbg_content_based_balanced_grid(ARRAY['public.test_table_01 geom'],20)) r;

    -- insert data data in to master grid
    command_string := Format('INSERT INTO %1$s(numtables_intersects,numrows_intersects,has_cell_minsize,%5$s)
    SELECT DISTINCT r.numtables_intersects, r.numrows_intersects, r.has_cell_minsize, r.%5$s FROM
     (
     SELECT q_grid.numtables_intersects, q_grid.numrows_intersects, q_grid.has_cell_minsize, ST_transform(q_grid.cell,%2$s)::Geometry(geometry,%2$s) as %5$s
       FROM (
         SELECT
         (r.cell_row).numtables_intersects,
         (r.cell_row).numrows_intersects,
         (r.cell_row).has_cell_minsize,
         (r.cell_row).cell_geom AS cell
         FROM (SELECT unnest(cbg_content_based_balanced_grid) cell_row from cbg_content_based_balanced_grid(%3$L,%4$s,%6$L)) r
       ) as q_grid
     ) r
     WHERE %6$L IS NULL OR (%6$L && r.%5$s AND ST_Area(ST_Intersection(%6$L,r.%5$s)) > 0.0)',
     _overlapgap_grid, --01
     _new_sf_table_srid,  -- 02
     cbg_tables, --03
     _max_rows_in_each_cell, --04
     _cell_geo_column_name, --05
     _input_boundary_boandary_area --06
     );
    -- execute the sql command
    EXECUTE command_string;

    -- count number of cells in grid
    command_string := Format('SELECT count(*) from %s', _overlapgap_grid);
    -- execute the sql command
    EXECUTE command_string INTO num_cells_master_grid;

    -- Create Index
    EXECUTE Format('CREATE INDEX ON %s USING GIST (%s)', _overlapgap_grid, _cell_geo_column_name);


  RETURN num_cells_master_grid;
END;
$$
LANGUAGE plpgsql;
