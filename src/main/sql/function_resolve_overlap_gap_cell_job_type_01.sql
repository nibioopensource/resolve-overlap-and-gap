/**

This function will pick out all lines from

*/

CREATE OR REPLACE PROCEDURE resolve_overlap_gap_cell_job_type_01 (
	_tables_ext_metainfo rog_overlay_source_table_metainfo_type[], -- A list input tables meta info

	_topology_info resolve_overlap_data_topology_type,
	---(_topology_info).topology_name varchar, -- The topology schema name where we store store sufaces and lines from the simple feature dataset and th efinal result
	-- NB. Any exting data will related to topology_name will be deleted
	--(_topology_info).topology_srid
	--(_topology_info).topology_is_utm
	--(_topology_info).topology_snap_tolerance float, -- this is tolerance used as base when creating the the postgis topolayer
	--(_topology_info).create_topology_attrbute_tables boolean -- if this is true and we value for line_table_name we create attribute tables refferances to
	-- this tables will have atrbuttes equal to the simple feauture tables for lines and feautures

	-- a flag for testing the diff using a temporary tempology or not or insert directly into final source topology
	-- this is a test related to performance and testing on that, the final results should be pretty much the same.
	-- (_topology_info).use_temp_topology boolean

	-- a flag to run checks like topology.ValidateTopology and https://github.com/larsop/resolve-overlap-and-gap
	-- To run this does take time, spesially when working on verry very datasets so we trun this when needed.
	-- In the final result there should be no Topology errors in the topology structure
	-- In the simple feature result produced from Postgis Topology there should be overlaps
	-- (_topology_info).do_qualitycheck_on_final_reseult boolean


	_zero_topology_info_topology_snap_tolerance float,
	-- We get this as separate variable to handle 0 values
	-- This will used when securing border line spacing between grid cell when topology_snap_tolerance  is zero

	_table_name_result_prefix varchar,
	--(_topology_info).topology_name character varying,
	--(_topology_info).topology_snap_tolerance float, -- this is tolerance used as base when creating the the postgis topolayer

	_clean_info resolve_overlap_data_clean_type, -- different parameters used if need to clean up your data
	--(_clean_info)._min_area_to_keep float default 0, -- if this a polygon  is below this limit it will merge into a neighbour polygon. The area is sqare meter.
	--(_clean_info).split_input_lines_at_vertex_n int, -- split all lines at given vertex num

	-- TODO REMOVE start , start to use (_clean_info).edge_input_tables_cleanup rog_clean_edge_type
	--(_clean_info)._simplify_tolerance float default 0, -- is this is more than zero simply will called with
	--(_clean_info)._simplify_max_average_vertex_length int default 0, -- in meter both for utm and deegrees, this used to avoid running ST_simplifyPreserveTopology for long lines lines with few points
	--(_clean_info)._chaikins_nIterations int default 0, -- IF 0 NO CHAKINS WILL BE DONE,  A big value here make no sense because the number of points will increaes exponential )
	--(_clean_info)._chaikins_max_length int default 0, --edge that are longer than this value will not be touched by _chaikins_min_degrees and _chaikins_max_degrees
	--(_clean_info)._chaikins_min_degrees int default 0, -- The angle has to be less this given value, This is used to avoid to touch all angles.
	--(_clean_info)._chaikins_max_degrees int default 0, -- OR the angle has to be greather than this given value, This is used to avoid to touch all angles
	--(_clean_info)._chaikins_min_steep_angle_degrees int default 0, -- The angle has to be less this given value, This is used to avoid to touch all angles.
	--(_clean_info)._chaikins_max_steep_angle_degrees int default 0-- OR The angle has to be greather than this given value, This is used to avoid to touch all angles
	-- TODO REMOVE end


	-- To limit and set as master area. This must be in the same coordsys as _new_sf_table_srid and must be polygon or a multipolgyon,
	-- This will added as the first geometry into postgis topology. Data outside the master area will not be used.
	-- If you send in a null geometry here this parameter will not have any effect.
	--(_clean_info).input_boundary geometry

	_job_list_name character varying,
	_overlapgap_grid varchar,
	_bb geometry,
	_cell_job_type int, -- add lines 1 inside cell, 2 boderlines, 3 exract simple
	_loop_number int,
	_box_id int,
	_tmp_simplified_border_lines_name text,

	-- Is set to true if job is done and don table can update
	INOUT is_job_all_done boolean
)
LANGUAGE plpgsql
AS $$
DECLARE


	border_topo_info topo_update.input_meta_info;
	command_string text;
	start_time timestamp WITH time zone;
	used_time real;
	has_edges_temp_table_name text;

	-- faces,egdes to remove after simplify
	face_edges_to_remove_list add_border_lines_faces_to_remove[];

	snap_tolerance_fixed float = (_topology_info).topology_snap_tolerance;
	bb_boundary_geoms rog_bb_boundary_geoms;

	--  Method 1 topology.TopoGeo_addLinestring
	--  Method 2 topology.TopoGeo_LoadGeometry
	--  Method 3 select from tmp toplogy into master topology
	transfer_method_from_temp_2_final_topology int = (_topology_info).transfer_method_from_temp_2_final_topology;

	added_lines_temp_topo rog_overlay_add_border_lines_type[];
	res_split_border_lines rog_get_simplified_split_border_lines_result;
	res_split_border_lines_tmp rog_get_simplified_split_border_lines_result[];
	-- TODO add as paramater
	_topology_is_utm boolean = FALSE;

	num_lines_to_add int;

	this_type_job_type int;

	next_sub_job_type_table_name text;
	next_sub_job_type_data_from_add_lines_table_name text;

	first_run_remove_small_areas boolean = true;

	set_next_type_job_type boolean = true;

	is_singlepass_run boolean = true;

	max_used_time int = (60*10);
	--max_used_time int = 1;

	lines_to_add rog_input_line_order[];


BEGIN

	bb_boundary_geoms = topo_update.compute_bb_boundary_geoms(_bb,(_topology_info).topology_is_utm,_zero_topology_info_topology_snap_tolerance);

	start_time := Clock_timestamp();


	has_edges_temp_table_name := (_topology_info).topology_name||'.edge_data_tmp_' || _box_id;

	next_sub_job_type_table_name := (_topology_info).topology_name||'.sub_job_type_' || _cell_job_type || '_box_id_' || _box_id;

	border_topo_info.topology_name := (_topology_info).topology_name || '_' || _box_id;
	border_topo_info.snap_tolerance := (_topology_info).topology_snap_tolerance;

	next_sub_job_type_data_from_add_lines_table_name := (border_topo_info).topology_name||'.data_from_add_lines' || _cell_job_type || '_box_id_' || _box_id;

	RAISE NOTICE 'Enter resolve_overlap_gap_cell_job_type_01 with pid % using transfer_method_from_temp_2_final_topology % with for has_edges_temp_table_name % _loop_number % at timeofday:% for layer % for cell id % with bbox %',
	pg_backend_pid(), transfer_method_from_temp_2_final_topology, has_edges_temp_table_name, _loop_number, Timeofday(), (_topology_info).topology_name, _box_id, _bb;

	is_job_all_done = FALSE;
	RAISE NOTICE 'use border_topo_info.topology_name % _loop_number %', border_topo_info.topology_name, _loop_number;



	-- init new topology if not exixt
	IF to_regclass(next_sub_job_type_table_name) IS NULL THEN
		is_singlepass_run = true;

		-- loop_number 1 get data from input layers and add to temp topology layers
		-- get the simple feature data both the line_types and the inner lines.
		-- the boundery lines are saved in a separate table for later usage

		-- topo_update.get_simplified_split_border_lines testing only, it gives errors in some cases
		-- topo_update.get_simplified_border_lines  in prod now

		-- DOC In loop number one it will extract a lines from all the input tables and do a intersection of those lines releated to bbox
		-- DOC and store the results in the object res_split_border_lines

		CALL topo_update.get_simplified_split_border_lines(
			_tables_ext_metainfo,
			_topology_info,
			_clean_info,
			bb_boundary_geoms,
			_box_id,
			_table_name_result_prefix,
			res_split_border_lines
		);

		num_lines_to_add = Array_length((res_split_border_lines).lines_to_add, 1);
		-- if no lines found just return and is_job_all_done = true;
		IF num_lines_to_add > 0 THEN
			RAISE NOTICE 'For has_edges_temp_table_name % lines_to_add temp topology size %', has_edges_temp_table_name, num_lines_to_add;
		ELSE
			is_job_all_done = true;
			RAISE NOTICE 'For has_edges_temp_table_name % no lines found to add just return', has_edges_temp_table_name;
			RETURN;
		END IF;


		-- Testing memory table space
		IF (SELECT 1 from pg_tablespace where spcname = 'unlogget_mem') THEN
			set default_tablespace TO unlogget_mem;
			RAISE NOTICE 'Use tablespace unlogget_mem';
		END IF;

		-- Create a temp topology for this cell so each thread can work totally indepent of any other threads
		PERFORM topology.CreateTopology (border_topo_info.topology_name, (_topology_info).topology_srid , snap_tolerance_fixed);
		EXECUTE Format($fmt$
			ALTER table %1$s.edge_data set unlogged;
			ALTER table %1$s.node set unlogged;
			ALTER table %1$s.face set unlogged;
			ALTER table %1$s.relation set unlogged;

			ALTER table %1$s.edge_data set (autovacuum_enabled = off);
			ALTER table %1$s.node set (autovacuum_enabled = off);
			ALTER table %1$s.face set (autovacuum_enabled = off);
			ALTER table %1$s.relation set (autovacuum_enabled = off);

			CREATE INDEX ON %1$s.relation(layer_id);
			CREATE INDEX ON %1$s.relation(abs(element_id));
			CREATE INDEX IF NOT EXISTS relation_element_id_idx ON %1$s.relation(element_id);
			CREATE INDEX ON %1$s.relation(topogeo_id);
			CREATE INDEX IF NOT EXISTS edge_data_abs_next_left_edge_idx ON %1$s.edge_data(abs_next_left_edge);
			CREATE INDEX IF NOT EXISTS edge_data_abs_next_right_edge_idx ON %1$s.edge_data(abs_next_right_edge);

			$fmt$,
			border_topo_info.topology_name,
			(_topology_info).topology_name
		);

		-- https://www.postgresql.org/message-id/CAKAnmmJMgVLaDAb9jedsFhWCCKc8sRtpzPsio%2BwB3oAj6yOZPw%40mail.gmail.com
		-- Test we have a lot of dead rows, tested issue 97 with rows it did not help
		-- CREATE INDEX ON %1$s.node(geom, containing_face) include (node_id);

		-- update to use global serial drop local ones
		IF transfer_method_from_temp_2_final_topology = 3 THEN

			command_string = Format($fmt$
				ALTER TABLE %1$s.edge_data ALTER COLUMN edge_id set default nextval(%2$L::regclass);
				ALTER TABLE %1$s.node ALTER COLUMN node_id set default nextval(%3$L::regclass);
				ALTER TABLE %1$s.face ALTER COLUMN face_id set default nextval(%4$L::regclass);

				$fmt$,
				border_topo_info.topology_name,
				pg_get_serial_sequence ( quote_ident((_topology_info).topology_name)||'.edge_data','edge_id') ,
				pg_get_serial_sequence ( quote_ident((_topology_info).topology_name)||'.node','node_id') ,
				pg_get_serial_sequence ( quote_ident((_topology_info).topology_name)||'.face','face_id')
			);
			EXECUTE command_string;

			command_string = Format($fmt$
				DROP SEQUENCE %1$s.edge_data_edge_id_seq;
				DROP SEQUENCE %1$s.node_node_id_seq;
				DROP SEQUENCE %1$s.face_face_id_seq;

				$fmt$,
				border_topo_info.topology_name
			);
			EXECUTE command_string;

		END IF;
		-- Update table next job_type
		this_type_job_type = 1;
	ELSE
		is_singlepass_run = FALSE;
	END IF; -- end init new topology if not exixt


	-- Get this_type_job_type status this is not a single passrun
	IF NOT is_singlepass_run THEN
		command_string = Format($fmt$
			SELECT max(next_type_job_type) FROM %1$s WHERE job_done_at IS NULL ;
			$fmt$,
			next_sub_job_type_table_name,
			this_type_job_type
		);
		EXECUTE command_string INTO this_type_job_type;
	END IF;

	-- this is first step idependt of is_singlepass_run
	IF this_type_job_type = 1 THEN

		IF NOT is_singlepass_run THEN
			-- Get res_split_border_lines from this_type_job_type 1
			-- Get face_edges_to_remove_list from this_type_job_type 1
			command_string = Format($fmt$
				SELECT s.lines_to_add, s.added_lines_temp_topo, face_edges_to_remove_list FROM %1$s s WHERE next_type_job_type = %2$s ;
				$fmt$,
				next_sub_job_type_data_from_add_lines_table_name,
				(1)
			);
			EXECUTE command_string INTO lines_to_add, added_lines_temp_topo, face_edges_to_remove_list;
		ELSE
			lines_to_add=(res_split_border_lines).lines_to_add;
		END IF;

		RAISE NOTICE 'Call resolve_overlap_gap_cell_job_type_01_add_lines with % lines to add, for has_edges_temp_table_name % and border_topo_info.topology_name % uses border_topo_info.snap_tolerance %',
		array_length(lines_to_add,1), has_edges_temp_table_name, border_topo_info.topology_name, border_topo_info.snap_tolerance;

		-- add data with retry to temp topology
		CALL resolve_overlap_gap_cell_job_type_01_add_lines (
			_topology_info,
			_zero_topology_info_topology_snap_tolerance,
			_table_name_result_prefix,
			_clean_info,
			_cell_job_type,
			_bb,
			_box_id,
			border_topo_info.topology_name,
			border_topo_info.snap_tolerance,
			_topology_is_utm,
			TRUE, -- aaa _check_geeomtry_after_added_line -- By default we will check for min area values and sliver value in topology created
			TRUE, -- aaa _do_analyze_topology_tables boolean, -- if many many threads and add lot of data to the same table from many theads nevner run with TRUE
			_tables_ext_metainfo::rog_overlay_source_table_metainfo_type[],
			max_used_time, -- max_time_in_secs_to int, -- if set this will return caller given time is spent
			is_singlepass_run,  -- is_singlepass_run is true first time, sort_input_lines boolean, -- if true rog_input_line_order will sorted, but we only need to do that the first time
			lines_to_add,
			added_lines_temp_topo,
			face_edges_to_remove_list
		);

		used_time := (Extract(EPOCH FROM (Clock_timestamp() - start_time)));

		-- prepare for this_type_job_type like 2 if there are no more lines to add
		IF array_length(lines_to_add,1) > 0 THEN
			set_next_type_job_type = false;
		END IF;

--		RAISE NOTICE 'leave resolve_overlap_gap_cell_job_type_01_add_lines border_topo_info.topology_name % added_lines_temp_topo %, face_edges_to_remove_list % lines_to_add %',
--		border_topo_info.topology_name, array_length(added_lines_temp_topo,1), array_length(face_edges_to_remove_list,1), array_length(face_edges_to_remove_list,1);

	END IF; --this_type_job_type = 1


	IF this_type_job_type = 2 THEN
		IF NOT is_singlepass_run THEN
			-- Get res_split_border_lines from this_type_job_type 1
			-- Get face_edges_to_remove_list from this_type_job_type 1
			command_string = Format($fmt$
				SELECT s.res_split_border_lines, s.face_edges_to_remove_list, first_run_remove_small_areas FROM %1$s s WHERE next_type_job_type = %2$s ;
				$fmt$,
				next_sub_job_type_data_from_add_lines_table_name,
				(1)
			);
			EXECUTE command_string INTO res_split_border_lines_tmp, face_edges_to_remove_list, first_run_remove_small_areas;
			res_split_border_lines = res_split_border_lines_tmp[1];
		END IF;

		-- removed small areas in temp topology
		CALL resolve_overlap_gap_cell_job_type_01_small_areas (
			_topology_info,
			_table_name_result_prefix,
			_clean_info,
			(res_split_border_lines).fixed_point_set,
			_cell_job_type,
			_bb,
			border_topo_info.topology_name,
			_topology_is_utm,
			first_run_remove_small_areas,
			max_used_time,
			face_edges_to_remove_list
		);



		-- prepare for this_type_job_type like 2 in this run is max time is has not been reached and this is still a is_singlepass_run
		IF array_length(face_edges_to_remove_list,1) > 0 THEN
			set_next_type_job_type = false;
		ELSE
			this_type_job_type = this_type_job_type + 1;
		END IF;

		RAISE NOTICE 'resolve_overlap_gap_cell_job_type_01 at resolve_overlap_gap_cell_job_type_01_small_areas (edges to remove is %) this_type_job_type % transfer_method_from_temp_2_final_topology % for has_edges_temp_table_name % with _loop_number % with used_time % at timeofday:% for layer % for cell id % with bbox %',
		array_length(face_edges_to_remove_list,1), this_type_job_type, transfer_method_from_temp_2_final_topology, has_edges_temp_table_name, _loop_number, used_time, Timeofday(), (_topology_info).topology_name, _box_id, _bb;

		RAISE NOTICE 'Leave resolve_overlap_gap_cell_job_type_01 next this_type_job_type  % for has_edges_temp_table_name % with _loop_number % with used_time % at timeofday:% for topology layer % for cell id %',
		this_type_job_type, has_edges_temp_table_name, _loop_number, used_time, Timeofday(), (_topology_info).topology_name, _box_id;

	END IF; --this_type_job_type = 2


	IF this_type_job_type = 3 THEN
		IF NOT is_singlepass_run THEN
			-- Get res_split_border_lines from this_type_job_type 1
			command_string = Format($fmt$
				SELECT s.res_split_border_lines FROM %1$s s WHERE next_type_job_type = %2$s ;
				$fmt$,
				next_sub_job_type_data_from_add_lines_table_name,
				(1)
			);
			EXECUTE command_string INTO res_split_border_lines_tmp;
			res_split_border_lines = res_split_border_lines_tmp[1];

			-- Get face_edges_to_remove_list from this_type_job_type 1
			command_string = Format($fmt$
				SELECT s.added_lines_temp_topo FROM %1$s s WHERE next_type_job_type = %2$s ;
				$fmt$,
				next_sub_job_type_data_from_add_lines_table_name,
				(1)
			);
			EXECUTE command_string INTO added_lines_temp_topo;
		END IF;

		-- simplify edges
		CALL resolve_overlap_gap_cell_type_01_simplify_edges(
			_tables_ext_metainfo ,
			_topology_info,
			_zero_topology_info_topology_snap_tolerance,
			_table_name_result_prefix,
			_clean_info,
			_job_list_name,
			_overlapgap_grid,
			_bb,
			_cell_job_type,
			_loop_number,
			_box_id,
			border_topo_info,
			_tmp_simplified_border_lines_name,
			added_lines_temp_topo,
			res_split_border_lines,
			_topology_is_utm
		);

		-- we find used and check time
		used_time := (Extract(EPOCH FROM (Clock_timestamp() - start_time)));
		-- prepare for this_type_job_type like 2 in this run is max time is has not been reached and this is still a is_singlepass_run
		IF is_singlepass_run AND used_time < max_used_time THEN
			this_type_job_type = this_type_job_type + 1;
		END IF;

		RAISE NOTICE 'Leave resolve_overlap_gap_cell_job_type_01 next this_type_job_type  % for has_edges_temp_table_name % with _loop_number % with used_time % at timeofday:% for topology layer % for cell id %',
		this_type_job_type, has_edges_temp_table_name, _loop_number, used_time, Timeofday(), (_topology_info).topology_name, _box_id;

	END IF; --this_type_job_type = 3


	IF this_type_job_type = 4 THEN
		IF NOT is_singlepass_run THEN
			-- Get res_split_border_lines from this_type_job_type 1
			command_string = Format($fmt$
				SELECT s.res_split_border_lines FROM %1$s s WHERE next_type_job_type = %2$s ;
				$fmt$,
				next_sub_job_type_data_from_add_lines_table_name,
				(1)
			);
			EXECUTE command_string INTO res_split_border_lines_tmp;
			res_split_border_lines = res_split_border_lines_tmp[1];
		END IF;

		CALL resolve_overlap_gap_cell_job_type_01_to_final_topo (
			_tables_ext_metainfo, -- A list input tables meta info
			_topology_info,
			border_topo_info,
			_zero_topology_info_topology_snap_tolerance,
			_table_name_result_prefix,
			_clean_info, -- Different parameters used if need to clean up your data
			_job_list_name,
			_overlapgap_grid,
			_bb,
			_cell_job_type, -- Add lines 1 inside cell, 2 borderlines, 3 extract simple
			_loop_number,
			_box_id,
			res_split_border_lines,
			_topology_is_utm
		);

		-- we find used and but we do check time used we skip to next job
		used_time := (Extract(EPOCH FROM (Clock_timestamp() - start_time)));
		this_type_job_type = this_type_job_type + 1;

		RAISE NOTICE 'Leave resolve_overlap_gap_cell_job_type_01 next this_type_job_type  % for has_edges_temp_table_name % with _loop_number % with used_time % at timeofday:% for topology layer % for cell id %',
		this_type_job_type, has_edges_temp_table_name, _loop_number, used_time, Timeofday(), (_topology_info).topology_name, _box_id;


	END IF; --this_type_job_type = 4


	IF this_type_job_type = 5 THEN

		-- Handle failed line in loop number 2 and abouve
		-- TODO here is BUG TODAY because remove small area and simplify will ned ran
		CALL resolve_overlap_gap_cell_job_type_01_rest_lines (
			_tables_ext_metainfo, -- A list input tables meta info
			_topology_info,
			border_topo_info,
			_zero_topology_info_topology_snap_tolerance,
			_table_name_result_prefix,
			_clean_info, -- Different parameters used if need to clean up your data
			_job_list_name,
			_overlapgap_grid,
			_bb,
			_cell_job_type, -- Add lines 1 inside cell, 2 borderlines, 3 extract simple
			_loop_number,
			_box_id,
			res_split_border_lines,
			_topology_is_utm
		);

		is_job_all_done = true;

	END IF; --this_type_job_type = 5

	IF is_job_all_done THEN
			-- Clean up jobtype it not needed any more
		command_string = Format($fmt$
				DROP TABLE IF EXISTS %1$s;
				$fmt$,
				next_sub_job_type_table_name
		);
		EXECUTE command_string;
	ELSE
		set execute_parallel.rerun_job=true;
		-- init phase for mulittable rum, make table for next jobtype
		IF to_regclass(next_sub_job_type_table_name) IS NULL THEN
			command_string = Format($fmt$
					DROP TABLE IF EXISTS %1$s;

					CREATE UNLOGGED TABLE %1$s(next_type_job_type int primary key, job_desc text, job_added_at timestamp DEFAULT Now(), job_done_at timestamp);

					$fmt$,
					next_sub_job_type_table_name
				);
			EXECUTE command_string;

			command_string = Format($fmt$
					INSERT INTO %1$s(next_type_job_type, job_desc) VALUES (%2$s,'CALL resolve_overlap_gap_cell_job_type_01_add_lines to add lines from simple feauture');
					$fmt$,
					next_sub_job_type_table_name,
					this_type_job_type
				);
			EXECUTE command_string;

			-- Make table for next jobtype for next_sub_job_type_data_from_add_lines_table_name
			command_string = Format($fmt$
					CREATE UNLOGGED TABLE %1$s(next_type_job_type int primary key,
					res_split_border_lines rog_get_simplified_split_border_lines_result[],
					lines_to_add rog_input_line_order[],
					added_lines_temp_topo rog_overlay_add_border_lines_type[],
					face_edges_to_remove_list add_border_lines_faces_to_remove[],
					first_run_remove_small_areas boolean default true)
					$fmt$,
					next_sub_job_type_data_from_add_lines_table_name
				);
			EXECUTE command_string;

			-- init job next_sub_job_type_data_from_add_lines_table_name
			command_string = Format($fmt$
					INSERT INTO %1$s(next_type_job_type, res_split_border_lines, lines_to_add, added_lines_temp_topo, face_edges_to_remove_list)
					VALUES (%2$s,%3$L,%4$L,%5$L,%6$L);
					$fmt$,
					next_sub_job_type_data_from_add_lines_table_name,
					1, -- this_type_job_type,
					ARRAY[res_split_border_lines],
					lines_to_add,
					added_lines_temp_topo,
					face_edges_to_remove_list
			);
			EXECUTE command_string;
		END IF; -- init phase for mulittable rum, make table for next jobtype

		IF this_type_job_type = 1 THEN
			-- Update table with rest of rows to next pas
			command_string = Format($fmt$
				UPDATE %1$s SET lines_to_add = %3$L, added_lines_temp_topo = %4$L, face_edges_to_remove_list = %5$L WHERE next_type_job_type = %2$s ;
				$fmt$,
				next_sub_job_type_data_from_add_lines_table_name,
				1, -- this_type_job_type,
				lines_to_add,
				added_lines_temp_topo,
				face_edges_to_remove_list
			);
			EXECUTE command_string ;
		ELSIF this_type_job_type = 2 THEN
			-- Update table with rest of rows to next pas
			command_string = Format($fmt$
				UPDATE %1$s SET face_edges_to_remove_list = %3$L, first_run_remove_small_areas = false WHERE next_type_job_type = %2$s ;
				$fmt$,
				next_sub_job_type_data_from_add_lines_table_name,
				(1),
				face_edges_to_remove_list
			);
			EXECUTE command_string ;

		END IF;

		-- if move on to next jobtype
		IF set_next_type_job_type THEN
			-- now update since of next_sub_job_type since that table is remove when topology are delted.
			command_string = Format($fmt$
					UPDATE %1$s SET job_done_at = now()  WHERE next_type_job_type = %2$s ;
					$fmt$,
					next_sub_job_type_table_name,
					this_type_job_type
			);
			EXECUTE command_string;

			-- Set next this_type_job_typethis_type_job_type
			command_string = Format($fmt$
					INSERT INTO %1$s(next_type_job_type, job_desc) VALUES (%2$s,'CALL next sub jobtype');
					$fmt$,
					next_sub_job_type_table_name,
					(this_type_job_type+1)
			);
			EXECUTE command_string;
		END IF;

	END IF;


	used_time := (Extract(EPOCH FROM (Clock_timestamp() - start_time)));

	-- do a final analyze not all work done
	IF  this_type_job_type < 5 AND used_time > max_used_time THEN
		command_string = FORMAT(
		$fmt$
			{
			"VACUUM (ANALYZE,VERBOSE) %1$s.node",
			"VACUUM (ANALYZE,VERBOSE) %1$s.relation",
			"VACUUM (ANALYZE,VERBOSE) %1$s.edge_data",
			"VACUUM (ANALYZE,VERBOSE) %1$s.face"
			}
		$fmt$,
		border_topo_info.topology_name
		);
		-- to avoid dead lock we have to block
		COMMIT;
		call do_execute_parallel(command_string::text[],current_setting('execute_parallel.connstring',true),1);

		RAISE NOTICE 'VACUUM (ANALYZE,VERBOSE) before leave resolve_overlap_gap_cell_job_type_01 with pid % is_singlepass_run % is_job_all_done % this_type_job_type % transfer_method_from_temp_2_final_topology % for has_edges_temp_table_name % with _loop_number % with used_time % at timeofday:% for layer % for cell id % with bbox %',
		pg_backend_pid(), is_singlepass_run, is_job_all_done, this_type_job_type, transfer_method_from_temp_2_final_topology, has_edges_temp_table_name, _loop_number, used_time, Timeofday(), (_topology_info).topology_name, _box_id, _bb;

/**

		EXECUTE Format('ANALYZE %s.node', border_topo_info.topology_name);
		EXECUTE Format('ANALYZE %s.relation', border_topo_info.topology_name);
		EXECUTE Format('ANALYZE %s.edge_data', border_topo_info.topology_name);
		EXECUTE Format('ANALYZE %s.face', border_topo_info.topology_name);

		RAISE NOTICE 'ANALYZE before leave resolve_overlap_gap_cell_job_type_01 with pid % is_singlepass_run % is_job_all_done % this_type_job_type % transfer_method_from_temp_2_final_topology % for has_edges_temp_table_name % with _loop_number % with used_time % at timeofday:% for layer % for cell id % with bbox %',
		pg_backend_pid(), is_singlepass_run, is_job_all_done, this_type_job_type, transfer_method_from_temp_2_final_topology, has_edges_temp_table_name, _loop_number, used_time, Timeofday(), (_topology_info).topology_name, _box_id, _bb;
*/
	END IF;

	RAISE NOTICE 'Leave resolve_overlap_gap_cell_job_type_01 with pid % is_singlepass_run % is_job_all_done % this_type_job_type % transfer_method_from_temp_2_final_topology % for has_edges_temp_table_name % with _loop_number % with used_time % at timeofday:% for layer % for cell id % with bbox %',
	pg_backend_pid(), is_singlepass_run, is_job_all_done, this_type_job_type, transfer_method_from_temp_2_final_topology, has_edges_temp_table_name, _loop_number, used_time, Timeofday(), (_topology_info).topology_name, _box_id, _bb;

END
$$;

