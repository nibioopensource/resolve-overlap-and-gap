/**

This function will pick out all lines from

*/

CREATE OR REPLACE PROCEDURE resolve_overlap_gap_cell_job_type_01_add_lines (
_topology_info resolve_overlap_data_topology_type,
---(_topology_info).topology_name varchar, -- The topology schema name where we store store sufaces and lines from the simple feature dataset and th efinal result
-- NB. Any exting data will related to topology_name will be deleted
--(_topology_info).topology_srid
--(_topology_info).topology_is_utm
--(_topology_info).topology_snap_tolerance float, -- this is tolerance used as base when creating the the postgis topolayer
--(_topology_info).create_topology_attrbute_tables boolean -- if this is true and we value for line_table_name we create attribute tables refferances to
-- this tables will have atrbuttes equal to the simple feauture tables for lines and feautures

-- a flag for testing the diff using a temporary tempology or not or insert directly into final source topology
-- this is a test related to performance and testing on that, the final results should be pretty much the same.
-- (_topology_info).use_temp_topology boolean

-- a flag to run checks like topology.ValidateTopology and https://github.com/larsop/resolve-overlap-and-gap
-- To run this does take time, spesially when working on verry very datasets so we trun this when needed.
-- In the final result there should be no Topology errors in the topology structure
-- In the simple feature result produced from Postgis Topology there should be overlaps
-- (_topology_info).do_qualitycheck_on_final_reseult boolean


_zero_topology_info_topology_snap_tolerance float,
-- We get this as separate variable to handle 0 values
-- This will used when securing border line spacing between grid cell when topology_snap_tolerance  is zero

_table_name_result_prefix varchar,
--(_topology_info).topology_name character varying,
--(_topology_info).topology_snap_tolerance float, -- this is tolerance used as base when creating the the postgis topolayer

_clean_info resolve_overlap_data_clean_type, -- different parameters used if need to clean up your data
--(_clean_info)._min_area_to_keep float default 0, -- if this a polygon  is below this limit it will merge into a neighbour polygon. The area is sqare meter.
--(_clean_info)._split_input_lines_at_vertex_n int default NULL, -- split all lines at given vertex num
--(_clean_info)._simplify_tolerance float default 0, -- is this is more than zero simply will called with
--(_clean_info)._simplify_max_average_vertex_length int default 0, -- in meter both for utm and deegrees, this used to avoid running ST_simplifyPreserveTopology for long lines lines with few points
--(_clean_info)._chaikins_nIterations int default 0, -- IF 0 NO CHAKINS WILL BE DONE,  A big value here make no sense because the number of points will increaes exponential )
--(_clean_info)._chaikins_max_length int default 0, --edge that are longer than this value will not be touched by _chaikins_min_degrees and _chaikins_max_degrees
--(_clean_info)._chaikins_min_degrees int default 0, -- The angle has to be less this given value, This is used to avoid to touch all angles.
--(_clean_info)._chaikins_max_degrees int default 0, -- OR the angle has to be greather than this given value, This is used to avoid to touch all angles
--(_clean_info)._chaikins_min_steep_angle_degrees int default 0, -- The angle has to be less this given value, This is used to avoid to touch all angles.
--(_clean_info)._chaikins_max_steep_angle_degrees int default 0-- OR The angle has to be greather than this given value, This is used to avoid to touch all angles

-- To limit and set as master area. This must be in the same coordsys as _new_sf_table_srid and must be polygon or a multipolgyon,
-- This will added as the first geometry into postgis topology. Data outside the master area will not be used.
-- If you send in a null geometry here this parameter will not have any effect.
--(_clean_info).input_boundary geometry

_cell_job_type int, -- add lines 1 inside cell, 2 boderlines, 3 exract simple

_bb geometry,
_box_id int,

_border_topo_info_topology_name text,
_border_topo_info_snap_tolerance float,
_topology_is_utm boolean,
_check_geeomtry_after_added_line boolean, -- this costs a lot
_do_analyze_topology_tables boolean, -- if many many threads and add lot of data to the same table from many theads nevner run with TRUE

_tables_ext_metainfo rog_overlay_source_table_metainfo_type[], -- A list input tables meta info

max_time_in_secs_to int, -- if this is bigger than 0 , then the code will return more this time ise used.
-- If this parameter is bigger than 0 resolve_overlap_gap_cell_job_type_01_find_edges_to_remove will not be called


sort_input_lines boolean, -- if true rog_input_line_order will sorted, but we only need to do that the first time

-- the lines to add
INOUT lines_to_add_to rog_input_line_order[],

-- the result off added lines
INOUT _added_lines_info rog_overlay_add_border_lines_type[],

INOUT _face_edges_to_remove_list add_border_lines_faces_to_remove[]

)
LANGUAGE plpgsql
AS $$
DECLARE

	-- holds dynamic sql to be able to use the same code for different
	command_string text;
	start_time_last_analyze_check timestamp WITH time zone;
	done_time timestamp WITH time zone;
	used_time real;
	used_time_inc_analyze real;
	start_time_delta_job timestamp WITH time zone;

	v_state text;
	v_msg text;
	v_detail text;
	v_hint text;
	v_context text;
	v_cnt_left_over_borders int;

	error_msg text;
	line_edges_remove integer[];
	line_edges_remove_group integer[];
	line_edges_tmp integer[];




	rec_new_line RECORD;

	rec_new_line_split RECORD;

	rec_face_to_remove RECORD;


	rec_new_edges_since_check int = 0;
	rec_new_edges_total int = 0;
	rec_new_edges_analyze int = 500;
	rec_mum_analyze_done int = 1;
	rec_analyze_done boolean = false;

	rec_new_lines_since_check int = 0;
	rec_new_lines_total int = 0;

	rec_remove_edges_since_anlyze int = 0;
	rec_remove_edges_analyze int;
	rec_remove_counter int = 0;
	rec_remove_edges_total int = 0;

	num_lines_to_add int;
	rec_new_edges_to_remove int = 0;


	-- This the output status from adding a new line
	rog_added_border_line rog_overlay_add_border_lines_type;

	-- faces to remove
	face_edges_to_remove_list add_border_lines_faces_to_remove[] = '{}'::add_border_lines_faces_to_remove[];
	face_edges_to_remove_list_step int = 1;
	face_edges_to_remove_sone_steps int = 0;
	face_edges_to_remove add_border_lines_faces_to_remove;
	face_edges_to_remove_group add_border_lines_faces_to_remove[];



	-- This is lines that we failed to , because off a topo error
	lines_to_retry rog_overlay_add_border_failed_line[] = '{}'::rog_overlay_add_border_failed_line[];

	lines_to_retry_final rog_overlay_add_border_failed_line[] = '{}'::rog_overlay_add_border_failed_line[];

	tmp_line_to_retry rog_overlay_add_border_failed_line;


	sql text;

	-- boundary_geoms and _bb for this _bb
	bb_boundary_geoms rog_bb_boundary_geoms;

	-- Used for the diffrent layer
	bb_transformed geometry;


	bb_validate_result validatetopology_returntype[];

	-- this name of the table where add failed lines
	no_cutline_filename text = _table_name_result_prefix || '_no_cut_line_failed';


	--
	res_spike_remove_lines rog_input_line_order[];
	vacuum_cmd text[];

	more_edges_to_check_for_removel boolean;

	splite_lines_result geometry[];

	num_remove_lines_since_last_vacuum int;
	remove_i int;
	done_vacuum_analyze int = 0;

	new_line_edges_unique integer[];
	tmp_line_edges_all integer[];
	tmp_line_edges_exist integer[];
	tmp_line_edges_new integer[];
	tmp_face_edges_to_remove_list add_border_lines_faces_to_remove[];
	rate_edges_added float;
	rate_lines_added float;

	loop_number int = 0;


	lines_to_add_to_next_batch rog_input_line_order[] = '{}';
	lines_to_add_to_this_batch rog_overlay_add_border_lines_type[] = '{}';

	return_to_caller boolean = false;

	error_split_line_at_vertex int;

	rec_new_edges_since_anlyze int = 0;

BEGIN



		/**

		Create table of input rows to test

		command_string = Format($fmt$

		DROP TABLE IF EXISTS input_data_%1$s;

		CREATE table input_data_%1$s AS
		SELECT r1.geom, r1.table_input_order, r1.src_table_pk_column_value, cover_num  FROM (
		SELECT s1.geom, s1.table_input_order, s1.src_table_pk_column_value, count(s2.geom) cover_num
		FROM
		(select geo AS geom, input_order AS table_input_order, src_table_pk_column_value FROM unnest(%2$L::rog_input_line_order[]) ) AS s1
		LEFT JOIN (select geo AS geom  FROM unnest(%2$L::rog_input_line_order[]) ) s2 ON ST_Covers(ST_Envelope(s1.geom),ST_Envelope(s2.geom)) AND NOT ST_Equals(s1.geom,s2.geom)
		GROUP BY s1.geom, s1.table_input_order, s1.src_table_pk_column_value
		) r1
		order by cover_num desc , ST_isClosed(geom) desc, ST_NPoints(geom) desc;
		$fmt$,
		_border_topo_info_topology_name||_box_id, --01
		lines_to_add_to --02
		);

		EXECUTE command_string;
		commit;



		*/


/**lines_to_add_to_this_batch
org order by
	command_string := Format('CREATE table input_data_%1$s AS SELECT r.geom, r.table_input_order, r.src_table_pk_column_value
	FROM (select geo AS geom, input_order AS table_input_order, src_table_pk_column_value
	FROM unnest(%4$L::rog_input_line_order[])
	) AS r
	ORDER BY r.table_input_order,  ST_IsClosed(r.geom) desc, ST_Length(r.geom,true) ASC',
	_border_topo_info_topology_name, --01
	_border_topo_info_snap_tolerance, --02
	_table_name_result_prefix, --03
	lines_to_add_to, --04
	(_clean_info).min_area_to_keep, --05
	_topology_is_utm --06
*/

		-- Tes new order by
		-- create a sql to loop through all input lines and add them to then temp topology
		-- faling lines will be adde to lines_to_retry array
		IF sort_input_lines THEN
			command_string = Format($fmt$
			SELECT r1.geom, r1.table_input_order, r1.src_table_pk_column_value FROM (
			SELECT s1.geom, s1.table_input_order, s1.src_table_pk_column_value, count(s2.geom) cover_num
			FROM
			(select geo AS geom, input_order AS table_input_order, src_table_pk_column_value FROM unnest(%1$L::rog_input_line_order[]) ) AS s1
			LEFT JOIN (select geo AS geom  FROM unnest(%1$L::rog_input_line_order[]) ) s2 ON ST_Covers(ST_Envelope(s1.geom),ST_Envelope(s2.geom)) AND NOT ST_Equals(s1.geom,s2.geom)
			GROUP BY s1.geom, s1.table_input_order, s1.src_table_pk_column_value
			) r1
			--order by cover_num desc , ST_isClosed(geom) desc, ST_NPoints(geom) desc
			order by ST_isClosed(geom) desc, ST_NPoints(geom) desc, cover_num desc
			$fmt$,
			lines_to_add_to --01
			);
		ELSE
			command_string = Format($fmt$
				SELECT geo AS geom, input_order AS table_input_order, src_table_pk_column_value FROM unnest(%1$L::rog_input_line_order[]);
			$fmt$,
			lines_to_add_to --01
			);

		END IF;


		-- start loop through lines from lines_to_add_to and add them to temp topology

		num_lines_to_add = Array_length(lines_to_add_to, 1);

		start_time_delta_job := Clock_timestamp();

		start_time_last_analyze_check := Clock_timestamp();

		FOR rec_new_line IN EXECUTE command_string
		LOOP


			IF return_to_caller THEN
				-- if return to caller add rest of to lines_to_add_to_next_batch
				lines_to_add_to_next_batch = lines_to_add_to_next_batch||(rec_new_line.geom,rec_new_line.table_input_order,rec_new_line.src_table_pk_column_value)::rog_input_line_order;
			ELSE
				--RAISE NOTICE 'Try to line number % (to add) rec_new_line.geom % rec_new_line.geom %', rec_new_lines_total, ST_NPoints(rec_new_line.geom), rec_new_line.geom;
				-- RAISE NOTICE 'with commit/vacuum/analyze test 631 SELECT topology.TopoGeo_addLinestring(%,%);', quote_nullable(_border_topo_info_topology_name), quote_nullable(rec_new_line.geom::text);

				rog_added_border_line = topo_update.add_border_lines(
				_border_topo_info_topology_name,
				rec_new_line.geom,
				(_clean_info).min_area_to_keep,
				(_clean_info).split_input_lines_at_vertex_n, -- split all lines at given vertex num
				(_clean_info).min_st_is_empty_buffer_value,
				rec_new_line.table_input_order,
				rec_new_line.src_table_pk_column_value,
				_border_topo_info_snap_tolerance,
				_table_name_result_prefix,
				FALSE, -- _do_retry_add
				FALSE, -- _previously_failed_line
				_topology_is_utm,
				FALSE, -- _log_failed_line_error_table boolean default TRUE, -- If true it will add new rows to _table_name_result_prefix || '_no_cut_line_failed';
				(_topology_info).use_temp_topology, -- _use_temp_edges_table_name_for_small_area boolean default FALSE -- If true a local table name will be used
				false, -- -- By default we will check for min area values and sliver value in topology created.
				_tables_ext_metainfo
				);

				-- Get lines to retry that failed
				IF (rog_added_border_line).failed_line IS NOT NULL THEN
					lines_to_retry = array_append(lines_to_retry,(rog_added_border_line).failed_line);
				END IF;

				IF (rog_added_border_line).ok_input_line_edges IS NOT NULL THEN
					lines_to_add_to_this_batch=lines_to_add_to_this_batch||rog_added_border_line;
					rec_new_edges_since_anlyze = rec_new_edges_since_anlyze + Array_Length((rog_added_border_line).ok_input_line_edges,1);
				END IF;

				rec_new_lines_total=rec_new_lines_total+1;
				rec_new_edges_since_check=rec_new_edges_since_check+1;

				IF rec_new_edges_since_check > face_edges_to_remove_list_step THEN
					used_time := (Extract(EPOCH FROM (Clock_timestamp() - start_time_delta_job)));
					rec_new_edges_since_check = 1;
					loop_number = loop_number + 1;
				END IF;

				-- Do analyze based on number of edges
				--IF rec_new_edges_since_anlyze > rec_new_edges_analyze THEN
				IF NOT rec_analyze_done THEN
					rec_new_edges_total = rec_new_edges_total + rec_new_edges_since_anlyze;

					rec_new_edges_since_anlyze = 0;
					rec_analyze_done = true;
					IF _do_analyze_topology_tables THEN
						-- rec_new_edges_analyze = rec_new_edges_analyze + rec_new_edges_analyze;

						EXECUTE Format('ANALYZE %1$s.node', _border_topo_info_topology_name);
						EXECUTE Format('ANALYZE %s.relation', _border_topo_info_topology_name);
						EXECUTE Format('ANALYZE %s.edge_data', _border_topo_info_topology_name);
						EXECUTE Format('ANALYZE %s.face', _border_topo_info_topology_name);

						RAISE NOTICE 'Done adding lines % (edges %)  of %, and done ANALYZE % rows at row % for % time .',
						rec_new_lines_total, rec_new_edges_total, num_lines_to_add, _border_topo_info_topology_name, rec_new_edges_analyze, rec_mum_analyze_done;
					ELSE
						RAISE NOTICE 'Done adding lines % (edges %)  of % for % .',
						rec_new_lines_total, rec_new_edges_total, num_lines_to_add, _border_topo_info_topology_name;
					END IF;

					rec_mum_analyze_done = rec_mum_analyze_done + 1;
					rec_new_edges_analyze = rec_new_edges_analyze+rec_new_edges_analyze;

				END IF ;

				IF max_time_in_secs_to IS NOT NULL AND used_time >= max_time_in_secs_to THEN
				--IF max_time_in_secs_to IS NOT NULL AND FALSE THEN
					return_to_caller = true;
				END IF;
			END IF;

		END LOOP;


/**

Analyze moved to the caller

		-- do a final analyze not all work done
		IF _do_analyze_topology_tables AND return_to_caller THEN
			vacuum_cmd = FORMAT(
			$fmt$
				{
				"VACUUM (ANALYZE,VERBOSE) %1$s.node",
				"VACUUM (ANALYZE,VERBOSE) %1$s.relation",
				"VACUUM (ANALYZE,VERBOSE) %1$s.edge_data",
				"VACUUM (ANALYZE,VERBOSE) %1$s.face"
				}
			$fmt$,
			_border_topo_info_topology_name);
			-- to avoid dead lock we have to block
			COMMIT;
			call do_execute_parallel(vacuum_cmd::text[],current_setting('execute_parallel.connstring',true),1);

			used_time := (Extract(EPOCH FROM (Clock_timestamp() - start_time_delta_job)));
			RAISE NOTICE 'Cleanup topolgy by, VACUUM (ANALYZE,VERBOSE) with % number of add_border_lines for _box_id % to temp topollogy (number of failed lines %), number edges to remove %, phase 0 for topology %  with tolerance % at % used_time: %',
			rec_new_lines_total,
			_box_id,
			Array_length(lines_to_retry, 1),
			Array_length(face_edges_to_remove_list,1),
			_border_topo_info_topology_name,
			(_topology_info).topology_snap_tolerance,
			Clock_timestamp(),
			used_time;

		ELSIF _do_analyze_topology_tables THEN
			EXECUTE Format('ANALYZE %s.node', _border_topo_info_topology_name);
			EXECUTE Format('ANALYZE %s.relation', _border_topo_info_topology_name);
			EXECUTE Format('ANALYZE %s.edge_data', _border_topo_info_topology_name);
			EXECUTE Format('ANALYZE %s.face', _border_topo_info_topology_name);

			RAISE NOTICE 'Cleanup topolgy by, ANALYZE with % number of add_border_lines for _box_id % to temp topollogy (number of failed lines %), number edges to remove %, phase 0 for topology %  with tolerance % at % used_time: %',
			rec_new_lines_total,
			_box_id,
			Array_length(lines_to_retry, 1),
			Array_length(face_edges_to_remove_list,1),
			_border_topo_info_topology_name,
			(_topology_info).topology_snap_tolerance,
			Clock_timestamp(),
			used_time;

		END IF;

*/

		used_time := (Extract(EPOCH FROM (Clock_timestamp() - start_time_delta_job)));
		RAISE NOTICE 'Done with % number of add_border_lines for _box_id % to temp topollogy (number of failed lines %), number edges to remove %, phase 0 for topology %  with tolerance % at % used_time: %',
		rec_new_lines_total,
		_box_id,
		Array_length(lines_to_retry, 1),
		Array_length(face_edges_to_remove_list,1),
		_border_topo_info_topology_name,
		(_topology_info).topology_snap_tolerance,
		Clock_timestamp(),
		used_time;





		-- if any lines failed try to add them one more time to the temp topology, but then break then by find all intersecting points.
		-- faling lines now will added to lines_to_retry_final to be handled at a later job type
		IF Array_length(lines_to_retry, 1) > 0  THEN

			-- start loop through lines from lines_to_add_to and add them to temp topology
			start_time_delta_job := Clock_timestamp();

			-- pick up failed lines split them afainst exiting edges to make it easier to work with
			command_string := Format($fmt$
				SELECT *
				FROM  (
						SELECT DISTINCT error_line AS geom , 1000 AS table_input_order, ''::text AS   src_table_pk_column_value
						FROM unnest(%1$L::rog_overlay_add_border_failed_line[]) AS r
				) r
				ORDER BY r.table_input_order, ST_X(ST_StartPoint(r.geom)), ST_Y(ST_StartPoint(r.geom))
				$fmt$,
				lines_to_retry
			);

			num_lines_to_add = Array_length(lines_to_retry, 1);

			vacuum_cmd = FORMAT(
			$fmt$
				{
				"VACUUM (FULL,ANALYZE,VERBOSE) %1$s.node",
				"VACUUM (FULL,ANALYZE,VERBOSE) %1$s.relation",
				"VACUUM (FULL,ANALYZE,VERBOSE) %1$s.edge_data",
				"VACUUM (FULL,ANALYZE,VERBOSE) %1$s.face"
				}
			$fmt$,
			_border_topo_info_topology_name);
			-- to avoid dead lock we have to block
			COMMIT;
			call do_execute_parallel(vacuum_cmd::text[],current_setting('execute_parallel.connstring',true),1);


			used_time := (Extract(EPOCH FROM (Clock_timestamp() - start_time_delta_job)));
			RAISE NOTICE 'Retry after VACUUM (FULL,ANALYZE,VERBOSE) with % failed lines in add_border_lines for _box_id % to temp topollogy (number of failed lines %), number edges to remove %, phase 0 for topology %  with tolerance % at % used_time: %',
			num_lines_to_add,
			_box_id,
			Array_length(lines_to_retry, 1),
			Array_length(face_edges_to_remove_list,1),
			_border_topo_info_topology_name,
			(_topology_info).topology_snap_tolerance,
			Clock_timestamp(),
			used_time;


			rec_new_lines_since_check = 0;

			-- start loop through lines from temp table
			start_time_delta_job := Clock_timestamp();
			FOR rec_new_line IN EXECUTE command_string
			LOOP


				-- just splitt extreme lines
				error_split_line_at_vertex = 10000;
				splite_lines_result = topo_update.split_line_on_vertex_n(rec_new_line.geom,error_split_line_at_vertex);

				FOR rec_new_line_split IN SELECT r.*
					FROM (
						SELECT topo_update.split_line_on_edges(_border_topo_info_topology_name,sl.line)
						FROM (SELECT unnest(splite_lines_result::geometry[]) line) sl
					) r
				LOOP


					--RAISE NOTICE 'TRYY, to adding error_line using error_split_line_at_vertex % rec_new_line_split.split_line_on_edges  %  to % from failed lines % to temp topology',
					--error_split_line_at_vertex, rec_new_line_split.split_line_on_edges, _border_topo_info_topology_name , _table_name_result_prefix||'_no_cut_line_failed';

					rog_added_border_line = topo_update.add_border_lines(
					_border_topo_info_topology_name,
					rec_new_line_split.split_line_on_edges,
					(_clean_info).min_area_to_keep,
					(_clean_info).split_input_lines_at_vertex_n, -- split all lines at given vertex num
					(_clean_info).min_st_is_empty_buffer_value,
					rec_new_line.table_input_order,
					rec_new_line.src_table_pk_column_value, -- src_table_pk_column_value
					_border_topo_info_snap_tolerance,
					_table_name_result_prefix,
					FALSE, -- _do_retry_add
					FALSE, -- _previously_failed_line
					_topology_is_utm,
					FALSE, -- _log_failed_line_error_table boolean default TRUE, -- If true it will add new rows to _table_name_result_prefix || '_no_cut_line_failed';
					(_topology_info).use_temp_topology, -- _use_temp_edges_table_name_for_small_area boolean default FALSE -- If true a local table name will be used
					_check_geeomtry_after_added_line, -- By default we will check for min area values and sliver value in topology created.
					_tables_ext_metainfo

					);

					-- Get lines to retry that failed
					IF (rog_added_border_line).failed_line IS NOT NULL THEN
						lines_to_retry_final = array_append(lines_to_retry_final,(rog_added_border_line).failed_line);
					END IF;


					-- Schek for remove line
					IF (rog_added_border_line).ok_input_line_edges IS NOT NULL THEN
						lines_to_add_to_this_batch=lines_to_add_to_this_batch||rog_added_border_line;
					END IF;

				END LOOP;


			END LOOP;

			used_time := (Extract(EPOCH FROM (Clock_timestamp() - start_time_delta_job)));
			RAISE NOTICE 'Done with retry failed lines(not spitted) % , after split %, number of add_border_lines for _box_id % to temp topollogy (number of lines_to_retry_final lines %), number edges to remove %, phase 0 for topology %  with tolerance % at % used_time: %',
			num_lines_to_add,
			rec_new_lines_since_check,
			_box_id,
			Array_length(lines_to_retry_final, 1),
			rec_new_edges_to_remove,
			_border_topo_info_topology_name,
			(_topology_info).topology_snap_tolerance,
			Clock_timestamp(),
			used_time;

		END IF; -- end if has failed lines to temp toplogy

				-- Add failed line to failed table
		-- if any lines failed try to add them one more time to the temp topology, but then break then by find all intersecting points.
		-- faling lines now will added to lines_to_retry_final to be handled at a later job type
		IF Array_length(lines_to_retry_final, 1) > 0  THEN

			-- pick up failed lines split them afainst exiting edges to make it easier to work with
			command_string := Format($fmt$
						SELECT r.*
						FROM unnest(%1$L::rog_overlay_add_border_failed_line[]) AS r
				$fmt$,
				lines_to_retry_final
			);

			FOR tmp_line_to_retry IN EXECUTE command_string LOOP
				RAISE NOTICE 'retry line added to % from topology % with N points %', no_cutline_filename, _border_topo_info_topology_name, ST_NPoints((tmp_line_to_retry).error_line);

				EXECUTE Format('INSERT INTO %s(line_geo_lost, error_info, d_state, d_msg, d_detail, d_hint, d_context, geo)
				VALUES(%L, %L, %L, %L, %L, %L, %L, %L)',
				no_cutline_filename,
				TRUE,
				'Will do retry in a later stage ',
				(tmp_line_to_retry).v_state,
				(tmp_line_to_retry).v_msg,
				(tmp_line_to_retry).v_detail,
				(tmp_line_to_retry).v_hint,
				(tmp_line_to_retry).v_context,
				(tmp_line_to_retry).error_line);
			END LOOP;

			RAISE NOTICE 'Done adding retry line added to % from topology % for % lines at %', no_cutline_filename, _border_topo_info_topology_name,  Array_length(lines_to_retry_final, 1),  Clock_timestamp();

		END IF;


		lines_to_add_to=lines_to_add_to_next_batch;


		CALL resolve_overlap_gap_cell_type_01_update_edge_origin (
		_border_topo_info_topology_name,
		_bb,
		_box_id,
		lines_to_add_to_this_batch
		);


		CALL resolve_overlap_gap_cell_job_type_01_find_edges_to_remove(
		_topology_info,
		_clean_info,
		_border_topo_info_topology_name,
		lines_to_add_to_this_batch,
		tmp_face_edges_to_remove_list);

		_added_lines_info=_added_lines_info||lines_to_add_to_this_batch;

		_face_edges_to_remove_list=_face_edges_to_remove_list||tmp_face_edges_to_remove_list;


/**
-- remove small polygons in temp in (_clean_info).min_area_to_keep
		IF (_clean_info).min_area_to_keep IS NOT NULL AND (_clean_info).min_area_to_keep > 0 AND
			face_edges_to_remove_list IS NOT NULL AND array_length(face_edges_to_remove_list,1) > 0 THEN
			line_edges_remove = '{}';


			FOREACH face_edges_to_remove IN ARRAY face_edges_to_remove_list
			LOOP

				-- remove duplicated edges line_edges_tmp integer[]
				line_edges_tmp = (select array_agg(distinct e)
				FROM unnest((face_edges_to_remove).edge_ids) e)
				WHERE NOT line_edges_remove @> (face_edges_to_remove).edge_ids;


				IF line_edges_tmp IS NOT NULL THEN
					CALL topo_update.do_merge_small_areas_no_block (
					_border_topo_info_topology_name,
					_table_name_result_prefix,
					(_clean_info).min_area_to_keep,
					(_clean_info).min_st_is_empty_buffer_value,
					_bb,
					_topology_is_utm,
					_cell_job_type,
					more_edges_to_check_for_removel, -- no neeed to check on more_edges_to_check_for_removel
					_res_split_border_lines_fixed_point_set,
					line_edges_tmp
					);

					line_edges_remove =  array_cat(line_edges_remove,line_edges_tmp);


				END IF;

				rec_remove_edges_since_anlyze = rec_remove_edges_since_anlyze + 1;
				rec_remove_counter = rec_remove_counter + 1;
				num_remove_lines_since_last_vacuum = num_remove_lines_since_last_vacuum + 1;

				-- Do analyze based on number of edges
				IF rec_remove_edges_since_anlyze > rec_remove_edges_analyze THEN
					IF _do_analyze_topology_tables THEN
						used_time := (Extract(EPOCH FROM (Clock_timestamp() - start_time_last_analyze_check)));
						-- do commit before anallyze
						-- COMMIT;


						-- With to often commit for other layer we get a lot  wait_event | BufFileWrite and iowait > goes more than 50 % and cpu false to
						-- If we use more than 1 sec pr. egde to remove, then try a vaccuum
						-- With 2.0 and 600 rows it will trigger vacuum full at 1200 secs
						IF used_time/rec_remove_edges_since_anlyze > 2.0 THEN
							-- Tested 3 times the same numbers every time
							-- Time: 267051.783 ms (04:27.052) for https://gitlab.com/nibioopensource/resolve-overlap-and-gap/-/issues/70
							-- NOTICE:  00000: Done clean small polygons for face_edges_to_remove_list size 2436 for face_table_name gronn_test_03_test_topo_5714.face at 2024-02-26 17:45:38.209123+01 used_time: 214.91571
							-- First I do a commit here to make sure that the data are avalable for the outside caller
							COMMIT;

							vacuum_cmd = FORMAT(
								$fmt$
								{"VACUUM (FULL,ANALYZE,VERBOSE) %1$s.node","VACUUM (FULL,ANALYZE,VERBOSE) %1$s.relation","VACUUM (FULL,ANALYZE,VERBOSE) %1$s.edge_data","VACUUM (FULL,ANALYZE,VERBOSE) %1$s.face"}
								$fmt$,
								_border_topo_info_topology_name
							);
							call do_execute_parallel(vacuum_cmd::text[],1);

							RAISE NOTICE 'Removed lines % (off % lines) in % secs, num_remove_lines_since_last_vacuum % and done COMMIT/VACUUM (FULL,ANALYZE,VERBOSE) for % at %.',
							rec_remove_counter, rec_remove_edges_total, used_time, num_remove_lines_since_last_vacuum,
							_border_topo_info_topology_name, Clock_timestamp();

							-- When I do analyze like this does it does help probably beacse dead rows are not removed, below are timming with this code
							-- Tested 3 times the same numbers every time
							-- Time: 556782.244 ms (09:16.782) https://gitlab.com/nibioopensource/resolve-overlap-and-gap/-/issues/70
							-- Done clean small polygons for face_edges_to_remove_list size 2436 for face_table_name gronn_test_03_test_topo_5714.face at 2024-02-26 17:21:51.649335+01 used_time: 487.48438

							num_remove_lines_since_last_vacuum = 0;
						ELSE
							EXECUTE Format('ANALYZE %s.node', _border_topo_info_topology_name);
							EXECUTE Format('ANALYZE %s.relation', _border_topo_info_topology_name);
							EXECUTE Format('ANALYZE %s.edge_data', _border_topo_info_topology_name);
							EXECUTE Format('ANALYZE %s.face', _border_topo_info_topology_name);
							RAISE NOTICE 'Removed lines % (off % lines) in % secs and done ANALYZE,VERBOSE for % at %.',
							rec_remove_counter, rec_remove_edges_total, used_time, _border_topo_info_topology_name, Clock_timestamp();
						END IF; -- IF used_time > 200 THEN
					ELSE
						RAISE NOTICE 'Removed lines % (off % lines) at % secs for % at %.',
						rec_remove_counter, rec_remove_edges_total, Clock_timestamp(), _border_topo_info_topology_name,Clock_timestamp();
					END IF;

					rec_remove_edges_since_anlyze = 0;
					start_time_last_analyze_check := Clock_timestamp();

				END IF ; -- IF rec_remove_edges_since_anlyze > rec_remove_edges_analyze THEN

			END LOOP;

			used_time := (Extract(EPOCH FROM (Clock_timestamp() - start_time_delta_job)));
			RAISE NOTICE 'Done clean small polygons for face_edges_to_remove_list size % for face_table_name % at % used_time: %',
			rec_remove_edges_total, _border_topo_info_topology_name|| '.face', Clock_timestamp(), used_time;

		END IF;
*/




/**


		FOREACH rog_added_border_line IN ARRAY _added_lines_info
		LOOP
		RAISE NOTICE 'Testing %', (rog_added_border_line).selected_edge_face_id;
						EXECUTE format(
						$fmt$

						INSERT INTO %1$s (table_input_order, grid_cell_id, src_table_pk_column_value,face_mbr,face_id,pk_value_set)
						SELECT %2$L, %3$L, %4$L, %5$L, l.left_face ,true
						FROM
						%7$I.edge_data l,
						%7$I.face f,
						test_14.nyesegmenter_testomrader old
						WHERE l.right_face = %6$L AND l.left_face != %6$L AND l.left_face > 0 AND l.right_face > 0
						AND f.face_id = l.left_face
						AND old.id = %4$L::int
						AND ST_Intersects( ST_Transform(old.geo,4258), f.mbr)
						AND ST_Intersects(
						ST_Transform(old.geo,4258),
						(ST_MaximumInscribedCircle(ST_GetFaceGeometry(%7$L,l.left_face))).center
						);

						INSERT INTO %1$s (table_input_order, grid_cell_id, src_table_pk_column_value,face_mbr,face_id,pk_value_set)
						SELECT %2$L, %3$L, %4$L, %5$L, l.right_face ,true
						FROM
						%7$I.edge_data l,
						%7$I.face f,
						test_14.nyesegmenter_testomrader old
						WHERE l.left_face = %6$L AND l.right_face != %6$L AND l.right_face > 0 AND l.left_face > 0
						AND f.face_id = l.right_face
						AND old.id = %4$L::int
						AND ST_Intersects( ST_Transform(old.geo,4258), f.mbr)
						AND ST_Intersects(
						ST_Transform(old.geo,4258),
						(ST_MaximumInscribedCircle(ST_GetFaceGeometry(%7$L,l.right_face))).center
						);


					$fmt$,
					_table_name_result_prefix||'_org_pk_value_to_face_id',
					(rog_added_border_line).table_input_order,
					NULL, -- maybe we need send as paramater
					(rog_added_border_line).src_table_pk_column_value,
					NULL,
					(rog_added_border_line).selected_edge_face_id,
					_border_topo_info_topology_name
					);


		END LOOP;
*/


END
$$;

