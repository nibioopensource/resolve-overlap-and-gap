/**

This function edges to remove based input parameters

*/

CREATE OR REPLACE PROCEDURE resolve_overlap_gap_cell_job_type_01_find_edges_to_remove (
_topology_info resolve_overlap_data_topology_type,
---(_topology_info).topology_name varchar, -- The topology schema name where we store store sufaces and lines from the simple feature dataset and th efinal result
-- NB. Any exting data will related to topology_name will be deleted
--(_topology_info).topology_srid
--(_topology_info).topology_is_utm
--(_topology_info).topology_snap_tolerance float, -- this is tolerance used as base when creating the the postgis topolayer
--(_topology_info).create_topology_attrbute_tables boolean -- if this is true and we value for line_table_name we create attribute tables refferances to
-- this tables will have atrbuttes equal to the simple feauture tables for lines and feautures

-- a flag for testing the diff using a temporary tempology or not or insert directly into final source topology
-- this is a test related to performance and testing on that, the final results should be pretty much the same.
-- (_topology_info).use_temp_topology boolean

-- a flag to run checks like topology.ValidateTopology and https://github.com/larsop/resolve-overlap-and-gap
-- To run this does take time, spesially when working on verry very datasets so we trun this when needed.
-- In the final result there should be no Topology errors in the topology structure
-- In the simple feature result produced from Postgis Topology there should be overlaps
-- (_topology_info).do_qualitycheck_on_final_reseult boolean


_clean_info resolve_overlap_data_clean_type, -- different parameters used if need to clean up your data
--(_clean_info)._min_area_to_keep float default 0, -- if this a polygon  is below this limit it will merge into a neighbour polygon. The area is sqare meter.
--(_clean_info)._split_input_lines_at_vertex_n int default NULL, -- split all lines at given vertex num
--(_clean_info)._simplify_tolerance float default 0, -- is this is more than zero simply will called with
--(_clean_info)._simplify_max_average_vertex_length int default 0, -- in meter both for utm and deegrees, this used to avoid running ST_simplifyPreserveTopology for long lines lines with few points
--(_clean_info)._chaikins_nIterations int default 0, -- IF 0 NO CHAKINS WILL BE DONE,  A big value here make no sense because the number of points will increaes exponential )
--(_clean_info)._chaikins_max_length int default 0, --edge that are longer than this value will not be touched by _chaikins_min_degrees and _chaikins_max_degrees
--(_clean_info)._chaikins_min_degrees int default 0, -- The angle has to be less this given value, This is used to avoid to touch all angles.
--(_clean_info)._chaikins_max_degrees int default 0, -- OR the angle has to be greather than this given value, This is used to avoid to touch all angles
--(_clean_info)._chaikins_min_steep_angle_degrees int default 0, -- The angle has to be less this given value, This is used to avoid to touch all angles.
--(_clean_info)._chaikins_max_steep_angle_degrees int default 0-- OR The angle has to be greather than this given value, This is used to avoid to touch all angles

_border_topo_info_topology_name text,


-- the result off added lines
_added_lines_info rog_overlay_add_border_lines_type[],

INOUT _face_edges_to_remove_list add_border_lines_faces_to_remove[]

)
LANGUAGE plpgsql
AS $$
DECLARE

	-- holds dynamic sql to be able to use the same code for different
	command_string text;
	start_time_last_analyze_check timestamp WITH time zone;
	done_time timestamp WITH time zone;
	used_time real;
	used_time_inc_analyze real;
	start_time_delta_job timestamp WITH time zone;


	rec_new_line RECORD;

	new_line_edges_unique integer[];
	tmp_line_edges_all integer[];
	tmp_line_edges_exist integer[];
	tmp_line_edges_new integer[];
	tmp_face_edges_to_remove_list add_border_lines_faces_to_remove[];
	rate_edges_added float;
	rate_lines_added float;

	-- faces to remove
	face_edges_to_remove_list add_border_lines_faces_to_remove[] = '{}'::add_border_lines_faces_to_remove[];
	face_edges_to_remove_list_step int = 500;
	face_edges_to_remove_sone_steps int = 0;
	face_edges_to_remove add_border_lines_faces_to_remove;
	face_edges_to_remove_group add_border_lines_faces_to_remove[];


BEGIN

		-- check for small area and colapsed area requested
		IF (_clean_info).min_area_to_keep > 0 OR (_clean_info).min_st_is_empty_buffer_value > 0 THEN
			command_string = Format($fmt$
				SELECT table_input_order, ok_input_line_edges, input_line
				FROM unnest(%1$L::rog_overlay_add_border_lines_type[])
				ORDER BY table_input_order;
			$fmt$,
			_added_lines_info --01
			);

			FOR rec_new_line IN EXECUTE command_string
			LOOP

				IF Array_length(rec_new_line.ok_input_line_edges, 1) > 0 THEN

					face_edges_to_remove_list := topo_update.check_added_updated_edge(
						_border_topo_info_topology_name,
						rec_new_line.ok_input_line_edges,
						(_clean_info).min_area_to_keep,
						(_clean_info).min_st_is_empty_buffer_value,
						rec_new_line.table_input_order,
						NULL,
						(_topology_info).use_temp_topology
					)::add_border_lines_faces_to_remove[];

--					RAISE NOTICE 'New line in % rec_new_line.ok_input_line_edges % face_edges_to_remove_list %',
--					_border_topo_info_topology_name, rec_new_line.ok_input_line_edges, face_edges_to_remove_list;



					_face_edges_to_remove_list = _face_edges_to_remove_list||face_edges_to_remove_list;
				END IF;

			END LOOP;

		END IF; -- END check for small area and colapsed area requested




END
$$;

