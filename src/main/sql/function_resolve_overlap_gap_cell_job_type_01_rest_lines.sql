/**

Add and handle lines/rest not yet added yet move to final topology

*/

CREATE OR REPLACE PROCEDURE resolve_overlap_gap_cell_job_type_01_rest_lines (
	_tables_ext_metainfo rog_overlay_source_table_metainfo_type[], -- A list input tables meta info
	_topology_info resolve_overlap_data_topology_type,
	_border_topo_info topo_update.input_meta_info,
	_zero_topology_info_topology_snap_tolerance float,
	_table_name_result_prefix varchar,
	_clean_info resolve_overlap_data_clean_type, -- Different parameters used if need to clean up your data
	_job_list_name character varying,
	_overlapgap_grid varchar,
	_bb geometry,
	_cell_job_type int, -- Add lines 1 inside cell, 2 borderlines, 3 extract simple
	_loop_number int,
	_box_id int,
	res_split_border_lines rog_get_simplified_split_border_lines_result,
	_topology_is_utm boolean
)
LANGUAGE plpgsql
AS $$
DECLARE

	command_string text;
	start_time timestamp WITH time zone;
	start_time_delta_job timestamp WITH time zone;

	used_time real;
	has_edges boolean;

	added_lines_main_topo rog_overlay_add_border_lines_type[];
	face_edges_to_remove_list add_border_lines_faces_to_remove[];

	--  Method 1 topology.TopoGeo_addLinestring
	--  Method 2 topology.TopoGeo_LoadGeometry
	--  Method 3 select from tmp toplogy into master topology
	transfer_method_from_temp_2_final_topology int = (_topology_info).transfer_method_from_temp_2_final_topology;

	has_edges_temp_table_name text = (_topology_info).topology_name||'.edge_data_tmp_' || _box_id;

	command_string_temp_table_name text;
	row_count int;
	new_lines_to_master_topo rog_input_line_order[];


BEGIN


	start_time := Clock_timestamp();

	RAISE NOTICE 'Enter resolve_overlap_gap_cell_job_type_01_failed_lines using transfer_method_from_temp_2_final_topology % with for has_edges_temp_table_name % _loop_number % at timeofday:% for layer % for cell id % with bbox %',
	transfer_method_from_temp_2_final_topology, has_edges_temp_table_name, _loop_number, Timeofday(), (_topology_info).topology_name, _box_id, _bb;





	IF transfer_method_from_temp_2_final_topology = 1 THEN

		command_string_temp_table_name := Format($fmt$
		WITH lines_to_add AS (
			SELECT r.* FROM
			%3$s r
			ORDER BY ST_IsClosed(r.geom) desc, ST_Length(r.geom,true) ASC
			LIMIT 1000
		),
		lines_to_deleted AS (
			DELETE FROM %3$s r
			USING lines_to_add a
			WHERE a.edge_id = r.edge_id
			RETURNING r.*
		)
		SELECT topology.TopoGeo_addLinestring(%1$L,r.geom,%2$s)
		FROM lines_to_deleted r
				$fmt$,
				(_topology_info).topology_name,
				(_topology_info).topology_snap_tolerance,
				has_edges_temp_table_name
		);

	ELSIF transfer_method_from_temp_2_final_topology = 2 THEN
		command_string_temp_table_name := Format($fmt$
		WITH lines_to_add AS (
			SELECT r.* FROM
			%3$s r
			ORDER BY ST_IsClosed(r.geom) desc, ST_Length(r.geom,true) ASC
			LIMIT 1000
		),
		lines_to_deleted AS (
			DELETE FROM %3$s r
			USING lines_to_add a
			WHERE a.edge_id = r.edge_id
			RETURNING r.*
		)
		SELECT topology.TopoGeo_LoadGeometry(%1$L::varchar,r.linestring_list::geometry,%2$s::float8)
		FROM (
			SELECT ST_Collect(r.geom) AS linestring_list
			FROM lines_to_deleted r
		) r
		WHERE r.linestring_list IS NOT NULL
				$fmt$,
				(_topology_info).topology_name,
				(_topology_info).topology_snap_tolerance,
				has_edges_temp_table_name
		);
	END IF;


	command_string := Format('SELECT 1 FROM (SELECT * FROM to_regclass(%L) ) r WHERE to_regclass IS NOT NULL;',
	has_edges_temp_table_name);
	EXECUTE command_string into has_edges;

	RAISE NOTICE 'Try to data from unnlogged resolve_overlap_gap_cell_job_type_01 from table % if exits % at _loop_number %' ,
	has_edges_temp_table_name, has_edges, _loop_number;




	-- If there are any edges to be added to the final topology sort what lines to add now and what to ne added later
	IF (has_edges) THEN

		IF _loop_number = 2 THEN
			-- data left from loop 1 because of subtrans lock or error
			LOOP

				EXECUTE command_string_temp_table_name;
				GET DIAGNOSTICS row_count = ROW_COUNT;
				COMMIT;
				IF row_count = 0 OR row_count IS NULL THEN
					EXIT;  -- exit loop
				END IF;
			END LOOP;

		ELSE
			-- data left from loop 2 because of errors
			command_string := Format($fmt$
			SELECT ARRAY_agg(
			(r.geom,r.table_input_order,r.src_table_pk_column_value) ORDER BY r.table_input_order, ST_X(ST_StartPoint(r.geom)), ST_Y(ST_StartPoint(r.geom))
			)
			FROM
			( SELECT DISTINCT geom, 1000 AS table_input_order, NULL AS src_table_pk_column_value FROM %1$s ) r
			$fmt$,
			has_edges_temp_table_name
			);
			EXECUTE command_string into new_lines_to_master_topo;

			-- add data with retry to main topology
			CALL resolve_overlap_gap_cell_job_type_01_add_lines (
				_topology_info,
				_zero_topology_info_topology_snap_tolerance,
				_table_name_result_prefix,
				_clean_info,
				_cell_job_type,
				_bb,
				_box_id,
				(_topology_info).topology_name,
				(_topology_info).topology_snap_tolerance,
				_topology_is_utm,
				FALSE, -- _check_geeomtry_after_added_line -- By default we will check for min area values and sliver value in topology created
				FALSE, -- _do_analyze_topology_tables boolean, -- if many many threads and add lot of data to the same table from many theads nevner run with TRUE
				_tables_ext_metainfo::rog_overlay_source_table_metainfo_type[],
				NULL, -- max_time_in_secs_to -- add rest lines if set this will return caller given time is spent
				TRUE, -- sort_input_lines boolean, -- if true rog_input_line_order will sorted, but we only need to do that the first time
				new_lines_to_master_topo,
				added_lines_main_topo,
				face_edges_to_remove_list
			);
		END IF; --  _loop_number > 3
	END IF;

	-- if we get here no error
	command_string := Format('DROP TABLE IF EXISTS %s',has_edges_temp_table_name);
	EXECUTE command_string;


END
$$;

