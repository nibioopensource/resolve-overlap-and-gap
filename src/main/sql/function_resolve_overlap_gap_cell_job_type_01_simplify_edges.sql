/**
 * This function simplifies edges if requested.
 */
CREATE OR REPLACE PROCEDURE resolve_overlap_gap_cell_type_01_simplify_edges (
		_tables_ext_metainfo rog_overlay_source_table_metainfo_type[],
		_topology_info resolve_overlap_data_topology_type,
		_zero_topology_info_topology_snap_tolerance float,
		_table_name_result_prefix varchar,
		_clean_info resolve_overlap_data_clean_type,
		_job_list_name character varying,
		_overlapgap_grid varchar,
		_bb geometry,
		_cell_job_type int,
		_loop_number int,
		_box_id int,
		_border_topo_info topo_update.input_meta_info,
		_tmp_simplified_border_lines_name text,
		_added_lines_temp_topo rog_overlay_add_border_lines_type[],
		_res_split_border_lines rog_get_simplified_split_border_lines_result,
		_topology_is_utm boolean
)
LANGUAGE plpgsql
AS $$
DECLARE
		command_string text;
		start_time timestamp WITH time zone;
		used_time real;
		added_lines_temp_topo_fixed rog_overlay_add_border_lines_type[];
		edgeids_to_remove_after_simplify int[];
		face_edges_to_remove_after_simplify add_border_lines_faces_to_remove[];
		transfer_method_from_temp_2_final_topology int = (_topology_info).transfer_method_from_temp_2_final_topology;
		lines_to_update_edgeids RECORD;


BEGIN
		start_time := Clock_timestamp();

		RAISE NOTICE 'Enter resolve_overlap_gap_cell_type_01_simplify_edges with transfer_method_from_temp_2_final_topology %, _loop_number %, timeofday: %, topology_name: %, _box_id %, _bb %',
				transfer_method_from_temp_2_final_topology, _loop_number, Timeofday(), (_topology_info).topology_name, _box_id, _bb;


/**

		CALL resolve_overlap_gap_cell_type_01_update_edge_origin (
		_border_topo_info.topology_name,
		_bb,
		_box_id,
		_added_lines_temp_topo
		);


			-- Find all edge id that are missing in r.ok_input_line_edges, for instance because of a splitted edge
			command_string = FORMAT(
					$fmt$
						SELECT ARRAY_agg(e1.edge_id) missing_ok_edges, r.al FROM
							(SELECT unnest(%1$L::rog_overlay_add_border_lines_type[]) al) r
							LEFT JOIN %3$s.edge_data e1 ON NOT (e1.edge_id = ANY ((r.al).ok_input_line_edges)) AND
							ST_Intersects(e1.geom,(r.al).input_line) AND
							ST_Intersects(ST_buffer(ST_LineInterpolatePoint(e1.geom,0.5),0.000000001)::geometry ,(r.al).input_line)
							WHERE (r.al).src_table_pk_column_value IS NOT NULL AND (r.al).src_table_pk_column_value  != ''
						GROUP BY r.al
					$fmt$,
					_added_lines_temp_topo,
					NULL,
					_border_topo_info.topology_name
			);

			added_lines_temp_topo_fixed = '{}';
			-- loop throug all edges to check for missing edges add info øike source table info
			FOR lines_to_update_edgeids IN EXECUTE command_string LOOP
					-- RAISE NOTICE 'missing_input_line_edge_ids %, (lines_to_update_edgeids.al).ok_input_line_edges %, for tmp_ext_metainfo.table_input_order % '
					-- ,lines_to_update_edgeids.missing_ok_edges , (lines_to_update_edgeids.al).ok_input_line_edges, tmp_ext_metainfo.table_input_order;

					IF lines_to_update_edgeids.missing_ok_edges IS NOT NULL THEN
						added_lines_temp_topo_fixed=added_lines_temp_topo_fixed||
						(
						(lines_to_update_edgeids.missing_ok_edges||(lines_to_update_edgeids.al).ok_input_line_edges),
						(lines_to_update_edgeids.al).failed_line,
						(lines_to_update_edgeids.al).face_edges_to_remove,
						(lines_to_update_edgeids.al).table_input_order,
						(lines_to_update_edgeids.al).src_table_pk_column_value,
						(lines_to_update_edgeids.al).input_line,
						(lines_to_update_edgeids.al).selected_edge_face_id
						)::rog_overlay_add_border_lines_type;
					ELSE
						added_lines_temp_topo_fixed=added_lines_temp_topo_fixed||lines_to_update_edgeids.al;
					END IF;
			END LOOP; -- FOR rec_new_line IN EXECUTE command_find_edges_to_handle LOOP

*/

		-- if to simlify egdes
		IF (_clean_info).edge_input_tables_cleanup IS NOT NULL AND ARRAY_Length((_clean_info).edge_input_tables_cleanup,1) > 0 THEN
			-- Call simplify
			call rog_simplify_edges_in_topo_v2 (
			_tables_ext_metainfo::rog_overlay_source_table_metainfo_type[],
			_topology_info,
			_table_name_result_prefix,
			_clean_info,
			_bb,
			_border_topo_info.topology_name,
			_added_lines_temp_topo
			);



			CALL resolve_overlap_gap_cell_type_01_update_edge_origin (
			_border_topo_info.topology_name,
			_bb,
			_box_id,
			_added_lines_temp_topo
			);

			CALL resolve_overlap_gap_cell_job_type_01_find_edges_to_remove(
			_topology_info,
			_clean_info,
			_border_topo_info.topology_name,
			_added_lines_temp_topo,
			face_edges_to_remove_after_simplify
			);


			RAISE NOTICE 'size face_edges_to_remove_after_simplify % edgeids_to_remove_after_simplify %',
			ARRAY_LENGTH(face_edges_to_remove_after_simplify,1), ARRAY_LENGTH(edgeids_to_remove_after_simplify,1);

			-- call remove small areas because of simplify
			CALL topo_update.do_remove_small_areas_no_block (
				_border_topo_info.topology_name,
				(_clean_info).min_area_to_keep,
				(_clean_info).min_st_is_empty_buffer_value,
				_bb,
				_topology_is_utm,
				_cell_job_type ,
				(_res_split_border_lines).fixed_point_set,
				NULL, -- max_time_in_secs_to int, -- if this is bigger than 0 , then the code will return more this time ise used.
				face_edges_to_remove_after_simplify
			);
		END IF; -- done to simlify egdes

		-- Log execution time
		used_time := (EXTRACT(EPOCH FROM (Clock_timestamp() - start_time)));
		RAISE NOTICE 'Leave resolve_overlap_gap_cell_type_01_simplify_edges with transfer_method_from_temp_2_final_topology %, _loop_number %, used_time: %, timeofday: %, topology_name: %, _box_id %, _bb %',
				transfer_method_from_temp_2_final_topology, _loop_number, used_time, Timeofday(), (_topology_info).topology_name, _box_id, _bb;

END;
$$;
