/**

This function will remove edges that is related to small area's found when adding add new edges

*/

CREATE OR REPLACE PROCEDURE resolve_overlap_gap_cell_job_type_01_small_areas (
_topology_info resolve_overlap_data_topology_type,
---(_topology_info).topology_name varchar, -- The topology schema name where we store store sufaces and lines from the simple feature dataset and th efinal result
-- NB. Any exting data will related to topology_name will be deleted
--(_topology_info).topology_srid
--(_topology_info).topology_is_utm
--(_topology_info).topology_snap_tolerance float, -- this is tolerance used as base when creating the the postgis topolayer
--(_topology_info).create_topology_attrbute_tables boolean -- if this is true and we value for line_table_name we create attribute tables refferances to
-- this tables will have atrbuttes equal to the simple feauture tables for lines and feautures

-- a flag for testing the diff using a temporary tempology or not or insert directly into final source topology
-- this is a test related to performance and testing on that, the final results should be pretty much the same.
-- (_topology_info).use_temp_topology boolean

-- a flag to run checks like topology.ValidateTopology and https://github.com/larsop/resolve-overlap-and-gap
-- To run this does take time, spesially when working on verry very datasets so we trun this when needed.
-- In the final result there should be no Topology errors in the topology structure
-- In the simple feature result produced from Postgis Topology there should be overlaps
-- (_topology_info).do_qualitycheck_on_final_reseult boolean


_table_name_result_prefix varchar,
--(_topology_info).topology_name character varying,
--(_topology_info).topology_snap_tolerance float, -- this is tolerance used as base when creating the the postgis topolayer

_clean_info resolve_overlap_data_clean_type, -- different parameters used if need to clean up your data
--(_clean_info)._min_area_to_keep float default 0, -- if this a polygon  is below this limit it will merge into a neighbour polygon. The area is sqare meter.
--(_clean_info)._split_input_lines_at_vertex_n int default NULL, -- split all lines at given vertex num
--(_clean_info)._simplify_tolerance float default 0, -- is this is more than zero simply will called with
--(_clean_info)._simplify_max_average_vertex_length int default 0, -- in meter both for utm and deegrees, this used to avoid running ST_simplifyPreserveTopology for long lines lines with few points
--(_clean_info)._chaikins_nIterations int default 0, -- IF 0 NO CHAKINS WILL BE DONE,  A big value here make no sense because the number of points will increaes exponential )
--(_clean_info)._chaikins_max_length int default 0, --edge that are longer than this value will not be touched by _chaikins_min_degrees and _chaikins_max_degrees
--(_clean_info)._chaikins_min_degrees int default 0, -- The angle has to be less this given value, This is used to avoid to touch all angles.
--(_clean_info)._chaikins_max_degrees int default 0, -- OR the angle has to be greather than this given value, This is used to avoid to touch all angles
--(_clean_info)._chaikins_min_steep_angle_degrees int default 0, -- The angle has to be less this given value, This is used to avoid to touch all angles.
--(_clean_info)._chaikins_max_steep_angle_degrees int default 0-- OR The angle has to be greather than this given value, This is used to avoid to touch all angles

-- To limit and set as master area. This must be in the same coordsys as _new_sf_table_srid and must be polygon or a multipolgyon,
-- This will added as the first geometry into postgis topology. Data outside the master area will not be used.
-- If you send in a null geometry here this parameter will not have any effect.
--(_clean_info).input_boundary geometry

_res_split_border_lines_fixed_point_set geometry,

_cell_job_type int, -- add lines 1 inside cell, 2 boderlines, 3 exract simple

_bb geometry,

_border_topo_info_topology_name text,
_topology_is_utm boolean,

_first_run boolean,

max_time_in_secs_to int, -- if this is bigger than 0 , then the code will return when more than is time is used at given break point

INOUT _face_edges_to_remove_list add_border_lines_faces_to_remove[]



)
LANGUAGE plpgsql
AS $$
DECLARE

	-- holds dynamic sql to be able to use the same code for different
	command_string text;
	start_time_last_analyze timestamp WITH time zone;
	done_time timestamp WITH time zone;
	used_time real;
	used_time_total real;
	used_time_inc_analyze real;

	start_time_delta_job timestamp WITH time zone;

	line_edges_remove integer[];
	line_edges_remove_group integer[];
	line_edges_tmp integer[];


	rec_remove_edges_since_anlyze int = 0;
	rec_remove_edges_analyze int;
	rec_remove_counter int = 0;
	rec_remove_edges_total int = 0;

	-- faces to remove
	face_edges_to_remove_list_step int = 10;
	face_edges_to_remove_sone_steps int = 0;
	face_edges_to_remove add_border_lines_faces_to_remove;
	face_edges_to_remove_group add_border_lines_faces_to_remove[];



	-- This is lines that we failed to , because off a topo error
	lines_to_retry rog_overlay_add_border_failed_line[] = '{}'::rog_overlay_add_border_failed_line[];

	lines_to_retry_final rog_overlay_add_border_failed_line[] = '{}'::rog_overlay_add_border_failed_line[];

	tmp_line_to_retry rog_overlay_add_border_failed_line;


	sql text;


	--
	res_spike_remove_lines rog_input_line_order[];
	vacuum_cmd text[];

	more_edges_to_check_for_removel boolean;

	splite_lines_result geometry[];

	num_remove_lines_since_last_vacuum int;
	remove_i int;

	face_edges_to_done_remove_index int = 1;

	max_vacuum_analyze int = 2;
	done_vacuum_analyze int = 0;

	rate_edges_removed float;

BEGIN



		-- remove small polygons in temp in (_clean_info).min_area_to_keep
		IF (_clean_info).min_area_to_keep IS NOT NULL AND (_clean_info).min_area_to_keep > 0 AND
			_face_edges_to_remove_list IS NOT NULL AND array_length(_face_edges_to_remove_list,1) > 0 THEN


			rec_remove_edges_total = array_length(_face_edges_to_remove_list,1);

			start_time_delta_job := Clock_timestamp();
			start_time_last_analyze := Clock_timestamp();

			rec_remove_edges_analyze = 600;

			RAISE NOTICE 'Start clean small polygons for _face_edges_to_remove_list size % for face_table_name % at %',
			rec_remove_edges_total, _border_topo_info_topology_name|| '.face', Clock_timestamp();

			line_edges_remove = '{}';
			num_remove_lines_since_last_vacuum = 0;

			-- test topo_update.do_remove_small_areas_no_block
			CALL topo_update.do_remove_small_areas_no_block (
			_border_topo_info_topology_name,
			(_clean_info).min_area_to_keep,
			(_clean_info).min_st_is_empty_buffer_value,
			_bb,
			_topology_is_utm,
			_cell_job_type,
			_res_split_border_lines_fixed_point_set,
			max_time_in_secs_to,
			_face_edges_to_remove_list);


			/**
			FOREACH face_edges_to_remove IN ARRAY _face_edges_to_remove_list
			LOOP

				face_edges_to_done_remove_index = face_edges_to_done_remove_index + 1;

				line_edges_remove_group = (face_edges_to_remove).edge_ids;

				-- Start to remove lines if there are any lines to remove
				IF line_edges_remove_group IS NOT NULL THEN
					RAISE NOTICE 'Start to remove face_id % table_input_order % line_edges_remove_group %  for face_table_name % at %',
					(face_edges_to_remove).face_id, (face_edges_to_remove).table_input_order, line_edges_remove_group, _border_topo_info_topology_name|| '.face', Clock_timestamp();

					command_string := Format('CALL topo_update.do_merge_small_areas_no_block(%L,%L,%s,%L,%L,%L,%s,%L,%L,%L,%L)',
					_border_topo_info_topology_name,
					_table_name_result_prefix,
					(_clean_info).min_area_to_keep,
					(_clean_info).min_st_is_empty_buffer_value,
					_bb,
					_topology_is_utm,
					_cell_job_type,
					more_edges_to_check_for_removel, -- no neeed to check on more_edges_to_check_for_removel
					_res_split_border_lines_fixed_point_set,
					line_edges_remove_group,
					(face_edges_to_remove).table_input_order
					);

					face_edges_to_remove_sone_steps = face_edges_to_remove_sone_steps + 1;
					EXECUTE command_string;
					used_time := (Extract(EPOCH FROM (Clock_timestamp() - start_time_last_analyze)));

					rate_edges_removed = used_time/face_edges_to_remove_list_step;

					used_time_total = (Extract(EPOCH FROM (Clock_timestamp() - start_time_delta_job)));
				ELSE

					RAISE NOTICE 'Not to remove face_id % table_input_order % line_edges_remove_group %  for face_table_name % at %',
					(face_edges_to_remove).face_id, (face_edges_to_remove).table_input_order, line_edges_remove_group, _border_topo_info_topology_name|| '.face', Clock_timestamp();

				END IF;


			END LOOP;
			_face_edges_to_remove_list=_face_edges_to_remove_list[face_edges_to_done_remove_index:];

			*/

			used_time := (Extract(EPOCH FROM (Clock_timestamp() - start_time_delta_job)));
			RAISE NOTICE 'Done clean small polygons for _face_edges_to_remove_list size % face_edges_to_done_remove_index % edges left to remove is % for face_table_name % at % used_time: %',
			rec_remove_edges_total, face_edges_to_done_remove_index, array_length(_face_edges_to_remove_list,1), _border_topo_info_topology_name|| '.face', Clock_timestamp(), used_time;

		END IF;



END
$$;

