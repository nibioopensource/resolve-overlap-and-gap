/**

This function will pick move data to to the fina topology

*/

CREATE OR REPLACE PROCEDURE resolve_overlap_gap_cell_job_type_01_to_final_topo (
	_tables_ext_metainfo rog_overlay_source_table_metainfo_type[], -- A list input tables meta info
	_topology_info resolve_overlap_data_topology_type,
	_border_topo_info topo_update.input_meta_info,
	_zero_topology_info_topology_snap_tolerance float,
	_table_name_result_prefix varchar,
	_clean_info resolve_overlap_data_clean_type, -- Different parameters used if need to clean up your data
	_job_list_name character varying,
	_overlapgap_grid varchar,
	_bb geometry,
	_cell_job_type int, -- Add lines 1 inside cell, 2 borderlines, 3 extract simple
	_loop_number int,
	_box_id int,
	res_split_border_lines rog_get_simplified_split_border_lines_result,
	_topology_is_utm boolean
)
LANGUAGE plpgsql
AS $$
DECLARE

	command_string text;
	start_time timestamp WITH time zone;
	start_time_delta_job timestamp WITH time zone;

	used_time real;
	has_edges boolean;
	rec_new_edges_total int = 0;

	bb_boundary_geoms rog_bb_boundary_geoms;
	face_edges_to_remove_list add_border_lines_faces_to_remove[];

	--  Method 1 topology.TopoGeo_addLinestring
	--  Method 2 topology.TopoGeo_LoadGeometry
	--  Method 3 select from tmp toplogy into master topology
	transfer_method_from_temp_2_final_topology int = (_topology_info).transfer_method_from_temp_2_final_topology;

	has_edges_temp_table_name text = (_topology_info).topology_name||'.edge_data_tmp_' || _box_id;





BEGIN

	bb_boundary_geoms = topo_update.compute_bb_boundary_geoms(_bb,(_topology_info).topology_is_utm,_zero_topology_info_topology_snap_tolerance);

	start_time := Clock_timestamp();

	RAISE NOTICE 'Enter resolve_overlap_gap_cell_job_type_01_to_final_topo using transfer_method_from_temp_2_final_topology % with for has_edges_temp_table_name % _loop_number % at timeofday:% for layer % for cell id % with bbox %',
	transfer_method_from_temp_2_final_topology, has_edges_temp_table_name, _loop_number, Timeofday(), (_topology_info).topology_name, _box_id, _bb;




	command_string := Format('SELECT EXISTS(SELECT 1 from  %1$s.edge limit 1)', _border_topo_info.topology_name);

	EXECUTE command_string into has_edges;

	-- If there are any edges to heal border edges because off removing small small polygins
	IF (has_edges) THEN
		command_string := Format('SELECT topo_update.do_healedges_no_block(%1$L,%2$L,%3$L,%4$L)',
			_border_topo_info.topology_name,
			_bb,
			(_clean_info).split_input_lines_at_vertex_n,
			(res_split_border_lines).fixed_point_set
		);

		start_time_delta_job := Clock_timestamp();
		EXECUTE command_string;
		used_time := (Extract(EPOCH FROM (Clock_timestamp() - start_time_delta_job)));
		RAISE NOTICE 'Done heal for has_edges_temp_table_name % edge % at % used_time: % command_string %',
		has_edges_temp_table_name, _border_topo_info.topology_name, Clock_timestamp(),
		used_time,
		(CASE WHEN LENGTH(command_string) < 100 THEN command_string
		ELSE substring(command_string,0,99)
		END);

		-- this the old way of transferrinf data to final topology
		IF transfer_method_from_temp_2_final_topology IN (1,2) THEN

			RAISE NOTICE 'Start create with transfer_method_from_temp_2_final_topology % for has_edges_temp_table_name % at % used_time: % ',
			transfer_method_from_temp_2_final_topology, has_edges_temp_table_name, Clock_timestamp(), used_time;

			-- create table with all edges to use
			command_string := Format($fmt$
				DROP table IF EXISTS %1$s;

				CREATE unlogged table %1$s AS
				SELECT e.geom, 0 leqr, e.edge_id
				FROM  %2$s.edge_data e;

				ALTER TABLE %1$s set (autovacuum_enabled = off);

				CREATE INDEX ON %1$s USING GIST(geom);

				ANALYZE %1$s;

			$fmt$,
			has_edges_temp_table_name, --01
			_border_topo_info.topology_name
			);

			start_time_delta_job := Clock_timestamp();
			EXECUTE command_string;
			used_time := (Extract(EPOCH FROM (Clock_timestamp() - start_time_delta_job)));
			RAISE NOTICE 'Done create with transfer_method_from_temp_2_final_topology % for has_edges_temp_table_name % at % used_time: %',
			transfer_method_from_temp_2_final_topology, has_edges_temp_table_name, Clock_timestamp(), used_time;

			-- insert edges that intersects the cell wall and that will unioned with rows from _border_line_segments_short
			command_string = Format($fmt$
					WITH cell_border_lines AS
					(DELETE FROM %2$s r WHERE ST_Intersects(%3$L,r.geom) RETURNING r.geom)
					INSERT INTO %1$s(geom)
					SELECT cb.geom FROM cell_border_lines cb
					$fmt$,
					_table_name_result_prefix||'_border_line_segments_lf_eq_rf',
					has_edges_temp_table_name,
					(bb_boundary_geoms).boundary_geom
			);


			start_time_delta_job := Clock_timestamp();
			EXECUTE command_string;
			used_time := (Extract(EPOCH FROM (Clock_timestamp() - start_time_delta_job)));
			RAISE NOTICE 'For insert with transfer_method_from_temp_2_final_topology % in has_edges_temp_table_name % done insert edges % that intersects the cell wall and that will unioned with at % used_time: % command_string %',
			transfer_method_from_temp_2_final_topology, has_edges_temp_table_name, _table_name_result_prefix||'_border_line_segments_lf_eq_rf', Clock_timestamp(), used_time, command_string;
			-- END -- transfer_method_from_temp_2_final_topology IN (1,2) THEN
		ELSIF transfer_method_from_temp_2_final_topology = 3 THEN
			-- here transfer data to final topology by using copy

			RAISE NOTICE 'Start create with transfer_method_from_temp_2_final_topology % for has_edges_temp_table_name % at % used_time: % ',
			transfer_method_from_temp_2_final_topology, has_edges_temp_table_name, Clock_timestamp(), used_time;

			-- but we will not transfer all edges now
			-- insert edges that intersects the cell wall and that will unioned with rows from _border_line_segments_short
			command_string = Format($fmt$
				WITH cell_border_lines AS
				( SELECT ST_RemEdgeModFace(%2$L,r.edge_id), r.geom FROM %2$s.edge_data r WHERE ST_Intersects(%3$L,r.geom) )
				INSERT INTO %1$s(geom)
				SELECT cb.geom FROM cell_border_lines cb
				$fmt$,
				_table_name_result_prefix||'_border_line_segments_lf_eq_rf',
				_border_topo_info.topology_name,
				(bb_boundary_geoms).boundary_geom
			);

			start_time_delta_job := Clock_timestamp();
			EXECUTE command_string;
			used_time := (Extract(EPOCH FROM (Clock_timestamp() - start_time_delta_job)));
			RAISE NOTICE 'Done create with transfer_method_from_temp_2_final_topology % for has_edges_temp_table_name % at % used_time: % ',
			transfer_method_from_temp_2_final_topology, has_edges_temp_table_name, Clock_timestamp(), used_time;

			-- create command for inserting to final topology
			command_string = Format($fmt$
				SET session_replication_role = replica;

				INSERT INTO %1$s.edge_data(edge_id,start_node,end_node,next_left_edge,abs_next_left_edge,next_right_edge,abs_next_right_edge,left_face, right_face, geom)
				SELECT edge_id,start_node,end_node,next_left_edge,abs_next_left_edge,next_right_edge,abs_next_right_edge,left_face, right_face, geom
				FROM %2$s.edge_data;

				INSERT INTO %1$s.node(node_id,containing_face,geom)
				SELECT node_id,containing_face,geom
				FROM %2$s.node;

				INSERT INTO %1$s.face(face_id,mbr)
				SELECT face_id,mbr
				FROM %2$s.face WHERE face_id != 0;

				-- there are now data in relatino method
				--            INSERT INTO %1$s.relation(topogeo_id,layer_id,element_id,element_type)
				--            SELECT topogeo_id,layer_id,element_id,element_type FROM %2$s.relation;

				$fmt$,
				(_topology_info).topology_name,
				_border_topo_info.topology_name
			);
			EXECUTE command_string;

			-- TODO decide if this a error or not ????
			-- Doing this here may cause a problem where center of ST_MaximumInscribedCircle will not map back to the original value after the edges has changed.


			-- Add face mapping areas now if transfer_method_from_temp_2_final_topology = 3
			RAISE NOTICE 'start to map face % for has_edges_temp_table_name % at % used_time: % ',
			transfer_method_from_temp_2_final_topology, has_edges_temp_table_name, Clock_timestamp(), used_time;

/**
			-- This can not be done here because  https://gitlab.com/nibioopensource/resolve-overlap-and-gap//issues/89
			-- If we should do here it can not be done all faces, only faces that can not be mergede in later.
					-- map back faces to primary key using
					CALL resolve_overlap_map_faceid_2_input(
					_tables_ext_metainfo,
					_border_topo_info.topology_name,
					_topology_info,
					_table_name_result_prefix,
					_overlapgap_grid,
					_box_id,
					_bb,
					_cell_job_type);
*/

		END IF; -- transfer_method_from_temp_2_final_topology = 3 THEN


	END IF; -- has_edges

	execute Format('SET CONSTRAINTS ALL IMMEDIATE');


	PERFORM topology.DropTopology (_border_topo_info.topology_name);

	IF ARRAY_LENGTH((res_split_border_lines).invalid_lines_to_add,1) > 0 THEN
		-- insert invalid lines so they will retried at a later stage
		command_string = Format($fmt$
			INSERT INTO %s(line_geo_lost, error_info, geo)
			SELECT
			TRUE,
			'Invalid from job type in job_type 1' ,
			geo
			FROM unnest(%2$L::rog_input_line_order[])
		$fmt$,
		no_cutline_filename,
		(res_split_border_lines).invalid_lines_to_add
		);

		start_time_delta_job := Clock_timestamp();
		EXECUTE command_string;

		used_time := (Extract(EPOCH FROM (Clock_timestamp() - start_time_delta_job)));
		RAISE NOTICE 'For has_edges_temp_table_name % done insert % invalid edges into % at % used_time: % command_string %',
		has_edges_temp_table_name, ARRAY_LENGTH((res_split_border_lines).invalid_lines_to_add,1), no_cutline_filename, Clock_timestamp(), used_time, command_string;
	END IF;

	-- commit all work done so this can be picked up in later in loop 2
	-- COMMIT;

	used_time := (Extract(EPOCH FROM (Clock_timestamp() - start_time)));
	RAISE NOTICE 'Leave resolve_overlap_gap_cell_job_type_01_to_final_topo transfer_method_from_temp_2_final_topology % for has_edges_temp_table_name % with _loop_number % with used_time % at timeofday:% for layer % for cell id % with bbox %',
	transfer_method_from_temp_2_final_topology, has_edges_temp_table_name, _loop_number, used_time, Timeofday(), (_topology_info).topology_name, _box_id, _bb;

END
$$;

