/**
 * This function simplifies edges if requested.
 */
CREATE OR REPLACE PROCEDURE resolve_overlap_gap_cell_type_01_update_edge_origin (
		_topology_name text,
		_bb geometry,
		_box_id int,
		INOUT _added_lines_temp_topo rog_overlay_add_border_lines_type[]
)
LANGUAGE plpgsql
AS $$
DECLARE
		command_string text;
		start_time timestamp WITH time zone;
		used_time real;
		lines_to_update_edgeids RECORD;
		added_lines_temp_topo_fixed rog_overlay_add_border_lines_type[];
		added_lines_temp_topo_tmp rog_overlay_add_border_lines_type;


BEGIN
		start_time := Clock_timestamp();

--		RAISE NOTICE 'Enter resolve_overlap_gap_cell_type_01_update_edge_origin timeofday: %, topology_name: %, _box_id %, _bb %',
--				Timeofday(), _topology_name, _box_id, _bb;

		-- Find all edge id that are missing in r.ok_input_line_edges, for instance because of a splitted edge
		-- And match then against the input table
		command_string = FORMAT(
				$fmt$
					SELECT ARRAY_agg(e1.edge_id) missing_ok_edges, r.al FROM
						(SELECT unnest(%1$L::rog_overlay_add_border_lines_type[]) al) r
						LEFT JOIN %3$s.edge_data e1 ON NOT (e1.edge_id = ANY ((r.al).ok_input_line_edges)) AND
						ST_Intersects(e1.geom,(r.al).input_line) AND
						ST_Intersects(ST_buffer(ST_LineInterpolatePoint(e1.geom,0.5),0.000000001)::geometry ,(r.al).input_line)
						WHERE (r.al).src_table_pk_column_value IS NOT NULL AND (r.al).src_table_pk_column_value  != ''
					GROUP BY r.al
				$fmt$,
				_added_lines_temp_topo,
				NULL,
				_topology_name
		);

		added_lines_temp_topo_fixed = '{}';
		-- loop throug all edges to check for missing edges add info øike source table info
		FOR lines_to_update_edgeids IN EXECUTE command_string LOOP

				IF TRUE = ANY(SELECT unnest(lines_to_update_edgeids.missing_ok_edges) IS NULL) THEN
					added_lines_temp_topo_fixed=added_lines_temp_topo_fixed||lines_to_update_edgeids.al;
				ELSE
					--RAISE NOTICE 'For _topology_name % add missing_input_line_edge_ids %, (lines_to_update_edgeids.al).ok_input_line_edges % for %',
					--_topology_name,lines_to_update_edgeids.missing_ok_edges , (lines_to_update_edgeids.al).ok_input_line_edges, (lines_to_update_edgeids.al).table_input_order;

					added_lines_temp_topo_tmp = (
					(lines_to_update_edgeids.missing_ok_edges||(lines_to_update_edgeids.al).ok_input_line_edges),
					(lines_to_update_edgeids.al).failed_line,
					(lines_to_update_edgeids.al).face_edges_to_remove,
					(lines_to_update_edgeids.al).table_input_order,
					(lines_to_update_edgeids.al).src_table_pk_column_value,
					(lines_to_update_edgeids.al).input_line,
					(lines_to_update_edgeids.al).selected_edge_face_id
					)::rog_overlay_add_border_lines_type;

					added_lines_temp_topo_fixed=added_lines_temp_topo_fixed||added_lines_temp_topo_tmp;

				END IF;
		END LOOP; -- FOR rec_new_line IN EXECUTE command_find_edges_to_handle LOOP

		_added_lines_temp_topo = added_lines_temp_topo_fixed;

		-- Log execution time
		used_time := (EXTRACT(EPOCH FROM (Clock_timestamp() - start_time)));
		RAISE NOTICE 'Leave resolve_overlap_gap_cell_type_01_update_edge_origin used_time: %, timeofday: %, topology_name: %, _box_id %, _bb %',
				used_time, Timeofday(), _topology_name, _box_id, _bb;

END;
$$;
