	-- this is internal helper function
-- this is a function that creates unlogged tables and the the grid neeed when later checking this table for overlap and gaps.

CREATE OR REPLACE FUNCTION resolve_overlap_gap_init (
_input_data_array resolve_overlap_data_input_type[], -- A list input tables fro users, we need to remove removed
_tables_ext_metainfo rog_overlay_source_table_metainfo_type[], -- A new type with more info abouth input tables, to replace _input_data_array resolve_overlap_data_input_type[] in the future
_topology_info resolve_overlap_data_topology_type,

_zero_topology_info_topology_snap_tolerance float,
-- We get this as separate variable to handle 0 values
-- This will used when securing border line spacing between grid cell when topology_snap_tolerance  is zero

_clean_info resolve_overlap_data_clean_type, -- different parameters used if need to clean up your data
	--(_clean_info)._min_area_to_keep float default 0, -- if this a polygon  is below this limit it will merge into a neighbour polygon. The area is sqare meter.
	--(_clean_info)._simplify_tolerance float default 0, -- is this is more than zero simply will called with
	--(_clean_info)._simplify_max_average_vertex_length int default 0, -- in meter both for utm and deegrees, this used to avoid running ST_simplifyPreserveTopology for long lines lines with few points
	--(_clean_info)._chaikins_nIterations int default 0, -- IF 0 NO CHAKINS WILL BE DONE,  A big value here make no sense because the number of points will increaes exponential )
	--(_clean_info)._chaikins_max_length int default 0, --edge that are longer than this value will not be touched by _chaikins_min_degrees and _chaikins_max_degrees
	--(_clean_info)._chaikins_min_degrees int default 0, -- The angle has to be less this given value, This is used to avoid to touch all angles.
	--(_clean_info)._chaikins_max_degrees int default 0, -- OR the angle has to be greather than this given value, This is used to avoid to touch all angles
	--(_clean_info)._chaikins_min_steep_angle_degrees int default 0, -- The angle has to be less this given value, This is used to avoid to touch all angles.
	--(_clean_info)._chaikins_max_steep_angle_degrees int default 0-- OR The angle has to be greather than this given value, This is used to avoid to touch all angles

	-- To limit and set as master area. This must be in the same coordsys as _new_sf_table_srid and must be polygon or a multipolgyon,
	-- This will added as the first geometry into postgis topology. Data outside the master area will not be used.
	-- If you send in a null geometry here this parameter will not have any effect.
	--(_clean_info).input_boundary geometry

_table_name_result_prefix varchar,
_max_rows_in_each_cell int, -- this is the max number rows that intersects with box before it's split into 4 new boxes
_overlapgap_grid varchar, -- The schema.table name of the grid that will be created and used to break data up in to managle pieces
_debug_options resolve_overlap_data_debug_options_type -- this used to set different debug parameters and is usually null

)
	RETURNS INTEGER
	AS $$
DECLARE
	-- used to run commands
	command_string text;
	-- the number of cells created in the grid
	num_cells_master_grid int;
	-- drop result tables
	drop_result_tables_ boolean = TRUE;
	-- table to keep track of results
	-- the name of the content based based on the grid so each thread work on diffrent cell
	overlapgap_grid_metagrid_name varchar;
	try_with_grid_metagrid_size int;
	overlapgap_grid_metagrid_name_num_cells int;

	layer_centroid geometry;
	i int;
	num_metagrids_try_loop int;
	last_grid_table_size int = 0;
	next_grid_table_num int = 1;
	tmp_overlapgap_grid varchar;
	unique_id_type varchar;
	_input_data resolve_overlap_data_input_type; -- this paramater should be removed.

	-- this the name the geometry column in the content based grid to be used
	-- this has to be the same and not depend on columns name of the input
	-- TODO this should input config settings
	cell_geo_column_name text = 'cell_bbox';

	tmp_ext_metainfo rog_overlay_source_table_metainfo_type;

	-- Used to create views
	view_column_list text[];
	view_column_name varchar;
	view_left_join_list varchar ='';

	-- Used to create content based grid
	cbg_tables text[];

	-- this is master input area from (_clean_info).input_boundary if (_clean_info).input_boundary IS NOT NULL;
	-- input_boundary_boandary_area IS NULL all area is used
	input_boundary_boandary_area geometry;

	cell_min_area float;
	min_distance int;

	cbg_schema_table_name text;
	new_cbg_table_name text;

	--  Method 1 topology.TopoGeo_addLinestring
	--  Method 2 topology.TopoGeo_LoadGeometry
	--  Method 3 select from tmp toplogy into master topology
	transfer_method_from_temp_2_final_topology int = (_topology_info).transfer_method_from_temp_2_final_topology;


BEGIN

-- We use this temporary, may be removed
	_input_data = _input_data_array[1];

	-- ############################# START # Create Topology master working schema
	-- drop schema if exists
	IF (drop_result_tables_ = TRUE AND (
		SELECT Count(*)
		FROM topology.topology
		WHERE name = (_topology_info).topology_name) = 1) THEN
		EXECUTE Format('SELECT topology.droptopology(%s)', Quote_literal((_topology_info).topology_name));
	END IF;
	-- drop this schema in case it exists
	EXECUTE Format('DROP SCHEMA IF EXISTS %s CASCADE', (_topology_info).topology_name);
	-- create topology
	EXECUTE Format('SELECT topology.createtopology(%s,%s,%s)', Quote_literal((_topology_info).topology_name), (_topology_info).topology_srid, (_topology_info).topology_snap_tolerance);
	-- Set unlogged to increase performance

	EXECUTE Format('GRANT USAGE ON SCHEMA %s TO PUBLIC', (_topology_info).topology_name);

	EXECUTE Format('ALTER TABLE %s.edge_data SET unlogged', (_topology_info).topology_name);
	EXECUTE Format('ALTER TABLE %s.node SET unlogged', (_topology_info).topology_name);
	EXECUTE Format('ALTER TABLE %s.face SET unlogged', (_topology_info).topology_name);
	EXECUTE Format('ALTER TABLE %s.relation SET unlogged', (_topology_info).topology_name);

	EXECUTE Format('ALTER TABLE %s.edge_data SET (autovacuum_enabled = off)', (_topology_info).topology_name);
	EXECUTE Format('ALTER TABLE %s.node SET (autovacuum_enabled = off)', (_topology_info).topology_name);
	EXECUTE Format('ALTER TABLE %s.face SET (autovacuum_enabled = off)', (_topology_info).topology_name);
	EXECUTE Format('ALTER TABLE %s.relation SET (autovacuum_enabled = off)', (_topology_info).topology_name);

	IF transfer_method_from_temp_2_final_topology != 3 THEN

		-- Create indexes
		EXECUTE Format('CREATE INDEX ON %s.relation(layer_id)', (_topology_info).topology_name);
		EXECUTE Format('CREATE INDEX ON %s.relation(abs(element_id))', (_topology_info).topology_name);
		EXECUTE Format('CREATE INDEX IF NOT EXISTS relation_element_id_idx ON %s.relation(element_id)', (_topology_info).topology_name);
		EXECUTE Format('CREATE INDEX ON %s.relation(topogeo_id)', (_topology_info).topology_name);
		--EXECUTE Format('CREATE INDEX ON %s.edge_data USING GIST (geom)', (_topology_info).topology_name);
		EXECUTE Format('CREATE INDEX IF NOT EXISTS edge_data_abs_next_left_edge_idx ON %1$s.edge_data(abs_next_left_edge)', (_topology_info).topology_name);
		EXECUTE Format('CREATE INDEX IF NOT EXISTS edge_data_abs_next_right_edge_idx ON %1$s.edge_data(abs_next_right_edge)', (_topology_info).topology_name);

		--EXECUTE Format('DROP INDEX %s.node_containing_face_idx', (_topology_info).topology_name);
		--EXECUTE Format('CREATE INDEX IF NOT EXISTS node_containing_face_idx ON %s.node(containing_face) WHERE containing_face IS NOT NULL', (_topology_info).topology_name);

	ELSE

		command_string =  Format($fmt$
			-- DROP FOREIGN KEY CONSTRAINTS
			ALTER TABLE %1$s.edge_data drop CONSTRAINT IF EXISTS end_node_exists; --" FOREIGN KEY (end_node) REFERENCES topo_test_09_topo.node(node_id)
			ALTER TABLE %1$s.edge_data drop CONSTRAINT IF EXISTS left_face_exists; --" FOREIGN KEY (left_face) REFERENCES topo_test_09_topo.face(face_id)
			ALTER TABLE %1$s.edge_data drop CONSTRAINT IF EXISTS next_left_edge_exists; --" FOREIGN KEY (abs_next_left_edge) REFERENCES topo_test_09_topo.edge_data(edge_id) DEFERRABLE INITIALLY DEFERRED
			ALTER TABLE %1$s.edge_data drop CONSTRAINT IF EXISTS next_right_edge_exists; --" FOREIGN KEY (abs_next_right_edge) REFERENCES topo_test_09_topo.edge_data(edge_id) DEFERRABLE INITIALLY DEFERRED
			ALTER TABLE %1$s.edge_data drop CONSTRAINT IF EXISTS right_face_exists; --" FOREIGN KEY (right_face) REFERENCES topo_test_09_topo.face(face_id)
			ALTER TABLE %1$s.edge_data drop CONSTRAINT IF EXISTS start_node_exists; --" FOREIGN KEY (start_node) REFERENCES topo_test_09_topo.node(node_id)
			ALTER TABLE %1$s.node drop CONSTRAINT IF EXISTS face_exists; --" FOREIGN KEY (containing_face) REFERENCES topo_test_09_topo.face(face_id)

			-- DROP primary keys and primary keys index
			ALTER TABLE %1$s.edge_data drop CONSTRAINT IF EXISTS edge_data_pkey;
			ALTER TABLE %1$s.node drop CONSTRAINT IF EXISTS node_primary_key;
			ALTER TABLE %1$s.face drop CONSTRAINT IF EXISTS face_primary_key;

			-- DROP other indexs
			-- DROP INDEX IF EXISTS %1$s.edge_data_pkey; --" PRIMARY KEY, btree (edge_id)
			DROP INDEX IF EXISTS %1$s.edge_data_abs_next_left_edge_idx; -- btree (abs_next_left_edge)
			DROP INDEX IF EXISTS %1$s.edge_data_abs_next_right_edge_idx; -- btree (abs_next_right_edge)
			DROP INDEX IF EXISTS %1$s.edge_end_node_idx; -- btree (end_node)
			DROP INDEX IF EXISTS %1$s.edge_gist; -- gist (geom)
			DROP INDEX IF EXISTS %1$s.edge_left_face_idx; -- btree (left_face)
			DROP INDEX IF EXISTS %1$s.edge_right_face_idx; -- btree (right_face)
			DROP INDEX IF EXISTS %1$s.edge_start_node_idx; -- btree (start_node)

			-- DROP INDEX IF EXISTS %1$s.node_primary_key; -- PRIMARY KEY, btree (node_id)
			DROP INDEX IF EXISTS %1$s.node_containing_face_idx; -- btree (containing_face)
			DROP INDEX IF EXISTS %1$s.node_gist; -- gist (geom)

			-- DROP INDEX IF EXISTS %1$s.face_primary_key; -- PRIMARY KEY, btree (face_id)
			DROP INDEX IF EXISTS %1$s.face_gist; -- gist (mbr)
			$fmt$,
			(_topology_info).topology_name

		);

		RAISE NOTICE 'DROP constraints, primary keys, indexes command_string % ', command_string;
		EXECUTE command_string;

	END IF;

	-- ----------------------------- DONE - Create Topology master working schema
	-- TODO find out what to do with help tables, they are now created in src/main/extern_pgtopo_update_sql/help_tables_for_logging.sql
	-- TODO what to do with /Users/lop/dev/git/topologi/skog/src/main/sql/table_border_line_segments.sql
	-- ############################# START # Handle content based grid init
	-- drop content based grid table if exits
	IF (drop_result_tables_ = TRUE) THEN
		EXECUTE Format('DROP TABLE IF EXISTS %s', _overlapgap_grid);
	END IF;


	-- create a content based grid table for input data
	-- ----------------------------- STARt - Handle content based grid init
	IF _debug_options.pre_defined_metagrid_tables IS NULL THEN

		IF (_clean_info).input_boundary IS NOT NULL THEN
			input_boundary_boandary_area = ST_Union((_clean_info).input_boundary);
		END IF;

		-- create the master grid
		num_cells_master_grid := resolve_overlap_gap_cb_grid(
		_overlapgap_grid,
		_max_rows_in_each_cell,
		_tables_ext_metainfo,
		(_topology_info).topology_srid,
		cell_geo_column_name,
		input_boundary_boandary_area);


		-- This setting is tested with 45112 grid cells
		num_metagrids_try_loop = 40;
		--try_with_grid_metagrid_size = (num_cells_master_grid/num_metagrids_try_loop)*0.3;
		try_with_grid_metagrid_size = 2;




		tmp_overlapgap_grid := _overlapgap_grid;

		EXECUTE Format('SELECT Min(ST_Area(c.%2$s,true)) from %1$s c', tmp_overlapgap_grid,cell_geo_column_name) INTO cell_min_area;
		min_distance = round(SQRT(cell_min_area))*8;

		-- Add colums to hold info about valadation errors if ran, if ok then validation is run and no error found
		EXECUTE Format('ALTER TABLE %s ADD column validate_topology_ok boolean', _overlapgap_grid);
		EXECUTE Format('ALTER TABLE %s ADD column validate_topology_exception text', _overlapgap_grid);
		-- Add colums to hold info about produconig simple feature
		EXECUTE Format('ALTER TABLE %s ADD column simple_feat_num integer default 0', _overlapgap_grid);
		EXECUTE Format('ALTER TABLE %s ADD column simple_feat_exception text', _overlapgap_grid);

		FOR i IN 1..num_metagrids_try_loop LOOP
			overlapgap_grid_metagrid_name := _overlapgap_grid||'_metagrid_'||to_char(next_grid_table_num, 'fm0000');

			EXECUTE Format('CREATE TABLE %s( id serial, %s geometry(Geometry,%s))',
			overlapgap_grid_metagrid_name, cell_geo_column_name, (_topology_info).topology_srid);

			command_string := Format('INSERT INTO %1$s(%5$s)
			 SELECT ST_transform(q_grid.cell,%2$s)::Geometry(geometry,%2$s) as %5$s
			 FROM (
				 SELECT (r.cell_row).cell_geom AS cell FROM (SELECT unnest(cbg_content_based_balanced_grid) cell_row from cbg_content_based_balanced_grid(array[ %3$L],%6$L,%7$s,%4$s)) r
			 ) as q_grid',
			 overlapgap_grid_metagrid_name, --01
			 (_topology_info).topology_srid, --02
			 _overlapgap_grid || ' '|| cell_geo_column_name, --03
			 try_with_grid_metagrid_size, --04
			 cell_geo_column_name, --05
			 ST_GeomFromText('POINT(0 0)'), --06
			 min_distance); --07

			-- execute the sql command
			EXECUTE command_string;
			-- count number of cells in grid
			command_string := Format('SELECT count(*) from %s', overlapgap_grid_metagrid_name);
			-- execute the sql command
			EXECUTE command_string INTO overlapgap_grid_metagrid_name_num_cells;


			IF last_grid_table_size = overlapgap_grid_metagrid_name_num_cells THEN
				EXECUTE Format('DROP TABLE %s', overlapgap_grid_metagrid_name);
			ELSE
				EXECUTE Format('UPDATE %1$s set %2$s = ST_Buffer(%2$s,-%3$s)',
				overlapgap_grid_metagrid_name,
				cell_geo_column_name,
				(_topology_info).topology_snap_tolerance);

				-- Create Index
				EXECUTE Format('CREATE INDEX ON %s USING GIST (%s)', overlapgap_grid_metagrid_name, cell_geo_column_name);

				-- Create a table of grid_metagrid_01 lines
				EXECUTE Format('CREATE TABLE %1$s( id serial, %2$s geometry(Geometry,%3$s))',
				overlapgap_grid_metagrid_name||'_lines',
				cell_geo_column_name,
				(_topology_info).topology_srid);

				command_string := Format('INSERT INTO %1$s(%2$s)
				SELECT distinct (ST_Dump(topo_update.get_single_lineparts(ST_Boundary(%2$s)))).geom as %2$s
				from %3$s',
				overlapgap_grid_metagrid_name||'_lines',
				cell_geo_column_name,
				overlapgap_grid_metagrid_name);
				EXECUTE command_string;
				-- Create Index
				EXECUTE Format('CREATE INDEX ON %s USING GIST (%s)',
				overlapgap_grid_metagrid_name||'_lines',
				cell_geo_column_name);

	-- If there is more than one grid cell we to split the extra cell space added acording to each grid cell
	IF overlapgap_grid_metagrid_name_num_cells > 1 THEN

		-- Create splitted boandary area and update meta grid table 01 more than one row.
		EXECUTE Format($fmt$
		WITH global_bbox AS (
		SELECT ST_Envelope(ST_Union(%4$s)) geom
		FROM %5$s
		),
		outer_buffer AS (
		SELECT
	ST_MakePolygon(
	ST_ExteriorRing(ST_Expand(r.geom,%2$s)),
	ARRAY[ST_ExteriorRing(ST_Expand(r.geom,-%2$s))]
	)::geometry(Polygon,%3$s) geom
	FROM
	global_bbox r
	),
	cut_lines AS (
	SELECT  ST_Union(ST_LineExtend(%4$s,%2$s,%2$s)) geom
	FROM
	%6$s l,
	global_bbox gb
	WHERE ST_Intersects(ST_ExteriorRing(gb.geom),l.%4$s) AND
	NOT ST_CoveredBy(l.%4$s,ST_Buffer(ST_ExteriorRing(gb.geom),%2$s,'quad_segs=2'))
	),
	outer_buffer_polygons AS (
	SELECT (ST_Dump(ST_Split(b.geom,cl.geom))).geom geom
	FROM
	cut_lines cl,
	outer_buffer b
	),
	new_outer_polygons AS (
	SELECT ST_Union(obp.geom) geom, g.id
	FROM
	outer_buffer_polygons obp,
	%5$s g
	WHERE obp.geom && g.%4$s AND ST_intersects(ST_Buffer(ST_Centroid(obp.geom),%2$s,'quad_segs=2'),g.%4$s)
	GROUP BY g.id, obp.geom, g.%4$s
	)
	--update_new_outer_result AS (
	UPDATE %5$s g SET %4$s = ST_Union(g.%4$s,og.geom)
	FROM new_outer_polygons og
	WHERE og.id = g.id;

		$fmt$,
				overlapgap_grid_metagrid_name||'_lines'||'_outer_boundary_cells', --01
				_zero_topology_info_topology_snap_tolerance, --02
				(_topology_info).topology_srid, --03
				cell_geo_column_name, --04
				overlapgap_grid_metagrid_name, --05
				overlapgap_grid_metagrid_name||'_lines' --06
		);
	ELSE
		-- Create splitted boandary area and update meta grid table 01 but with only rows
		-- So then we can do a simple expand
		EXECUTE Format($fmt$
	--update_new_outer_result AS (
	UPDATE %1$s r SET %2$s = ST_Expand(r.%2$s,%3$s);

		$fmt$,
				overlapgap_grid_metagrid_name, --01
				cell_geo_column_name, --02
				_zero_topology_info_topology_snap_tolerance --03
		);

	END IF;

				next_grid_table_num := next_grid_table_num + 1;
				last_grid_table_size := overlapgap_grid_metagrid_name_num_cells;
				tmp_overlapgap_grid := overlapgap_grid_metagrid_name;
			END IF;




					--  Will not be used any more, may removed it ???
			IF i = 1 THEN
				EXECUTE Format('ALTER TABLE %s ADD column inside_cell boolean default false', _overlapgap_grid);
				EXECUTE Format('UPDATE %1$s g SET inside_cell = true from %2$s t where ST_covers(t.%3$s,g.%3$s)',
				_overlapgap_grid,
				overlapgap_grid_metagrid_name,
				cell_geo_column_name);

				EXECUTE Format('ALTER TABLE %s ADD column grid_thread_cell int default 0', _overlapgap_grid);
				EXECUTE Format('UPDATE %1$s g SET grid_thread_cell = t.id from %2$s t where ST_Intersects(t.%3$s,g.%3$s)',
				_overlapgap_grid,
				overlapgap_grid_metagrid_name,
				cell_geo_column_name);
			END IF;

			RAISE NOTICE 'For metatable % try_with_grid_metagrid_size % num_cells_master_grid %, min_distance %, i %, overlapgap_grid_metagrid_name_num_cells %',
			overlapgap_grid_metagrid_name, try_with_grid_metagrid_size, num_cells_master_grid, min_distance, i, overlapgap_grid_metagrid_name_num_cells;

			EXIT WHEN overlapgap_grid_metagrid_name_num_cells < 2 ;

		EXECUTE Format('SELECT Max(ST_Area(c.%2$s,true)) from %1$s c', tmp_overlapgap_grid,cell_geo_column_name) INTO cell_min_area;
		min_distance = min_distance + min_distance/2;


		END LOOP;


		EXECUTE Format('ALTER TABLE %s ADD column num_polygons int default 0', _overlapgap_grid);
		EXECUTE Format('UPDATE %1$s g SET num_polygons = r.num_polygons FROM
		(select count(t.*) as num_polygons, g.id from %2$s t, %1$s g where t.%3$s && g.%4$s group by g.id) as r
		where r.id = g.id',
		_overlapgap_grid,
		(_input_data).polygon_table_name,
		(_input_data).polygon_table_geo_collumn,
		cell_geo_column_name
		);

				-- find centroid
		EXECUTE Format('SELECT ST_Centroid(ST_Union(%s)) from %s',
		cell_geo_column_name, _overlapgap_grid) into layer_centroid;

		EXECUTE Format('ALTER TABLE %s ADD column row_number int default 0', _overlapgap_grid);

		EXECUTE Format('UPDATE %1$s g SET row_number = r.row_number FROM
		(select id,
		ROW_NUMBER()  OVER (PARTITION BY grid_thread_cell
		order by ST_distance(%2$s,%3$L) desc)
		from %4$s) as r
		where r.id = g.id',
		_overlapgap_grid,
		cell_geo_column_name,
		layer_centroid,
		_overlapgap_grid);

		-- ----------------------------- DONE - Handle content based grid init



		-- make input (_clean_info).input_boundary equal content based grod
		IF (_clean_info).input_boundary IS NOT NULL THEN
			-- set master inpuet equal to boadry of the grid cells
			command_string := Format('SELECT ST_Union(c.%2$s) from %1$s c', _overlapgap_grid,cell_geo_column_name);
			-- execute the sql command
			EXECUTE command_string INTO _clean_info.input_boundary;
		END IF;
	ELSE
		RAISE NOTICE 'Use predifened content content based grid % ', _debug_options.pre_defined_metagrid_tables;

		cbg_tables = _debug_options.pre_defined_metagrid_tables;

		FOREACH cbg_schema_table_name IN ARRAY cbg_tables LOOP
			RAISE NOTICE 'table_name %' , cbg_schema_table_name;
			new_cbg_table_name = (string_to_array(_overlapgap_grid, '.')::text[])[1]||'.'||(string_to_array(cbg_schema_table_name, '.')::text[])[2];


			EXECUTE Format($fmt$

			CREATE TABLE %1$s ( like  %2$s);
			INSERT INTO %1$s SELECT * FROM %2$s;
			ALTER TABLE %1$s ADD primary key(id);

			ALTER TABLE %1$s ADD COLUMN IF NOT EXISTS numrows_intersects int;

			CREATE INDEX ON %1$s USING GIST (cell_bbox);

			$fmt$,
				new_cbg_table_name,
				cbg_schema_table_name
			);

		END LOOP;

	END IF;

	-- Create a table for used for validate toplogy
	EXECUTE Format('CREATE TABLE %s( id int primary key, %s geometry(Geometry,%s), validate_result topology.validatetopology_returntype[])',
	_overlapgap_grid||'_validate_topology',
	cell_geo_column_name,
	(_topology_info).topology_srid);


	-- Create a table of grid lines based content in grid tables
	EXECUTE Format('CREATE TABLE %1$s( id serial, %2$s geometry(Geometry,%3$s))',
	_overlapgap_grid||'_lines',
	cell_geo_column_name,
	(_topology_info).topology_srid);
	command_string := Format('INSERT INTO %1$s(%2$s)
	SELECT distinct (ST_Dump(topo_update.get_single_lineparts(ST_Boundary(%2$s)))).geom as %2$s
	FROM %3$s',
	_overlapgap_grid||'_lines',
	cell_geo_column_name,
	_overlapgap_grid);
	EXECUTE command_string;
	-- Create Index
	EXECUTE Format('CREATE INDEX ON %s USING GIST (%s)',
	_overlapgap_grid||'_lines',
	cell_geo_column_name);

	-- Check number of metagrids for first grid
	EXECUTE Format($fmt$
	SELECT count(*) FROM %1$s
	$fmt$,
	_overlapgap_grid||'_metagrid_'||to_char(1, 'fm0000')
	) INTO i;



	-- ----------------------------- Create help tables
	-- TOOD find out how to handle log tables used for debug

EXECUTE Format('CREATE TABLE %1$s (
	id serial PRIMARY KEY NOT NULL,
	log_time timestamp DEFAULT Now(),
	line_geo_lost boolean,
	input_geo_is_valid boolean,
	table_input_order int,
	error_info text,
	d_state text,
	d_msg text,
	d_detail text,
	d_hint text,
	d_context text,
	geo Geometry(LineString, %2$s),
	other_geom Geometry(Geometry, %2$s) -- other geoms that are not a line string
)',_table_name_result_prefix||'_no_cut_line_failed',(_topology_info).topology_srid);


EXECUTE Format('CREATE UNLOGGED TABLE %s (
	id serial PRIMARY KEY NOT NULL, log_time timestamp DEFAULT Now(), execute_time real, info text,
	geo Geometry(LineString, %s)
)',_table_name_result_prefix||'_long_time_logl',(_topology_info).topology_srid);

EXECUTE Format('CREATE UNLOGGED TABLE %s (
	id serial PRIMARY KEY NOT NULL, log_time timestamp DEFAULT Now(), execute_time real, info text,
	sql text, geo Geometry(Polygon, %s)
)',_table_name_result_prefix||'_long_time_log2',(_topology_info).topology_srid);


-- This is verry small border lines created by cutting put very small part of the lines crossing a cell border
-- This also keep tracks points not to touch when working temp tables
command_string = Format($fmt$
CREATE UNLOGGED TABLE %1$s (
	id serial PRIMARY KEY NOT NULL,
	log_time timestamp DEFAULT Now(),
	geom Geometry(LineString, %2$s),
	table_input_order int, -- the order off which tables are added
	src_table_pk_column_value text, -- the primary key of input
	point_geo Geometry(Point, %2$s)
);
ALTER TABLE %1$s ADD column column_data_as_json jsonb;
CREATE INDEX ON %1$s USING GIST (geom);
$fmt$,
_table_name_result_prefix||'_border_line_segments_short',
(_topology_info).topology_srid
);
EXECUTE command_string;

-- This border line segements where left_face equal right_face
-- This rows will be union together with intersection rows from _border_line_segments into one line
command_string = Format($fmt$
CREATE UNLOGGED TABLE %1$s (
	id serial PRIMARY KEY NOT NULL,
	added_to_final boolean default false,
	geom Geometry(LineString, %2$s
)
);
CREATE INDEX ON %1$s USING GIST (geom);
CREATE INDEX ON %1$s(added_to_final);

$fmt$,
_table_name_result_prefix||'_border_line_segments_lf_eq_rf',
(_topology_info).topology_srid
);
EXECUTE command_string;

-- This is where we find the final result where we merge the result from _border_line_segments_lf_eq_rf and
command_string = Format($fmt$
CREATE UNLOGGED TABLE %1$s (
	id serial PRIMARY KEY NOT NULL,
	log_time timestamp DEFAULT Now(),
	added_to_master boolean default false,
	geom Geometry(LineString, %2$s),
	table_input_order int, -- the order off which tables are added
	src_table_pk_column_value text, -- the primary key of input
	point_geo Geometry(Point, %2$s)
);
ALTER TABLE %1$s ADD column column_data_as_json jsonb;
CREATE INDEX ON %1$s USING GIST (geom);
CREATE INDEX ON %1$s(added_to_master);
$fmt$,
_table_name_result_prefix||'_border_line_segments_final',
(_topology_info).topology_srid
);
EXECUTE command_string;


-- create table to keep more info for debug related grid cells for debugging
EXECUTE Format('CREATE UNLOGGED TABLE %s (
	id serial PRIMARY KEY NOT NULL, log_time timestamp DEFAULT Now(), cell_id int, log_key varchar, geo Geometry(Geometry, %s)
)',_table_name_result_prefix||'_grid_log_info',(_topology_info).topology_srid,(_topology_info).topology_srid);


EXECUTE Format('CREATE UNLOGGED TABLE %s (
	edge_id int PRIMARY KEY NOT NULL,
	add_at timestamp DEFAULT statement_timestamp(),
	is_handled_at timestamp DEFAULT NULL,
	not_removed_reasonn text DEFAULT NULL,
	more_info text DEFAULT NULL,
	table_input_order int,
	geo Geometry(LineString, %s),
	face_area float,
	face_id int
)',_table_name_result_prefix||'_edges_to_remove',(_topology_info).topology_srid);

EXECUTE Format('CREATE INDEX ON %s USING GIST (%s)',_table_name_result_prefix||'_edges_to_remove', 'geo');


	-- create a table to keep track off mpping old ids to new face id's
	EXECUTE Format('CREATE UNLOGGED TABLE %s (
		table_input_order integer,
		grid_cell_id integer,
		src_table_pk_column_value text,
		face_mbr Geometry(Polygon, %s),
		face_id int,
		pk_value_set boolean,
		PRIMARY KEY(table_input_order,face_id,src_table_pk_column_value)
	)',
	_table_name_result_prefix||'_org_pk_value_to_face_id',
	(_topology_info).topology_srid
	);

	-- Create the simple feature result table with a fixed structure since we can have data form many different source
	command_string = FORMAT(
		$fmt$
		CREATE UNLOGGED TABLE %1$s(
			face_id int primary key,
			geom public.geometry(Polygon,%2$s)
		)
		$fmt$,
		_table_name_result_prefix,
		(_topology_info).topology_srid
	);

	EXECUTE command_string;

	-- for each craete a primary key value column tables_ext_metainfo
	FOREACH tmp_ext_metainfo IN ARRAY _tables_ext_metainfo
	LOOP
		command_string = FORMAT($fmt$

		ALTER TABLE %1$s ADD column %2$I %3$s[] DEFAULT NULL;
		ALTER TABLE %1$s ADD column %4$I %3$s DEFAULT NULL;

		$fmt$,
		_table_name_result_prefix, --01
		ros_table_metainfo_pk_list_name(tmp_ext_metainfo), --02
		tmp_ext_metainfo.table_pk_type, --03
		ros_table_metainfo_pk_name(tmp_ext_metainfo) --04
		);

		EXECUTE command_string;

	END LOOP;


	FOREACH tmp_ext_metainfo IN ARRAY _tables_ext_metainfo
	LOOP
			FOR view_column_name IN SELECT attname FROM pg_attribute WHERE  attrelid = tmp_ext_metainfo.table_name::regclass AND attnum > 0 AND NOT attisdropped ORDER BY attnum
			LOOP
				RAISE NOTICE ' view_column_name %', view_column_name;

				IF view_column_name = tmp_ext_metainfo.table_pk_colum THEN
					command_string = FORMAT('CASE WHEN mt.%1$I IS NULL THEN TRUE ELSE FALSE END AS %2$I',
					ros_table_metainfo_pk_name(tmp_ext_metainfo),
					ros_table_metainfo_gaps_name(tmp_ext_metainfo)
					);
					view_column_list=ARRAY_APPEND(view_column_list,command_string);

					command_string = FORMAT('CASE WHEN mt.%1$I IS NOT NULL THEN TRUE ELSE FALSE END AS %2$I',
					ros_table_metainfo_pk_list_name(tmp_ext_metainfo),
					ros_table_metainfo_overlaps_name(tmp_ext_metainfo)
					);
					view_column_list=ARRAY_APPEND(view_column_list,command_string);

				END IF;

			END LOOP;

			FOR view_column_name IN SELECT attname FROM pg_attribute WHERE  attrelid = tmp_ext_metainfo.table_name::regclass AND attnum > 0 AND NOT attisdropped ORDER BY attnum
			LOOP
				RAISE NOTICE ' view_column_name %', view_column_name;

				IF view_column_name != tmp_ext_metainfo.table_pk_colum AND view_column_name != tmp_ext_metainfo.table_geo_column THEN
					command_string = FORMAT('%1$s.%2$I AS %3$I ',
					ros_table_metainfo_column_prefix(tmp_ext_metainfo),
					view_column_name,
					ros_table_metainfo_column_prefix(tmp_ext_metainfo)||'_'||view_column_name
					);
					view_column_list=ARRAY_APPEND(view_column_list,command_string);
				END IF;
			END LOOP;

	-- add ref and cond to join tables for columns for each table
		command_string = FORMAT(' LEFT JOIN %1$s %2$s ON %2$s.%3$s=mt.%4$I ',
		tmp_ext_metainfo.table_name,
		ros_table_metainfo_column_prefix(tmp_ext_metainfo),
		tmp_ext_metainfo.table_pk_colum,
		ros_table_metainfo_pk_name(tmp_ext_metainfo)
		);
		view_left_join_list = view_left_join_list||command_string;
	END LOOP;

	-- create view
	command_string = FORMAT(
		$fmt$
		CREATE VIEW %1$s AS
		SELECT mt.*, %3$s
		FROM  %2$s mt %4$s ;

		GRANT SELECT ON %1$s TO PUBLIC;

		$fmt$,
		_table_name_result_prefix||'_v',
		_table_name_result_prefix,
		array_to_string(view_column_list,','),
		view_left_join_list

	);

	RAISE NOTICE 'test view : %', command_string;

	EXECUTE command_string;


-- Find a get type primary key
EXECUTE Format('SELECT vsr_get_data_type(%L,%L)',(_input_data).polygon_table_name,(_input_data).polygon_table_pk_column) into unique_id_type;

-- Add an extra column to hold a list of other intersections surfaces using unique type
EXECUTE Format('ALTER TABLE %s ADD column _other_intersect_id_list %s[]',_table_name_result_prefix,unique_id_type);

-- Add an extra column to hold a list of all intersections surfaces using unique type
EXECUTE Format('ALTER TABLE %s ADD column _all_intersect_id_list %s[]',_table_name_result_prefix,unique_id_type);

-- Add an extra column to hold info about all intersection columns also it self
EXECUTE Format('ALTER TABLE %s ADD column _input_geo_is_valid boolean',_table_name_result_prefix);

IF (_topology_info).create_topology_attrbute_tables = true THEN
	IF(_input_data).line_table_name is not null THEN
		EXECUTE Format('CREATE UNLOGGED TABLE %s(%s) ',(_topology_info).topology_name||'.edge_attributes',(_input_data).line_table_other_collumns_def);
		EXECUTE Format('SELECT topology.AddTopoGeometryColumn(%L, %L, %L, %L, %L)',
		(_topology_info).topology_name, (_topology_info).topology_name,'edge_attributes',(_input_data).line_table_geo_collumn,'LINESTRING');
	ELSE
		-- TODO REMOVE HACK when we find out how to do this
		EXECUTE Format('CREATE UNLOGGED TABLE %s(%s) ',(_topology_info).topology_name||'.edge_attributes','id serial primary key,id_test integer');
		EXECUTE Format('SELECT topology.AddTopoGeometryColumn(%L, %L, %L, %L, %L)',
		(_topology_info).topology_name, (_topology_info).topology_name,'edge_attributes',(_input_data).line_table_geo_collumn,'LINESTRING');
	END IF;

	EXECUTE Format('CREATE UNLOGGED TABLE %s(%s) ',(_topology_info).topology_name||'.face_attributes',(_input_data).polygon_table_other_collumns_def);
	EXECUTE Format('SELECT topology.AddTopoGeometryColumn(%L, %L, %L, %L, %L)',
	(_topology_info).topology_name, (_topology_info).topology_name,'face_attributes',(_input_data).polygon_table_geo_collumn,'POLYGON');

END IF;






	RETURN num_cells_master_grid;
END;
$$
LANGUAGE plpgsql;
