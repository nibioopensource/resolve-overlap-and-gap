-- This is the main funtion used resolve overlap and gap
CREATE OR REPLACE FUNCTION resolve_overlap_gap_job_list (
_input_data_array resolve_overlap_data_input_type[], -- A list input tables
--(_input_data).polygon_table_name varchar, -- The table to resolv, imcluding schema name
--(_input_data).polygon_table_pk_column varchar, -- The primary of the input table
--(_input_data).polygon_table_geo_collumn varchar, -- the name of geometry column on the table to analyze
--(_input_data).table_srid int, -- the srid for the given geo column on the table analyze
--(_input_data).utm boolean,
_tables_ext_metainfo rog_overlay_source_table_metainfo_type[], -- A new type with more info abouth input tables, to replace _input_data_array resolve_overlap_data_input_type[] in the future
_topology_info resolve_overlap_data_topology_type,
---(_topology_info).topology_name varchar, -- The topology schema name where we store store sufaces and lines from the simple feature dataset and th efinal result
-- NB. Any exting data will related to topology_name will be deleted
--(_topology_info).topology_srid
--(_topology_info).topology_snap_tolerance float, -- this is tolerance used as base when creating the the postgis topolayer
--(_topology_info).create_topology_attrbute_tables boolean -- if this is true and we value for line_table_name we create attribute tables refferances to
-- this tables will have atrbuttes equal to the simple feauture tables for lines and feautures

_zero_topology_info_topology_snap_tolerance float,
-- We get this as separate variable to handle 0 values
-- This will used when securing border line spacing between grid cell when topology_snap_tolerance  is zero

_overlapgap_grid varchar, -- the name of the content based grid table
_table_name_result_prefix varchar,
--(_topology_info).topology_name varchar, -- The topology schema name where we store store sufaces and lines from the simple feature dataset. -- NB. Any exting data will related to topology_name will be deleted
--(_topology_info).topology_snap_tolerance float, -- the tolrence to be used when add data
_job_list_name varchar, -- the name of job_list table, this table is ued to track of done jobs
_clean_info resolve_overlap_data_clean_type, -- different parameters used if need to clean up your data
--(_clean_info).simplify_tolerance float, -- is this is more than zero simply will called with
--(_clean_info).do_chaikins boolean, -- here we will use chaikins togehter with simply to smooth lines
--(_clean_info).min_area_to_keep float, -- if this a polygon  is below this limit it will merge into a neighbour polygon. The area is sqare meter.
_max_parallel_jobs int, -- this is the max number of paralell jobs to run. There must be at least the same number of free connections
_cell_job_type int,-- add lines 1 inside cell, 2 boderlines, 3 exract simple,
_loop_number int
)
  RETURNS void
  AS $$
DECLARE
  command_string text;
  -- the number of cells
  num_cells int;
  -- just to create sql
  command_string_var varchar;
  -- the sql used for blocking cells
  sql_to_block_cmd varchar;
  -- the sql resilve simple feature data
  sql_to_run_grid varchar;

  -- This is used to sure that no lines can snap to each other between two cells
  -- The size wil the this value multiplied by (_topology_info).topology_snap_tolerance;
  -- TODO make this as parameter
  cell_boundary_tolerance_with_multi real = 12;

  job_list_row_count int;

  meta_grid_exist varchar;

  -- this the name the geometry column in the content based grid to be used
  -- this has to be the same and not depend on columns name of the input
  -- TODO this should input config settings
  cell_geo_column_name text = 'cell_bbox';

    -- A temp variable for first table in the list
  _input_data resolve_overlap_data_input_type;


BEGIN

  IF _input_data_array IS NULL OR ARRAY_LENGTH(_input_data_array,1) = 0 THEN
    RAISE EXCEPTION 'Not valid _input_data_array for % for topology %', _input_data_array, topology_schema_name;
  END IF;


  -- We use this temporary, may be removed
  _input_data = _input_data_array[1];


  -- ############################# START # create jobList tables
  command_string := Format('DROP table if exists %s', _job_list_name);
  RAISE NOTICE 'command_string %', command_string;
  EXECUTE command_string;
  command_string := Format('CREATE unlogged table %s(id serial, start_time timestamp with time zone, numrows_intersects int, inside_cell boolean, grid_thread_cell int, num_polygons int, row_number int, sql_to_block varchar, sql_to_run varchar, cell_geo geometry(geometry,%s),block_bb Geometry(geometry,%s), blocked_by_id int, worker_id int)',
  _job_list_name,(_topology_info).topology_srid,(_topology_info).topology_srid);
  RAISE NOTICE 'command_string %', command_string;
  EXECUTE command_string;
  -- create a table for don jobs
  command_string := Format('DROP table if exists %s', _job_list_name || '_donejobs');
  RAISE NOTICE 'command_string %', command_string;
  EXECUTE command_string;
  command_string := Format('CREATE unlogged table %s(id int, done_time timestamp with time zone default clock_timestamp(), analyze_time timestamp with time zone )',
  _job_list_name || '_donejobs');
  RAISE NOTICE 'command_string %', command_string;
  EXECUTE command_string;


  sql_to_run_grid := Format('CALL resolve_overlap_gap_single_cell(
  %L,%L,%L,%s,%s,
  %L,
  %s,%s,',
  _input_data_array,
  _tables_ext_metainfo,
  _topology_info,
  _zero_topology_info_topology_snap_tolerance,
  Quote_literal(_table_name_result_prefix),
  _clean_info,
  Quote_literal(_job_list_name),
  Quote_literal(_overlapgap_grid));

  RAISE NOTICE 'sql_to_run_grid %',
  (CASE WHEN LENGTH(sql_to_run_grid) < 100 THEN sql_to_run_grid
  ELSE substring(sql_to_run_grid,0,99)
  END);

  sql_to_block_cmd := Format('select resolve_overlap_gap_block_cell(%s,%s,%s,%s,',
  Quote_literal((_input_data).polygon_table_name), Quote_literal((_input_data).polygon_table_geo_collumn), Quote_literal((_input_data).polygon_table_pk_column), Quote_literal(_job_list_name));



  -- add inside cell polygons
  -- TODO solve how to find r.geom
  IF _cell_job_type = 4 or _cell_job_type = 5 THEN
    command_string := Format('
    INSERT INTO %s(inside_cell,grid_thread_cell,num_polygons,row_number,sql_to_run,cell_geo,sql_to_block)
    SELECT
    true as inside_cell,
    r.grid_thread_cell,
    r.num_polygons,
    r.row_number,
    %s||quote_literal(r.'||cell_geo_column_name||'::Varchar)||%s as sql_to_run,
    r.'||cell_geo_column_name||' as cell_geo,
    %s||quote_literal(r.'||cell_geo_column_name||'::Varchar)||%s as sql_to_block
    from (
    select
    r.grid_thread_cell,
    r.num_polygons,
    r.row_number,
    l.geom as %s ,
    ROW_NUMBER() OVER(PARTITION BY l.geom order by r.num_polygons desc) as cell_number from
    (
      select ST_Expand((ST_Dump(geom)).geom,%s) as geom from (
        select topo_update.get_single_lineparts((ST_Dump(ST_Union(geom))).geom) as geom
        from (
          select ST_ExteriorRing(%s) as geom from %s
        ) as r
      ) as r
    ) as l,
    %s as r
    where r.%s && l.geom
    ) as r WHERE r.cell_number = 1',
    _job_list_name,
    Quote_literal(sql_to_run_grid),
    Quote_literal(',' || _cell_job_type || ','),
    Quote_literal(sql_to_block_cmd),
    Quote_literal(');'),

    cell_geo_column_name,
    _zero_topology_info_topology_snap_tolerance * cell_boundary_tolerance_with_multi,
    cell_geo_column_name,
    _overlapgap_grid,
    _overlapgap_grid,
    cell_geo_column_name);
    EXECUTE command_string;

  ELSIF _cell_job_type IN (2,3) THEN
    command_string := Format('select to_regclass(%L)',_overlapgap_grid||'_metagrid_'||to_char(_loop_number, 'fm0000') );
    execute command_string into meta_grid_exist;

    IF meta_grid_exist is not null THEN
    command_string := Format('
    INSERT INTO %s(inside_cell,grid_thread_cell,num_polygons,row_number,sql_to_run,cell_geo,sql_to_block)
    SELECT
    false inside_cell,
    0 grid_thread_cell,
    0 num_polygons,
    0 row_number,
    %s||quote_literal(r.'||cell_geo_column_name||'::Varchar)||%s as sql_to_run,
    r.'||cell_geo_column_name||' as cell_geo,
    %s||quote_literal(r.'||cell_geo_column_name||'::Varchar)||%s as sql_to_block
    from %s r',
    _job_list_name,
    Quote_literal(sql_to_run_grid),
    Quote_literal(',' || _cell_job_type || ','),
    Quote_literal(sql_to_block_cmd),
    Quote_literal(');'),
    _overlapgap_grid||'_metagrid_'||to_char(_loop_number, 'fm0000'));

    RAISE NOTICE 'command_string %',
    (CASE WHEN LENGTH(command_string) < 100 THEN command_string
  	ELSE substring(command_string,0,99)
  	END)
    ;
    EXECUTE command_string;
    END IF;
  ELSE
    command_string := Format('
    INSERT INTO %s(numrows_intersects,inside_cell,grid_thread_cell,num_polygons,row_number,sql_to_run,cell_geo,sql_to_block)
    SELECT
    r.numrows_intersects,
    r.inside_cell,
    r.grid_thread_cell,
    r.num_polygons,
    r.row_number,
    %s||quote_literal(r.'||cell_geo_column_name||'::Varchar)||%s as sql_to_run,
    r.'||cell_geo_column_name||' as cell_geo,
    %s||quote_literal(r.'||cell_geo_column_name||'::Varchar)||%s as sql_to_block
    from %s r',
    _job_list_name,
    Quote_literal(sql_to_run_grid),
    Quote_literal(',' || _cell_job_type || ','),
    Quote_literal(sql_to_block_cmd),
    Quote_literal(');'),
    _overlapgap_grid);

    RAISE NOTICE 'command_string %',
    (CASE WHEN LENGTH(command_string) < 100 THEN command_string
  	ELSE substring(command_string,0,99)
  	END);
    EXECUTE command_string;

  END IF;


  GET DIAGNOSTICS job_list_row_count = ROW_COUNT;

  RAISE NOTICE 'Created joblist  % with % rows for cell_job_type % and _loop_number % with meta_grid_exist table name %',
  _job_list_name, job_list_row_count, _cell_job_type, _loop_number, meta_grid_exist ;


  EXECUTE Format('CREATE INDEX ON %s USING GIST (cell_geo);', _job_list_name);
  EXECUTE Format('CREATE INDEX ON %s USING GIST (block_bb);', _job_list_name);
  EXECUTE Format('CREATE INDEX ON %s (id);', _job_list_name);
  EXECUTE Format('CREATE INDEX ON %s (num_polygons);', _job_list_name);
  EXECUTE Format('CREATE INDEX ON %s (inside_cell);', _job_list_name);
  EXECUTE Format('CREATE INDEX ON %s (id);', _job_list_name || '_donejobs');

  EXECUTE Format('UPDATE %1$s g SET worker_id = MOD((id-1),%2$s) + 1', _job_list_name, _max_parallel_jobs);

  IF _cell_job_type = 4 or _cell_job_type = 5 THEN
    command_string := Format('UPDATE %1$s u
    SET num_polygons = r1.num_polygons
    FROM
    (
    SELECT count(*) num_polygons, a1.id
    FROM
    %1$s a1,
    %2$s a2
    WHERE a1.cell_geo && a2.%3$s
    GROUP BY a1.id
    ) r1
    WHERE r1.id = u.id',
    _job_list_name, _table_name_result_prefix||'_border_line_segments_final', 'geom');
    RAISE NOTICE 'command_string %', command_string;
    EXECUTE command_string;


    -- TOD remove this
    EXECUTE Format('UPDATE %1$s g SET inside_cell = false
    from
    %4$s as t
    where ST_Intersects(g.cell_geo,t.%3$s)',
    _job_list_name,
    _overlapgap_grid||'_metagrid_'||to_char(1, 'fm0000'),
    cell_geo_column_name,
    _overlapgap_grid||'_metagrid_'||to_char(1, 'fm0000')||'_lines');

  END IF;

END;
$$
LANGUAGE plpgsql;

