-- This is the main funtion used resolve overlap and gap
CREATE OR REPLACE FUNCTION resolve_overlap_gap_mergeborder_lines (
  _topology_info resolve_overlap_data_topology_type,
  table_name_result_prefix text
)
  RETURNS void
  AS $$
DECLARE

command_string text;
num_rows int;

BEGIN

command_string = Format($fmt$

CREATE TEMP TABLE rows_unique_short_border ON COMMIT DROP AS
SELECT b.* FROM
(
  SELECT min(b.id) AS id
  FROM %2$s AS b
  GROUP BY   ST_Normalize(geom) , b.table_input_order
) nb,
%2$s AS b
WHERE nb.id = b.id
ORDER BY nb.id;

CREATE INDEX ON rows_unique_short_border USING GIST (geom);

ALTER TABLE rows_unique_short_border ADD CONSTRAINT all_area_pkey PRIMARY KEY (id);

WITH
-- Get left eq right face that has interctions with a start point in cell border lines
rows_connect_startpoint AS (
              SELECT DISTINCT ON (ce.id)
              b.id AS short_id, b.geom AS short_geom, b.table_input_order, b.src_table_pk_column_value, ce.id AS startpoint_id, ce.geom AS startpoint_geom
              FROM
              rows_unique_short_border AS b,
              %3$s AS ce
              WHERE ST_Intersects(ST_StartPoint(b.geom), ce.geom)
              ORDER BY ce.id
),
-- Get left eq right face that has interctions with a end point in cell border lines
rows_connect_endpoint AS (
              SELECT DISTINCT ON (ce.id)
              b.id AS short_id, b.geom AS short_geom, b.table_input_order, b.src_table_pk_column_value, ce.id AS endpoint_id, ce.geom AS endpoint_geom
              FROM
              rows_unique_short_border AS b,
              %3$s AS ce
              WHERE ST_Intersects(ST_EndPoint(b.geom), ce.geom)
              AND NOT EXISTS (SELECT TRUE FROM rows_connect_startpoint s WHERE ce.id = s.startpoint_id)
              ORDER BY ce.id
),
-- Get the union of start and end points
rows_connect_end_or_start AS (
              SELECT
              ss.short_id, ss.short_geom, ss.table_input_order, ss.src_table_pk_column_value, startpoint_id AS rlid, startpoint_geom AS rlgeom
              FROM rows_connect_startpoint ss
              UNION ALL
              SELECT
              se.short_id, se.short_geom, se.table_input_order, se.src_table_pk_column_value, endpoint_id AS rlid, endpoint_geom AS rlgeom
              FROM rows_connect_endpoint se
),
-- Get left over short borderes
rows_cell_wall_border_rest AS (
              SELECT DISTINCT ON (b.id)
              b.id AS short_id, b.geom AS short_geom, b.table_input_order, b.src_table_pk_column_value
              FROM
              rows_unique_short_border AS b
              WHERE NOT EXISTS (SELECT TRUE FROM rows_connect_end_or_start s WHERE b.id = s.short_id )
              ORDER by b.id
),
-- Get left eq right face that has no interctions should not be many
rows_connect_rest AS (
              SELECT DISTINCT ON (ce.id)
              ce.id AS short_id, ST_SetSrid('LINESTRING EMPTY'::Geometry,%4$s) AS short_geom, 100000 AS table_input_order, '' AS src_table_pk_column_value, ce.id AS endpoint_id, ce.geom AS endpoint_geom
              FROM
              %3$s AS ce
              WHERE NOT EXISTS (SELECT TRUE FROM rows_connect_startpoint s WHERE ce.id = s.startpoint_id )
              ORDER BY ce.id
),
rows_connect AS (
              SELECT
              ss.short_id, ss.short_geom, ss.table_input_order, ss.src_table_pk_column_value, rlid, rlgeom
              FROM rows_connect_end_or_start ss
              UNION ALL
              SELECT
              se.short_id, se.short_geom, se.table_input_order, se.src_table_pk_column_value, -1 AS rlid, short_geom AS rlgeom
              FROM rows_cell_wall_border_rest se
              UNION ALL
              SELECT
              se.short_id, se.short_geom, se.table_input_order, se.src_table_pk_column_value, endpoint_id AS rlid, endpoint_geom AS rlgeom
              FROM rows_connect_rest se
),
rows_connect_update AS (
            UPDATE %3$s u SET added_to_final = true
            FROM rows_connect b
            WHERE b.rlid = u.id
            RETURNING b.*
),
rows_connect_group AS (
              SELECT
              short_id, short_geom, min(table_input_order) table_input_order, src_table_pk_column_value, ARRAY_AGG(rlid) rlids, ST_Collect(rlgeom) rlgeoms
              FROM rows_connect_update
              GROUP BY short_id, short_geom, src_table_pk_column_value
),
rows_all AS (
              SELECT
              ST_LineMerge(ST_Union(ST_Collect(short_geom,rlgeoms))) line,
              /** we need a way to remove those to point added
              CASE
              WHEN cg.short_geom IS NOT NULL AND ARRAY_LENGTH(rlids,1) = 2 THEN
                   ST_LineMerge(ST_Collect(short_geom,rlgeoms))
              WHEN cg.short_geom IS NOT NULL AND ARRAY_LENGTH(rlids,1) = 1 THEN
                   ST_LineMerge(ST_Collect(short_geom,rlgeoms))
              WHEN cg.short_geom IS NOT NULL AND ARRAY_LENGTH(rlids,1) = 0 THEN
                   ST_LineMerge(ST_Collect(short_geom,rlgeoms))
              ELSE ST_LineMerge(ST_Collect(short_geom,rlgeoms))
              END AS line,
              */
              table_input_order,
              src_table_pk_column_value
              FROM rows_connect_group cg GROUP BY cg.table_input_order, cg.src_table_pk_column_value, cg.short_geom, cg.rlgeoms, cg.rlids
              UNION ALL
              SELECT short_geom line, table_input_order, src_table_pk_column_value
              FROM rows_cell_wall_border_rest

)

/**

This slows down performance when adding cell border lies

,
rows_cluster AS (
              SELECT
              ST_LineMerge(ST_Union(r.line)) line,
              min(table_input_order) table_input_order,
              0 AS src_table_pk_column_value
              FROM (
                SELECT b.table_input_order, b.line,
                ST_ClusterIntersectingWin(b.line) OVER () AS cluster
                FROM rows_all AS b
              ) r  group by cluster
)
*/
          INSERT INTO %1$s(geom, table_input_order,  src_table_pk_column_value)
          SELECT (ST_Dump(ur.line)).geom AS geom, ur.table_input_order, ur.src_table_pk_column_value
          FROM rows_all ur;

          $fmt$,
          table_name_result_prefix||'_border_line_segments_final',
          table_name_result_prefix||'_border_line_segments_short',
          table_name_result_prefix||'_border_line_segments_lf_eq_rf',
          (_topology_info).topology_srid
        );

      -- analyze
      EXECUTE command_string;

        command_string = Format($fmt$
          ANALYZE %1$s;
          ANALYZE %3$s;
          $fmt$,
          table_name_result_prefix||'_border_line_segments_final',
          table_name_result_prefix||'_border_line_segments_short',
          table_name_result_prefix||'_border_line_segments_lf_eq_rf'
        );
      EXECUTE command_string;


      -- update lines that may be connected because of line merge or node

        command_string = Format($fmt$
          WITH rows AS (
              SELECT ce.id lr_eq_lf_id, ce.geom lr
              FROM
              %3$s AS ce
              WHERE added_to_final = false
          ),
          up_rows AS (
            UPDATE %3$s u SET added_to_final = true
            FROM rows b
            WHERE b.lr_eq_lf_id = u.id
            RETURNING b.lr
          )
          INSERT INTO %1$s(geom,table_input_order)
          SELECT ur.lr AS geom, 1000 AS table_input_order
          FROM up_rows ur;

          $fmt$,
          table_name_result_prefix||'_border_line_segments_final',
          table_name_result_prefix||'_border_line_segments_short',
          table_name_result_prefix||'_border_line_segments_lf_eq_rf'
        );

          EXECUTE command_string;
        GET DIAGNOSTICS num_rows = ROW_COUNT;
        RAISE NOTICE 'Update % rows with connected lines for %', num_rows, (_topology_info).topology_name;

END;
$$
LANGUAGE plpgsql;

