-- this function that adds indexes and set table to logged soooo


CREATE OR REPLACE FUNCTION resolve_overlap_gap_post (
  _tables_ext_metainfo rog_overlay_source_table_metainfo_type[],
  _input_data resolve_overlap_data_input_type,
  _topology_info resolve_overlap_data_topology_type,
  _table_name_result_prefix varchar
)
  RETURNS VOID
  AS $$
DECLARE
  -- used to run commands
  command_string text;
  tmp_ext_metainfo rog_overlay_source_table_metainfo_type;
  sf_table_geom_colemn_comment_ref text;
  sf_table_src_ref text;
  view_column_name varchar;


BEGIN

IF ARRAY_LENGTH(_tables_ext_metainfo,1) > 1 THEN
  -- Create the simple feature result table with a fixed structure since we can have data form many different source

  command_string = FORMAT(
  $fmt$

  CREATE INDEX ON %1$s USING GIST (%2$I);

  GRANT SELECT ON TABLE %1$s TO PUBLIC;

  ALTER TABLE %1$s SET logged;

  $fmt$,
  _table_name_result_prefix,
  'geom'
  );

  EXECUTE command_string;

  -- for each craete a primary key value column tables_ext_metainfo
  FOREACH tmp_ext_metainfo IN ARRAY _tables_ext_metainfo
  LOOP
    command_string = FORMAT($fmt$

    CREATE INDEX ON %1$s(%2$I);

    $fmt$,
    _table_name_result_prefix,
    ros_table_metainfo_pk_name(tmp_ext_metainfo)
    );

    EXECUTE command_string;

  END LOOP;

  -- ADD COMMENTS
  perform resolve_overlap_gap_post_add_comments(
  _tables_ext_metainfo,
  (_topology_info).topology_name,
  _table_name_result_prefix,
  _table_name_result_prefix||'_v');


ELSE

  IF (_topology_info).create_topology_attrbute_tables = true OR (_topology_info).create_topology_attrbute_tables = true THEN
    EXECUTE Format('ALTER TABLE %s.relation SET logged', (_topology_info).topology_name);
    EXECUTE Format('ALTER TABLE %s.face SET logged', (_topology_info).topology_name);
    EXECUTE Format('ALTER TABLE %s.node SET logged', (_topology_info).topology_name);
    EXECUTE Format('ALTER TABLE %s.edge_data SET logged', (_topology_info).topology_name);
  END IF;



  IF (_topology_info).create_topology_attrbute_tables = true and (_input_data).line_table_name is not null THEN
    EXECUTE Format('ALTER TABLE %s SET logged',(_topology_info).topology_name||'.edge_attributes');
  END IF;

  IF (_topology_info).create_topology_attrbute_tables = true and (_input_data).polygon_table_name is not null THEN
    EXECUTE Format('ALTER TABLE %s SET logged',(_topology_info).topology_name||'.face_attributes');
  END IF;

  IF (_topology_info).create_topology_attrbute_tables = true and (_input_data).line_table_name is not null THEN
    EXECUTE Format('CREATE INDEX ON %s(((%s).id)) ',
    (_topology_info).topology_name||'.edge_attributes',
    (_input_data).line_table_geo_collumn);
  END IF;

  IF (_topology_info).create_topology_attrbute_tables = true and (_input_data).polygon_table_name is not null THEN
    EXECUTE Format('CREATE INDEX ON %s(((%s).id)) ',
    (_topology_info).topology_name||'.face_attributes',
    (_input_data).polygon_table_geo_collumn);

    EXECUTE Format('CREATE INDEX ON %s(%s) ',
    (_topology_info).topology_name||'.relation',
    'topogeo_id');
  ELSE


    -- marks those rows that have relation to invalid polygons
    -- ST_Intersects(f.geo,t.geo) if try this we end up with ERROR:  XX000: GEOSIntersects: TopologyException: si

    EXECUTE Format('UPDATE %1$s t
    SET _input_geo_is_valid = FALSE
    FROM
    %2$s f
    WHERE t.%3$s IS NULL AND
    f.%4$s && t.%4$s AND
    ST_IsValid(f.%4$s) = FALSE'
    , _table_name_result_prefix,
    (_input_data).polygon_table_name,
    (_input_data).polygon_table_pk_column,
    (_input_data).polygon_table_geo_collumn
    );


    EXECUTE Format('ALTER TABLE %s SET logged',_table_name_result_prefix);

    EXECUTE Format('CREATE INDEX ON %s USING GIST (%s)', _table_name_result_prefix,(_input_data).polygon_table_geo_collumn);

    EXECUTE Format('GRANT SELECT ON ALL TABLES IN %s TO public', (_topology_info).topology_name);

    EXECUTE Format('GRANT select ON TABLE %s TO PUBLIC',_table_name_result_prefix||'_v');

  END IF;
END IF;

-- show sql to renmpve temp table


END;
$$
LANGUAGE plpgsql;


-- this function that adds indexes and set table to logged soooo


CREATE OR REPLACE FUNCTION resolve_overlap_gap_post_add_comments (
  _tables_ext_metainfo rog_overlay_source_table_metainfo_type[],
  _topology_name varchar,
  _table_name_result varchar,
  _view_name_result varchar
)
  RETURNS VOID
  AS $$
DECLARE
  -- used to run commands
  command_string text;
  tmp_ext_metainfo rog_overlay_source_table_metainfo_type;
  sf_table_geom_colemn_comment_ref text;
  sf_table_src_ref text;
  view_column_name varchar;


BEGIN
    sf_table_src_ref = ARRAY_LENGTH(_tables_ext_metainfo,1)||' tables [';
    sf_table_geom_colemn_comment_ref = 'ST_getFaceGeometry('||_topology_name||',face_id) topology built from [';

    -- Make colemns ref comments
    FOREACH tmp_ext_metainfo IN ARRAY _tables_ext_metainfo
    LOOP
      -- comment primary key refs. on views
      command_string = FORMAT($fmt$
        COMMENT ON COLUMN %7$s.%2$I is 'Only has a value if there are overlap in input table %5$I. The values are a ref to primary key column %6$I.';
        COMMENT ON COLUMN %7$s.%4$I is 'This is selected primary key used to get values from %5$I. The value is a ref to primary key column %6$I. If no data for this area in table %5$I, there will be a null value here.';
      $fmt$,
      _table_name_result, --01
      ros_table_metainfo_pk_list_name(tmp_ext_metainfo), --02
      tmp_ext_metainfo.table_pk_type, --03
       ros_table_metainfo_pk_name(tmp_ext_metainfo), --04
      tmp_ext_metainfo.table_name, --05
      tmp_ext_metainfo.table_pk_colum, --06
      _view_name_result --07
      );
      EXECUTE command_string;

      -- comment primary key refs on tables
      IF _table_name_result IS NOT NULL THEN
        command_string = FORMAT($fmt$
        COMMENT ON COLUMN %1$s.%2$I is 'Only has a value if there are overlap in input table %5$I. The values are a ref to primary key column %6$I.';
        COMMENT ON COLUMN %1$s.%4$I is 'This is selected primary key used to get values from %5$I. The value is a ref to primary key column %6$I. If no data for this area in table %5$I, there will be a null value here.';
        $fmt$,
        _table_name_result, --01
        ros_table_metainfo_pk_list_name(tmp_ext_metainfo), --02
        tmp_ext_metainfo.table_pk_type, --03
        ros_table_metainfo_pk_name(tmp_ext_metainfo), --04
        tmp_ext_metainfo.table_name, --05
        tmp_ext_metainfo.table_pk_colum, --06
        _view_name_result --07
        );
        EXECUTE command_string;
      END IF;

      -- comment rest of the collums
      FOR view_column_name IN SELECT attname FROM pg_attribute WHERE  attrelid = tmp_ext_metainfo.table_name::regclass AND attnum > 0 AND NOT attisdropped ORDER BY attnum
      LOOP
        IF view_column_name = tmp_ext_metainfo.table_pk_colum THEN
          command_string = FORMAT($fmt$
          COMMENT ON COLUMN %1$s.%2$I IS %3$L;
          COMMENT ON COLUMN %1$s.%4$I IS %5$L;
           $fmt$,
          _view_name_result, --01
          ros_table_metainfo_gaps_name(tmp_ext_metainfo), --02
          'TRUE when this area does NOT exist in input table '||tmp_ext_metainfo.table_name||'. This means that attributes from ' ||tmp_ext_metainfo.table_name|| ' for this area is NULL.' , --03
          ros_table_metainfo_overlaps_name(tmp_ext_metainfo), --04
          'TRUE when there are OVERLAPS input '||tmp_ext_metainfo.table_name||'. This means we have to select a attribute value from ' ||tmp_ext_metainfo.table_name|| ' for this area. ' ||
          'The attribute value selected is found based on column ' || tmp_ext_metainfo.table_input_order || '-' || tmp_ext_metainfo.table_name || '-pk-' || tmp_ext_metainfo.table_pk_colum || '. The id selected is from the source row covers this area that is biggest.' --05
          );

          EXECUTE command_string;
        END IF;
      END LOOP;

      FOR view_column_name IN SELECT attname FROM pg_attribute WHERE  attrelid = tmp_ext_metainfo.table_name::regclass AND attnum > 0 AND NOT attisdropped ORDER BY attnum
      LOOP
        IF view_column_name != tmp_ext_metainfo.table_pk_colum AND view_column_name != tmp_ext_metainfo.table_geo_column THEN
          command_string = FORMAT($fmt$

          COMMENT ON COLUMN %1$s.%2$I IS %3$L;
           $fmt$,
          _view_name_result, --01
          ros_table_metainfo_column_prefix(tmp_ext_metainfo)||'_'||view_column_name, --02
          'Selected attribute ' || view_column_name || ' value from table '||tmp_ext_metainfo.table_name||' for this area. ' --03
          );
          EXECUTE command_string;

        END IF;
      END LOOP;


      -- prepare global coments
      sf_table_src_ref = sf_table_src_ref || ' (' || tmp_ext_metainfo.table_input_order || ') ' || tmp_ext_metainfo.table_name ;

      sf_table_geom_colemn_comment_ref = sf_table_geom_colemn_comment_ref ||' (' || tmp_ext_metainfo.table_name || ')'
      ||tmp_ext_metainfo.table_geo_column||' srid='||tmp_ext_metainfo.table_geo_srid;


  END LOOP;

  sf_table_src_ref = sf_table_src_ref || '] . Done proccesed at ' || now() || '';
  command_string = FORMAT($fmt$
    COMMENT ON VIEW %2$s IS %4$L;
    COMMENT ON COLUMN %2$s.face_id IS %5$L;
    COMMENT ON COLUMN %2$s.geom IS %6$L;
    $fmt$,
    _table_name_result, --01
    _view_name_result, --02
    'A table with primary key refs. to input from '|| sf_table_src_ref, --03
    'A view with attribute values refs. to input from '|| sf_table_src_ref, --04
    'primary key value from '||_topology_name||'.face table', --05
    sf_table_geom_colemn_comment_ref --06
   );
  EXECUTE command_string;

  -- Make table comments
  IF _table_name_result IS NOT NULL THEN
    command_string = FORMAT($fmt$
    COMMENT ON TABLE %1$s IS %3$L;
    COMMENT ON COLUMN %1$s.face_id IS %5$L;
    COMMENT ON COLUMN %1$s.geom IS %6$L;
    $fmt$,
    _table_name_result, --01
    _view_name_result, --02
    'A table with primary key refs. to input from '|| sf_table_src_ref, --03
    'A view with attribute values refs. to input from '|| sf_table_src_ref, --04
    'primary key value from '||_topology_name||'.face table', --05
    sf_table_geom_colemn_comment_ref --06
    );
    EXECUTE command_string;
  END IF;


END;
$$
LANGUAGE plpgsql;
