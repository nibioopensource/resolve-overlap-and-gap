

-- This is the main funtion used resolve overlap and gap
CREATE OR REPLACE PROCEDURE resolve_overlap_gap_run (

	_input_data_array resolve_overlap_data_input_type[], -- A list input tables
	--(input_data_first_table).polygon_table_name varchar, -- The table to resolv, imcluding schema name
	--(input_data_first_table).polygon_table_pk_column varchar, -- The primary of the input table
	--(input_data_first_table).polygon_table_geo_collumn varchar, -- the name of geometry column on the table to analyze
	--(input_data_first_table).table_srid int, -- the srid for the given geo column on the table analyze
	--(input_data_first_table).utm boolean,

	_topology_info resolve_overlap_data_topology_type,
	-- input_topology_result_schema_name
	---(_topology_info).topology_name varchar, -- The topology schema name where we store store sufaces and lines from the simple feature dataset and th efinal result
	-- NB. Any exting data will related to topology_name will be deleted
	--(_topology_info).topology_srid
	--(_topology_info).topology_snap_tolerance float, -- this is tolerance used as base when creating the the postgis topolayer
	--(_topology_info).create_topology_attrbute_tables boolean -- if this is true and we value for line_table_name we create attribute tables refferances to
	-- this tables will have atrbuttes equal to the simple feauture tables for lines and feautures
	-- a flag for testing the diff using a temporary tempology or not or insert directly into final source topology
	-- this is a test related to performance and testing on that, the final results should be pretty much the same.
	-- (_topology_info).use_temp_topology boolean
	-- (_topology_info).transfer_method_from_temp_2_final_topology


	_clean_info resolve_overlap_data_clean_type, -- different parameters used if need to clean up your data
	--(_clean_info)._min_area_to_keep float default 0, -- if this a polygon  is below this limit it will merge into a neighbour polygon. The area is sqare meter.
	--(_clean_info)._simplify_tolerance float default 0, -- is this is more than zero simply will called with
	--(_clean_info)._simplify_max_average_vertex_length int default 0, -- in meter both for utm and deegrees, this used to avoid running ST_simplifyPreserveTopology for long lines lines with few points
	--(_clean_info)._chaikins_nIterations int default 0, -- IF 0 NO CHAKINS WILL BE DONE,  A big value here make no sense because the number of points will increaes exponential )
	--(_clean_info)._chaikins_max_length int default 0, --edge that are longer than this value will not be touched by _chaikins_min_degrees and _chaikins_max_degrees
	--(_clean_info)._chaikins_min_degrees int default 0, -- The angle has to be less this given value, This is used to avoid to touch all angles.
	--(_clean_info)._chaikins_max_degrees int default 0, -- OR the angle has to be greather than this given value, This is used to avoid to touch all angles
	--(_clean_info)._chaikins_min_steep_angle_degrees int default 0, -- The angle has to be less this given value, This is used to avoid to touch all angles.
	--(_clean_info)._chaikins_max_steep_angle_degrees int default 0-- OR The angle has to be greather than this given value, This is used to avoid to touch all angles

	-- To limit and set as master area. This must be in the same coordsys as _new_sf_table_srid and must be polygon or a multipolgyon,
	-- This will added as the first geometry into postgis topology. Data outside the master area will not be used.
	-- If you send in a null geometry here this parameter will not have any effect.
	-- (_clean_info).input_boundary geometry

	-- ST_IsEmpty(ST_Buffer(face_geom,-min_st_is_empty_buffer))
	-- This is an approximate value min_st_is_empty_buffer in meter used remove sliver polygon,
	-- The formula used to convert from meter used for testing now
	-- SELECT ST_Length(ST_Boundary(ST_Envelope(((ST_Buffer('POINT (10.061995275172155 59.716094977883)'::geography,1,'quad_segs=1'))::geometry))))/9;
	-- (_clean_info).min_st_is_empty_buffer_value

	_max_parallel_jobs int, -- this is the max number of paralell jobs to run. There must be at least the same number of free connections
	_max_rows_in_each_cell int, -- this is the max number rows that intersects with box before it's split into 4 new boxes, default is 5000

	_debug_options resolve_overlap_data_debug_options_type -- this used to set different debug parameters and is usually null
	--resolve_overlap_data_debug_options_func(
	--_contiune_after_stat_exception boolean default true, -- if set to false, it will do topology.ValidateTopology and stop to if the this call returns any rows
	--_validate_topoplogy_for_each_run boolean default false, -- if set to true, it will do topology.ValidateTopology at each loop return if it's error
	--_run_add_border_line_as_single_thread boolean default true, --  if set to false, it will in many cases generate topo errors beacuse of running in many parralell threads
	--_start_at_job_type int default 1, -- if set to more than 1 it will skip init procces and start at given job_type
	--_start_at_loop_nr int default 1, -- many of jobs are ran in loops beacuse because if get an exception or cell is not allowed handle because cell close to is also started to work , this cell will gandled in the next loop.
	--_stop_at_job_type int default 0, -- if set to more than 0 the job will stop  when this job type is reday to run and display a set sql to run
	--_stop_at_loop_nr int default 0 -- if set to more than 0 the job will stop  when this job type is reday to run and display a set sql to run
	--)

)
LANGUAGE plpgsql
AS $$
DECLARE
	command_string text;
	num_rows int;
	part text;
	id_list_tmp int[];
	this_list_id int;
	-- Holds the list of func_call to run
	stmts text[];
	stats_done text[]; -- This should be null when called , and new statements done will be added to this list.

	stmts_rerun text[];
	stmts_jobid_done int[];
	-- holds a single job
	stmt_sql text;
	-- jobids sent
	stmts_jobid int;
	stmts_pos int;

	-- Holds the sql for a functin to call
	func_call text;
	-- the number of cells created in the grid
	num_cells int;
	-- the table name prefix to be used for results tables
	table_name_result_prefix varchar;
	-- the name of the content based grid table
	overlapgap_grid varchar;
	-- The schema.table name of the grid that will be created and used to break data up in to managle pieces
	-- the name of job_list table, this table is ued to track of done jobs
	job_list_name varchar;
	-- the sql used for blocking cells
	sql_to_block_cmd varchar;
	-- just to create sql
	command_string_var varchar;
	-- this is the tolerance used when creating the topo layer
	cell_job_type int;
	-- add lines 1 inside cell, 2 boderlines, 3 exract simple
	topology_schema_name varchar = (_topology_info).topology_name;
	-- for now we use the same schema as the topology structure
	loop_number int default 1;

	i_stmts int;
	analyze_stmts int;

	last_run_stmts int;

	num_topo_error_in_final_layer int;
	v_state text;
	v_msg text;
	v_detail text;
	v_hint text;
	v_context text;


	start_time timestamp WITH time zone;

	-- Used for debug
	contiune_after_stat_exception boolean DEFAULT true; -- DEFAULT true, -- if set to false, it will do topology.ValidateTopology and stop to if the this call returns any rows
	validate_topoplogy_for_each_run boolean DEFAULT false; -- if set to true, it will do topology.ValidateTopology at each loop return if it's error
	run_add_border_line_as_single_thread boolean default false; --  if set to false, it will in many cases generate topo errors beacuse of running in many parralell threads
	start_at_job_type int default 1; -- if set to more than 1 it will skip init procces and start at given input
	start_at_loop_nr int default 1; -- many of jobs are ran in loops beacuse because if get an exception or cell is not allowed handle because cell close to is also started to work , this cell will gandled in the next loop.
	stop_at_job_type int default 0; -- if set to more than 0 the job will stop  when this job type is reday to run and display a set sql to run
	stop_at_loop_nr int default 0; -- if set to more than 0 the job will stop  when this job type is reday to run and display a set sql to run


	-- We get this as separate variable to handle 0 values
	-- This will used when securing border line spacing between grid cell when topology_snap_tolerance  is zero
	this_topology_info_topology_snap_tolerance float = (_topology_info).topology_snap_tolerance;

	-- A temp variable for first table in the list
	input_data_first_table resolve_overlap_data_input_type;

	tables_ext_metainfo rog_overlay_source_table_metainfo_type[]; -- A new type with more info

	-- Used for parralel jobs
	max_parallel_jobs int;

	rec_new_line RECORD;
	rec_new_line_split RECORD;
	rec_new_temp_edges int[];
	rec_new_edges_since_anlyze int = 0;
	rec_new_edges_total int = 0;
	rec_new_edges_analyze int = 50;
	rec_new_counter int = 0;
	rog_added_border_line rog_overlay_add_border_lines_type;

	start_time_delta_job timestamp WITH time zone;
	used_time real;

	lines_to_add geometry[];

	rec_new_edges_to_remove int = 0;
	lines_to_retry rog_overlay_add_border_failed_line[] = '{}'::rog_overlay_add_border_failed_line[];
	tmp_line_to_retry rog_overlay_add_border_failed_line;

	vacuum_cmd text[];

	-- start used by start_execute_parallel
	connstr text; -- this will be buildt up inside start_execute_parallel at first call
	conntions_array text[]; -- this will be buildt up inside start_execute_parallel at first call
	conn_stmts text[]; -- this will be buildt up inside start_execute_parallel at first call
	all_stmts_done boolean = false; -- this will be buildt up inside start_execute_parallel at first call
	start_execute_parallel_counter int = 0;
	seconds_to_wait_before_check_result int = 300;
	-- done used by start_execute_parallel

	num_jobs_to_run int;

	done_add_cell_crossing_lines boolean;

	more_edges_to_check_for_removel boolean;

	--  Method 1 topology.TopoGeo_addLinestring
	--  Method 2 topology.TopoGeo_LoadGeometry
	--  Method 3 select from tmp toplogy into master topology
	transfer_method_from_temp_2_final_topology int = (_topology_info).transfer_method_from_temp_2_final_topology;

	meta_grid_number int = 1;

	job_ids int[];
	box_id int;
	border_info_topology_name text;

	job_cell_numbers int;


BEGIN

	-- Test input
	IF _input_data_array IS NULL OR ARRAY_LENGTH(_input_data_array,1) = 0 THEN
		RAISE EXCEPTION 'Not valid _input_data_array for % for topology %', _input_data_array, topology_schema_name;
	END IF;

	IF (_topology_info).use_temp_topology != TRUE THEN
			RAISE EXCEPTION 'We only support temp topology for % for topology %', _input_data_array, topology_schema_name;
	END IF;

	IF (_clean_info).input_boundary IS NOT NULL AND NOT ST_Area((_clean_info).input_boundary) > 0 THEN
			RAISE EXCEPTION 'Invalid input_boundary % for topology %', ST_AsText((_clean_info).input_boundary) , topology_schema_name;
	END IF;


	-- tables_ext_metainfo, including more  info
	tables_ext_metainfo = rog_map_2_rog_overlay_source_table_metainfo_type(_input_data_array);

	-- We use this temporary, may be removed
	input_data_first_table = _input_data_array[1];

	-- Just a test see wrapper fuction works as berfore
	input_data_first_table = input_data_first_table;


	-- (_topology_info).topology_srid is new so this wrap around just to support old call tempoary for for now
	IF (_topology_info).topology_srid IS NULL THEN
		_topology_info.topology_srid = (input_data_first_table).table_srid;
	END IF;

	IF _debug_options IS NOT NULL THEN
		contiune_after_stat_exception = (_debug_options).contiune_after_stat_exception;
		validate_topoplogy_for_each_run = (_debug_options).validate_topoplogy_for_each_run;
		run_add_border_line_as_single_thread = (_debug_options).run_add_border_line_as_single_thread;

		start_at_job_type  = (_debug_options).start_at_job_type;
		start_at_loop_nr = (_debug_options).start_at_loop_nr;
		stop_at_job_type = (_debug_options).stop_at_job_type;
		stop_at_loop_nr = (_debug_options).stop_at_loop_nr;

		loop_number = start_at_loop_nr;
	END IF;

	table_name_result_prefix := (_topology_info).topology_name || Substring((input_data_first_table).polygon_table_name FROM (Position('.' IN (input_data_first_table).polygon_table_name)));

	--table_name_result_prefix := (_topology_info).topology_name || '_'||(ARRAY_LENGTH(_input_data_array,1))::text||'_tables_1';
	table_name_result_prefix := (_topology_info).topology_name||'.'||ros_view_sf_result_base_name((_topology_info).input_topology_result_schema_name,ARRAY_LENGTH(_input_data_array,1))::text;


	--table_name_result_prefix := (_topology_info).topology_name || Substring((input_data_first_table).polygon_table_name FROM (Position('.' IN (input_data_first_table).polygon_table_name)));


	-- This is table name prefix including schema used for the result tables
	-- || '_overlap'; -- The schema.table name for the overlap/intersects found in each cell
	-- || '_gap'; -- The schema.table name for the gaps/holes found in each cell
	-- || '_grid'; -- The schema.table name of the grid that will be created and used to break data up in to managle pieces
	-- || '_boundery'; -- The schema.table name the outer boundery of the data found in each cell
	-- NB. Any exting data will related to this table names will be deleted
	-- the name of the content based grid table
	overlapgap_grid := table_name_result_prefix || '_grid';
	-- The schema.table name of the grid that will be created and used to break data up in to managle pieces
	-- the name of job_list table, this table is ued to track of done jobs
	job_list_name := table_name_result_prefix || '_job_list';
	-- Call init method to create content based create and main topology schema


	-- We can moved this to a post job, we now only need ref to table and primary key value

	IF (_topology_info).create_topology_attrbute_tables = true THEN
		IF (input_data_first_table).line_table_name is not null THEN
			EXECUTE Format('select
			Array_to_string(array_agg(column_name||%L||data_type),%L) as column_def,
			Array_to_string(array_agg(column_name),%L) as column_name
			from INFORMATION_SCHEMA.COLUMNS where  table_schema = %L and table_name = %L and data_type != %L and column_name != %L',
			' ',',',',',
			split_part((input_data_first_table).line_table_name, '.', 1),
			split_part((input_data_first_table).line_table_name, '.', 2),
			'USER-DEFINED',
			(input_data_first_table).line_table_geo_collumn) INTO input_data_first_table.line_table_other_collumns_def, input_data_first_table.line_table_other_collumns_list;
		ELSE
			-- TODO REMOVE HACK when we find out how to do this
			input_data_first_table.line_table_other_collumns_def := 'id integer';
			input_data_first_table.line_table_other_collumns_list := 'id';
			input_data_first_table.line_table_pk_column = 'id';
			input_data_first_table.line_table_geo_collumn = (input_data_first_table).polygon_table_geo_collumn;
		END IF;

		IF (input_data_first_table).polygon_table_other_collumns_def is null THEN
			EXECUTE Format('select
			Array_to_string(array_agg(column_name||%L||data_type),%L) as column_def,
			Array_to_string(array_agg(column_name),%L) as column_name
			from INFORMATION_SCHEMA.COLUMNS where  table_schema = %L and table_name = %L and data_type != %L and column_name != %L',
			' ',',',',',
			split_part((input_data_first_table).polygon_table_name, '.', 1),
			split_part((input_data_first_table).polygon_table_name, '.', 2),
			'USER-DEFINED',
				(input_data_first_table).polygon_table_geo_collumn) INTO input_data_first_table.polygon_table_other_collumns_def, input_data_first_table.polygon_table_other_collumns_list;
			END IF;
		END IF;


	-- This is hack for now to be removed when moved to post job
	_input_data_array[1] = input_data_first_table;

		-- The problem here is how handle when this_topology_info_topology_snap_tolerance is zer0
	-- TODO find a way check if this is degrees

	IF (_topology_info).topology_srid = 4258 THEN
		-- if we work in degrees and have zero tolerance we need an no zero value here
		-- to clean up the after using sinmple feature on the bordere lines
		-- using  0.00000001, causes systen to snap
		IF (this_topology_info_topology_snap_tolerance = 0) THEN
			this_topology_info_topology_snap_tolerance = 0.0000001;
		END IF;
	ELSE
		-- if we work in utm and have zero tolerance we need an no zero value here
		IF (this_topology_info_topology_snap_tolerance = 0) THEN
			this_topology_info_topology_snap_tolerance = 0.000001;
		END IF;

		RAISE EXCEPTION 'We only support degrees (4258) and not % for % and schema % ',
		(_topology_info).topology_srid, _input_data_array, topology_schema_name;
	END IF;


	IF start_at_job_type = 1 THEN
		SELECT resolve_overlap_gap_init(
		_input_data_array,
		tables_ext_metainfo,
		_topology_info,
		this_topology_info_topology_snap_tolerance,
		_clean_info,
		table_name_result_prefix,
		_max_rows_in_each_cell,
		overlapgap_grid,
		_debug_options::resolve_overlap_data_debug_options_type)  INTO num_cells;
	END IF;


	IF (_topology_info).create_topology_attrbute_tables = true THEN

	command_string := FORMAT('SELECT layer_id
	FROM topology.layer l, topology.topology t
	WHERE t.name = %L AND
	t.id = l.topology_id AND
	l.schema_name = %L AND
	l.table_name = %L AND
	l.feature_column = %L',
		(_topology_info).topology_name,
		(_topology_info).topology_name,
		'edge_attributes',
		(input_data_first_table).line_table_geo_collumn);
	EXECUTE command_string INTO _topology_info.topology_attrbute_tables_border_layer_id;
	RAISE NOTICE 'command_string % set _topology_info.topology_attrbute_tables_border_layer_id to %',command_string , (_topology_info).topology_attrbute_tables_border_layer_id;

	command_string := FORMAT('SELECT layer_id
	FROM topology.layer l, topology.topology t
	WHERE t.name = %L AND
	t.id = l.topology_id AND
	l.schema_name = %L AND
	l.table_name = %L AND
	l.feature_column = %L',
		(_topology_info).topology_name,
		(_topology_info).topology_name,
		'face_attributes',
		(input_data_first_table).line_table_geo_collumn);
	EXECUTE command_string INTO _topology_info.topology_attrbute_tables_surface_layer_id;
	RAISE NOTICE 'command_string % set _topology_info.topology_attrbute_tables_border_layer_id to %',command_string , (_topology_info).topology_attrbute_tables_border_layer_id;

	END IF;

	FOR cell_job_type IN start_at_job_type..7 LOOP

		IF cell_job_type = 3 and loop_number = 1 THEN
			-- add very long lines feature in single thread
			-- Most parts of this will not be healed and smooting if we keep it this way
			IF (_topology_info).create_topology_attrbute_tables = true and (input_data_first_table).line_table_name is not null THEN
				command_string := Format('WITH lines_addes AS (
				SELECT topology.toTopoGeom(r.geo, %1$L, %2$L , %3$L) as topogeometry, column_data_as_json
				FROM %4$s as r where r.added_to_master = false
				ORDER BY ST_X(ST_Centroid(r.geo)), ST_Y(ST_Centroid(r.geo)))
				INSERT INTO %5$s(%7$s,%6$s)
				SELECT x.*, ee.topogeometry as %6$s FROM lines_addes ee, jsonb_to_record(ee.column_data_as_json) AS x(%8$s)',
				(_topology_info).topology_name,
				(_topology_info).topology_attrbute_tables_border_layer_id,
				this_topology_info_topology_snap_tolerance,
				table_name_result_prefix||'_border_line_segments',
				(_topology_info).topology_name||'.edge_attributes',
				(input_data_first_table).line_table_geo_collumn,
				(input_data_first_table).line_table_other_collumns_list,
				(input_data_first_table).line_table_other_collumns_def
				);
				EXECUTE command_string;
			ELSE
				-- Add border line parts not adde to connect lines crossing cell borders and remove any area below min surface size
				CALL topo_update.add_cellborder_lines(
				_topology_info, --01
				_clean_info, --02
				table_name_result_prefix, --03
				this_topology_info_topology_snap_tolerance, --04
				NULL, -- no bbox since we should all rest off the cell crossing lines
				FALSE, -- _return_if_long_execute_time boolean, -- if too long time is used the code will return and the caller has to retry TRUE
				tables_ext_metainfo,
				done_add_cell_crossing_lines
				);

			END IF;

			COMMIT;

		ELSEIF cell_job_type = 4 and loop_number = 1 THEN
			-- try fixed failed lines before make simple feature in single thread
			-- this code is messy needs to fixed in anither way
			command_string := Format('UPDATE %1$s u
			SET line_geo_lost = false
			FROM %1$s o
			where ST_DWithin(u.geo,u.geo,%2$s) and u.line_geo_lost = false',
			table_name_result_prefix||'_no_cut_line_failed',
			(_topology_info).topology_snap_tolerance);

			RAISE NOTICE 'Mark lines as NOT lost if there are lines as this in final topology  %', command_string;

			EXECUTE command_string;

			command_string := Format('SELECT r.geom , r.id, r.table_input_order
				FROM (
					SELECT distinct (ST_Dump(ST_Multi(ST_LineMerge(ST_union(r.geom))))).geom as geom, r.id, r.table_input_order
						FROM (
							select r.geo AS geom, r.id, r.table_input_order
							from %1$s r
							where line_geo_lost = true
						) as r
						GROUP BY r.id, r.table_input_order
					) as r
				ORDER BY r.table_input_order, ST_X(ST_StartPoint(r.geom)), ST_Y(ST_StartPoint(r.geom))
			',
			table_name_result_prefix||'_no_cut_line_failed'
			);



			RAISE NOTICE 'Try to add failed lines with retry to master topo layer %', command_string;

			-- start loop through lines from temp table
			start_time_delta_job := Clock_timestamp();
			FOR rec_new_line IN EXECUTE command_string LOOP
				FOR rec_new_line_split IN SELECT * FROM topo_update.split_line_on_edges((_topology_info).topology_name,rec_new_line.geom) LOOP
					RAISE NOTICE 'TRY, done adding error_line rec_new_line.geom  %  to % from failed lines %',
					rec_new_line_split.split_line_on_edges, (_topology_info).topology_name , table_name_result_prefix||'_no_cut_line_failed';

					rog_added_border_line = topo_update.add_border_lines(
					(_topology_info).topology_name,
					rec_new_line_split.split_line_on_edges,
					(_clean_info).min_area_to_keep,
					(_clean_info).split_input_lines_at_vertex_n, -- split all lines at given vertex num
					(_clean_info).min_st_is_empty_buffer_value,
					rec_new_line.table_input_order,
					''::text, -- src_table_pk_column_value
					this_topology_info_topology_snap_tolerance,
					table_name_result_prefix,
					FALSE, -- _do_retry_add
					TRUE, -- _previously_failed_line
					(_topology_info).topology_is_utm,
					TRUE , -- _log_failed_line_error_table If true it will add new rows to _table_name_result_prefix || '_no_cut_line_failed';
					FALSE, -- _use_temp_edges_table_name_for_small_area boolean default FALSE, -- If true a local table name will be used
					TRUE, --_check_geeomtry_after_added_line boolean default TRUE, -- By default we will check for min area values and sliver value in topology created.
					tables_ext_metainfo
					);
				END LOOP;


				EXECUTE Format('UPDATE %s SET line_geo_lost = FALSE, error_info = %L WHERE id = %s',
				table_name_result_prefix||'_no_cut_line_failed', 'line split done', rec_new_line.id);
			END LOOP;


			COMMIT;

			-- removed smallines not rempved
			-- remove small polygons in temp in (_clean_info).min_area_to_keep
			IF (_clean_info).min_area_to_keep IS NOT NULL AND (_clean_info).min_area_to_keep > 0 THEN
				start_time_delta_job := Clock_timestamp();
				RAISE NOTICE 'Start 4 clean small polygons for border plygons face_table_name % at %', (_topology_info).topology_name||'.face', Clock_timestamp();
				-- remove small polygons in temp
				call topo_update.do_merge_small_areas_no_block (
				(_topology_info).topology_name,
				table_name_result_prefix,
				(_clean_info).min_area_to_keep,
					(_clean_info).min_st_is_empty_buffer_value,
				null,
				(_topology_info).topology_is_utm,
				cell_job_type,
				more_edges_to_check_for_removel -- no neeed to check on more_edges_to_check_for_removel
				);
				used_time := (Extract(EPOCH FROM (Clock_timestamp() - start_time_delta_job)));
				RAISE NOTICE 'clean 4 small polygons for after adding to main face_table_name % at % used_time: %', (_topology_info).topology_name||'.face', Clock_timestamp(), used_time;
			END IF;

		END IF; -- END cell_job_type = 4 and loop_number = 1

		-- update joblist table cell_job_type not in 1 and 2.
		-- cell_job_type not in 1 and 2 do change or update joblist based on loop_number
		IF cell_job_type NOT IN (1,2) THEN
			command_string := Format('SELECT resolve_overlap_gap_job_list(%L,%L,%L,%s,%L,%L,%L,%L,%s,%s,%s)',
			_input_data_array,
			tables_ext_metainfo,
			_topology_info,
			this_topology_info_topology_snap_tolerance,
			overlapgap_grid,
			table_name_result_prefix,
			job_list_name,
			_clean_info,
			_max_parallel_jobs,
			cell_job_type,loop_number);
			EXECUTE command_string;
			COMMIT;
		END IF; -- END  loop_number = 1 AND cell_job_type NOT IN (2,3)

		-- recreate indexes constrains on master topology before starting to border crossing cell borders
		-- merge all border lines from _border_line_segments_short and _border_line_segments_lf_eq_rf into _border_line_segments_final
		IF cell_job_type = 2 THEN

			IF transfer_method_from_temp_2_final_topology = 3 THEN

				command_string =  Format($fmt$
					-- CREATE primary keys and primary keys index
					ALTER TABLE %1$s.edge_data ADD PRIMARY KEY (edge_id);
					ALTER TABLE %1$s.node ADD PRIMARY KEY (node_id);
					ALTER TABLE %1$s.face ADD PRIMARY KEY (face_id);

					-- CREATE other indexs
					CREATE INDEX IF NOT EXISTS edge_data_abs_next_left_edge_idx ON %1$s.edge_data(abs_next_left_edge);
					CREATE INDEX IF NOT EXISTS edge_data_abs_next_right_edge_idx ON %1$s.edge_data(abs_next_right_edge);
					CREATE INDEX IF NOT EXISTS edge_end_node_idx ON %1$s.edge_data(end_node);
					CREATE INDEX IF NOT EXISTS edge_left_face_idx ON %1$s.edge_data(left_face);
					CREATE INDEX IF NOT EXISTS edge_right_face_idx ON %1$s.edge_data(right_face);
					CREATE INDEX IF NOT EXISTS edge_start_node_idx ON %1$s.edge_data(start_node);
					CREATE INDEX IF NOT EXISTS edge_gist ON %1$s.edge_data USING GIST (geom);

					CREATE INDEX IF NOT EXISTS node_containing_face_idx ON %1$s.node(containing_face);
					CREATE INDEX IF NOT EXISTS node_gist ON %1$s.node USING GIST (geom);

					CREATE INDEX IF NOT EXISTS face_gist ON %1$s.face USING GIST (mbr);

					-- ANALYZE after adding indexes
					ANALYZE  %1$s.edge_data;
					ANALYZE  %1$s.node;
					ANALYZE  %1$s.face;

					-- CREATE FOREIGN KEY CONSTRAINTS
					ALTER TABLE %1$s.edge_data ADD CONSTRAINT end_node_exists FOREIGN KEY (end_node) REFERENCES %1$s.node(node_id);
					ALTER TABLE %1$s.edge_data ADD CONSTRAINT left_face_exists FOREIGN KEY (left_face) REFERENCES %1$s.face(face_id);
					ALTER TABLE %1$s.edge_data ADD CONSTRAINT next_left_edge_exists FOREIGN KEY (abs_next_left_edge) REFERENCES %1$s.edge_data(edge_id) DEFERRABLE INITIALLY DEFERRED;
					ALTER TABLE %1$s.edge_data ADD CONSTRAINT next_right_edge_exists FOREIGN KEY (abs_next_right_edge) REFERENCES %1$s.edge_data(edge_id) DEFERRABLE INITIALLY DEFERRED;
					ALTER TABLE %1$s.edge_data ADD CONSTRAINT right_face_exists FOREIGN KEY (right_face) REFERENCES %1$s.face(face_id);
					ALTER TABLE %1$s.edge_data ADD CONSTRAINT start_node_exists FOREIGN KEY (start_node) REFERENCES %1$s.node(node_id);
					ALTER TABLE %1$s.node ADD CONSTRAINT face_exists FOREIGN KEY (containing_face) REFERENCES %1$s.face(face_id);

					-- https://trac.osgeo.org/postgis/ticket/5766#comment:10
					-- UPDATE mbr for universal face
					-- UPDATE %1$s.face SET mbr = ( SELECT ST_SetSRID(ST_Extent(mbr),%2$s) as table_extent FROM %1$s.face )
					-- WHERE face_id = 0;

					ANALYZE  %1$s.face;

					$fmt$,
					(_topology_info).topology_name,
					(_topology_info).topology_srid
				);

				RAISE NOTICE 'RECREATE constraints, primary keys, indexes command_string and mbr for universal face % ', command_string;
				EXECUTE command_string;

				-- commit so it's ready to use for other threads
				COMMIT;
			END IF;

			-- merge border to add crossing cell borders
			EXECUTE resolve_overlap_gap_mergeborder_lines(
			_topology_info,
			table_name_result_prefix);
		END IF;

		last_run_stmts := 0;

		-- One cell_job_type may need to run many loops so we need to start loop here
		LOOP

			-- Do VACUUM on some cases before starting next batch off jobs
			IF cell_job_type IN (2,3) THEN
				-- We do not need to run vaccum in first loop when runing transfer_method_from_temp_2_final_topology = 3
				IF (transfer_method_from_temp_2_final_topology = 3 AND loop_number  > 1) OR
					transfer_method_from_temp_2_final_topology != 3 THEN
					start_time_delta_job := Clock_timestamp();
					RAISE NOTICE 'Start VACUUM (ANALYZE,VERBOSE) for tables for final topology % at loop_number % before starting to add cell border crossing lines  at %',
					(_topology_info).topology_name, loop_number, Clock_timestamp();

					vacuum_cmd = FORMAT(
						$fmt$
						{"VACUUM (ANALYZE,VERBOSE) %1$s.node","VACUUM (ANALYZE,verbose) %1$s.relation","VACUUM (ANALYZE,verbose) %1$s.edge_data","VACUUM (ANALYZE,verbose) %1$s.face"}
						$fmt$,
						(_topology_info).topology_name
					);
					call do_execute_parallel(vacuum_cmd::text[],current_setting('execute_parallel.connstring',true),1);

					used_time := (Extract(EPOCH FROM (Clock_timestamp() - start_time_delta_job)));
					RAISE NOTICE 'Done VACUUM (FULL,ANALYZE,VERBOSE) for tables for final topology % at loop_number % before starting to add cell border crossing lines  at % used_time: %',
					(_topology_info).topology_name, loop_number, Clock_timestamp(),used_time;
				END IF;
			END IF;

			-- Clean up old stuff before starting the job in
			IF (cell_job_type = 1 AND  loop_number = 1) THEN
				command_string := Format($fmt$
				SELECT resolve_overlap_gap_job_list(%L,%L,%L,%s,%L,%L,%L,%L,%s,%s,%s)
				$fmt$,
				_input_data_array,
				tables_ext_metainfo,
				_topology_info,
				this_topology_info_topology_snap_tolerance,
				overlapgap_grid, table_name_result_prefix, job_list_name, _clean_info, _max_parallel_jobs, cell_job_type,loop_number);
				EXECUTE command_string;

				-- check old diretoririie and clean up

				command_string := Format($fmt$
				SELECT ARRAY_AGG(DISTINCT s.id)
				FROM
				(
				SELECT j.id FROM %1$s j
				) s
				$fmt$,
				job_list_name
				);
				EXECUTE command_string INTO job_ids;

				<<"FOREACH job_ids">>
				FOREACH box_id IN ARRAY job_ids LOOP
					border_info_topology_name := (_topology_info).topology_name || '_' || box_id;
					--raise info 'border_info_topology_name %', border_info_topology_name;
					IF ((SELECT Count(*) FROM topology.topology WHERE name = border_info_topology_name) = 1) THEN
						EXECUTE Format('SELECT topology.droptopology(%s)', Quote_literal(border_info_topology_name));
					END IF;
					EXECUTE Format('DROP SCHEMA IF EXISTS %s CASCADE', border_info_topology_name);

					EXECUTE Format('DROP TABLE IF EXISTS %s', ((_topology_info).topology_name||'.sub_job_type_1_box_id_' || box_id) );

				END LOOP "FOREACH job_ids";

				COMMIT;
			END IF; -- END cell_job_type IN (2,3) OR (cell_job_type IN (1) AND loop_number < 4)

			-- Create a new joblist table for job for cell_job_type 2 but also check if there are old jobs to run first, which needs non standard handeling
			-- create jobs for adding lines temp topologies, master topology and border lines into into
			-- In this face we also pick the jobs based cell size and X coordinates ,Y coordinates
			-- We need to make this code simpler in some way.
			IF (cell_job_type = 2) THEN
				-- check if there any statement to run from the last loop
				IF loop_number > 1 THEN
					stmts := '{}';
					command_string := Format($fmt$
					SELECT ARRAY_AGG(DISTINCT s.func_call)
					FROM (
						SELECT j.sql_to_run||%1$L||'); --jobid='||j.id as func_call
						FROM
						%2$s j,
						%4$s l
						WHERE j.block_bb is null
						AND l.added_to_master = FALSE
						AND l.geom && j.cell_geo
						ORDER BY  ST_area(j.%3$s,true) ASC, ST_X(ST_Centroid(j.%3$s)), ST_Y(ST_Centroid(j.%3$s))
					) s
					$fmt$,
					meta_grid_number, --01
					job_list_name, --02
					'cell_geo', --03
					table_name_result_prefix||'_border_line_segments_final' --04
					);
					EXECUTE command_string INTO stmts;
				END IF;

				-- if there are now old jobs to run or this is first loop check if can create new jobs.
				IF Array_length(stmts, 1) IS NULL OR stmts IS NULL OR loop_number = 1 THEN
					RAISE NOTICE 'Try to create joblist for line crossing borders for % cell for cell_job_type % at loop_number % and meta_grid_number % for topology % at % ',
					num_jobs_to_run, cell_job_type, loop_number, meta_grid_number, (_topology_info).topology_name, Clock_timestamp();

					command_string := Format($fmt$
					SELECT resolve_overlap_gap_job_list(%L,%L,%L,%s,%L,%L,%L,%L,%s,%s,%s)
					$fmt$,
					_input_data_array,
					tables_ext_metainfo,
					_topology_info,
					this_topology_info_topology_snap_tolerance,
					overlapgap_grid, table_name_result_prefix, job_list_name, _clean_info, _max_parallel_jobs, cell_job_type,meta_grid_number);
					EXECUTE command_string;
					COMMIT;

					stmts := '{}';
					command_string := Format($fmt$
					SELECT ARRAY_AGG(DISTINCT s.func_call)
					FROM (
						SELECT j.sql_to_run||%1$L||'); --jobid='||j.id as func_call
						FROM
						%2$s j,
						%4$s l
						WHERE j.block_bb is null
						AND l.added_to_master = FALSE
						AND l.geom && j.cell_geo
						ORDER BY  ST_area(j.%3$s,true) ASC, ST_X(ST_Centroid(j.%3$s)), ST_Y(ST_Centroid(j.%3$s))
					) s
					$fmt$,
					meta_grid_number, --01
					job_list_name, --02
					'cell_geo', --03
					table_name_result_prefix||'_border_line_segments_final' --04
					);
					EXECUTE command_string INTO stmts;

					meta_grid_number = meta_grid_number + 1;
				END IF;

				-- get number of total cells
				EXECUTE Format($fmt$ SELECT count(*) FROM  %1$s $fmt$, job_list_name ) INTO job_cell_numbers;

				RAISE NOTICE '% Done create joblist with % job (totall cells %) jobs for line crossing borders at loop_number % and meta_grid_number % for topology % at % ',
				job_list_name, Array_length(stmts, 1) , job_cell_numbers, loop_number, meta_grid_number, (_topology_info).topology_name, Clock_timestamp();
				--  END (cell_job_type = 2)
			ELSIF (cell_job_type IN (3,4)) THEN
				-- get statements to run, but limit to row with cell crossing borders
				stmts := '{}';
				command_string := Format($fmt$
				SELECT ARRAY_AGG(DISTINCT s.func_call)
				FROM (
					SELECT j.sql_to_run||%1$L||'); --jobid='||j.id as func_call
					FROM
					%2$s j,
					%4$s l
					WHERE j.block_bb is null
					AND l.geom && j.cell_geo
					-- order by number of rows intersecion in the cells
					ORDER BY j.numrows_intersects desc, ST_area(j.%3$s,true) ASC, ST_X(ST_Centroid(j.%3$s)), ST_Y(ST_Centroid(j.%3$s))
				) s
				$fmt$,
				loop_number,
				job_list_name,
				'cell_geo',
				table_name_result_prefix||'_border_line_segments_final' --04
				);
				EXECUTE command_string INTO stmts;
			ELSE
				-- get statements to run, the default way based number off surfaces in current cell (this
				stmts := '{}';
				command_string := Format($fmt$
				SELECT ARRAY_AGG(DISTINCT s.func_call)
				FROM (
					SELECT j.sql_to_run||%1$L||'); --jobid='||j.id as func_call
					FROM %2$s j
					WHERE j.block_bb is null
					-- order by number of rows intersecion in the cells
					ORDER BY j.numrows_intersects desc, ST_area(j.%3$s,true) ASC, ST_X(ST_Centroid(j.%3$s)), ST_Y(ST_Centroid(j.%3$s))
				) s
				$fmt$,
				loop_number,
				job_list_name,
				'cell_geo');
				EXECUTE command_string INTO stmts;

			END IF;


			-- Print message if no jobs
			IF Array_length(stmts, 1) IS NULL OR stmts IS NULL THEN
				RAISE NOTICE 'NOT Kicking off joblist in resolve_overlap_gap_run % jobs with % workers for cell_job_type % at loop_number % for topology % at % ',
					Array_length(stmts, 1), max_parallel_jobs, cell_job_type, loop_number, (_topology_info).topology_name, Clock_timestamp();
			END IF;

			-- end loop if no statements to run
			EXIT WHEN
				Array_length(stmts, 1) IS NULL OR
				stmts IS NULL ;

			-- prepeare to run statements
			start_time := Clock_timestamp();
			start_time_delta_job = Clock_timestamp();


			all_stmts_done = false;
			start_execute_parallel_counter = 0;
			connstr = NULL;
			conntions_array = NULL;
			conn_stmts = NULL;
			seconds_to_wait_before_check_result = 60;


			RAISE NOTICE 'Start runningfor cell_job_type % at loop_number % for topology % at % ',
			cell_job_type, loop_number, (_topology_info).topology_name, Clock_timestamp();


			-- if number of jobs less than number of workers
			num_jobs_to_run = COALESCE (Array_length(stmts, 1),0);
			IF num_jobs_to_run < _max_parallel_jobs THEN
				max_parallel_jobs = num_jobs_to_run;
			ELSE
				max_parallel_jobs = _max_parallel_jobs;
			END IF;

			-- Start job all stmts
			WHILE all_stmts_done = FALSE LOOP

				stats_done = null;

				-- This call will wait for start_execute_parallel_counter seconds before it returns
				CALL start_execute_parallel(
					stmts,
					connstr,
					conntions_array,
					conn_stmts,
					stats_done,
					all_stmts_done,
					seconds_to_wait_before_check_result/2,
					max_parallel_jobs,
					true,
					contiune_after_stat_exception
				);

				RAISE NOTICE 'after start par conntions_array Array_length(conntions_array, 1), % conntions_array % loop_number % for topology % at %',
				Array_length(conntions_array, 1), conntions_array, loop_number, (_topology_info).topology_name, Clock_timestamp();

				RAISE NOTICE 'after start par stats_done Array_length(stats_done, 1), % stats_done % loop_number % for topology % at %',
				Array_length(stats_done, 1), stats_done, loop_number, (_topology_info).topology_name, Clock_timestamp();


				IF all_stmts_done = FALSE THEN
					-- setting this sleep time to high, will cause performance issues
					perform pg_sleep(seconds_to_wait_before_check_result/(seconds_to_wait_before_check_result/2));
				END IF;

				-- Controll back to caller
				start_execute_parallel_counter = start_execute_parallel_counter + 1;
				stmts_rerun := '{}';



				RAISE NOTICE 'Num jobs done % for cell_job_type % stmts_jobid_done % start_execute_parallel_counter % for topology % at % in % secs',
				Array_length(stmts_jobid_done, 1), cell_job_type, stmts_jobid_done , start_execute_parallel_counter, (_topology_info).topology_name, Clock_timestamp(), (Extract(EPOCH FROM (Clock_timestamp() - start_time)));

				used_time := (Extract(EPOCH FROM (Clock_timestamp() - start_time_delta_job)));
				-- Do a commit every 5 minute
				IF used_time > (seconds_to_wait_before_check_result*5) THEN
					start_time_delta_job = Clock_timestamp();
					COMMIT;

					RAISE NOTICE 'New snapshot after % secs, Status left % jobs for cell_job_type % running in % threads at loop_number % for topology % at % in % secs',
					used_time, Array_length(stmts, 1), cell_job_type, coalesce(Array_length(conntions_array, 1),0), loop_number, (_topology_info).topology_name, Clock_timestamp(), (Extract(EPOCH FROM (Clock_timestamp() - start_time)));
				END IF;


			END LOOP; -- for running statements when all_stmts_done

			RAISE NOTICE 'Done running % jobs for cell_job_type % at loop_number % for topology % at % in % secs',
			num_jobs_to_run, cell_job_type, loop_number, (_topology_info).topology_name, Clock_timestamp(), (Extract(EPOCH FROM (Clock_timestamp() - start_time)));

/*
			EXCEPTION WHEN OTHERS THEN
				GET STACKED DIAGNOSTICS v_state = RETURNED_SQLSTATE, v_msg = MESSAGE_TEXT, v_detail = PG_EXCEPTION_DETAIL, v_hint = PG_EXCEPTION_HINT, v_context = PG_EXCEPTION_CONTEXT;
				RAISE NOTICE 'Failed run execute_parallel cell_job_type: % , in loop_number %, state  : % message: % detail : % hint : % context: %',
						cell_job_type, loop_number, v_state, v_msg, v_detail, v_hint, v_context;
				IF contiune_after_stat_exception = false THEN
					-- Do a validation is any erros found stop to execute
					start_time := Clock_timestamp();

					command_string := Format('SELECT count(*) FROM topology.ValidateTopology(%L)',(_topology_info).topology_name );
					RAISE NOTICE 'Start to ValidateTopology for cell_job_type % at loop_number % running % ',
					cell_job_type, loop_number, command_string;
					execute command_string into num_topo_error_in_final_layer;

					RAISE NOTICE 'Found % errors when ValidateTopology for cell_job_type % at loop_number % for topology % in % secs',
					num_topo_error_in_final_layer, cell_job_type, loop_number, (_topology_info).topology_name, (Extract(EPOCH FROM (Clock_timestamp() - start_time)));

					IF num_topo_error_in_final_layer > 0 THEN
						-- If any erros found break
						RAISE EXCEPTION 'Failed run execute_parallel cell_job_type and error found : % , in loop_number %, state  : % message: % detail : % hint : % context: %',
						cell_job_type, loop_number, v_state, v_msg, v_detail, v_hint, v_context;
					END IF;
				END IF;

			END; -- EXCEPTION WHEN OTHERS THEN
*/

			IF validate_topoplogy_for_each_run THEN
				start_time := Clock_timestamp();

				command_string := Format('SELECT count(*) FROM topology.ValidateTopology(%L)',(_topology_info).topology_name );
				RAISE NOTICE 'Start to ValidateTopology because validate_topoplogy_for_each_run is true for cell_job_type % at loop_number % running % ',
				cell_job_type, loop_number, command_string;
				execute command_string into num_topo_error_in_final_layer;

				RAISE NOTICE 'Found % errors when ValidateTopology for cell_job_type % at loop_number % for topology % in % secs',
				num_topo_error_in_final_layer, cell_job_type, loop_number, (_topology_info).topology_name, (Extract(EPOCH FROM (Clock_timestamp() - start_time)));

				IF num_topo_error_in_final_layer > 0 THEN
					-- If any erros found break
					RAISE EXCEPTION 'error found when topology.ValidateTopology for job_type : % , in loop_number %',
					cell_job_type, loop_number;
				END IF;

			END IF;



			last_run_stmts := Array_length(stmts, 1);
			loop_number := loop_number + 1;

			IF stop_at_job_type >= cell_job_type AND loop_number >= stop_at_loop_nr  THEN
				RAISE WARNING 'EXIT with % jobs for cell_job_type % at loop_number % for topology % ',
					Array_length(stmts, 1), cell_job_type, loop_number, (_topology_info).topology_name;
					RAISE WARNING 'stmts to run --> %', stmts;
					return ;
			END IF;


		END LOOP; -- end loop for this cell_job_type

		-- Prepare for next cell_job_type and at loop_number 1
		loop_number := 1;

	END LOOP; -- End all cell_job_type

	-- map back faces to primary key
	CALL resolve_overlap_map_faceid_2_input(
	tables_ext_metainfo,
	(_topology_info).topology_name,
	_topology_info,
	table_name_result_prefix,
	overlapgap_grid,
	0, --box_id,
	NULL, --_bb,
	8 --_cell_job_type
	);


	Execute Format('SELECT resolve_overlap_gap_post(%L,%L,%L,%L)',
		tables_ext_metainfo,
		input_data_first_table,
		_topology_info,
		table_name_result_prefix);

END
$$;


-- Wrapper for omitting the _debug_options parameter
CREATE OR REPLACE PROCEDURE resolve_overlap_gap_run(
	_input_data_first_table resolve_overlap_data_input_type,
	_topology_info resolve_overlap_data_topology_type,
	_clean_info resolve_overlap_data_clean_type,
	_max_parallel_jobs int,
	_max_rows_in_each_cell int
)
LANGUAGE plpgsql
AS $$
DECLARE
	input_data_array resolve_overlap_data_input_type[]; -- A list with one entry input tables

BEGIN
	input_data_array = ARRAY[_input_data_first_table];

	CALL resolve_overlap_gap_run(
		input_data_array ,
		_topology_info,
		_clean_info,
		_max_parallel_jobs,
		_max_rows_in_each_cell,
		null::resolve_overlap_data_debug_options_type)
	;
END
$$;


/**

This is wrapper function to support old format of a single table input_data_first_table resolve_overlap_data_input_type

*/

CREATE OR REPLACE PROCEDURE resolve_overlap_gap_run(
	_input_data_first_table resolve_overlap_data_input_type,
	_topology_info resolve_overlap_data_topology_type,
	_clean_info resolve_overlap_data_clean_type,
	_max_parallel_jobs int,
	_max_rows_in_each_cell int,
	_debug_options resolve_overlap_data_debug_options_type
)
LANGUAGE plpgsql
AS $$
DECLARE
	input_data_array resolve_overlap_data_input_type[]; -- A list with one entry input tables

BEGIN
	input_data_array = ARRAY[_input_data_first_table];

	CALL resolve_overlap_gap_run(
		input_data_array ,
		_topology_info,
		_clean_info,
		_max_parallel_jobs,
		_max_rows_in_each_cell,
		_debug_options)
	;
END
$$;


