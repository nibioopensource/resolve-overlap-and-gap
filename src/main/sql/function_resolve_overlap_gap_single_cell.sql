CREATE OR REPLACE PROCEDURE resolve_overlap_gap_single_cell (
_input_data_array resolve_overlap_data_input_type[], -- A list input tables
--(_input_data).line_table_name varchar, -- The table with simple feature lines,
	-- If this has a value then data from table will used to form all valid surfaces.
	-- this may be empty, the polygon_table_geo_collumn must of type polygon to be abale to generate a polygon layer
--(_input_data).line_table_pk_column varchar, -- A unique primary column of the line input table
--(_input_data).line_table_geo_collumn varchar, -- The name of geometry column for the line strings
--(_input_data).polygon_table_name varchar, -- The table to resolv, imcluding schema name
--(_input_data).polygon_table_pk_column varchar, -- The primary of the input table
--(_input_data).polygon_table_geo_collumn varchar, -- the name of geometry column on the table to analyze
--(_input_data).table_srid int, -- the srid for the given geo column on the table analyze
--(_input_data).utm boolean,
_tables_ext_metainfo rog_overlay_source_table_metainfo_type[], -- A new type with more info abouth input tables, to replace _input_data_array resolve_overlap_data_input_type[] in the future

_topology_info resolve_overlap_data_topology_type,
---(_topology_info).topology_name varchar, -- The topology schema name where we store store sufaces and lines from the simple feature dataset and th efinal result
-- NB. Any exting data will related to topology_name will be deleted
--(_topology_info).topology_srid
--(_topology_info).topology_is_utm
--(_topology_info).topology_snap_tolerance float, -- this is tolerance used as base when creating the the postgis topolayer
--(_topology_info).create_topology_attrbute_tables boolean -- if this is true and we value for line_table_name we create attribute tables refferances to
-- this tables will have atrbuttes equal to the simple feauture tables for lines and feautures

-- a flag for testing the diff using a temporary tempology or not or insert directly into final source topology
-- this is a test related to performance and testing on that, the final results should be pretty much the same.
-- (_topology_info).use_temp_topology boolean

-- a flag to run checks like topology.ValidateTopology and https://github.com/larsop/resolve-overlap-and-gap
-- To run this does take time, spesially when working on verry very datasets so we trun this when needed.
-- In the final result there should be no Topology errors in the topology structure
-- In the simple feature result produced from Postgis Topology there should be overlaps
-- (_topology_info).do_qualitycheck_on_final_reseult boolean


_zero_topology_info_topology_snap_tolerance float,
-- We get this as separate variable to handle 0 values
-- This will used when securing border line spacing between grid cell when topology_snap_tolerance  is zero


_table_name_result_prefix varchar,
--(_topology_info).topology_name character varying,
--(_topology_info).topology_snap_tolerance float, -- this is tolerance used as base when creating the the postgis topolayer

_clean_info resolve_overlap_data_clean_type, -- different parameters used if need to clean up your data
--(_clean_info)._min_area_to_keep float default 0, -- if this a polygon  is below this limit it will merge into a neighbour polygon. The area is sqare meter.
--(_clean_info)._simplify_tolerance float default 0, -- is this is more than zero simply will called with
--(_clean_info)._simplify_max_average_vertex_length int default 0, -- in meter both for utm and deegrees, this used to avoid running ST_simplifyPreserveTopology for long lines lines with few points
--(_clean_info)._chaikins_nIterations int default 0, -- IF 0 NO CHAKINS WILL BE DONE,  A big value here make no sense because the number of points will increaes exponential )
--(_clean_info)._chaikins_max_length int default 0, --edge that are longer than this value will not be touched by _chaikins_min_degrees and _chaikins_max_degrees
--(_clean_info)._chaikins_min_degrees int default 0, -- The angle has to be less this given value, This is used to avoid to touch all angles.
--(_clean_info)._chaikins_max_degrees int default 0, -- OR the angle has to be greather than this given value, This is used to avoid to touch all angles
--(_clean_info)._chaikins_min_steep_angle_degrees int default 0, -- The angle has to be less this given value, This is used to avoid to touch all angles.
--(_clean_info)._chaikins_max_steep_angle_degrees int default 0-- OR The angle has to be greather than this given value, This is used to avoid to touch all angles

-- To limit and set as master area. This must be in the same coordsys as _new_sf_table_srid and must be polygon or a multipolgyon,
-- This will added as the first geometry into postgis topology. Data outside the master area will not be used.
-- If you send in a null geometry here this parameter will not have any effect.
--(_clean_info).input_boundary geometry

_job_list_name character varying,
_overlapgap_grid varchar,
_bb geometry,
_cell_job_type int, -- add lines 1 inside cell, 2 boderlines, 3 exract simple
_loop_number int
)
LANGUAGE plpgsql
AS $$
DECLARE
	-- holds dynamic sql to be able to use the same code for different
	command_string text;
	start_time timestamp WITH time zone;
	done_time timestamp WITH time zone;
	used_time real;
	start_time_delta_job timestamp WITH time zone;
	is_done integer = 0;
	area_to_block geometry;
	num_boxes_intersect integer;
	num_boxes_free integer;
	num_rows_removed integer;
	box_id integer;
	face_table_name varchar;
	-- This is used when adding lines hte tolrannce is different when adding lines inside and box and the border;
	snap_tolerance_fixed float = (_topology_info).topology_snap_tolerance;
	temp_table_name varchar;
	temp_table_id_column varchar;
	final_result_table_name varchar;
	update_fields varchar;
	update_fields_source varchar;
	subtransControlLock_start timestamp;
	subtransControlLock_count int;
	subtranscontrollock int;

	v_state text;
	v_msg text;
	v_detail text;
	v_hint text;
	v_context text;
	v_cnt_left_over_borders int;

	error_msg text;



	-- This is points that may intersesct with outher boudary lines
	-- Tht we should not touch before all data are added
	fixed_point_set geometry;

	-- This is the inner area tha we safly can fix
	inner_cell_geom geometry;

	-- Distance to inner safe area
	-- TODo find a valid value  here
	inner_cell_distance int = 10;

	edgelist_to_change int[];

	lines_crossing_cell_borders rog_overlay_add_border_lines_type[];

	edge_id_heal integer;

	heal_edge_status int;
	heal_edge_retry_num int;

	num_locked int;

	tmp_simplified_border_lines_name text;

	border_line_rec RECORD;
	line_edges_geo_failed geometry[];

	this_worker_id int;
	num_jobs_worker_id int;
	num_min_since_last_analyze int;

	analyze_done_at timestamp WITH time zone default null;

	edges_to_heal  integer[][];

	row_count int;

	-- A temp variable for first table in the list
	_input_data resolve_overlap_data_input_type;
	tmp_ext_metainfo rog_overlay_source_table_metainfo_type;
	do_st_transform boolean;

	--TODO compute this value and move to resolve_overlap_data_topology_type
	temp_topology_utm boolean;

	rec_new_line RECORD;
	rec_new_temp_edges int[];
	rec_new_edges_since_anlyze int = 0;
	rec_new_edges_total int = 0;
	rec_new_edges_analyze int = 50;
	rec_new_counter int = 0;

	sql text;

	-- Used for the diffrent layer
	bb_transformed geometry;


	bb_validate_result validatetopology_returntype[];

	analyze_cmd text[];

	done_add_cell_crossing_lines boolean;

	simplify_tolerance float;
	tmp_edge_input_tables_cleanup rog_clean_edge_type;
	edge_input_tables_cleanup_array rog_clean_edge_type[];

	more_edges_to_check_for_removel boolean;

	-- faces,egdes to remove after simplify
	face_edges_to_remove_after_simplify add_border_lines_faces_to_remove[] = '{}'::add_border_lines_faces_to_remove[];
	edgeids_to_remove_after_simplify int[];

		-- a job can return status if all done or not
	is_job_all_done boolean default true;



BEGIN

	IF _input_data_array IS NULL OR ARRAY_LENGTH(_input_data_array,1) = 0 THEN
		RAISE EXCEPTION 'Not valid _input_data_array for % for topology %', _input_data_array, topology_schema_name;
	END IF;

	-- We use this temporary, may be removed
	_input_data = _input_data_array[1];

	command_string := Format('select id from %1$s where cell_geo = %2$L', _job_list_name, _bb);
	RAISE NOTICE '% ', command_string;
	EXECUTE command_string INTO box_id;

	IF CURRENT_USER != 'postgres' AND current_setting('execute_parallel.connstring',true) IS NULL THEN
		RAISE NOTICE 'Not valid a execute_parallel.connstring for user % _cell_job_type % for topology % box_id %',
		CURRENT_USER, _cell_job_type , (_topology_info).topology_name, box_id;
	END IF;



	tmp_simplified_border_lines_name := 'temp_simplified_lines'|| (_topology_info).topology_name || box_id;

	RAISE NOTICE 'Enter work at timeofday:% for layer %, with _cell_job_type % _loop_number % for cell % with bbox %',
	Timeofday(), (_topology_info).topology_name, _cell_job_type, _loop_number, box_id, _bb;


	-- check if job is done already
	command_string := Format('select count(*) from %s as gt, %s as done
		where ST_Equals(gt.cell_geo,%3$L) and gt.id = done.id', _job_list_name, _job_list_name || '_donejobs', _bb);
	EXECUTE command_string INTO is_done;
	IF is_done = 1 THEN
		RAISE NOTICE 'Job is_done for  : %', box_id;
		RETURN;
	END IF;
	start_time := Clock_timestamp();

	-- get area to block and set
	-- I don't see why we need this code ??????????? why cant we just the _bb as it is so I test thi snow
	area_to_block := _bb;

	-- Check if area is locked.
	IF topo_update.lock_area((_topology_info).topology_name, _overlapgap_grid, 'cell_bbox', _bb, 1) = FALSE THEN
		RAISE NOTICE 'Arealock false for : % _cell_job_type %, _loop_number % ', box_id, _cell_job_type, _loop_number;
		RETURN;
	ELSE
		RAISE NOTICE 'Arealock true for : % _cell_job_type %, _loop_number % ', box_id, _cell_job_type, _loop_number;
	END IF;




	-- check if any 'SubtransControlLock' is there
	subtransControlLock_start = clock_timestamp();
	subtransControlLock_count = 0;

	-- this isnot good beacuse if have code making this block
	-- we need to make a check on for relation
	-- SELECT relation::regclass, * FROM pg_locks WHERE NOT GRANTED;
	--relation           | test_topo_ar5.edge_data
	LOOP
		EXECUTE Format('SELECT count(*) from pg_stat_activity where query like %L and wait_event in (%L,%L,%L)',
		'CALL resolve_overlap_gap_single_cell%','SubtransControlLock','relation','SubtransSLRU') into subtransControlLock;
		--EXIT WHEN subtransControlLock = 0 OR subtransControlLock_count > 100;
		EXIT WHEN subtransControlLock < 3 OR subtransControlLock_count > 100;

		subtransControlLock_count := subtransControlLock_count + 1;
		PERFORM pg_sleep(subtransControlLock*subtransControlLock_count*0.1);
	END LOOP;

	IF subtransControlLock_count > 0 THEN
		RAISE NOTICE 'At timeofday:% number % subtransControlLock,relation,SubtransSLRU loops sleep % seconds to wait for release, for _cell_job_type %',
		Timeofday(),
		subtransControlLock_count,
		(Extract(EPOCH FROM (clock_timestamp() - subtransControlLock_start))),
		_cell_job_type;
	END IF;


	IF _cell_job_type = 1 THEN
		-- get the simple feature data both the line_types and the inner lines.
		-- the boundery lines are saved in a separate table for later usage

		-- topo_update.get_simplified_split_border_lines testing only, it gives errors in some cases
		-- topo_update.get_simplified_border_lines  in prod now

		CALL resolve_overlap_gap_cell_job_type_01(
		_tables_ext_metainfo,
		_topology_info,
		_zero_topology_info_topology_snap_tolerance,
		_table_name_result_prefix,
		_clean_info,
		_job_list_name,
		_overlapgap_grid,
		_bb,
		_cell_job_type,
		_loop_number,
		box_id,
		tmp_simplified_border_lines_name,
		is_job_all_done
		);
		-- if the tho job is not just return (we run this job many loops to avoid )
		IF NOT is_job_all_done THEN
			EXECUTE topo_update.release_lock_area((_topology_info).topology_name, _overlapgap_grid, 'cell_bbox', _bb);
			RAISE NOTICE 'Arealock release for % _cell_job_type %, _loop_number % ', box_id, _cell_job_type, _loop_number;
			RAISE NOTICE 'Leave work not done_add_cell_crossing_lines at timeofday:% for layer %, with _cell_job_type % _loop_number % for cell % with bbox %',
			Timeofday(), (_topology_info).topology_name, _cell_job_type, _loop_number, box_id, _bb;
		END IF;


	ELSIF _cell_job_type = 2 THEN
		-- Add border lines for crossing grids for short lines from *_border_line_segments_final
		-----------------------------------------------------
		CALL topo_update.add_cellborder_lines(
			_topology_info, --01
			_clean_info, --02
			_table_name_result_prefix, --03
			--this_topology_info_topology_snap_tolerance --04
			_zero_topology_info_topology_snap_tolerance, --04
			_bb, --05
			TRUE, -- _return_if_long_execute_time boolean, -- if too long time is used the code will return and the caller has to retry when TRUE
			_tables_ext_metainfo,
			done_add_cell_crossing_lines
		);

		-- If are not just commit the current and return who will trigger jobs again to the rest
		IF done_add_cell_crossing_lines = FALSE THEN
			-- Reelase locked area, so next loop can try to finish the job
			EXECUTE topo_update.release_lock_area((_topology_info).topology_name, _overlapgap_grid, 'cell_bbox', _bb);
			RAISE NOTICE 'Arealock release for % _cell_job_type %, _loop_number % ', box_id, _cell_job_type, _loop_number;
			--COMMIT ;
			RAISE NOTICE 'Leave work not done_add_cell_crossing_lines at timeofday:% for layer %, with _cell_job_type % _loop_number % for cell % with bbox %',
			Timeofday(), (_topology_info).topology_name, _cell_job_type, _loop_number, box_id, _bb;
			RETURN;
		END IF;

		-- TODO find out what  this code part ???
		IF (_topology_info).create_topology_attrbute_tables THEN
			IF (_input_data).line_table_name is not null THEN
				command_string := Format('WITH lines_addes AS (
					SELECT DISTINCT ON(e.edge_id) e.edge_id, g.column_data_as_json
					FROM
					%1$s as g,
					%2$s as e
					where g.geo && %10$L and
					ST_DWithin( e.geom, g.geo, %4$L) and
					ST_DWithin( ST_StartPoint(e.geom), g.geo, %4$L) and
					ST_DWithin( ST_EndPoint(e.geom), g.geo, %4$L)
					)
					INSERT INTO %5$s(%6$s,%3$s)
					SELECT x.*,
					topology.CreateTopoGeom(%7$L,2,%8$L,ARRAY[ARRAY[ee.edge_id,2]]::topology.topoelementarray ) as %3$s
					FROM lines_addes ee,
					jsonb_to_record(ee.column_data_as_json) AS x(%9$s)',
					tmp_simplified_border_lines_name||'short',
					(_topology_info).topology_name||'.edge_data',
					(_input_data).line_table_geo_collumn,
					(_topology_info).topology_snap_tolerance,
					(_topology_info).topology_name||'.edge_attributes',
					(_input_data).line_table_other_collumns_list,
					(_topology_info).topology_name,
					(_topology_info).topology_attrbute_tables_border_layer_id,
					(_input_data).line_table_other_collumns_def,
					_bb
					);
					EXECUTE command_string;
				ELSE
					command_string := Format('WITH lines_addes AS (
					SELECT DISTINCT ON(e.edge_id) e.edge_id, e.geom
					FROM
					%1$s as g,
					%2$s as e
					where g.geo && %10$L and
					ST_DWithin( e.geom, g.geo, %4$L) and
					ST_DWithin( ST_StartPoint(e.geom), g.geo, %4$L) and
					ST_DWithin( ST_EndPoint(e.geom), g.geo, %4$L)
					)
					INSERT INTO %5$s(%3$s)
					SELECT toTopoGeom(ee.geom, %7$L, %8$L,  %4$L) as %3$s
					FROM lines_addes ee',
					tmp_simplified_border_lines_name||'long',
					(_topology_info).topology_name||'.edge_data',
					(_input_data).line_table_geo_collumn,
					(_topology_info).topology_snap_tolerance,
					(_topology_info).topology_name||'.edge_attributes',
					(_input_data).line_table_other_collumns_list,
					(_topology_info).topology_name,
					(_topology_info).topology_attrbute_tables_border_layer_id,
					(_input_data).line_table_other_collumns_def,
					_bb
					);
					--EXECUTE command_string;
			END IF;
		END IF;


	ELSIF _cell_job_type = 3 THEN
		-- When adding border line crossing cells in jobtype new small areas may be created and they will be removed here

		IF area_to_block is NULL or ST_Area(area_to_block) = 0.0 THEN
			area_to_block := _bb;
		END IF;

		-- Remome small polygons based (_clean_info).min_area_to_keep
		IF (_clean_info).min_area_to_keep IS NOT NULL AND (_clean_info).min_area_to_keep > 0 THEN
			start_time_delta_job := Clock_timestamp();
			RAISE NOTICE 'Start clean small polygons for face_table_name % at %', (_topology_info).topology_name || '.face', Clock_timestamp();
			CALL topo_update.do_merge_small_areas_no_block (
			(_topology_info).topology_name,
			_table_name_result_prefix,
			(_clean_info).min_area_to_keep,
			(_clean_info).min_st_is_empty_buffer_value,
			_bb,
			(_topology_info).topology_is_utm,
			_cell_job_type,
			more_edges_to_check_for_removel, -- no neeed to check on more_edges_to_check_for_removel
			fixed_point_set
			);
			used_time := (Extract(EPOCH FROM (Clock_timestamp() - start_time_delta_job)));
			RAISE NOTICE 'Done clean small polygons for (more edges to remove) more_edges_to_check_for_removel % for face_table_name % at % used_time: %',
			more_edges_to_check_for_removel, (_topology_info).topology_name || '.face', Clock_timestamp(), used_time;

			IF more_edges_to_check_for_removel THEN
				COMMIT;
					RAISE NOTICE 'Leave work min_area_to_keep at timeofday:% for layer %, with _cell_job_type % _loop_number % for cell % with bbox %',
					Timeofday(), (_topology_info).topology_name, _cell_job_type, _loop_number, box_id, _bb;

				RETURN;
			END IF;
		END IF;


	ELSIF _cell_job_type = 4 THEN
		-- When adding new edges and removing small arease we need to heal egdes or else we can run simplify in jobtype 5


--    IF _loop_number < 1 THEN
			-- In first loop only block by egdes
			command_string := Format('SELECT ST_Union(geom) from (SELECT ST_Expand(ST_Envelope(%1$s),%2$s) as geom from %3$s where ST_intersects(%1$s,%4$L) ) as r',
			'geom', (_topology_info).topology_snap_tolerance, (_topology_info).topology_name||'.edge_data', _bb);
/**

		This is old code is not complient with multiple table input

		ELSE
			-- In second loop block by input geo size
			command_string := Format('SELECT ST_Expand(ST_Envelope(ST_collect(%1$s)),%2$s) from %3$s where ST_intersects(%1$s,%4$L);',
			(_input_data).polygon_table_geo_collumn, (_topology_info).topology_snap_tolerance, (_input_data).polygon_table_name, _bb);
		END IF;
*/

		EXECUTE command_string INTO area_to_block;

		IF area_to_block is NULL or ST_Area(area_to_block) = 0.0 THEN
			area_to_block := _bb;
		END IF;

		-- Check if we have som area to block has anay eges to heal
		command_string := Format('SELECT topo_update.do_find_edges_to_heal(%1$L,%2$L)',
				(_topology_info).topology_name, _bb);
		EXECUTE command_string INTO edges_to_heal;

		-- Check that nedded area is free
		IF edges_to_heal IS NOT NULL AND (Array_length(edges_to_heal, 1) > 0 ) THEN

			command_string := Format('select count(*) from (select * from %1$s where cell_geo && %2$L and ST_intersects(cell_geo,%2$L) for update SKIP LOCKED) as r;', _job_list_name, area_to_block);
			EXECUTE command_string INTO num_boxes_free;

			command_string := Format('select count(*) from %1$s where cell_geo && %2$L and ST_intersects(cell_geo,%2$L);', _job_list_name, area_to_block);
			EXECUTE command_string INTO num_boxes_intersect;

			IF num_boxes_intersect != num_boxes_free THEN
				RAISE NOTICE 'Wait to handle add cell border edges for _cell_job_type %, num_boxes_intersect %, num_boxes_free %, for area_to_block % ',
				_cell_job_type, num_boxes_intersect, num_boxes_free, area_to_block;
				EXECUTE topo_update.release_lock_area((_topology_info).topology_name, _overlapgap_grid, 'cell_bbox', _bb);
				RAISE NOTICE 'Arealock release for % _cell_job_type %, _loop_number % ', box_id, _cell_job_type, _loop_number;
				-- Just return not OK free arae
				RETURN;
			END IF;

			-- Start the heal edge job
			start_time_delta_job := Clock_timestamp();
			command_string := Format('SELECT topo_update.do_healedges_no_block(%1$L,%2$L,%3$L)',
				(_topology_info).topology_name,
				_bb,
				(_clean_info).split_input_lines_at_vertex_n
			);
			EXECUTE command_string;
			RAISE NOTICE 'Did Heal lines for topo % and bb % at % after added edges for border lines used_time %',
			(_topology_info).topology_name, _bb, Clock_timestamp(), (Extract(EPOCH FROM (Clock_timestamp() - start_time_delta_job)));

		END IF;

	ELSIF _cell_job_type = 5 THEN
		-- Simplify and heal edges crossing many lines

		command_string := Format('SELECT ST_Union(geom) from (select ST_Expand(ST_Envelope(%1$s),%2$s) as geom from %3$s where ST_intersects(%1$s,%4$L) ) as r',
		'geom', (_topology_info).topology_snap_tolerance, (_topology_info).topology_name||'.edge_data', _bb);
		EXECUTE command_string INTO area_to_block;

		IF area_to_block is NULL or ST_Area(area_to_block) = 0.0 THEN
			area_to_block := _bb;
		END IF;

		command_string := Format('select count(*) from (select * from %1$s where cell_geo && %2$L and ST_intersects(cell_geo,%2$L) for update SKIP LOCKED) as r;', _job_list_name, area_to_block);
		EXECUTE command_string INTO num_boxes_free;

		command_string := Format('select count(*) from %1$s where cell_geo && %2$L and ST_intersects(cell_geo,%2$L);', _job_list_name, area_to_block);
		EXECUTE command_string INTO num_boxes_intersect;

		IF num_boxes_intersect != num_boxes_free THEN
			RAISE NOTICE 'Wait to handle add cell border edges for _cell_job_type %, num_boxes_intersect %, num_boxes_free %, for area_to_block % ',
			_cell_job_type, num_boxes_intersect, num_boxes_free, area_to_block;
			EXECUTE topo_update.release_lock_area((_topology_info).topology_name, _overlapgap_grid, 'cell_bbox', _bb);
			RAISE NOTICE 'Arealock release for % _cell_job_type %, _loop_number % ', box_id, _cell_job_type, _loop_number;

			RETURN;
		END IF;


		BEGIN

			-- IF we should run clean
			IF (_clean_info).edge_input_tables_cleanup IS NOT NULL AND ARRAY_Length((_clean_info).edge_input_tables_cleanup,1) > 0 THEN

				-- find rows crosing cell borders
				command_string := Format('CREATE TEMP table temp_left_over_borders ON COMMIT DROP as select table_input_order, src_table_pk_column_value, geo FROM
				(select table_input_order, src_table_pk_column_value, geo from topo_update.get_left_over_borders(%1$L,%2$L,%3$L,%4$L) as r) as r',
				_overlapgap_grid, (_input_data).polygon_table_geo_collumn, _bb, _table_name_result_prefix,(_topology_info).topology_snap_tolerance*inner_cell_distance);
				EXECUTE command_string;
				GET DIAGNOSTICS v_cnt_left_over_borders = ROW_COUNT;

				-- if rows found, get edges id
				IF v_cnt_left_over_borders > 0 AND simplify_tolerance IS NOT NULL AND simplify_tolerance > 0  THEN

					start_time_delta_job := Clock_timestamp();
					SELECT ARRAY_AGG(be)
							FROM
							(
								SELECT (inputLineEdgeIds,dropInputLines,failed_line,face_edges_to_remove_list,table_input_order,src_table_pk_column_value,new_line)::rog_overlay_add_border_lines_type be
								FROM
								(
									SELECT
										NULL AS inputLineEdgeIds,
										NULL AS dropInputLines,
										NULL AS failed_line,
										NULL AS face_edges_to_remove_list,
										lb.table_input_order,
										lb.src_table_pk_column_value,
										geo AS new_line
									FROM
										temp_left_over_borders lb
									) AS e
							) AS r
					INTO lines_crossing_cell_borders;

/**
					-- Call simplify for those edges
					call rog_simplify_edges_in_topo (
					_tables_ext_metainfo,
					_topology_info,
					_table_name_result_prefix,
					_clean_info,
					_bb,
					lines_crossing_cell_borders,
					_topology_info.topology_name,
					false, --complete_list_edgeids boolean,
					face_edges_to_remove_after_simplify
					);
*/

					-- Call simplify
					call rog_simplify_edges_in_topo_v2 (
						_tables_ext_metainfo::rog_overlay_source_table_metainfo_type[],
						_topology_info,
						_table_name_result_prefix,
						_clean_info,
						_bb,
						_border_topo_info.topology_name,
						lines_crossing_cell_borders
					);



					CALL resolve_overlap_gap_cell_type_01_update_edge_origin (
						_border_topo_info.topology_name,
						_bb,
						_box_id,
						lines_crossing_cell_borders
					);

					CALL resolve_overlap_gap_cell_job_type_01_find_edges_to_remove(
						_topology_info,
						_clean_info,
						_border_topo_info.topology_name,
						_added_lines_temp_topo,
						face_edges_to_remove_after_simplify
					);


					RAISE NOTICE 'size face_edges_to_remove_after_simplify % edgeids_to_remove_after_simplify %',
					ARRAY_LENGTH(face_edges_to_remove_after_simplify,1), ARRAY_LENGTH(edgeids_to_remove_after_simplify,1);

					-- call remove small areas because of simplify
					CALL topo_update.do_remove_small_areas_no_block (
						_border_topo_info.topology_name,
						(_clean_info).min_area_to_keep,
						(_clean_info).min_st_is_empty_buffer_value,
						_bb,
						_topology_is_utm,
						_cell_job_type ,
						(_res_split_border_lines).fixed_point_set,
						face_edges_to_remove_after_simplify
					);

				-- TODO small areas remove
			END IF;

/**

this is thw old code for simplify
				command_string := Format('SELECT ARRAY(SELECT e.edge_id
					FROM (
					SELECT distinct e1.edge_id
					FROM
						%1$s.edge_data e1,
						temp_left_over_borders lb
					WHERE
				ST_Intersects(lb.geo,e1.geom) AND
				ST_NumPoints(e1.geom) > 2
				ORDER by e1.edge_id
				) as e
				)',
				(_topology_info).topology_name);
				EXECUTE command_string into edgelist_to_change;


				IF edgelist_to_change IS NOT NULL AND (Array_length(edgelist_to_change, 1)) IS NOT NULL THEN
					FOREACH edge_id_heal IN ARRAY edgelist_to_change
					LOOP
						heal_edge_retry_num := 1;
						LOOP
							command_string := FORMAT('SELECT topo_update.try_ST_ChangeEdgeGeom(e.geom,%1$L,%4$L,%5$L,e.edge_id,ST_simplifyPreserveTopology(e.geom,%2$s))
							from %1$s.edge_data e where e.edge_id = %3$s',
							(_topology_info).topology_name, simplify_tolerance/heal_edge_retry_num, edge_id_heal, (_clean_info).simplify_max_average_vertex_length, (_topology_info).topology_is_utm);
							EXECUTE command_string into heal_edge_status;
							EXIT WHEN heal_edge_status in (0,1) or heal_edge_retry_num > 5;
							heal_edge_retry_num := heal_edge_retry_num  + 1;
						END LOOP;
						IF heal_edge_status = -1 THEN
							RAISE NOTICE 'Failed to run topo_update.try_ST_ChangeEdgeGeom using ST_simplifyPreserveTopologyfor edge_id % for topology % using tolerance % .' ,
							edge_id_heal, (_topology_info).topology_name, simplify_tolerance;
						END IF;
					END LOOP;
				END IF;

*/
				RAISE NOTICE 'Did ST_simplifyPreserveTopology for topo % and bb % at % used_time %',
				(_topology_info).topology_name, _bb, Clock_timestamp(), (Extract(EPOCH FROM (Clock_timestamp() - start_time_delta_job)));



			END IF;
			-- if any left over borders and do chaikins
			IF v_cnt_left_over_borders > 0 AND (_clean_info).chaikins_nIterations > 0 THEN
				start_time_delta_job := Clock_timestamp();

				command_string := Format('SELECT topo_update.try_ST_ChangeEdgeGeom(e.geom,%1$L,%6$L,%3$L,e.edge_id,
				ST_simplifyPreserveTopology(topo_update.chaikinsAcuteAngle(e.geom,%3$L,%4$L), %5$s ))
				FROM (
				SELECT distinct e1.edge_id, e1.geom
					FROM
						%1$s.edge_data e1,
						temp_left_over_borders lb
					WHERE
				ST_Intersects(lb.geo,e1.geom)) e',
				(_topology_info).topology_name, _bb, (_topology_info).topology_is_utm, _clean_info, (_topology_info).topology_snap_tolerance/2,(_clean_info).simplify_max_average_vertex_length);
				EXECUTE command_string;
				RAISE NOTICE 'Did chaikinsAcuteAngle for topo % and bb % at % used_time %',
				(_topology_info).topology_name, _bb, Clock_timestamp(), (Extract(EPOCH FROM (Clock_timestamp() - start_time_delta_job)));

			END IF;

			-- remove
			face_table_name = (_topology_info).topology_name || '.face';

			-- DO need to do this if v_cnt_left_over_borders is 0 ??????????

			-- remove small polygons after healed/simplify long edges inside bbox
			IF (_clean_info).min_area_to_keep IS NOT NULL AND (_clean_info).min_area_to_keep > 0 THEN
				start_time_delta_job := Clock_timestamp();
				RAISE NOTICE 'Start 2 clean small polygons for border plygons face_table_name % at %', face_table_name, Clock_timestamp();
				CALL topo_update.do_merge_small_areas_no_block (
					(_topology_info).topology_name,
					_table_name_result_prefix,
					(_clean_info).min_area_to_keep,
					(_clean_info).min_st_is_empty_buffer_value,
					ST_Expand(_bb,((_topology_info).topology_snap_tolerance * -6)),
					(_topology_info).topology_is_utm,
					_cell_job_type,
					more_edges_to_check_for_removel -- no neeed to check on more_edges_to_check_for_removel
					);
				used_time := (Extract(EPOCH FROM (Clock_timestamp() - start_time_delta_job)));
				RAISE NOTICE 'clean 2 small polygons for after adding to main face_table_name % at % used_time: %', face_table_name, Clock_timestamp(), used_time;
			END IF;

			--drop table temp_left_over_borders;
		EXCEPTION
			WHEN OTHERS THEN
				GET STACKED DIAGNOSTICS v_state = RETURNED_SQLSTATE, v_msg = MESSAGE_TEXT, v_detail = PG_EXCEPTION_DETAIL, v_hint = PG_EXCEPTION_HINT,
				v_context = PG_EXCEPTION_CONTEXT;

			RAISE NOTICE 'Fail, do rollback at timeofday:% for layer %, with _cell_job_type % and box id % , : % message: % detail : % hint : % context: %',
			Timeofday(), (_topology_info).topology_name || '_', _cell_job_type, box_id,v_state, v_msg, v_detail, v_hint, v_context;
			ROLLBACK;
			EXECUTE topo_update.release_lock_area((_topology_info).topology_name, _overlapgap_grid, 'cell_bbox', _bb);
			RAISE NOTICE 'Arealock release for % _cell_job_type %, _loop_number % ', box_id, _cell_job_type, _loop_number;

			RETURN;
		END;

	ELSIF _cell_job_type = 6 THEN

/**
	This is old code is not complient with multiple table input

		command_string := Format('SELECT ST_Expand(ST_Envelope(ST_collect(%1$s)),%2$s) from %3$s where ST_intersects(%1$s,%4$L);',
		(_input_data).polygon_table_geo_collumn, (_topology_info).topology_snap_tolerance, (_input_data).polygon_table_name, _bb);
		EXECUTE command_string INTO area_to_block;
*/
		IF area_to_block is NULL or ST_Area(area_to_block) = 0.0 THEN
			area_to_block := _bb;
		END IF;

		command_string := Format('select count(*) from %1$s where cell_geo && %2$L and ST_intersects(cell_geo,%2$L);', _job_list_name, area_to_block);
		EXECUTE command_string INTO num_boxes_intersect;
		command_string := Format('select count(*) from (select * from %1$s where cell_geo && %2$L and ST_intersects(cell_geo,%2$L) for update SKIP LOCKED) as r;', _job_list_name, area_to_block);
		EXECUTE command_string INTO num_boxes_free;
		IF num_boxes_intersect != num_boxes_free THEN
			RAISE NOTICE 'Wait to handle add cell border edges for _cell_job_type %, num_boxes_intersect %, num_boxes_free %, for area_to_block % ',
			_cell_job_type, num_boxes_intersect, num_boxes_free, area_to_block;
			EXECUTE topo_update.release_lock_area((_topology_info).topology_name, _overlapgap_grid, 'cell_bbox', _bb);
			RAISE NOTICE 'Arealock release for % _cell_job_type %, _loop_number % ', box_id, _cell_job_type, _loop_number;

			RETURN;
		END IF;

		-- remove
		face_table_name = (_topology_info).topology_name || '.face';

		-- remove small polygons in temp in (_clean_info).min_area_to_keep
		IF (_clean_info).min_area_to_keep IS NOT NULL AND (_clean_info).min_area_to_keep > 0 THEN
			start_time_delta_job := Clock_timestamp();
			RAISE NOTICE 'Start 3 clean small polygons for border plygons face_table_name % at %', face_table_name, Clock_timestamp();
			-- remove small polygons in temp
			call topo_update.do_merge_small_areas_no_block (
			(_topology_info).topology_name,
			_table_name_result_prefix,
			(_clean_info).min_area_to_keep,
			(_clean_info).min_st_is_empty_buffer_value,
			ST_Expand(_bb,((_topology_info).topology_snap_tolerance * -6)),
			(_topology_info).topology_is_utm,
			_cell_job_type,
			more_edges_to_check_for_removel -- no neeed to check on more_edges_to_check_for_removel
			);
			used_time := (Extract(EPOCH FROM (Clock_timestamp() - start_time_delta_job)));
			RAISE NOTICE 'clean 3 small polygons for after adding to main face_table_name % at % used_time: %', face_table_name, Clock_timestamp(), used_time;
		END IF;

		IF ((_clean_info).resolve_based_on_attribute).attribute_resolve_list IS NOT NULL THEN
				start_time_delta_job := Clock_timestamp();
				RAISE NOTICE 'Start 3 resolve polygons based on attribute type for face_table_name % at %', face_table_name, Clock_timestamp();
				-- TODO call add new code to call
					CALL topo_update.do_merge_based_on_attribute_type_no_block (
					_input_data,
					_clean_info,
					(_topology_info).topology_name,
					(_topology_info).topology_snap_tolerance,
					face_table_name,
					_bb,
					_table_name_result_prefix
					);
				used_time := (Extract(EPOCH FROM (Clock_timestamp() - start_time_delta_job)));
				RAISE NOTICE 'Done 3 resolve polygons based on attribute type for face_table_name % at % used_time: %', face_table_name, Clock_timestamp(), used_time;
		END IF;

	ELSIF _cell_job_type = 7 THEN

		IF (_topology_info).do_qualitycheck_on_final_reseult THEN
			BEGIN
				-- ValidateTopology and insert result into log table if requested
				command_string := FORMAT(
				$fmt$
				SELECT array_remove(
					ARRAY_AGG(
					(SELECT r.validatetopology FROM ( SELECT topology.ValidateTopology(%1$L,%2$L)) r )
					)::validatetopology_returntype[]
					,
					NULL
				)
				$fmt$,
				(_topology_info).topology_name,
				_bb
				);


				EXECUTE command_string INTO bb_validate_result;



				IF '{}' = bb_validate_result::text OR ARRAY_LENGTH(bb_validate_result,1) = 0 THEN
					EXECUTE Format('UPDATE %s SET validate_topology_ok = TRUE WHERE id = %s', _overlapgap_grid, box_id);
				ELSE
					EXECUTE Format('UPDATE %s SET validate_topology_ok = FALSE WHERE id = %s', _overlapgap_grid, box_id);

					EXECUTE Format('
					INSERT INTO %1$s (id,validate_result,cell_bbox)
					SELECT %2$s AS id, %3$L AS validate_result, %4$L AS cell_bbox ',
				_overlapgap_grid||'_validate_topology',
				box_id,
				bb_validate_result,
					_bb
					);

				END IF;

			EXCEPTION WHEN OTHERS THEN
				GET STACKED DIAGNOSTICS v_state = RETURNED_SQLSTATE, v_msg = MESSAGE_TEXT, v_detail = PG_EXCEPTION_DETAIL, v_hint = PG_EXCEPTION_HINT,
				v_context = PG_EXCEPTION_CONTEXT;

				EXECUTE Format('UPDATE %s SET validate_topology_ok = FALSE WHERE id = %s', _overlapgap_grid, box_id);

				error_msg = Format('EXCEPTION when run ValidateTopology at timeofday %s for layer %s, with _cell_job_type %s and box id %s , : %s message: %s detail : %s hint : %s context: %s',
				Timeofday(), (_topology_info).topology_name || '_', _cell_job_type, box_id,v_state, v_msg, v_detail, v_hint, v_context);

				EXECUTE Format('UPDATE %s SET validate_topology_exception = %L WHERE id = %s', _overlapgap_grid, error_msg, box_id);

			END;

		END IF;

		-- map back faces to primary key
		CALL resolve_overlap_map_faceid_2_input(
		_tables_ext_metainfo,
		(_topology_info).topology_name,
		_topology_info,
		_table_name_result_prefix,
		_overlapgap_grid,
		box_id,
		_bb,
		_cell_job_type);
	ELSE
		RAISE EXCEPTION 'Invalid _cell_job_type % ', _cell_job_type;
	END IF;


	IF is_job_all_done THEN

		command_string := Format('update %1$s set block_bb = %2$L where cell_geo = %3$L', _job_list_name, _bb, _bb);
		EXECUTE command_string;

		-- do analyse if add data using addd_linstring to final topology
		-- if just copy data  like with like 3 there is now need for this
		IF (_topology_info).transfer_method_from_temp_2_final_topology in (1,2) AND _cell_job_type < 2 THEN
			command_string := Format('select worker_id from %1$s where id = %2$s', _job_list_name, box_id);
			EXECUTE command_string INTO this_worker_id;

			-- 1599830100.882998 |
			-- 1599835466.322007

	--    SELECT (EXTRACT(EPOCH FROM TRANSACTION_TIMESTAMP()))- EXTRACT(EPOCH FROM (d.analyze_time)),d.analyze_time, d.done_time from  test_topo_jm_t2.jm_ukomm_flate_job_list_donejobs d

			IF this_worker_id = 1 THEN
				command_string := Format('select count(d.*),
				((EXTRACT(EPOCH FROM Clock_timestamp())-EXTRACT(EPOCH FROM max(d.analyze_time)))/60)::int time_diff
				from
				%1$s l,
				%2$s d
				where d.id = l.id and l.worker_id = %3$s',
				_job_list_name,
				_job_list_name||'_donejobs',
				this_worker_id,
				box_id);

				EXECUTE command_string INTO num_jobs_worker_id, num_min_since_last_analyze;

				-- Maybe find a better way for this, Do analyze the first 3 rounds and the at least once hour or more more of slowing down
				IF num_jobs_worker_id  < 3 or num_jobs_worker_id is null or
					num_min_since_last_analyze > num_jobs_worker_id*2 or
					num_min_since_last_analyze > 30 THEN


					RAISE NOTICE 'Start VACUUM (ANALYZE,verbose) for tables for final topology % for num_jobs_worker_id % and box_id % snd _cell_job_type % and num_min_since_last_analyze % at %',
					(_topology_info).topology_name, num_jobs_worker_id, box_id, _cell_job_type, num_min_since_last_analyze, Clock_timestamp();

					analyze_cmd = FORMAT(
						$fmt$
						{"VACUUM (ANALYZE,verbose) %1$s.node","VACUUM (ANALYZE,verbose) %1$s.relation","VACUUM (ANALYZE,verbose) %1$s.edge_data","VACUUM (ANALYZE,verbose) %1$s.face"}
						$fmt$,
						(_topology_info).topology_name
					);
					call do_execute_parallel(analyze_cmd::text[],current_setting('execute_parallel.connstring',true),1);

	/**

					EXECUTE Format('ANALYZE VERBOSE %1$s.node', (_topology_info).topology_name);
					EXECUTE Format('ANALYZE VERBOSE %s.relation', (_topology_info).topology_name);
					EXECUTE Format('ANALYZE VERBOSE %s.edge_data', (_topology_info).topology_name);
					EXECUTE Format('ANALYZE VERBOSE %s.face', (_topology_info).topology_name);
	*/

					RAISE NOTICE 'Done ANALYZE for tables for final topology % for num_jobs_worker_id % and box_id % snd _cell_job_type % and num_min_since_last_analyze % at %',
					(_topology_info).topology_name, num_jobs_worker_id, box_id, _cell_job_type, num_min_since_last_analyze, Clock_timestamp();

					analyze_done_at := TRANSACTION_TIMESTAMP();
				ELSE
					RAISE NOTICE 'Not Do analyze for % for num_jobs_worker_id % and box_id % snd _cell_job_type % and num_min_since_last_analyze % at %',
					(_topology_info).topology_name, num_jobs_worker_id, box_id, _cell_job_type, num_min_since_last_analyze, Clock_timestamp();
				END IF;
			END IF;
		END IF;


		command_string := Format('insert into %1$s(id,analyze_time) select gt.id, %4$L from %2$s as gt
				where ST_Equals(gt.cell_geo,%3$L)',
				_job_list_name || '_donejobs',
				_job_list_name,
				_bb,
				analyze_done_at);
		EXECUTE command_string;


		done_time := Clock_timestamp();
		used_time := (Extract(EPOCH FROM (done_time - start_time)));
		IF used_time > 60 THEN
			EXECUTE Format('INSERT INTO %s (execute_time, info, sql, geo) VALUES (%s, %L, %L, %L)', _table_name_result_prefix || '_long_time_log2', used_time, 'simplefeature_c2_topo_surface_border_retry', command_string, _bb);
		END IF;

		-- Check if area is locked.
		EXECUTE topo_update.release_lock_area((_topology_info).topology_name, _overlapgap_grid, 'cell_bbox', _bb);
		RAISE NOTICE 'Arealock release for % _cell_job_type %, _loop_number % ', box_id, _cell_job_type, _loop_number;

		RAISE NOTICE 'Done work using transfer_method_from_temp_2_final_topology % at timeofday:% for layer %, with _cell_job_type % _loop_number % for cell %, using % sec',
		(_topology_info).transfer_method_from_temp_2_final_topology, Timeofday(), (_topology_info).topology_name, _cell_job_type, _loop_number, box_id, used_time;
	END IF;


END
$$;


--truncate table test_topo_ar50_t11.ar50_utvikling_flate_job_list_donejobs;
--
--CALL resolve_overlap_gap_single_cell(
--  'sl_esh.ar50_utvikling_flate','geo','sl_sdeid','test_topo_ar50_t11.ar50_utvikling_flate',
--  'test_topo_ar50_t11',1,25833,'true',
--  '(300,9,500,3,140,120,240,3,35)',
--  'test_topo_ar50_t11.ar50_utvikling_flate_job_list','test_topo_ar50_t11.ar50_utvikling_flate_grid',
--  '0103000020E9640000010000000500000000000000B0A6074100000000F9F05A4100000000B0A607410000008013F75A4100000000006A08410000008013F75A4100000000006A084100000000F9F05A4100000000B0A6074100000000F9F05A41'
--  ,1,1);
--
--CALL resolve_overlap_gap_single_cell(
--  'sl_esh.ar50_utvikling_flate','geo','sl_sdeid','test_topo_ar50_t11.ar50_utvikling_flate',
--  'test_topo_ar50_t11',1,25833,'true',
--  '(300,9,500,3,140,120,240,3,35)',
--  'test_topo_ar50_t11.ar50_utvikling_flate_job_list','test_topo_ar50_t11.ar50_utvikling_flate_grid',
--  '0103000020E9640000010000000500000000000000B0A60741000000C0EBED5A4100000000B0A6074100000000F9F05A41000000005808084100000000F9F05A410000000058080841000000C0EBED5A4100000000B0A60741000000C0EBED5A41'
--  ,1,1);


--truncate table test_topo_ar50_t11.ar50_utvikling_flate_job_list_donejobs;
--
--CALL resolve_overlap_gap_single_cell(
--  'sl_esh.ar50_utvikling_flate','geo','sl_sdeid','test_topo_ar50_t11.ar50_utvikling_flate',
--  'test_topo_ar50_t11',1,25833,'true',
--  '(300,9,500,3,140,120,240,3,35)',
--  'test_topo_ar50_t11.ar50_utvikling_flate_job_list','test_topo_ar50_t11.ar50_utvikling_flate_grid',
--  '0103000020E9640000010000000500000000000000B0A6074100000000F9F05A4100000000B0A607410000008013F75A4100000000006A08410000008013F75A4100000000006A084100000000F9F05A4100000000B0A6074100000000F9F05A41'
--  ,2,1);
--
--CALL resolve_overlap_gap_single_cell(
--  'sl_esh.ar50_utvikling_flate','geo','sl_sdeid','test_topo_ar50_t11.ar50_utvikling_flate',
--  'test_topo_ar50_t11',1,25833,'true',
--  '(300,9,500,3,140,120,240,3,35)',
--  'test_topo_ar50_t11.ar50_utvikling_flate_job_list','test_topo_ar50_t11.ar50_utvikling_flate_grid',
--  '0103000020E9640000010000000500000000000000B0A60741000000C0EBED5A4100000000B0A6074100000000F9F05A41000000005808084100000000F9F05A410000000058080841000000C0EBED5A4100000000B0A60741000000C0EBED5A41'
--  ,2,1);
--
--truncate table test_topo_ar50_t11.ar50_utvikling_flate_job_list_donejobs;
--
--CALL resolve_overlap_gap_single_cell(
--  'sl_esh.ar50_utvikling_flate','geo','sl_sdeid','test_topo_ar50_t11.ar50_utvikling_flate',
--  'test_topo_ar50_t11',1,25833,'true',
--  '(300,9,500,3,140,120,240,3,35)',
--  'test_topo_ar50_t11.ar50_utvikling_flate_job_list','test_topo_ar50_t11.ar50_utvikling_flate_grid',
--  '0103000020E964000001000000050000000000000050A6074100000000F6F05A410000000050A6074100000000FCF05A4100000000B808084100000000FCF05A4100000000B808084100000000F6F05A410000000050A6074100000000F6F05A41'
--  ,3,1);
--
--SELECT topo_update.do_healedges_no_block('test_topo_ar50_t11','0103000020E964000001000000050000000000000050A6074100000000F6F05A410000000050A6074100000000FCF05A4100000000B808084100000000FCF05A4100000000B808084100000000F6F05A410000000050A6074100000000F6F05A41');


		--perform pg_sleep(1);


--truncate table test_topo_ar50_t11.ar50_utvikling_flate_job_list_donejobs;

--CALL resolve_overlap_gap_single_cell(
--   'sl_kbj.trl_2019_test_segmenter_mindredata','geo','gid','topo_sr16_mdata_05.trl_2019_test_segmenter_mindredata',
--    'topo_sr16_mdata_05',1,25833,'true',
--    '(49,5,10000,3,25,120,240,3,35)',
--    'topo_sr16_mdata_05.trl_2019_test_segmenter_mindredata_job_list','topo_sr16_mdata_05.trl_2019_test_segmenter_mindredata_grid',
--    '0103000020E9640000010000000500000000000000A0EA0A4162A964474BDD5A4100000000A0EA0A4142C6ED8430E25A4100000000B49E0B4142C6ED8430E25A4100000000B49E0B4162A964474BDD5A4100000000A0EA0A4162A964474BDD5A41'
--  ,1,1);



--CALL resolve_overlap_gap_single_cell(
--          '(,,,public.clc2018_test_single_polygon,new_id,geom,3035,t,,,,)','(test_02_cls2018,0,f,,)','test_02_cls2018.clc2018_test_single_polygon',
--          '(0,"(,0,0)",0,10,0,10000,120,240,0,40)',
--          'test_02_cls2018.clc2018_test_single_polygon_job_list','test_02_cls2018.clc2018_test_single_polygon_grid','0103000020DB0B000001000000050000002085EB41706A4F41D8A3706DEF3F48412085EB41706A4F41343333130EC1544186EB51581CE95B41343333130EC1544186EB51581CE95B41D8A3706DEF3F48412085EB41706A4F41D8A3706DEF3F4841'
--,2,4);
--  2504.9883 sec
