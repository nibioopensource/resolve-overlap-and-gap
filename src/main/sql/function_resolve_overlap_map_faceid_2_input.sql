CREATE OR REPLACE PROCEDURE resolve_overlap_map_faceid_2_input (

_tables_ext_metainfo rog_overlay_source_table_metainfo_type[], -- A new type with more info abouth input tables, to replace _input_data_array resolve_overlap_data_input_type[] in the future

_topology_name varchar,


_topology_info resolve_overlap_data_topology_type,
---_topology_name varchar, -- The topology schema name where we store store sufaces and lines from the simple feature dataset and th efinal result
-- NB. Any exting data will related to topology_name will be deleted
--(_topology_info).topology_srid
--(_topology_info).topology_is_utm
--(_topology_info).topology_snap_tolerance float, -- this is tolerance used as base when creating the the postgis topolayer
--(_topology_info).create_topology_attrbute_tables boolean -- if this is true and we value for line_table_name we create attribute tables refferances to
-- this tables will have atrbuttes equal to the simple feauture tables for lines and feautures

-- a flag for testing the diff using a temporary tempology or not or insert directly into final source topology
-- this is a test related to performance and testing on that, the final results should be pretty much the same.
-- (_topology_info).use_temp_topology boolean

-- a flag to run checks like topology.ValidateTopology and https://github.com/larsop/resolve-overlap-and-gap
-- To run this does take time, spesially when working on verry very datasets so we trun this when needed.
-- In the final result there should be no Topology errors in the topology structure
-- In the simple feature result produced from Postgis Topology there should be overlaps
-- (_topology_info).do_qualitycheck_on_final_reseult boolean


_table_name_result_prefix varchar,
--(_topology_info).topology_snap_tolerance float, -- this is tolerance used as base when creating the the postgis topolayer


_overlapgap_grid varchar,
_box_id integer,
_bb geometry,
_cell_job_type integer
)
LANGUAGE plpgsql
AS $$
DECLARE
  -- holds dynamic sql to be able to use the same code for different
  command_string text;
  start_time timestamp WITH time zone;
  done_time timestamp WITH time zone;
  used_time real;
  start_time_delta_job timestamp WITH time zone;
  is_done integer = 0;
  area_to_block geometry;
  num_boxes_intersect integer;
  num_boxes_free integer;
  num_rows_removed integer;
  face_table_name varchar;
  -- This is used when adding lines hte tolrannce is different when adding lines inside and box and the border;
  snap_tolerance_fixed float = (_topology_info).topology_snap_tolerance;
  temp_table_name varchar;
  temp_table_id_column varchar;
  final_result_table_name varchar;
  update_fields varchar;
  update_fields_source varchar;
  subtransControlLock_start timestamp;
  subtransControlLock_count int;
  subtranscontrollock int;

  v_state text;
  v_msg text;
  v_detail text;
  v_hint text;
  v_context text;
  v_cnt_left_over_borders int;

  error_msg text;

  row_count int;

  -- A temp variable for first table in the list
  tmp_ext_metainfo rog_overlay_source_table_metainfo_type;
  do_st_transform boolean;

  -- Used for the diffrent layer
  bb_transformed geometry;







BEGIN


    -- Create a temp table name
    temp_table_name := '_result_temp_' || _box_id;
    temp_table_id_column := '_id' || temp_table_name;
    final_result_table_name := _table_name_result_prefix;

    start_time = Clock_timestamp();





    -- Drop/Create a temp to hold data temporay for job
    -- EXECUTE Format('DROP TABLE IF EXISTS %s', temp_table_name);
    -- Create the temp for result simple feature result table  as copy of the input table
    EXECUTE Format('CREATE TEMP TABLE %s ON COMMIT DROP AS TABLE %s with NO DATA', temp_table_name, final_result_table_name);
    EXECUTE Format('ALTER TABLE %s ADD column point_in_max_inscribed_circle geometry ', temp_table_name);
    --EXECUTE Format('ALTER TABLE %s ADD column radius_in_max_inscribed_circle float ', temp_table_name);
    EXECUTE Format('ALTER TABLE %s ADD column tmp_p geometry ', temp_table_name);
    EXECUTE Format('ALTER TABLE %s ADD PRIMARY KEY (face_id) ', temp_table_name);

    BEGIN

    -- Here is how to find/select primary key values from the original table . This primary key will then be used to select attribute values.
    -- Step ONE is to make ST_MaximumInscribedCircle and pick the  center point for each new face created in Postgis Topology
    -- Stemp TWO Them we use this point to find all original id's to surfaces that intersects this point and sort on this on the original polygon area DESC
    -- The first key will the primary key, the rest will be added to the list of other keys.


    -- Insert new geos based on all face id do not check on input table
    -- We will us this table to get values from the orignal tables
    -- Create ST_MaximumInscribedCircle for each new topology geometry and pick the senter point

    IF _cell_job_type = 1 THEN
      -- work on values from inside cell borders and then we know that now others jobs ae working on this faces.
      command_string := FORMAT(
      $fmt$
      insert into %3$s(%5$s,face_id,point_in_max_inscribed_circle)
      SELECT %5$s,face_id,
      (SELECT center FROM ST_MaximumInscribedCircle(r.%5$s)) AS point_in_max_inscribed_circle
      --,(SELECT radius FROM ST_MaximumInscribedCircle(r.%5$s)) AS radius_in_max_inscribed_circle
      FROM (
        SELECT *
        FROM (select (ST_Dump(topo_update.get_face_geo(%1$L,face_id,%7$s))).geom::Geometry(Polygon,%8$s) as %5$s, face_id
          FROM (
          SELECT f.face_id FROM
          %1$s.face f
          WHERE f.face_id > 0
          )
        ) AS r
      ) AS r
      $fmt$,
      _topology_name, --01
      _bb, --02
      temp_table_name, --03
      _table_name_result_prefix || '_job_list', --04
      'geom',  --05
      _box_id, --06
      snap_tolerance_fixed, --07
      (_topology_info).topology_srid, --08
      final_result_table_name --09
      );

    ELSIF _cell_job_type = 7 THEN
      command_string := FORMAT(
      $fmt$
      insert into %3$s(%5$s,face_id,point_in_max_inscribed_circle)
      SELECT %5$s,face_id,
      (SELECT center FROM ST_MaximumInscribedCircle(r.%5$s)) AS point_in_max_inscribed_circle
      --,(SELECT radius FROM ST_MaximumInscribedCircle(r.%5$s)) AS radius_in_max_inscribed_circle
      FROM (
        SELECT *
        FROM (select (ST_Dump(topo_update.get_face_geo(%1$L,face_id,%7$s))).geom::Geometry(Polygon,%8$s) as %5$s, face_id
          FROM (
          SELECT f.face_id, min(jl.id) as cell_id  FROM
          %1$s.face f,
          %4$s jl
          WHERE f.face_id > 0 AND f.mbr && %2$L and jl.cell_geo && f.mbr
          AND NOT EXISTS (SELECT TRUE FROM %9$s ftr WHERE ftr.face_id = f.face_id)
          GROUP BY f.face_id
          ) AS r WHERE cell_id = %6$s
        ) AS r WHERE ST_IsValid(r.%5$s)
      ) AS r
      $fmt$,
      _topology_name, --01
      _bb, --02
      temp_table_name, --03
      _table_name_result_prefix || '_job_list', --04
      'geom',  --05
      _box_id, --06
      snap_tolerance_fixed, --07
      (_topology_info).topology_srid, --08
      final_result_table_name --09
      );
    ELSIF _cell_job_type = 8 THEN
      command_string := FORMAT(
      $fmt$
      insert into %3$s(%5$s,face_id,point_in_max_inscribed_circle)
      SELECT %5$s,face_id,
      (SELECT center FROM ST_MaximumInscribedCircle(r.%5$s)) AS point_in_max_inscribed_circle
      --,(SELECT radius FROM ST_MaximumInscribedCircle(r.%5$s)) AS radius_in_max_inscribed_circle
      FROM (
        SELECT *
        FROM (select (ST_Dump(topo_update.get_face_geo(%1$L,face_id,%7$s))).geom::Geometry(Polygon,%8$s) as %5$s, face_id
          FROM (
          SELECT distinct f.face_id FROM
          %1$s.face f,
          %4$s grid_lines
          WHERE f.face_id > 0 AND ST_Intersects(f.mbr,grid_lines.cell_bbox)
          AND NOT EXISTS (SELECT TRUE FROM %9$s ftr WHERE ftr.face_id = f.face_id)
          ) AS r
        ) AS r
      ) AS r
      $fmt$,
      _topology_name, --01
      _bb, --02
      temp_table_name, --03
      _table_name_result_prefix || '_grid_lines', --04
      'geom',  --05
      _box_id, --06
      snap_tolerance_fixed, --07
      (_topology_info).topology_srid, --08
      final_result_table_name --09
      );
    END IF;

    start_time_delta_job := Clock_timestamp();

    EXECUTE command_string;

    GET DIAGNOSTICS row_count = ROW_COUNT;

    used_time := (Extract(EPOCH FROM (Clock_timestamp() - start_time_delta_job)));

--  RAISE NOTICE 'Find/Select primary keys, step ONE, done creating table % with % rows with center point of ST_MaximumInscribedCircle for face_table_name % at % used_time: %',
--  temp_table_name, row_count, _topology_name || '.face', Clock_timestamp(), used_time;

  -- update log table with som debug info
  EXECUTE Format('UPDATE %s SET simple_feat_num = simple_feat_num + %s WHERE id = %s', _overlapgap_grid,row_count,_box_id);


  -- for each we need update primary key value column tables_ext_metainfo
  FOREACH tmp_ext_metainfo IN ARRAY _tables_ext_metainfo
  LOOP

      start_time_delta_job := Clock_timestamp();


/**
      -- create bbox to make faster to get values
      bb_transformed = _bb;
       If (_topology_info).topology_srid != tmp_ext_metainfo.table_geo_srid THEN
         do_st_transform = true;
         bb_transformed = ST_Transform(bb_transformed,tmp_ext_metainfo.table_geo_srid);
       END IF;
*/

    If (_topology_info).topology_srid != tmp_ext_metainfo.table_geo_srid THEN
      do_st_transform = true;
    END IF;

      -- adjust point point_in_max_inscribed_circle to correct utm zone
    command_string = FORMAT(
      $fmt$
      UPDATE %1$s t SET tmp_p =
      CASE WHEN %2$L THEN ST_Transform(point_in_max_inscribed_circle, %3$s)
     ELSE point_in_max_inscribed_circle
    END;
      $fmt$,
      temp_table_name, --01
      do_st_transform, --02
      tmp_ext_metainfo.table_geo_srid --03
    );
      EXECUTE command_string;

      -- create index on this point_in_max_inscribed_circle for correct utm zone
    command_string = FORMAT(
      $fmt$
      CREATE INDEX %1$s ON %2$s USING GIST (tmp_p);
      $fmt$,
      _topology_name||'_tmp_p', --01
    temp_table_name
    );
    EXECUTE command_string;

    -- find list of primary keys from the original table found based on ST_MaximumInscribedCircle
    -- and select one those as a primary key.
    command_string = FORMAT(
      $fmt$
      UPDATE %1$s t SET %2$I = o.%3$I, %7$I = o.%7$I
      FROM (
        SELECT
        CASE WHEN ARRAY_LENGTH(o.%3$I,1) > 1 THEN o.%3$I[2:]::%6$s[]
        ELSE NULL::%6$s[] END AS %3$I,
        CASE WHEN ARRAY_LENGTH(o.%3$I,1) > 0 THEN o.%3$I[1]
        ELSE NULL END AS %7$I,

        o.face_id
      FROM (
          SELECT ARRAY_AGG(a.%3$I ORDER BY a.area DESC) AS %3$I, a.face_id
          FROM (
          SELECT DISTINCT %3$I AS %3$I, tt.face_id, ST_Area(o.%5$I) AS area
        FROM
        /** this takes to long time need to fix in another way for the test rog_overlay_test_03
        (SELECT
        o.%3$I,
        CASE WHEN topo_update.st_isValid_wrapper(o.%5$I) THEN o.%5$I
        ELSE topo_update.st_makevalid_wrapper(o.%5$I,NULL) END AS %5$I
        FROM %4$s o WHERE %8$L && o.%5$I
        ) o,
        */
        %4$s o,
        %1$s tt
        WHERE ST_Intersects(tt.tmp_p,o.%5$I)
        ) AS a
        GROUP BY face_id
      ) AS o
    ) AS o
    WHERE o.face_id = t.face_id AND o.%7$I IS NOT NULL
      $fmt$,
      temp_table_name, --01

      ros_table_metainfo_pk_list_name(tmp_ext_metainfo), --02
      tmp_ext_metainfo.table_pk_colum, --03
      tmp_ext_metainfo.table_name, --04
      tmp_ext_metainfo.table_geo_column, --05
      tmp_ext_metainfo.table_pk_type, --06
      ros_table_metainfo_pk_name(tmp_ext_metainfo),  --07
      bb_transformed --08
    );
    --RAISE NOTICE 'command_string %', command_string;
    EXECUTE command_string;
    GET DIAGNOSTICS row_count = ROW_COUNT;

    command_string = FORMAT(
      $fmt$
      DROP INDEX %1$s;
      $fmt$,
      _topology_name||'_tmp_p'
    );
    EXECUTE command_string;

    used_time := (Extract(EPOCH FROM (Clock_timestamp() - start_time_delta_job)));

--    RAISE NOTICE 'Find/Select primary keys, step TWO from table name % updateing % rows for face_table_name % at % used_time: %',
--    tmp_ext_metainfo.table_name, row_count, _topology_name || '.face', Clock_timestamp(), used_time;


  END LOOP;


  /**


  -- NB We can not use this code because this gives values areas thta should have any values also

  -- There may be rows with missing values because edge has changed so we can use ST_MaximumInscribedCircle
  -- like this case https://gitlab.com/nibioopensource/resolve-overlap-and-gap/-/issues/82#note_2123264835
  -- Maybe handle this here

  -- for each we need update primary key value column tables_ext_metainfo
  FOREACH tmp_ext_metainfo IN ARRAY _tables_ext_metainfo
  LOOP

    start_time_delta_job := Clock_timestamp();

    If (_topology_info).topology_srid != tmp_ext_metainfo.table_geo_srid THEN
      do_st_transform = true;
    END IF;


    -- adjust point point_in_max_inscribed_circle to correct utm zone
    command_string = FORMAT(
      $fmt$
      UPDATE %1$s t SET tmp_p =
      CASE WHEN %2$L THEN ST_Transform(ST_Buffer(point_in_max_inscribed_circle,radius_in_max_inscribed_circle/2.0), %3$s)
      ELSE ST_Buffer(point_in_max_inscribed_circle,radius_in_max_inscribed_circle/2.0)
      END
      WHERE t.%4$I IS NULL;
      $fmt$,
      temp_table_name, --01
      do_st_transform, --02
      tmp_ext_metainfo.table_geo_srid, --03
      ros_table_metainfo_pk_name(tmp_ext_metainfo),  --04
      'geom'  --05

    );
    EXECUTE command_string;

      -- create index on this point_in_max_inscribed_circle for correct utm zone
    command_string = FORMAT(
      $fmt$
      CREATE INDEX %1$s ON %2$s USING GIST (tmp_p);
      $fmt$,
      _topology_name||'_tmp_p', --01
    temp_table_name
    );
    EXECUTE command_string;

    -- find list of primary keys from the original table found based on ST_MaximumInscribedCircle
    -- and select one those as a primary key.
    command_string = FORMAT(
      $fmt$
      UPDATE %1$s t SET %2$I = o.%3$I, %7$I = o.%7$I
      FROM (
        SELECT
        CASE WHEN ARRAY_LENGTH(o.%3$I,1) > 1 THEN o.%3$I[2:]::%6$s[]
        ELSE NULL::%6$s[] END AS %3$I,
        CASE WHEN ARRAY_LENGTH(o.%3$I,1) > 0 THEN o.%3$I[1]
        ELSE NULL END AS %7$I,

        o.face_id
      FROM (
          SELECT ARRAY_AGG(a.%3$I ORDER BY a.area DESC) AS %3$I, a.face_id
          FROM (
          SELECT DISTINCT %3$I AS %3$I, tt.face_id, ST_Area(o.%5$I) AS area
        FROM
        %4$s o,
        %1$s tt
        WHERE tt.%7$I IS NULL AND ST_Intersects(tt.tmp_p,o.%5$I)
        ) AS a
        GROUP BY face_id
      ) AS o
    ) AS o
    WHERE o.face_id = t.face_id AND o.%7$I IS NOT NULL
      $fmt$,
      temp_table_name, --01
      ros_table_metainfo_pk_list_name(tmp_ext_metainfo), --02
      tmp_ext_metainfo.table_pk_colum, --03
      tmp_ext_metainfo.table_name, --04
      tmp_ext_metainfo.table_geo_column, --05
      tmp_ext_metainfo.table_pk_type, --06
      ros_table_metainfo_pk_name(tmp_ext_metainfo),  --07
      bb_transformed --08
    );
    --RAISE NOTICE 'command_string %', command_string;
    EXECUTE command_string;
    GET DIAGNOSTICS row_count = ROW_COUNT;

    command_string = FORMAT(
      $fmt$
      DROP INDEX %1$s;
      $fmt$,
      _topology_name||'_tmp_p'
    );
    EXECUTE command_string;

    used_time := (Extract(EPOCH FROM (Clock_timestamp() - start_time_delta_job)));

    RAISE NOTICE 'Find/Select primary keys, step TWO from table name % updateing % rows for face_table_name % at % used_time: %',
    tmp_ext_metainfo.table_name, row_count, _topology_name || '.face', Clock_timestamp(), used_time;


  END LOOP;

  */

  EXECUTE Format('ALTER TABLE %s DROP column point_in_max_inscribed_circle', temp_table_name);
  --EXECUTE Format('ALTER TABLE %s DROP column radius_in_max_inscribed_circle', temp_table_name);
  EXECUTE Format('ALTER TABLE %s DROP column tmp_p', temp_table_name);



  command_string := Format('insert into %1$s select * from %2$s', final_result_table_name, temp_table_name);
  EXECUTE command_string;

  EXCEPTION WHEN unique_violation THEN
        GET STACKED DIAGNOSTICS v_state = RETURNED_SQLSTATE, v_msg = MESSAGE_TEXT, v_detail = PG_EXCEPTION_DETAIL, v_hint = PG_EXCEPTION_HINT,
        v_context = PG_EXCEPTION_CONTEXT;

      error_msg = Format('EXCEPTION when run ValidateTopology at timeofday %s for layer %s, and box id %s , : %s message: %s detail : %s hint : %s context: %s',
      Timeofday(), _topology_name || '_', _box_id,v_state, v_msg, v_detail, v_hint, v_context);

      EXECUTE Format('UPDATE %s SET simple_feat_exception = %L WHERE id = %s', _overlapgap_grid, error_msg, _box_id);

  END;






  done_time := Clock_timestamp();
  used_time := (Extract(EPOCH FROM (done_time - start_time)));

  RAISE NOTICE 'Done map primary key for _cell_job_type % using transfer_method_from_temp_2_final_topology % at timeofday:% for layer %, for cell %, using % sec',
  _cell_job_type, (_topology_info).transfer_method_from_temp_2_final_topology, Timeofday(), _topology_name, _box_id, used_time;


END
$$;

