
/**

This is a function that collect metainfo for each table in the _input_tables and
returns a new  list _tables_ext_metainfo where is added.

*/

CREATE OR REPLACE PROCEDURE rog_compute_esri_union_source_table_metainfo_type(
_input_tables text[], -- List of table with including schema name on this format ARRAY['sk_grl.n5_forv_fylke2021_flate n5','sk_grl.n2000_komm_flate']
INOUT _tables_ext_metainfo rog_overlay_source_table_metainfo_type[]
)
AS $BODY$
DECLARE
    pos int;

    this_table_input_info text;
    this_table text;

    this_table_pk_colum text;
    this_table_pk_type text;

    this_table_selected_alias text;
    this_table_selected_colum_name text;

    this_table_geo_column text;
    this_table_geo_type text;
    this_table_geo_srid int;


    this_table_schema_name text;
    this_table_name_only text;

    this_table_input_order smallint;

  this_table_geo_is_utm boolean;

  tables_ext_metainfo rog_overlay_source_table_metainfo_type[];

  tmp_ext_metainfo rog_overlay_source_table_metainfo_type;

  tmp_area real;
  sql_cmd text;

BEGIN

    this_table_input_order = 0;

  -- Merge data into a common table
   FOREACH this_table_input_info IN ARRAY _input_tables
   LOOP
    -- a counter to be used a ref neeed to access to all tabe
       this_table_input_order = this_table_input_order + 1;

       -- find table name including schema
       pos = position (' ' IN this_table_input_info);
       IF pos > 1 THEN
         this_table := split_part(this_table_input_info, ' ', 1);
       ELSE
         this_table = this_table_input_info;
       END IF;

       -- find primary key
       SELECT a.attname, format_type(a.atttypid, a.atttypmod) INTO this_table_pk_colum, this_table_pk_type
       FROM pg_index i
       JOIN pg_attribute a ON a.attrelid = i.indrelid
       AND a.attnum = ANY(i.indkey)
       WHERE i.indrelid = this_table::regclass
       AND i.indisprimary;


       -- find table name including schema
       pos = position (' ' IN this_table_input_info);
       IF pos > 1 THEN
         this_table := split_part(this_table_input_info, ' ', 1);
         -- if add alieas after the table an alieas will be used
         -- then you ARRAY['sk_grl.n5_forv_fylke2021_flate n5fylke','sk_grl.n2000_komm_flate n2000kom']
         this_table_selected_alias :=  split_part(this_table_input_info, ' ', 2);
       ELSE
         this_table = this_table_input_info;
         this_table_selected_alias = NULL;
       END IF;


    RAISE NOTICE 'Start to find metainfo for table num % ,  name % and this_table_selected_alias %',
    this_table_input_order, this_table, this_table_selected_alias ;


    -- split input in schema and table name
    this_table_schema_name := split_part(this_table, '.', 1);
    this_table_name_only := split_part(this_table, '.', 2);


     -- find geo column name for input table
    SELECT type, f_geometry_column INTO this_table_geo_type,  this_table_geo_column
    FROM   geometry_columns
    WHERE  f_table_schema = this_table_schema_name
    AND f_table_name = this_table_name_only;

    -- TODO add a check if there more than one geomerry column
    IF this_table_geo_column IS NULL THEN
      RAISE EXCEPTION 'Cannot determine geometry column for table %', this_table;
    END IF;

    -- find geo column srid
    this_table_geo_srid := find_srid(this_table_schema_name, this_table_name_only, this_table_geo_column);
    IF this_table_geo_srid IS NULL THEN
      RAISE EXCEPTION 'Cannot determine srid from input table % and column %', this_table, this_table_geo_column;
    END IF;
    IF this_table_geo_srid = 0 THEN
      RAISE EXCEPTION 'Cannot use unknown srid in input table % and column %', this_table, this_table_geo_column;
    END IF;

    RAISE NOTICE 'At %, found geometry column name % of type % , for %s.%s ', now(), this_table_geo_column, this_table_geo_type, this_table_schema_name, this_table_name_only  ;

    --

    BEGIN
      sql_cmd = FORMAT($fmt$
        SELECT ST_Area(%1$s,true) FROM %2$s WHERE %1$s IS NOT NULL LIMIT 1;
      $fmt$,
      this_table_geo_column,
      this_table
      );

      EXECUTE sql_cmd INTO tmp_area;
      this_table_geo_is_utm = false;

    EXCEPTION WHEN OTHERS THEN
      this_table_geo_is_utm = true;
    END;

         tmp_ext_metainfo.table_input_order = this_table_input_order;
         tmp_ext_metainfo.table_name = this_table;

         tmp_ext_metainfo.table_pk_colum = this_table_pk_colum;
         tmp_ext_metainfo.table_pk_type = this_table_pk_type;

         tmp_ext_metainfo.table_selected_alias = this_table_selected_alias;
         tmp_ext_metainfo.table_geo_column = this_table_geo_column;
         tmp_ext_metainfo.table_geo_type = this_table_geo_type;
         tmp_ext_metainfo.table_geo_srid = this_table_geo_srid;

    tmp_ext_metainfo.table_geo_is_utm = this_table_geo_is_utm;

    tables_ext_metainfo = tables_ext_metainfo||tmp_ext_metainfo;

    -- Check if primary key is valid
    IF tmp_ext_metainfo.table_pk_colum IS NULL OR tmp_ext_metainfo.table_pk_type IS NULL THEN
           RAISE EXCEPTION 'Faild to find a primary key for table %' , this_table;
    END IF;






   END LOOP;

   _tables_ext_metainfo = tables_ext_metainfo;
END
$BODY$ LANGUAGE 'plpgsql';

