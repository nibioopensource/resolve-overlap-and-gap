/**

Create a function that can read form many different table

*/
CREATE OR REPLACE FUNCTION rog_create_read_data_function_tmp(
_tables_ext_metainfo rog_overlay_source_table_metainfo_type[], -- the set of input tables to use
-- From og_esri_union_source_table_metainfo_type we need
-- tmp_ext_metainfo.table_geo_srid,
-- tmp_ext_metainfo.table_name, --02
-- tmp_ext_metainfo.table_geo_column, --03
-- tmp_ext_metainfo.table_input_order, --05
_bb geometry, -- The bbox the data from. This bbox will in the _new_sf_table_srid mean we have transform this using this bbox
_new_sf_table_srid integer, -- The SRID to be used in the output table geometry column

-- if the evenlope of an input polygons is bigger than the content based grid cell used the code will break it up at the cell border.
-- Like https://gitlab.com/nibioopensource/resolve-overlap-and-gap/uploads/2a4ce05e531ced49e8881b5051cf85ca/Screenshot_2024-04-02_at_21.10.37.png
-- st_npoints | st_area  | mdr_area  | st_numinteriorrings
-- 3627257 | 20311057 | 742819770 |               10586
_if_break_up_big_polygons boolean,

_do_validate_geometry boolean, -- IF true ST_Validate will done and the result will saved in input_geo_is_valid

_try_fix_invalid_geometry_not_used boolean, -- IF true ST_MakeValid will done and the result will be used and column geo_tried_fix_invalid will be set to true.

_drop_new_sf_table_if_exists boolean -- IF true the out table will be  dropped if it exists . If called with false the _new_sf_table_name must not exists.
)
RETURNS TABLE (
      table_input_order int,
      src_table_pk_column_value text, -- TODO maybe use jsonb, the we can support any primary key. Is text fast enought ? for instance when using integer
      geo public.geometry,
      input_geo_is_valid boolean,
      geo_tried_fix_invalid boolean
)
AS $func$
DECLARE

  tmp_ext_metainfo rog_overlay_source_table_metainfo_type;

    sql_cmd text;
  help_text text;
  new_sf_temp_table varchar = 'rog_tmp_data_sf_collect' || Md5(ST_AsBinary (_bb));

  row_count int;
  result_table_rows int;

  do_st_transform boolean;
  bb_to_use geometry; -- This is the box to use i each and that

  _try_fix_invalid_geometry boolean = true;


BEGIN

  DROP table IF EXISTS new_sf_temp_table;


  -- Create input table
  sql_cmd = FORMAT($fmt$
    CREATE TEMP TABLE %1$s (
      table_input_order int,
      src_table_pk_column_value text, -- TODO maybe use jsonb, the we can support any primary key. Is text fast enought ? for instance when using integer
      geo public.geometry(Polygon,%2$s),
      input_geo_is_valid boolean,
      geo_tried_fix_invalid boolean
      ) ON COMMIT DROP
       $fmt$,
       new_sf_temp_table,
       _new_sf_table_srid
  );

  -- RAISE NOTICE 'At % run %', now(), sql_cmd  ;
  EXECUTE sql_cmd;


  -- Merge data into a common table
   FOREACH tmp_ext_metainfo IN ARRAY _tables_ext_metainfo
   LOOP
    -- Transfrom bbox to get according to the input data
       If _new_sf_table_srid != tmp_ext_metainfo.table_geo_srid THEN
         do_st_transform = true;
         bb_to_use = ST_Transform(_bb,tmp_ext_metainfo.table_geo_srid);

         -- RAISE NOTICE 'Transform from % to % for table % for bbox %', _new_sf_table_srid, tmp_ext_metainfo.table_geo_srid,tmp_ext_metainfo.table_name,_bb  ;
       ELSE
         do_st_transform = false;
         bb_to_use = _bb;

       END IF;

     sql_cmd = FORMAT($fmt$
          INSERT INTO %1$s (table_input_order, src_table_pk_column_value, geo, input_geo_is_valid, geo_tried_fix_invalid)
          SELECT DISTINCT table_input_order, src_table_pk_column_value,
          CASE WHEN ST_GeometryType(r.geo) = 'ST_Polygon' THEN r.geo::geometry(Polygon, %4$s)
          ELSE NULL
          END AS geo,
          input_geo_is_valid,
          geo_tried_fix_invalid
          FROM (
            SELECT table_input_order, src_table_pk_column_value, (ST_Dump(geo)).geom AS geo, input_geo_is_valid, geo_tried_fix_invalid
            FROM (
              SELECT * FROM (
                SELECT
                r.table_input_order,
                r.src_table_pk_column_value,
              CASE WHEN %8$L AND input_geo_is_valid = FALSE THEN (topo_update.st_makevalid_wrapper(r.geo,r.org_geo))
              ELSE r.geo
              END AS geo,
              r.input_geo_is_valid,
              CASE WHEN %8$L AND input_geo_is_valid = FALSE THEN TRUE
              ELSE FALSE
              END AS geo_tried_fix_invalid
              FROM
              (
                SELECT
                r.table_input_order,
                r.src_table_pk_column_value,
                (ST_Dump(r.geo)).geom AS geo,
                CASE WHEN %7$L THEN topo_update.st_isValid_wrapper(r.geo)
                END AS input_geo_is_valid,
                org_geo
                FROM (
                SELECT
                  %5$s AS table_input_order,
                  r.src_table_pk_column_value,
                  CASE
                    WHEN %11$L AND ST_Area(ST_Envelope(r.geo)) >  ST_Area(%12$L)
                    THEN ST_Intersection(r.geo,%12$L)
                  ELSE
                    r.geo
                  END AS geo,
                  org_geo
                    FROM (
                      SELECT
                      CAST (r1.%10$s AS text) AS src_table_pk_column_value,
                      -- TODO test on valid area before running ST_Transform
                      CASE WHEN %6$L THEN ST_Transform(r1.%3$s, %4$s)
                        ELSE r1.%3$s
                      END AS geo,
                      r1.%3$s AS org_geo
                      FROM %2$s r1
                      WHERE r1.%3$s && %9$L
                    ) r WHERE r.geo IS NOT NULL
                 ) r WHERE r.geo IS NOT NULL
              ) AS r
            ) AS r
          ) AS r
        ) AS r
          $fmt$,
          new_sf_temp_table, --01
          tmp_ext_metainfo.table_name, --02
          tmp_ext_metainfo.table_geo_column, --03
          _new_sf_table_srid, --04
          tmp_ext_metainfo.table_input_order, --05
          do_st_transform , --06
          _do_validate_geometry, --07
          _try_fix_invalid_geometry, --08
          bb_to_use, --09
          tmp_ext_metainfo.table_pk_colum, --10
          _if_break_up_big_polygons, --11
          _bb --12
          );


    EXECUTE sql_cmd;

     get DIAGNOSTICS row_count = ROW_COUNT;
    --RAISE NOTICE 'At % run get data for % rows inserted % ', now(), tmp_ext_metainfo.table_name, row_count  ;

   END LOOP;

     sql_cmd = FORMAT($fmt$
    SELECT count(*) FROM %1$s
    $fmt$
    , new_sf_temp_table
  );

  EXECUTE sql_cmd INTO result_table_rows;
  RAISE NOTICE 'At % number off rows % in result table %', now(), result_table_rows, new_sf_temp_table ;

  sql_cmd = FORMAT($fmt$
    SELECT * FROM %1$s
       $fmt$,
       new_sf_temp_table
  );

  RETURN QUERY EXECUTE sql_cmd;


END;
$func$ LANGUAGE 'plpgsql';


/**

A wrapper frunction to read lines needed to from edges from many different table based on bbox

*/
CREATE OR REPLACE FUNCTION rog_create_read_data_function(
_tables_ext_metainfo rog_overlay_source_table_metainfo_type[], -- A list input tables meta info
_bb geometry, -- The bbox the data from. This bbox will in the _new_sf_table_srid mean we have transform this using this bbox
_new_sf_table_srid integer, -- The SRID to be used in the output table geometry column
_if_break_up_big_polygons boolean, -- if the evenlope of an input polygons is bigger than the content based grid cell used the code will break it up at the cell border.
_do_validate_geometry boolean, -- IF true ST_Validate will done and the result will saved in input_geo_is_valid
_try_fix_invalid_geometry boolean, -- IF true ST_MakeValid will done and the result will be used and column geo_tried_fix_invalid will be set to true.
_drop_new_sf_table_if_exists boolean -- IF true the out table will be  dropped if it exists . If called with false the _new_sf_table_name must not exists.
)
-- We may need to reformat the return data
RETURNS TABLE (
      table_input_order int,
      src_table_pk_column_value text, -- TODO maybe use jsonb, the we can support any primary key. Is text fast enought ? for instance when using integer
      geo public.geometry,
      input_geo_is_valid boolean,
      geo_tried_fix_invalid boolean
)
AS $func$
DECLARE



BEGIN

  RETURN QUERY SELECT * FROM rog_create_read_data_function_tmp(
    _tables_ext_metainfo,
    _bb, -- The bbox the data from. This bbox will in the _new_sf_table_srid mean we have transform this using this bbox
    _new_sf_table_srid, -- The SRID to be used in the output table geometry column
    _if_break_up_big_polygons, -- if the evenlope of an input polygons is bigger than the content based grid cell used the code will break it up at the cell border.
    _do_validate_geometry, -- IF true ST_Validate will done and the result will saved in input_geo_is_valid
    _try_fix_invalid_geometry, -- IF true ST_MakeValid will done and the result will be used and column geo_tried_fix_invalid will be set to true.
    _drop_new_sf_table_if_exists -- IF true the out table will be  dropped if it exists . If called with false the _new_sf_table_name must not exists.
  );
END;
$func$ LANGUAGE 'plpgsql';


