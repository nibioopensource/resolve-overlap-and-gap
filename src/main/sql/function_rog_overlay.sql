/*

This is a function that is used to collect/check data from many tables and save into a common table with a common srid.

The input tables may have different geometries columns name, srid and geometry (GEOMETRY,polygon/Multipolygon).
If ST_Dump does not return a polygon it will fail

What's done with the diffrent tables are logged into a comment on column src_table_ref .

In the src_table_column_data all other columns data except the geometry colum put into  a JSONB structure.

This is a table overlay

The process of stacking digital representations of various spatial data on top of each other so that each position in the area covered can be analysed in terms of these data.
Burrough, P.A. (1986) Principles of Geographic Information Systems for Land Resource Assessment. Monographs on Soil and Resources Survey No. 12, Oxford Science Publications, New York.

*/


CREATE OR REPLACE PROCEDURE rog_overlay(
_input_tables text[], -- List of table with including schema name on this format ARRAY['sk_grl.n5_forv_fylke2021_flate','sk_grl.n2000_komm_flate']
_topology_result_schema_name varchar, -- The name for topology and schema
_new_sf_table_srid int, -- The SRID to be used in the output table geometry column

_topology_snap_tolerance float, -- This is tolerance used as base when creating the the postgis topolayer

_min_area_to_keep float, -- Will merge into a neighbour polygon by remving the line added latest. The area is sqare meter.

_split_input_lines_at_vertex_n int, -- split all lines at given vertex num

-- if the evenlope of an input polygons is bigger than the content based grid cell used the code will break it up at the cell border.
_break_up_big_polygons boolean,


-- ST_IsEmpty(ST_Buffer(face_geom,-min_st_is_empty_buffer))
-- This is an approximate value min_st_is_empty_buffer in meter used remove sliver polygon,
-- The formula used to convert from meter used for testing now
-- SELECT ST_Length(ST_Boundary(ST_Envelope(((ST_Buffer('POINT (10.061995275172155 59.716094977883)'::geography,1,'quad_segs=1'))::geometry))))/9;
_min_st_is_empty_buffer_value float,


-- To limit and set input. This must be in the same coordsys as _new_sf_table_srid and must be polygon or a multipolgyon,
-- This will added as the first geometry into postgis topology. Data outside the master area will not be used.
-- If you send in a null geometry here this parameter will not have any effect.
-- This will affect the name of _topology_result_schema_name by append (rog_input_boundary).name to _topology_result_schema_name
_input_boundary_list rog_input_boundary[],

-- a flag to run checks like topology.ValidateTopology and https://github.com/larsop/resolve-overlap-and-gap
-- To run this does take time, spesially when working on verry, very big datasets
-- In the final result there should be no Topology errors in the topology structure
-- In the simple feature result produced from Postgis Topology there should be overlaps
_do_qualitycheck_on_final_reseult boolean,

-- How to move data from tmp topology to final topology
--  Method 1 topology.TopoGeo_addLinestring
--  Method 2 topology.TopoGeo_LoadGeometry
--  Method 3 select from tmp toplogy into master topology
_transfer_method_from_temp_2_final_topology int,

-- this is the max number of tlotal paralell jobs to run. There must be at least the same number of free connections
-- if you have a _input_boundary_list the number of jobs pr job in list have to shared along the list of jobs here.
_max_parallel_jobs_in_total int,
_max_rows_in_each_cell int, -- this is the max number rows that intersects with box before it's split into 4 new boxes
INOUT _tables_ext_metainfo rog_overlay_source_table_metainfo_type[],

	-- This contains info about how clean edges from the input layers
	-- TODO move up this parameter
	-- TODO support an ARRAY ff you have two tables as input only want clean up second on kepp NULL in row number ser
	_edge_input_tables_cleanup rog_clean_edge_type[] DEFAULT NULL,
	--(_edge_input_tables_cleanup)._simplify_tolerance float default 0, -- is this is more than zero simply will called with
	--(_edge_input_tables_cleanup)._simplify_max_average_vertex_length int default 0, -- in meter both for utm and deegrees, this used to avoid running ST_simplifyPreserveTopology for long lines lines with few points
	--(_edge_input_tables_cleanup)._chaikins_nIterations int default 0, -- IF 0 NO CHAKINS WILL BE DONE,  A big value here make no sense because the number of points will increaes exponential )
	--(_edge_input_tables_cleanup)._chaikins_max_length int default 0, --edge that are longer than this value will not be touched by _chaikins_min_degrees and _chaikins_max_degrees
	--(_edge_input_tables_cleanup)._chaikins_min_degrees int default 0, -- The angle has to be less this given value, This is used to avoid to touch all angles.
	--(_edge_input_tables_cleanup)._chaikins_max_degrees int default 0, -- OR the angle has to be greather than this given value, This is used to avoid to touch all angles
	--(_edge_input_tables_cleanup)._chaikins_min_steep_angle_degrees int default 0, -- The angle has to be less this given value, This is used to avoid to touch all angles.
	--(_edge_input_tables_cleanup)._chaikins_max_steep_angle_degrees int default 0-- OR The angle has to be greather than this given value, This is used to avoid to touch all angles

	-- Used for debug areas or start and stop at certain area
	_debug_options resolve_overlap_data_debug_options_type DEFAULT NULL

)
AS $BODY$
DECLARE

input_boundary_list rog_input_boundary[] = '{}';
tmp_rog_input_boundary rog_input_boundary;

topology_result_schema_name text;
sql_command text = '';

-- This holds a union all the simple feature results for all the sub jobs
view_sf_result_base_name text; -- the view name
view_sf_result_text text = ''; -- the view content

count_number int = 0;
unique_id_prefix text;

-- Holds the list of func_call to run
stmts text[];

-- Holds the reseult from paralell calls
call_result int;

-- This is the number sub_jobs to run parallel .
-- Default is one and that will be adjusted according how many toplogies needed.
max_parallel_sub_jobs int = 1;

-- this the number of parallel jobs for each sub job
-- IF If the _input_boundary_list is empty max_parallel_jobs_pr_sub_job be the same as _max_parallel_jobs_in_total
max_parallel_jobs_pr_sub_job int = _max_parallel_jobs_in_total;


contiune_after_stat_exception boolean = TRUE;


	-- start used by start_execute_parallel
	connstr text; -- this will be buildt up inside start_execute_parallel at first call
	conntions_array text[]; -- this will be buildt up inside start_execute_parallel at first call
	conn_stmts text[]; -- this will be buildt up inside start_execute_parallel at first call
	stats_done text[]; -- This should be null when called , and new statements done will be added to this list.
	all_stmts_done boolean = false; -- this will be buildt up inside start_execute_parallel at first call
	start_execute_parallel_counter int = 0;
	seconds_to_wait_before_check_result int = 300;
	-- done used by start_execute_parallel

	start_time timestamp WITH time zone;
	start_time_delta_job timestamp WITH time zone;
	used_time real;

	-- _do_qualitycheck_on_final_reseult is true we get 3 more table
	-- 1: test_topo_04_t2_test_01.multi_input_2_rogtest_gap
	-- 2: test_topo_04_t2_test_01.multi_input_2_rogtest_overlap
	-- 3: test_topo_04_t2_test_01.multi_input_2_grid_validate_topology
	-- This holds a union all the simple feature results for all the sub jobs
	view_sf_gap_base_name text;
	view_sf_gap_text text = '';
	view_sf_overlap_base_name text;
	view_sf_overlap_text text = '';
	view_sf_validate_base_name text;
	view_sf_validate_text text = '';

	-- The structure used by rog_overlay computed based list of _input_tables text[]
	-- This is the structure that is returned to the user to list what columns used, srid and so on.
	tables_ext_metainfo rog_overlay_source_table_metainfo_type[];

	name_created_boundary_list text;
	num_cells_created_boundary_list int;

	-- This number need to be tested more it probaly has to reduced
	deafault_num_max_boundary_list_size int = 20000;
	max_rows_pr_topology int;

BEGIN

	CALL topo_rog_static.rog_compute_esri_union_source_table_metainfo_type(_input_tables,tables_ext_metainfo);

	-- Create a input_boundary_list by using a content based grid.
	IF _input_boundary_list IS NULL THEN
		-- create the grid
		name_created_boundary_list = ros_view_sf_result_schema_name(_topology_result_schema_name)||'.'||ros_view_sf_result_base_name(_topology_result_schema_name,_input_tables)::text||'_created_boundary_list';
		max_rows_pr_topology = _max_rows_in_each_cell*(deafault_num_max_boundary_list_size/ARRAY_LENGTH(_input_tables,1));

		RAISE NOTICE '_input_boundary_list IS NULL, create table % for % input table with max_rows_pr_topology %.',
		name_created_boundary_list,
		ARRAY_LENGTH(_input_tables,1),
		max_rows_pr_topology;

		sql_command = FORMAT($fmt$
		CREATE SCHEMA IF NOT EXISTS %1$s;
		DROP TABLE IF EXISTS %2$s;
		$fmt$,
			ros_view_sf_result_schema_name(_topology_result_schema_name),
			name_created_boundary_list
			);
		EXECUTE sql_command;



		num_cells_created_boundary_list := resolve_overlap_gap_cb_grid(
		name_created_boundary_list,
		max_rows_pr_topology,
		tables_ext_metainfo,
		_new_sf_table_srid,
		'geom',-- cell_geo_column_name,
		NULL -- input_boundary_boandary_area
		);

		IF num_cells_created_boundary_list = 1 THEN
			-- If number cell is one, use the old topology name 'topo'
			sql_command = FORMAT($fmt$
			SELECT ARRAY_AGG((bl.geom,'topo')::rog_input_boundary)
			FROM %1$s bl
			$fmt$,
			name_created_boundary_list
			);
		ELSE
			-- Make a topo grid for each grid created and starte with the biggest jobs first to avoid start the biggest job as the final job.
			sql_command = FORMAT($fmt$
			SELECT ARRAY_AGG( (bl.geom,'topo_'||LPAD(bl.id::text, 3, '0'))::rog_input_boundary ORDER BY bl.numrows_intersects DESC )
			FROM %1$s bl
			$fmt$,
			name_created_boundary_list
			);
		END IF;
		EXECUTE sql_command INTO input_boundary_list;
	ELSE
		-- Use input boadary as it is
		input_boundary_list = _input_boundary_list;
	END IF;

		-- if we sub jobs set max_parallel_sub_jobs and max_parallel_jobs_pr_sub_job
		IF Array_length(input_boundary_list, 1) > 1 THEN
			-- Here I have just picked value to 4 , TODO maybe add this as parameter
			max_parallel_sub_jobs = 4;

			IF Array_length(input_boundary_list, 1) < max_parallel_sub_jobs THEN
				max_parallel_sub_jobs = Array_length(stmts, 1);
			END IF;

			max_parallel_jobs_pr_sub_job = round(_max_parallel_jobs_in_total/max_parallel_sub_jobs);

			IF max_parallel_jobs_pr_sub_job = 0 THEN
				max_parallel_jobs_pr_sub_job = 1;
			END IF;

		END IF;


		RAISE NOTICE 'Run with max_parallel_sub_jobs % and max_parallel_jobs_pr_sub_job % for topology_result_schema_name % ',
		max_parallel_sub_jobs, max_parallel_jobs_pr_sub_job, _topology_result_schema_name;

		-- make result name simple feature view
		view_sf_result_base_name = ros_view_sf_result_base_name(_topology_result_schema_name,_input_tables)::text||'_v';

		---

		-- if do _do_qualitycheck_on_final_reseult also make top level view controll views
		IF _do_qualitycheck_on_final_reseult THEN
			view_sf_gap_base_name = ros_view_sf_result_base_name(_topology_result_schema_name,_input_tables)::text||'_rogtest_gap';
			view_sf_overlap_base_name = ros_view_sf_result_base_name(_topology_result_schema_name,_input_tables)::text||'_rogtest_overlap';
			view_sf_validate_base_name = ros_view_sf_result_base_name(_topology_result_schema_name,_input_tables)::text||'_grid_validate_topology';
		END IF;




		stmts := '{}';

			-- for each craete a primary key value column tables_ext_metainfo
			FOREACH  tmp_rog_input_boundary IN ARRAY input_boundary_list
			LOOP

				topology_result_schema_name = ros_view_sf_result_topo_name(_topology_result_schema_name,tmp_rog_input_boundary);

				count_number = count_number + 1;

				unique_id_prefix = (tmp_rog_input_boundary).name||'_';

				-- make result view tect simple feature
				view_sf_result_text = view_sf_result_text || ' SELECT ' || quote_literal(unique_id_prefix) || '||face_id::text AS boundary_face_id, * FROM ' || topology_result_schema_name || '.' || view_sf_result_base_name;

				-- if do _do_qualitycheck_on_final_reseult also make top level view controll views
				IF _do_qualitycheck_on_final_reseult THEN
					view_sf_gap_text = view_sf_gap_text || ' SELECT ' || quote_literal(unique_id_prefix) || '||id::text AS global_id, * FROM ' || topology_result_schema_name || '.' || view_sf_gap_base_name;
					view_sf_overlap_text = view_sf_overlap_text || ' SELECT ' || quote_literal(unique_id_prefix) || '||id::text AS global_id, * FROM ' || topology_result_schema_name || '.' || view_sf_overlap_base_name;
					view_sf_validate_text =  view_sf_validate_text || ' SELECT ' || quote_literal(unique_id_prefix) || '||id::text AS global_id, * FROM ' || topology_result_schema_name || '.' || view_sf_validate_base_name;

					-- if more result add union all to result
					IF count_number < ARRAY_LENGTH(input_boundary_list,1) THEN
						view_sf_gap_text = view_sf_gap_text || ' UNION ALL ';
						view_sf_overlap_text = view_sf_overlap_text || ' UNION ALL ';
						view_sf_validate_text = view_sf_validate_text || ' UNION ALL ';
					END IF;
				END IF;


				-- if more result add union all to result
				IF count_number < ARRAY_LENGTH(input_boundary_list,1) THEN
					view_sf_result_text = view_sf_result_text || ' UNION ALL ';
				END IF;


				sql_command = FORMAT($fmt$
					CALL _rog_overlay(%1$L,%2$L,%3$L,%4$L,%5$L,%6$L,%7$L,%8$L,%9$L,%10$L,%11$L,%12$L,%13$L,%14$L,%15$L,%16$L)
					$fmt$,
					_input_tables, --01 List of table with including schema name on this format ARRAY['sk_grl.n5_forv_fylke2021_flate','sk_grl.n2000_komm_flate']
					_topology_result_schema_name, --02 The name for topology and schema
					_new_sf_table_srid, --03 The SRID to be used in the output table geometry column,
					_topology_snap_tolerance, --04
					_min_area_to_keep, -- 05 if this a polygon  is below this limit it will merge into a neighbour polygon. The area is sqare meter.
					_split_input_lines_at_vertex_n,  --06 split all lines at given vertex num
					_break_up_big_polygons, -- 07 if the evenlope of an input polygons is bigger than the content based grid cell used the code will break it up at the cell border.
					_min_st_is_empty_buffer_value, --08
					tmp_rog_input_boundary, --09
					_do_qualitycheck_on_final_reseult, --10
					_transfer_method_from_temp_2_final_topology, --11
					max_parallel_jobs_pr_sub_job, --12 this is the max number of paralell jobs to run. There must be at least the same number of free connections
					_max_rows_in_each_cell, --13 this is the max number rows that intersects with box before it's split into 4 new boxes
					_tables_ext_metainfo, --14
					_edge_input_tables_cleanup, --15
						-- Used for debug areas or start and stop at certain area
					_debug_options --16


				);

				stmts=stmts||sql_command;

			END LOOP;

		COMMIT;
		start_time := Clock_timestamp();
		start_time_delta_job = Clock_timestamp();


		RAISE NOTICE 'Kicking off joblist in rog_overlay % jobs with % workersfor _topology_result_schema_name % at % ',
		Array_length(stmts, 1), max_parallel_sub_jobs, _topology_result_schema_name, Clock_timestamp();


		-- SELECT execute_parallel (stmts, max_parallel_sub_jobs,true,null,contiune_after_stat_exception) INTO call_result;

		all_stmts_done = FALSE;
		start_execute_parallel_counter = 0;
		connstr = NULL;
		conntions_array = NULL;
		conn_stmts = NULL;
		WHILE all_stmts_done = FALSE LOOP
			stats_done = null;

			CALL start_execute_parallel(
				stmts,
				connstr,
				conntions_array,
				conn_stmts,
				stats_done,
				all_stmts_done,
				seconds_to_wait_before_check_result,
				max_parallel_sub_jobs
			);
			start_execute_parallel_counter = start_execute_parallel_counter + 1;


			used_time := (Extract(EPOCH FROM (Clock_timestamp() - start_time_delta_job)));
			IF used_time > seconds_to_wait_before_check_result THEN
				start_time_delta_job = Clock_timestamp();
				RAISE NOTICE 'Status for start_execute_parallel in rog_overlay for _topology_result_schema_name % at loop number %  all_stmts_done % conntions_array size % after start_execute_parallel statements list left to run  % at % in % secs',
				_topology_result_schema_name, start_execute_parallel_counter, all_stmts_done, coalesce(Array_length(conntions_array, 1),0), coalesce(Array_length(stmts, 1),0), Clock_timestamp(), (Extract(EPOCH FROM (Clock_timestamp() - start_time)));
			END IF;

			IF all_stmts_done = FALSE THEN
				perform pg_sleep(seconds_to_wait_before_check_result/10);
				COMMIT;
			END IF;

		END LOOP;


		RAISE NOTICE 'Done running % jobs for _topology_result_schema_name % at % in % secs',
		Array_length(stmts, 1), _topology_result_schema_name,  Clock_timestamp(), (Extract(EPOCH FROM (Clock_timestamp() - start_time)));

		-- Create schema and table for simple feature result
		sql_command = FORMAT($fmt$

		CREATE SCHEMA IF NOT EXISTS %1$s;
		GRANT USAGE ON SCHEMA %1$s TO PUBLIC;
		DROP VIEW  IF EXISTS %1$s.%2$s;
		CREATE VIEW %1$s.%2$s AS %3$s;
		GRANT SELECT ON %1$s.%2$s TO PUBLIC;
		$fmt$,
			ros_view_sf_result_schema_name(_topology_result_schema_name),
			view_sf_result_base_name,
			view_sf_result_text
			);
		RAISE NOTICE 'sql_command for schema and table for simple feature result %', sql_command;
		EXECUTE sql_command;


		perform resolve_overlap_gap_post_add_comments(
			tables_ext_metainfo,
			' FROM ' || ARRAY_LENGTH(input_boundary_list,1) || ' topologies',
			NULL, --_table_name_result_prefix,
			ros_view_sf_result_schema_name(_topology_result_schema_name)||'.'||view_sf_result_base_name
		);

	sql_command = FORMAT($fmt$
			COMMENT ON COLUMN %1$s.%2$I is %3$L;
			$fmt$,
			ros_view_sf_result_schema_name(_topology_result_schema_name)||'.'||view_sf_result_base_name, --01
			'boundary_face_id', --02
			'This is global unique key across ' || ARRAY_LENGTH(input_boundary_list,1) || ' topologies, which contains sub. name of the topology followed by face_id.'
			);
	EXECUTE sql_command;



		-- if do _do_qualitycheck_on_final_reseult also make top level view controll views
		IF _do_qualitycheck_on_final_reseult THEN
			sql_command = FORMAT($fmt$
			DROP VIEW  IF EXISTS %1$s;
			CREATE VIEW %1$s AS %2$s;
			COMMENT ON VIEW %1$s IS %3$L;
			COMMENT ON COLUMN %1$s.geom IS %4$L;
			GRANT SELECT ON %1$s TO PUBLIC;
			$fmt$,
				ros_view_sf_result_schema_name(_topology_result_schema_name)||'.'||view_sf_gap_base_name,
				view_sf_gap_text,
				'This is view where you find all gaps in the result view, if input covers all areas it means that there is a bug',
				'This is the area to check for errors related to missing data.'
			);
			RAISE NOTICE 'sql_command for view result for gaps if _do_qualitycheck_on_final_reseult %', sql_command;
			EXECUTE sql_command;

			sql_command = FORMAT($fmt$
			DROP VIEW  IF EXISTS %1$s;
			CREATE VIEW %1$s AS %2$s;
			COMMENT ON VIEW %1$s IS %3$L;
			COMMENT ON COLUMN %1$s.geom IS %4$L;
			GRANT SELECT ON %1$s TO PUBLIC;
			$fmt$,
				ros_view_sf_result_schema_name(_topology_result_schema_name)||'.'||view_sf_overlap_base_name,
				view_sf_overlap_text,
				'This is view where you find all overlap in the result view, if there are rows in this table there is a bug the result',
				'This is the area to check for errors related to overlap.'
			);
			RAISE NOTICE 'sql_command for view result for overlaps if _do_qualitycheck_on_final_reseult %', sql_command;
			EXECUTE sql_command;

			sql_command = FORMAT($fmt$
			DROP VIEW  IF EXISTS %1$s;
			CREATE VIEW %1$s AS %2$s;
			COMMENT ON VIEW %1$s IS %3$L;
			COMMENT ON COLUMN %1$s.cell_bbox IS %4$L;
			GRANT SELECT ON %1$s TO PUBLIC;
			$fmt$,
				ros_view_sf_result_schema_name(_topology_result_schema_name)||'.'||view_sf_validate_base_name,
				view_sf_validate_text,
				'This is view where you find all bbox where there are topology errors, if there are rows in this table there is a bug the result',
				'This is the area to check for errors related to topology validation'

			);
			RAISE NOTICE 'sql_command for view result for validate topology if _do_qualitycheck_on_final_reseult %', sql_command;
			EXECUTE sql_command;

		END IF;

		RAISE NOTICE 'Global result view  % ',
		ros_view_sf_result_schema_name(_topology_result_schema_name)||'.'||view_sf_result_base_name;


END
$BODY$ LANGUAGE 'plpgsql';


/*

This is a internal function that is used to collect/check data from many tables and save into a common table with a common srid.

The input tables may have different geometries columns name, srid and geometry (GEOMETRY,polygon/Multipolygon).
If ST_Dump does not return a polygon it will fail

What's done with the diffrent tables are logged into a comment on column src_table_ref .

In the src_table_column_data all other columns data except the geometry colum put into  a JSONB structure.

This is a table overlay

The process of stacking digital representations of various spatial data on top of each other so that each position in the area covered can be analysed in terms of these data.
Burrough, P.A. (1986) Principles of Geographic Information Systems for Land Resource Assessment. Monographs on Soil and Resources Survey No. 12, Oxford Science Publications, New York.

*/


CREATE OR REPLACE PROCEDURE _rog_overlay(
_input_tables text[], -- List of table with including schema name on this format ARRAY['sk_grl.n5_forv_fylke2021_flate','sk_grl.n2000_komm_flate']
_topology_result_schema_name varchar, -- The name for topology and schema
_new_sf_table_srid int, -- The SRID to be used in the output table geometry column

_topology_snap_tolerance float, -- This is tolerance used as base when creating the the postgis topolayer

_min_area_to_keep float, -- Will merge into a neighbour polygon by remving the line added latest. The area is sqare meter.

_split_input_lines_at_vertex_n int, -- split all lines at given vertex num

-- if the evenlope of an input polygons is bigger than the content based grid cell used the code will break it up at the cell border.
_break_up_big_polygons boolean,

-- ST_IsEmpty(ST_Buffer(face_geom,-min_st_is_empty_buffer))
-- This is an approximate value min_st_is_empty_buffer in meter used remove sliver polygon,
-- The formula used to convert from meter used for testing now
-- SELECT ST_Length(ST_Boundary(ST_Envelope(((ST_Buffer('POINT (10.061995275172155 59.716094977883)'::geography,1,'quad_segs=1'))::geometry))))/9;
_min_st_is_empty_buffer_value float,

-- To limit and set as master area. This must be in the same coordsys as _new_sf_table_srid and must be polygon or a multipolgyon,
-- This will added as the first geometry into postgis topology. Data outside the master area will not be used.
-- If you send in a null geometry here this parameter will not have any effect.
_tmp_rog_input_boundary rog_input_boundary,

-- a flag to run checks like topology.ValidateTopology and https://github.com/larsop/resolve-overlap-and-gap
-- To run this does take time, spesially when working on verry very datasets
-- In the final result there should be no Topology errors in the topology structure
-- In the simple feature result produced from Postgis Topology there should be overlaps
_do_qualitycheck_on_final_reseult boolean,

-- How to move data from tmp topology to final topology
--  Method 1 topology.TopoGeo_addLinestring
--  Method 2 topology.TopoGeo_LoadGeometry
--  Method 3 select from tmp toplogy into master topology
_transfer_method_from_temp_2_final_topology int,

_max_parallel_jobs int, -- this is the max number of paralell jobs to run. There must be at least the same number of free connections
_max_rows_in_each_cell int, -- this is the max number rows that intersects with box before it's split into 4 new boxes
INOUT _tables_ext_metainfo rog_overlay_source_table_metainfo_type[],

	-- This contains info about how clean edges from the input layers
	-- TODO support an ARRAY ff you have two tables as input only want clean up second on kepp NULL in row number ser

	__edge_input_tables_cleanup rog_clean_edge_type[] DEFAULT NULL,
	--(_edge_input_tables_cleanup).simplify_tolerance float default 0, -- is this is more than zero simply will called with
	--(_edge_input_tables_cleanup).simplify_max_average_vertex_length int default 0, -- in meter both for utm and deegrees, this used to avoid running ST_simplifyPreserveTopology for long lines lines with few points
	--(_edge_input_tables_cleanup).chaikins_nIterations int default 0, -- IF 0 NO CHAKINS WILL BE DONE,  A big value here make no sense because the number of points will increaes exponential )
	--(_edge_input_tables_cleanup).chaikins_max_length int default 0, --edge that are longer than this value will not be touched by _chaikins_min_degrees and _chaikins_max_degrees
	--(_edge_input_tables_cleanup).chaikins_min_degrees int default 0, -- The angle has to be less this given value, This is used to avoid to touch all angles.
	--(_edge_input_tables_cleanup).chaikins_max_degrees int default 0, -- OR the angle has to be greather than this given value, This is used to avoid to touch all angles
	--(_edge_input_tables_cleanup).chaikins_min_steep_angle_degrees int default 0, -- The angle has to be less this given value, This is used to avoid to touch all angles.
	--(_edge_input_tables_cleanup).chaikins_max_steep_angle_degrees int default 0-- OR The angle has to be greather than this given value, This is used to avoid to touch all angles

	-- Used for debug areas or start and stop at certain area
	_debug_options resolve_overlap_data_debug_options_type DEFAULT NULL

)
AS $BODY$
DECLARE

	-- TODO we may add as parameters
	-- _do_validate_geometry boolean = true; -- IF true ST_Validate will done and the result will saved in geo_isvalid
	-- _try_fix_invalid_geometry boolean = true; -- IF true ST_MakeValid will done and the result will be used and column geo_tried_fix_invalid will be set to true.
	-- _drop_new_sf_table_if_exists boolean = true; -- IF true the out table will be  dropped if it exists . If called with false the _new_sf_table_name must not exists.

	-- used cleaning up the final result
	resolve_overlap_data_clean resolve_overlap_data_clean_type;

	-- Used to crete info about topology layer to be used
	topology_info resolve_overlap_data_topology_type;

	-- Used for debug areas or start and stop at certain area
	debug_options resolve_overlap_data_debug_options_type;

	-- The structure used by rog_overlay computed based list of _input_tables text[]
	-- This is the structure that is returned to the user to list what columns used, srid and so on.
	tables_ext_metainfo rog_overlay_source_table_metainfo_type[];

	-- Used by topo_rog_static.resolve_overlap_gap_run
	tables_out resolve_overlap_data_input_type[];

	-- The table that contains the simple feature table result
	result_table_name_sf text;

	-- Number of overlaps found in the result
	num_overlaps int;

	-- Number of grid cell with where we have Toplogy invalidates
	num_topo_cells_with_topo_errors int;

	-- Table name prefix used to test for overlay and invalid topologies
	table_name_result_prefix text;

	-- The error message to return if any error found
	error_message_rog_topo_error text = '';

	-- The warn message to return if any warn found
	warn_message_rog_topo_error text = '';

	-- Number lines return an error when after retry when added to Postgis Topology
	topo_errors_when_add_line int;

	sql_cmd text;

	-- a flag for testing the diff using a temporary tempology or not or insert directly into final source topology
	-- this is a test related to performance and testing on that, the final results should be pretty much the same.
	-- false ends up in phase one locking low CPU usage
	--[ RECORD 16 ]-----+---------------------------------------
	--relation           | prosj_arealregnskap_overlay_v2_t7topo_.node
	--db01tempdata.nibio.no postgres@compare_ar5_test=#   select relation::regclass, * from pg_locks where not granted;
	use_temp_topology boolean = true;

	-- SELECT ST_Length(ST_Boundary(ST_Envelope(((ST_Buffer('POINT (10.061995275172155 59.716094977883)'::geography,1,'quad_segs=1'))::geometry))))/9;
	min_st_is_empty_buffer_value float;

	my_pid int;
	rog_command text[];

	-- start used by start_execute_parallel
	connstr text; -- this will be buildt up inside start_execute_parallel at first call
	conntions_array text[]; -- this will be buildt up inside start_execute_parallel at first call
	conn_stmts text[]; -- this will be buildt up inside start_execute_parallel at first call
	stats_done text[]; -- This should be null when called , and new statements done will be added to this list.
	all_stmts_done boolean = false; -- this will be buildt up inside start_execute_parallel at first call
	start_execute_parallel_counter int = 0;
	seconds_to_wait_before_check_result int = 300;
	-- done used by start_execute_parallel

	start_time timestamp WITH time zone;
	start_time_delta_job timestamp WITH time zone;
	used_time real;

BEGIN


	-- check input table and make a structure for all the tables like geometry columns, primary keys and so that needed overlap and gap
	CALL topo_rog_static.rog_compute_esri_union_source_table_metainfo_type(_input_tables,tables_ext_metainfo);

		-- map this structure to for new structure used by topo_rog_static.resolve_overlap_gap_run
	tables_out = topo_rog_static.rog_map_2_resolve_overlap_data_input_type(tables_ext_metainfo);

	IF _min_st_is_empty_buffer_value IS NOT NULL AND _min_st_is_empty_buffer_value > 0 THEN
		-- TODO find a better method to copute this value
		min_st_is_empty_buffer_value = ST_Length(ST_Boundary(ST_Envelope(((ST_Buffer('POINT (10.061995275172155 59.716094977883)'::geography,_min_st_is_empty_buffer_value,'quad_segs=1'))::geometry))))/9.0;
	END IF;



	-- info about the topology structure to use
	topology_info = resolve_overlap_data_topology_type_func(
								_input_topology_result_schema_name=>_topology_result_schema_name,

								_topology_name=>ros_view_sf_result_topo_name(_topology_result_schema_name,_tmp_rog_input_boundary)::varchar, -- The topology schema name where we store store sufaces and lines from the simple feature dataset and th efinal result, Any exting data will related to topology_name will be deleted

								_topology_srid=>_new_sf_table_srid, -- This is srid to be  used when created

								_topology_snap_tolerance=>_topology_snap_tolerance, -- This is tolerance used as base when creating the the postgis topolayer

								_create_topology_attrbute_tables=>TRUE, --  if this is true and we value for line_table_name we create attribute tables refferances to, this tables will have atrbuttes equal to the simple feauture tables for lines and feautures

								-- a flag for testing the diff using a temporary tempology or not or insert directly into final source topology
								-- this is a test related to performance and testing on that, the final results should be pretty much the same.
								_use_temp_topology=>use_temp_topology,

								-- a flag to run checks like topology.ValidateTopology and https://github.com/larsop/resolve-overlap-and-gap
								-- To run this does take time, spesially when working on verry very datasets
								-- In the final result there should be no Topology errors in the topology structure
								-- In the simple feature result produced from Postgis Topology there should be overlaps
								_do_qualitycheck_on_final_reseult=>_do_qualitycheck_on_final_reseult,

								-- How to move data from tmp topology to final topology
								--  Method 1 topology.TopoGeo_addLinestring
								--  Method 2 topology.TopoGeo_LoadGeometry
								--  Method 3 select from tmp toplogy into master topology
								_transfer_method_from_temp_2_final_topology=>_transfer_method_from_temp_2_final_topology

	);


	-- set values for clean data
	resolve_overlap_data_clean = resolve_overlap_data_clean_type_func(  -- TYPE resolve_overlap_data_clean

									_min_area_to_keep=>_min_area_to_keep,  -- if this a polygon  is below this limit it will merge into a neighbour polygon. The area is sqare meter.

									_split_input_lines_at_vertex_n=>_split_input_lines_at_vertex_n,  -- split all lines at given vertex num.

									-- if the evenlope of an input polygons is bigger than the content based grid cell used the code will break it up at the cell border.
									_break_up_big_polygons=>_break_up_big_polygons,

									_min_st_is_empty_buffer_value=>min_st_is_empty_buffer_value , -- This is an approximate value min_st_is_empty_buffer in meter used remove sliver polygon,

									-- To limit and set as master area. This must be in the same coordsys as _new_sf_table_srid and must be polygon or a multipolgyon,
									-- This will added as the first geometry into postgis topology. Data outside the master area will not be used.
									-- If you send in a null geometry here this parameter will not have any effect.
									_input_boundary=>(_tmp_rog_input_boundary).geom, -- limit the input area
									_edge_input_tables_cleanup=>__edge_input_tables_cleanup -- parameter in meter for used remove sliver polygon
	);

	-- set debug options default
	IF _debug_options IS NULL THEN
		debug_options = resolve_overlap_data_debug_options_func(
										_contiune_after_stat_exception=>true --if set to false, it will do topology.ValidateTopology and stop to if the this call returns any rows
		);
	ELSE
		debug_options = _debug_options;
	END IF;



	COMMIT;

	-- CLEAN up columns not used any more
	sql_cmd = FORMAT($fmt$
			CALL topo_rog_static.resolve_overlap_gap_run(
								%1$L::resolve_overlap_data_input_type[], -- TYPE resolve_overlap_data_input
								%2$L::resolve_overlap_data_topology_type, -- TYPE resolve_overlap_data_topology
								%3$L::resolve_overlap_data_clean_type,
								%4$L::int, -- _max_parallel_jobs int, -- this is the max number of paralell jobs to run. There must be at least the same number of free connections
								%5$L::int, -- _max_rows_in_each_cell int, -- this is the max number rows that intersects with box before it's split into 4 new boxes, default is 5000
								%6$L::resolve_overlap_data_debug_options_type );
			$fmt$,
			tables_out, -- TYPE resolve_overlap_data_input
			topology_info, -- TYPE resolve_overlap_data_topology
			resolve_overlap_data_clean,
			_max_parallel_jobs, -- _max_parallel_jobs int, -- this is the max number of paralell jobs to run. There must be at least the same number of free connections
			_max_rows_in_each_cell, -- _max_rows_in_each_cell int, -- this is the max number rows that intersects with box before it's split into 4 new boxes, default is 5000
			debug_options
	);


	-- run the job the background and reload in one single tgread
	rog_command = ARRAY[sql_cmd];

	all_stmts_done = FALSE;
	start_execute_parallel_counter = 0;
	connstr = NULL;
	conntions_array = NULL;
	conn_stmts = NULL;

	start_time_delta_job = Clock_timestamp();

	WHILE all_stmts_done = FALSE LOOP
		stats_done = NULL;
		CALL start_execute_parallel(
			rog_command::text[],
			connstr,
			conntions_array,
			conn_stmts,
			stats_done,
			all_stmts_done,
			seconds_to_wait_before_check_result,
			1
		);
		start_execute_parallel_counter = start_execute_parallel_counter + 1;

		used_time := (Extract(EPOCH FROM (Clock_timestamp() - start_time_delta_job)));
		IF used_time > seconds_to_wait_before_check_result THEN
			start_time_delta_job = Clock_timestamp();
			RAISE NOTICE 'Status for start_execute_parallel in _rog_overlay at loop number %  all_stmts_done % conntions_array size % after start_execute_parallel statements list left to run  % at % in % secs',
			start_execute_parallel_counter, all_stmts_done, coalesce(Array_length(conntions_array, 1),0), coalesce(Array_length(rog_command, 1),0), Clock_timestamp(), (Extract(EPOCH FROM (Clock_timestamp() - start_time)));
		END IF;


		IF all_stmts_done = FALSE THEN
			perform pg_sleep(seconds_to_wait_before_check_result/10);
			COMMIT;
		END IF;


	END LOOP;

	--call do_execute_parallel(rog_command);




	-- TODO this should be returned from  topo_rog_static.resolve_overlap_gap_run
	table_name_result_prefix = ros_view_sf_result_topo_name(_topology_result_schema_name,_tmp_rog_input_boundary)||'.'||ros_view_sf_result_base_name(_topology_result_schema_name,_input_tables)::text;
	result_table_name_sf = table_name_result_prefix;


	-- CLEAN up columns not used any more
	sql_cmd = FORMAT($fmt$
		ALTER TABLE %1$s DROP COLUMN _other_intersect_id_list;
		ALTER TABLE %1$s DROP COLUMN _all_intersect_id_list;
		ALTER TABLE %1$s DROP COLUMN _input_geo_is_valid;
		ALTER TABLE %1$s SET LOGGED;

			$fmt$,
			result_table_name_sf
	);
	EXECUTE sql_cmd;

	-- the checks has/is be run for qualitycheck
	IF _do_qualitycheck_on_final_reseult THEN


		-- Enable when https://gitlab.com/nibioopensource/find-overlap-and-gap is up running

		-- Test for overlap and gap on simple feature result genrated by from postgis topology
		-- In test we not use Postgis Topology but only simple feature
		CALL find_overlap_gap_run(
		result_table_name_sf,
		'geom',
		_new_sf_table_srid,
		result_table_name_sf||'_rogtest',
			_max_parallel_jobs, -- _max_parallel_jobs int, -- this is the max number of paralell jobs to run. There must be at least the same number of free connections
			_max_rows_in_each_cell -- _max_rows_in_each_cell int, -- this is the max number rows that intersects with box before it's split into 4 new boxes, default is 5000
			);


		-- Test for overlap on the simple feature result using https://gitlab.com/nibioopensource/find-overlap-and-gap
		EXECUTE format($fmt$
				SELECT count(*) FROM %s;
			$fmt$,
			result_table_name_sf||'_rogtest_overlap')
			INTO num_overlaps;

		-- We should never have overlaps in the simple feature result throws an error if that happens
		IF num_overlaps > 0 THEN
		error_message_rog_topo_error = format($fmt$
		Num overlap errors found %s in table %s can be viewed in %s .
		$fmt$,
		num_overlaps,
		result_table_name_sf,
		table_name_result_prefix||'_rogtest_overlap');
		END IF;

		-- Test ValidateTopology using https://postgis.net/docs/en/ValidateTopology.html
		EXECUTE format($fmt$
				SELECT count(*) FROM %s WHERE validate_result IS NOT NULL;;
			$fmt$,
			table_name_result_prefix||'_grid_validate_topology')
			INTO num_topo_cells_with_topo_errors;

		-- We should not have Topology errors
		IF num_topo_cells_with_topo_errors > 0 THEN
			error_message_rog_topo_error = error_message_rog_topo_error || format($fmt$
			Topology validate errors found %s in table %s can be viewed in %s .
			$fmt$,
			num_topo_cells_with_topo_errors,
			result_table_name_sf,
			table_name_result_prefix||'_grid_validate_topology');
		END IF;


		-- Test ValidateTopology using https://postgis.net/docs/en/ValidateTopology.html
		-- SELECT count(*) from tmp_topoarea_topo_res_komid_1507.multi_input_8_tables_no_cut_line_failed where line_geo_lost ;
		EXECUTE format($fmt$
				SELECT count(*) FROM %s WHERE line_geo_lost;
			$fmt$,
			table_name_result_prefix||'_no_cut_line_failed')
			INTO topo_errors_when_add_line;

		-- We should not have Topology errors
		IF topo_errors_when_add_line > 0 THEN
			warn_message_rog_topo_error =
			format($fmt$
			Number lines failed to add to Postgis Topology  %s in table %s can be viewed in %s .
			$fmt$,
			topo_errors_when_add_line,
			result_table_name_sf,
			table_name_result_prefix||'_no_cut_line_failed');
		END IF;

			-- Return exception if overlay or Topo Validate errors
		IF num_topo_cells_with_topo_errors > 0 OR num_overlaps > 0 OR topo_errors_when_add_line > 0 THEN
			RAISE NOTICE '% . %, With this input % .',
			error_message_rog_topo_error,
			warn_message_rog_topo_error,
			tables_ext_metainfo;
		END IF;
	ELSE
		RAISE NOTICE 'No do_qualitycheck_on_final_reseult has be run on this result %',tables_ext_metainfo;


		END IF;
	-- set return result
	_tables_ext_metainfo = tables_ext_metainfo;
END
$BODY$ LANGUAGE 'plpgsql';

/**

This is wrapper function with default values for

min_area_to_keep float = 1;  -- Will merge into a neighbour polygon by remving the line added latest. The area is sqare meter.
topology_snap_tolerance float = 0 -- this is tolerance used as base when creating the the postgis topolayer

*/

CREATE OR REPLACE PROCEDURE rog_overlay(
_input_tables text[], -- List of table with including schema name on this format ARRAY['sk_grl.n5_forv_fylke2021_flate','sk_grl.n2000_komm_flate']
_topology_result_schema_name text, -- The name for topology and schema
_min_area_to_keep float default 1.0,  -- Will merge into a neighbour polygon by remving the line added latest. The area is sqare meter.
_max_parallel_jobs int default 15, -- this is the max number of paralell jobs to run. There must be at least the same number of free connections
_max_rows_in_each_cell int default 600 -- this is the max number rows that intersects with box before it's split into 4 new boxes
)
AS $BODY$
DECLARE

new_sf_table_srid int = 4258; -- The SRID to be used in the output table geometry column


split_input_lines_at_vertex_n int; -- split all lines at given vertex num

-- if the evenlope of an input polygons is bigger than the content based grid cell used the code will break it up at the cell border.
-- Like https://gitlab.com/nibioopensource/resolve-overlap-and-gap/uploads/2a4ce05e531ced49e8881b5051cf85ca/Screenshot_2024-04-02_at_21.10.37.png
-- st_npoints | st_area  | mdr_area  | st_numinteriorrings
-- 3627257 | 20311057 | 742819770 |               10586
break_up_big_polygons boolean = false;

-- ST_IsEmpty(ST_Buffer(face_geom,-min_st_is_empty_buffer))
-- This is an approximate value min_st_is_empty_buffer in meter used remove sliver polygon,
-- The formula used to convert from meter used for testing now
-- SELECT ST_Length(ST_Boundary(ST_Envelope(((ST_Buffer('POINT (10.061995275172155 59.716094977883)'::geography,1,'quad_segs=1'))::geometry))))/9;
min_st_is_empty_buffer_value float = 0;

-- To limit and set as master area. This must be in the same coordsys as _new_sf_table_srid and must be polygon or a multipolgyon,
-- Data outside the master area will not be used.
-- If you send in a null geometry here this parameter will not have any effect.
input_boundary_list rog_input_boundary[] = NULL;

-- This is tolerance used as base when creating the the postgis topolayer
topology_snap_tolerance float = 0.0;

-- a flag to run checks like topology.ValidateTopology and https://github.com/larsop/resolve-overlap-and-gap
	-- To run this does take time, spesially when working on verry very datasets
	-- In the final result there should be no Topology errors in the topology structure
	-- In the simple feature result produced from Postgis Topology there should be overlaps
do_qualitycheck_on_final_reseult boolean = true;

-- How to move data from tmp topology to final topology
	--  Method 1 topology.TopoGeo_addLinestring
	--  Method 2 topology.TopoGeo_LoadGeometry
	--  Method 3 select from tmp toplogy into master topology
transfer_method_from_temp_2_final_topology int default 3;

tables_ext_metainfo rog_overlay_source_table_metainfo_type[];


BEGIN

		-- check input table and make a structure for all the tables like geometry columns, primary keys and so that needed overlap and gap
CALL rog_overlay(
_input_tables, -- List of table with including schema name on this format ARRAY['sk_grl.n5_forv_fylke2021_flate','sk_grl.n2000_komm_flate']
_topology_result_schema_name, -- The name for topology and schema
new_sf_table_srid, -- The SRID to be used in the output table geometry column,
topology_snap_tolerance,
_min_area_to_keep, -- if this a polygon  is below this limit it will merge into a neighbour polygon. The area is sqare meter.
split_input_lines_at_vertex_n, -- split all lines at given vertex num
break_up_big_polygons, -- if the evenlope of an input polygons is bigger than the content based grid cell used the code will break it up at the cell border
min_st_is_empty_buffer_value, -- if ST_IsEmpty(ST_Buffer(face_geom,-min_st_is_empty_buffer)) is TRUE
input_boundary_list,

do_qualitycheck_on_final_reseult,
transfer_method_from_temp_2_final_topology,

_max_parallel_jobs, -- this is the max number of paralell jobs to run. There must be at least the same number of free connections
_max_rows_in_each_cell, -- this is the max number rows that intersects with box before it's split into 4 new boxes
tables_ext_metainfo
);

END
$BODY$ LANGUAGE 'plpgsql';


