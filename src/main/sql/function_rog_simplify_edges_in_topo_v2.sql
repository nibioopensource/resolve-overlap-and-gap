/**

This function wilt try to simplfy based on given input rules.

*/

CREATE OR REPLACE PROCEDURE rog_simplify_edges_in_topo_v2 (
_tables_ext_metainfo rog_overlay_source_table_metainfo_type[],

_topology_info resolve_overlap_data_topology_type,
---(_topology_info).topology_name varchar, -- The topology schema name where we store store sufaces and lines from the simple feature dataset and th efinal result
-- NB. Any exting data will related to topology_name will be deleted
--(_topology_info).topology_srid
--(_topology_info).topology_is_utm
--(_topology_info).topology_snap_tolerance float, -- this is tolerance used as base when creating the the postgis topolayer
--(_topology_info).create_topology_attrbute_tables boolean -- if this is true and we value for line_table_name we create attribute tables refferances to
-- this tables will have atrbuttes equal to the simple feauture tables for lines and feautures

-- a flag for testing the diff using a temporary tempology or not or insert directly into final source topology
-- this is a test related to performance and testing on that, the final results should be pretty much the same.
-- (_topology_info).use_temp_topology boolean

-- a flag to run checks like topology.ValidateTopology and https://github.com/larsop/resolve-overlap-and-gap
-- To run this does take time, spesially when working on verry very datasets so we trun this when needed.
-- In the final result there should be no Topology errors in the topology structure
-- In the simple feature result produced from Postgis Topology there should be overlaps
-- (_topology_info).do_qualitycheck_on_final_reseult boolean

_table_name_result_prefix varchar,
--(_topology_info).topology_name character varying,
--(_topology_info).topology_snap_tolerance float, -- this is tolerance used as base when creating the the postgis topolayer

_clean_info resolve_overlap_data_clean_type, -- different parameters used if need to clean up your data
--(_clean_info)._min_area_to_keep float default 0, -- if this a polygon  is below this limit it will merge into a neighbour polygon. The area is sqare meter.

-- TODO REMOVE start , start to use (_clean_info).edge_input_tables_cleanup rog_clean_edge_type
--(_clean_info)._simplify_tolerance float default 0, -- is this is more than zero simply will called with
--(_clean_info)._simplify_max_average_vertex_length int default 0, -- in meter both for utm and deegrees, this used to avoid running ST_simplifyPreserveTopology for long lines lines with few points
--(_clean_info)._chaikins_nIterations int default 0, -- IF 0 NO CHAKINS WILL BE DONE,  A big value here make no sense because the number of points will increaes exponential )
--(_clean_info)._chaikins_max_length int default 0, --edge that are longer than this value will not be touched by _chaikins_min_degrees and _chaikins_max_degrees
--(_clean_info)._chaikins_min_degrees int default 0, -- The angle has to be less this given value, This is used to avoid to touch all angles.
--(_clean_info)._chaikins_max_degrees int default 0, -- OR the angle has to be greather than this given value, This is used to avoid to touch all angles
--(_clean_info)._chaikins_min_steep_angle_degrees int default 0, -- The angle has to be less this given value, This is used to avoid to touch all angles.
--(_clean_info)._chaikins_max_steep_angle_degrees int default 0-- OR The angle has to be greather than this given value, This is used to avoid to touch all angles
-- TODO REMOVE end


-- To limit and set as master area. This must be in the same coordsys as _new_sf_table_srid and must be polygon or a multipolgyon,
-- This will added as the first geometry into postgis topology. Data outside the master area will not be used.
-- If you send in a null geometry here this parameter will not have any effect.
--(_clean_info).input_boundary geometry

_bb geometry,

-- This is parameter separate parameter since this will vary if work temp topolgies or not
_topology_name text,


INOUT _added_lines_temp_topo rog_overlay_add_border_lines_type[]

)
LANGUAGE plpgsql
AS $$
DECLARE

	rec_new_simplify_res RECORD;
	rec_new_line RECORD;
	has_edges boolean;
	has_edges_temp_table_name text;


	result_retry_simplify int;

	tmp_ext_metainfo rog_overlay_source_table_metainfo_type;
	do_st_transform boolean;
	bb_to_use geometry; -- This is the box to use i each and that

	tmp_edge_input_tables_cleanup rog_clean_edge_type;
	edge_input_tables_cleanup_array rog_clean_edge_type[];
	layer_i int;

	command_find_edges_to_handle text;
	command_string_temp_table_name text;
	command_string_simplify text;
	command_find_edges_to_replace text;
	command_find_edges_to_affected text;




	lines_try_to_simplify text;
	res_split_border_lines rog_get_simplified_split_border_lines_result;

	inputLineEdgeIds integer[];
	inputLineEdgeIdsHandled integer[];

	face_edge_ids_replace integer[];
	tmp_face_edge_ids_replace integer[];
	face_edge_ids_replace_affected integer[];
	tmp_face_edge_ids_replace_affected integer[];

	v_state text;
	v_msg text;
	v_detail text;
	v_hint text;
	v_context text;

	face_edges_to_remove_list add_border_lines_faces_to_remove[];
	added_simplify_lines_temp_topo rog_overlay_add_border_lines_type[];
	result rog_overlay_add_border_lines_type ;


BEGIN

	edge_input_tables_cleanup_array = (_clean_info).edge_input_tables_cleanup;

	-- IF tmp_edge_input_tables_cleanup IS NOT NULL THEN
	IF edge_input_tables_cleanup_array IS NOT NULL AND ARRAY_Length(edge_input_tables_cleanup_array,1) > 0 THEN

		layer_i = 0;
		-- The list of id's
		inputLineEdgeIdsHandled = '{}';

		-- Loop through each entry in _tables_ext_metainfo
		FOREACH tmp_ext_metainfo IN ARRAY _tables_ext_metainfo LOOP
			inputLineEdgeIds = '{}';
			layer_i = layer_i + 1;
			tmp_edge_input_tables_cleanup = edge_input_tables_cleanup_array[layer_i];

			-- SQL doe get a list of edges thats have the same table_input_order as current tmp_ext_metainfo.table_input_order
			-- from added_lines_temp_topo
			command_find_edges_to_handle = FORMAT(
				$fmt$
				SELECT r.ok_input_line_edges, r.table_input_order, r.src_table_pk_column_value, r.input_line, r.added_line_structure
				FROM
				(
					SELECT ab.ok_input_line_edges, ab.table_input_order, ab.src_table_pk_column_value, ab.input_line, ab as added_line_structure
					FROM unnest(%1$L::rog_overlay_add_border_lines_type[]) ab
				)
				AS r
				WHERE r.src_table_pk_column_value IS NOT NULL AND r.src_table_pk_column_value  != ''
				AND %2$s = r.table_input_order
				$fmt$,
				_added_lines_temp_topo,
				tmp_ext_metainfo.table_input_order
			);

			-- Run simplify if tmp_edge_input_tables_cleanup.simplify_tolerance is set this table
			IF tmp_edge_input_tables_cleanup.simplify_tolerance > 0.0 THEN

				RAISE NOTICE 'Will try to run simplify for topology % with tmp_edge_input_tables_cleanup % for tmp_ext_metainfo % Clock_timestamp() %'
				,_topology_name, tmp_edge_input_tables_cleanup, tmp_ext_metainfo, Clock_timestamp();

				-- We need to know srid of input tables
				If (_topology_info).topology_srid != tmp_ext_metainfo.table_geo_srid THEN
					do_st_transform = true;
					bb_to_use = ST_Transform(_bb,tmp_ext_metainfo.table_geo_srid);
				ELSE
					do_st_transform = false;
					bb_to_use = _bb;
				END IF;

				-- This is the command used to run simplify for the edges found
				-- when do HAVE a complete lost of edges id

				IF (tmp_edge_input_tables_cleanup.simplify_attribute_values).attribute_name IS NOT NULL THEN
					command_string_simplify = FORMAT(
					$fmt$
						SELECT
						r.*,
						topo_update.try_ST_ChangeEdgeGeom(r.geom, %1$L, NULL, %4$L, r.edge_id, simplify_geom) AS changed
						FROM (
							SELECT
							r.tollerance,
							r.ord,
							r.edge_id,
							r.geom,
							ST_simplifyPreserveTopology( r.geom, r.tollerance) simplify_geom
							FROM (
								SELECT DISTINCT ON (e1.edge_id) e1.edge_id,
								sim.ord,
								e1.geom,
								CASE WHEN (%11$L::float[])[sim.ord] IS NULL THEN %5$s ELSE (%11$L::float[])[sim.ord] END AS tollerance
								FROM
								%1$s.edge_data e1,
								%6$s input
								LEFT JOIN unnest(%10$L::text[]) WITH ORDINALITY AS sim(%9$s, ord) ON ( CAST(input.%9$s AS text) = CAST(sim.%9$s AS text))
								WHERE e1.edge_id = ANY ($4::integer[]) AND
								input.%7$s = $2::%8$s AND
								(ST_Disjoint(e1.geom,%3$L) OR %3$L is null) AND
								ST_NumPoints(e1.geom) > 2 AND
								(e1.edge_id = ANY ($3::integer[])) IS NOT TRUE
								ORDER by e1.edge_id
							) r WHERE r.tollerance > 0.0 ORDER BY r.ord, ST_length(r.geom,true), r.edge_id
						) r
					$fmt$,
					_topology_name, --01
					NULL, --02
					(res_split_border_lines).fixed_point_set, --03
					(_topology_info).topology_is_utm, --04
					tmp_edge_input_tables_cleanup.simplify_tolerance, --05
					tmp_ext_metainfo.table_name, --06
					tmp_ext_metainfo.table_pk_colum, --07
					tmp_ext_metainfo.table_pk_type, --08
					(tmp_edge_input_tables_cleanup.simplify_attribute_values).attribute_name, --09
					(tmp_edge_input_tables_cleanup.simplify_attribute_values).attribute_values, --10
					(tmp_edge_input_tables_cleanup.simplify_attribute_values).simplify_tolerances --11
					);
				ELSE
					command_string_simplify = FORMAT(
					$fmt$
						SELECT
						r.*,
						topo_update.try_ST_ChangeEdgeGeom(r.geom, %1$L, NULL, %4$L, r.edge_id, simplify_geom) AS changed
						FROM (
							SELECT
							r.tollerance,
							r.edge_id,
							r.geom,
							ST_simplifyPreserveTopology(r.geom, r.tollerance) simplify_geom
							FROM (
								SELECT DISTINCT ON (e1.edge_id) e1.edge_id,
								e1.geom,
								%5$s tollerance
								FROM
								%1$s.edge_data e1,
								%6$s input
								WHERE e1.edge_id = ANY ($4::integer[]) AND
								input.%7$s = $2::%8$s AND
								(ST_Disjoint(e1.geom,%3$L) OR %3$L is null) AND
								ST_NumPoints(e1.geom) > 2 AND
								(e1.edge_id = ANY ($3::integer[])) IS NOT TRUE
								ORDER by e1.edge_id
							) r WHERE r.tollerance > 0.0 ORDER BY ST_length(r.geom,true), r.edge_id
						) r
					$fmt$,
					_topology_name, --01
					NULL, --02
					(res_split_border_lines).fixed_point_set, --03
					(_topology_info).topology_is_utm, --04
					tmp_edge_input_tables_cleanup.simplify_tolerance, --05
					tmp_ext_metainfo.table_name, --06
					tmp_ext_metainfo.table_pk_colum, --07
					tmp_ext_metainfo.table_pk_type --08
					);

				END IF;

				-- Find all edges this tmp_ext_metainfo.table_input_order
				FOR rec_new_line IN EXECUTE command_find_edges_to_handle LOOP

					--RAISE NOTICE 'ok_input_line_edges %, input_line_edges_to_drop %, table_input_order %, src_table_pk_column_value % ',
					--rec_new_line.ok_input_line_edges, rec_new_line.input_line_edges_to_drop, rec_new_line.table_input_order, rec_new_line.src_table_pk_column_value;
					-- Run simplify for each line
					FOR rec_new_simplify_res IN
						EXECUTE command_string_simplify USING
						NULL, --$1
						rec_new_line.src_table_pk_column_value, --$2
						inputLineEdgeIdsHandled, --$3
						rec_new_line.ok_input_line_edges --$4

					LOOP
						inputLineEdgeIds = '{}';
						-- if no chages because of and error, try smaller simplify
						IF rec_new_simplify_res.changed = -1 THEN
							perform ST_RemEdgeModFace(_topology_name, rec_new_simplify_res.edge_id);
							SELECT array_agg(abs(topogeo_addlinestring))
							FROM topology.TopoGeo_addLinestring(_topology_name,rec_new_simplify_res.simplify_geom,0)
							INTO tmp_face_edge_ids_replace;
							inputLineEdgeIds=inputLineEdgeIds||tmp_face_edge_ids_replace;
						ELSIF rec_new_simplify_res.changed = 1 THEN
							-- build up the recod for the changed lines
							inputLineEdgeIds=inputLineEdgeIds||rec_new_simplify_res.edge_id;
						ELSE
							-- no changes
							inputLineEdgeIds=inputLineEdgeIds||rec_new_line.ok_input_line_edges;
						END IF;

						--result = (inputLineEdgeIds,failed_line,face_edges_to_remove_list,_table_input_order,_src_table_pk_column_value,new_line,selected_edge_face_id);
						result = (inputLineEdgeIds,NULL,NULL,rec_new_line.table_input_order,rec_new_line.src_table_pk_column_value,rec_new_simplify_res.simplify_geom,NULL);
						added_simplify_lines_temp_topo=added_simplify_lines_temp_topo||result;

--						RAISE NOTICE 'rog_simplify_edges_in_topo in _topology_name by replace % result %'
--						,_topology_name, result;
						inputLineEdgeIdsHandled=inputLineEdgeIdsHandled||inputLineEdgeIds;
					END LOOP; -- FOR rec_new_simplify_res IN EXECUTE command_find_edges_to_handle LOOP

				END LOOP; -- FOR rec_new_line IN EXECUTE command_find_edges_to_handle LOOP

			ELSE
				-- will not try to run simplify but we need to update inputLineEdgeIdsHandled so this lines are simplified because line it relates another surface
				-- Find all edges this tmp_ext_metainfo.table_input_order
				FOR rec_new_line IN EXECUTE command_find_edges_to_handle LOOP
--					RAISE NOTICE 'rog_simplify_edges_in_topo in _topology_name by NO simple % for edge rec_new_simplify_res.edge_id % using line %'
--					,_topology_name, rec_new_line.ok_input_line_edges, rec_new_line.input_line;

					inputLineEdgeIdsHandled=inputLineEdgeIdsHandled||rec_new_line.ok_input_line_edges;
					added_simplify_lines_temp_topo=added_simplify_lines_temp_topo||rec_new_line.added_line_structure;

				END LOOP; -- FOR rec_new_line IN EXECUTE command_find_edges_to_handle LOOP

			END IF; --IF tmp_edge_input_tables_cleanup IS NOT NULL AND tmp_edge_input_tables_cleanup.simplify_tolerance > 0 THEN

		END LOOP; -- FOREACH tmp_ext_metainfo IN ARRAY _tables_ext_metainfo LOOP

		_added_lines_temp_topo = added_simplify_lines_temp_topo;


	END IF; -- IF tmp_edge_input_tables_cleanup

END
$$;

