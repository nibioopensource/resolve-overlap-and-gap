CREATE OR REPLACE FUNCTION vsr_get_data_type (_t regclass, _c text)
RETURNS text AS
$BODY$
  SELECT pg_catalog.format_type(a.atttypid, a.atttypmod)
    FROM pg_catalog.pg_attribute a
    WHERE a.attrelid = _t
      AND a.attname = _c;
$BODY$ LANGUAGE sql;

