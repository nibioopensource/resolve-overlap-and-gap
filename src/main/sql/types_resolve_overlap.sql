
/**

This is the primary type in reslove overlap and gap for for now.

It has overlap with info from rog_overlay_source_table_metainfo_type.
We need find out we should merge this two types
or keep resolve_overlap_data_input_type as internal format and dataholder.

*/

CREATE TYPE resolve_overlap_data_input_type AS (
 line_table_name varchar, -- The table with simple feature lines,
 -- If this has a value then data from table will used to form all valid surfaces.
 -- this may be empty, the polygon_table_geo_collumn must of type polygon to be abale to generate a polygon layer

 line_table_pk_column varchar, -- A unique primary column of the line input table
 line_table_geo_collumn varchar, -- The name of geometry column for the line strings


 polygon_table_name varchar, -- The table with simple feature polygons or points attributtes to resolve, imcluding schema name
 -- If we in this tables only have a point and no polygons, the we will need a set of tables the lines also

 polygon_table_alias varchar,

 polygon_table_pk_column varchar, -- A unique primary column of the polygon input table

 polygon_table_geo_collumn varchar, -- the name of geometry column on the table
 -- If the type is a point we also need a set of lines to be used as the border the polygons

 table_srid int, -- the srid for the given geo column on the table analyze
 table_is_utm boolean,

 -- This values are computed by default, mayby check if that ius needed is his ass added by the user
 -- should be on this format qms_id_grense character varying,objtype character varying,aravgrtype character varying,maalemetode character varying,noyaktighet integer,synbarhet character varying,verifiseringsdato date,datafangstdato date,kartid character varying,kjoringsident date,arkartstd character varying,opphav character varying,informasjon character varying,registreringsversjon_produkt character varying,registreringsversjon_versjon character varying,registreringsversjon_undertype character varying,qms_navnerom character varying,qms_versjonid character varying,qms_oppdateringsdato timestamp without time zone,qms_prosesshistorie character varying,qms_kopidata_omraadeid integer,qms_kopidata_originaldatavert character varying,qms_kopidata_kopidato timestamp without time zone,sl_dummy_grense_id integer
 line_table_other_collumns_def varchar,
 -- should be on this format qms_id_grense,objtype,aravgrtype,maalemetode,noyaktighet,synbarhet,verifiseringsdato,datafangstdato,kartid,kjoringsident,arkartstd,opphav,informasjon,registreringsversjon_produkt,registreringsversjon_versjon,registreringsversjon_undertype,qms_navnerom,qms_versjonid,qms_oppdateringsdato,qms_prosesshistorie,qms_kopidata_omraadeid,qms_kopidata_originaldatavert,qms_kopidata_kopidato,sl_dummy_grense_id
 line_table_other_collumns_list varchar,

 -- This values are computed by default, mayby check if that ius needed is his ass added by the user
 -- should be on this format qms_id_grense character varying,objtype character varying,aravgrtype character varying,maalemetode character varying,noyaktighet integer,synbarhet character varying,verifiseringsdato date,datafangstdato date,kartid character varying,kjoringsident date,arkartstd character varying,opphav character varying,informasjon character varying,registreringsversjon_produkt character varying,registreringsversjon_versjon character varying,registreringsversjon_undertype character varying,qms_navnerom character varying,qms_versjonid character varying,qms_oppdateringsdato timestamp without time zone,qms_prosesshistorie character varying,qms_kopidata_omraadeid integer,qms_kopidata_originaldatavert character varying,qms_kopidata_kopidato timestamp without time zone,sl_dummy_grense_id integer
 polygon_table_other_collumns_def varchar,
 -- should be on this format qms_id_grense,objtype,aravgrtype,maalemetode,noyaktighet,synbarhet,verifiseringsdato,datafangstdato,kartid,kjoringsident,arkartstd,opphav,informasjon,registreringsversjon_produkt,registreringsversjon_versjon,registreringsversjon_undertype,qms_navnerom,qms_versjonid,qms_oppdateringsdato,qms_prosesshistorie,qms_kopidata_omraadeid,qms_kopidata_originaldatavert,qms_kopidata_kopidato,sl_dummy_grense_id
 polygon_table_other_collumns_list varchar

);


CREATE TYPE resolve_overlap_data_topology_type AS (
  input_topology_result_schema_name varchar,

  topology_name varchar, -- The topology schema name where we store store sufaces and lines from the simple feature dataset and th efinal result
  -- NB. Any exting data will related to topology_name will be deleted
  topology_srid int, -- This is srid to be  used when created

  topology_is_utm boolean, -- This is needed know how compute areas and lengths corectly

  topology_snap_tolerance float, -- this is tolerance used as base when creating the the postgis topolayer

  create_topology_attrbute_tables boolean, -- if this is true and we value for line_table_name we create attribute tables refferances to
  -- this tables will have atrbuttes equal to the simple feauture tables for lines and feautures

  -- this is computed startup
  topology_attrbute_tables_border_layer_id int,
  -- this is computed startup
  topology_attrbute_tables_surface_layer_id int,

  -- a flag for testing the diff using a temporary tempology or not or insert directly into final source topology
  -- this is a test related to performance and testing on that, the final results should be pretty much the same.
  use_temp_topology boolean,


  -- a flag to run checks like topology.ValidateTopology and https://github.com/larsop/resolve-overlap-and-gap
  -- To run this does take time, spesially when working on verry very datasets so we trun this when needed.
  -- In the final result there should be no Topology errors in the topology structure
  -- In the simple feature result produced from Postgis Topology there should be overlaps
  do_qualitycheck_on_final_reseult boolean,


  --  Method 1 topology.TopoGeo_addLinestring
  --  Method 2 topology.TopoGeo_LoadGeometry
  --  Method 3 select from tmp toplogy into master topology

  transfer_method_from_temp_2_final_topology int




);

CREATE OR REPLACE FUNCTION resolve_overlap_data_topology_type_func(

  _input_topology_result_schema_name varchar default NULL,

  _topology_name varchar default NULL, -- The topology schema name where we store store sufaces and lines from the simple feature dataset and th efinal result
  -- NB. Any exting data will related to topology_name will be deleted
  _topology_srid int default NULL, -- This is srid to be  used when created

  _topology_snap_tolerance float default 0.0, -- this is tolerance used as base when creating the the postgis topolayer

  _create_topology_attrbute_tables boolean default NULL, -- if this is true and we value for line_table_name we create attribute tables refferances to
  -- this tables will have atrbuttes equal to the simple feauture tables for lines and feautures

  -- a flag for testing the diff using a temporary tempology or not or insert directly into final source topology
  -- this is a test related to performance and testing on that, the final results should be pretty much the same.
  _use_temp_topology boolean default false,

  -- a flag to run checks like topology.ValidateTopology and https://github.com/larsop/resolve-overlap-and-gap
  -- To run this does take time, spesially when working on verry very datasets
  -- In the final result there should be no Topology errors in the topology structure
  -- In the simple feature result produced from Postgis Topology there should be overlaps
  _do_qualitycheck_on_final_reseult boolean default false,

  -- How to move data from tmp topology to final topology
  --  Method 1 topology.TopoGeo_addLinestring
  --  Method 2 topology.TopoGeo_LoadGeometry
  --  Method 3 select from tmp toplogy into master topology
  _transfer_method_from_temp_2_final_topology int default 1


)
RETURNS resolve_overlap_data_topology_type
  AS $$
DECLARE

  -- this is computed startup
  topology_attrbute_tables_border_layer_id int;

  -- this is computed startup
  topology_attrbute_tables_surface_layer_id int;

  -- TODO add code so this to computed based srid
  topology_is_utm boolean = false;


  ct resolve_overlap_data_topology_type;
BEGIN
  ct = (
  _input_topology_result_schema_name,
  _topology_name,
  _topology_srid,
  topology_is_utm,
  _topology_snap_tolerance,
  _create_topology_attrbute_tables,
  topology_attrbute_tables_border_layer_id,
  topology_attrbute_tables_surface_layer_id,
  _use_temp_topology,
  _do_qualitycheck_on_final_reseult,
  _transfer_method_from_temp_2_final_topology
  );

  return ct;
END;
$$
LANGUAGE plpgsql;


/**


*/

CREATE TYPE rog_input_boundary AS (
  geom geometry,
  name text
);

-----------------------------------------------------------------------------------------------
-- resolve_based_on_attribute_type for attributes that have equal values ------------------------------
-----------------------------------------------------------------------------------------------
CREATE TYPE resolve_based_on_attribute_type AS (
attribute_resolve_list text, -- A list of attributes to resolve on this format 'attr1 attr2 attr3
attribute_min_common_border_length float, -- Min. length of common border before resolving
attribute_max_common_area_size float -- Max area of objects to resolve
);

CREATE OR REPLACE FUNCTION resolve_based_on_attribute_type_func(
_attribute_resolve_list text default null, -- A list of attributes to resolve
_attribute_min_common_border_length float default 0, -- Min. length of common border before resolving
_attribute_max_common_area_size float default 0 -- Max area of objects to resolve
)
RETURNS resolve_based_on_attribute_type
  AS $$
DECLARE
  ct resolve_based_on_attribute_type;
BEGIN
  ct = (
    _attribute_resolve_list,
    _attribute_min_common_border_length,
    _attribute_max_common_area_size
    );

  return ct;
END;
$$
LANGUAGE plpgsql;

-----------------------------------------------------------------------------------------------
-- Info about about simplify for each attribute value type  --------------------------------------
-----------------------------------------------------------------------------------------------
/**
          'clname', --09
          '{treeLayer,fieldLayer,greyArea,trees,bushLayer,FKB_bygg}',
          '{0.00001,0.00001,0.00001,0.00001,0.00001,0.00002}'
*/

CREATE TYPE rog_clean_simplify_attribute_values AS (
  attribute_name text,
  attribute_values text[],
  simplify_tolerances float[]
);


-----------------------------------------------------------------------------------------------
-- Info about how to clean up edges for each input layer --------------------------------------
-----------------------------------------------------------------------------------------------
CREATE TYPE rog_clean_edge_type AS (
  simplify_tolerance float, -- is this is more than zero ST_simplifyPreserveTopology will called with this tolerance
  simplify_max_average_vertex_length int, -- in meter both for utm and deegrees, this used to avoid running ST_simplifyPreserveTopology for long lines lines with few points
  simplify_attribute_values rog_clean_simplify_attribute_values,

  -- here we will use chaikins togehter with simply to smooth lines
  -- The basic idea idea is to use smooth out sharp edges in another way than
  chaikins_nIterations int, -- -- IF 0 NO CHAKINS WILL BE DONE,  A big value here make no sense because the number of points will increaes exponential )
  chaikins_max_length int, --edge that are longer than this value will not be touched for chaikins_min_degrees or chaikins_max_degrees
  chaikins_min_degrees int, -- The angle has to be less this given value, This is used to avoid to touch all angles.
  chaikins_max_degrees int, -- OR rhe angle has to be greather than this given value, This is used to avoid to touch all angles

  -- This is used to round angles that are verry step and does not depend chaikins_max_length
  chaikins_min_steep_angle_degrees int, -- The angle has to be less this given value, This is used to avoid to touch all angles.
  chaikins_max_steep_angle_degrees int -- OR the angle has to be greather than this given value, This is used to avoid to touch all angles
);


CREATE OR REPLACE FUNCTION rog_clean_edge_type_func(
_simplify_tolerance float default 0, -- if this is more than zero this is the default value for simplify
_simplify_max_average_vertex_length int default 0, -- if this is more than zero this is the default value to trigger simplify, in meter both for utm and deegrees, this used to avoid running ST_simplifyPreserveTopology for long lines lines with few points
_simplify_attribute_values rog_clean_simplify_attribute_values DEFAULT NULL,
_chaikins_nIterations int default 0, -- IF 0 NO CHAKINS WILL BE DONE,  A big value here make no sense because the number of points will increaes exponential )
_chaikins_max_length int default 0, --edge that are longer than this value will not be touched by _chaikins_min_degrees and _chaikins_max_degrees
_chaikins_min_degrees int default 0, -- The angle has to be less this given value, This is used to avoid to touch all angles.
_chaikins_max_degrees int default 0, -- OR the angle has to be greather than this given value, This is used to avoid to touch all angles
_chaikins_min_steep_angle_degrees int default 0, -- The angle has to be less this given value, This is used to avoid to touch all angles.
_chaikins_max_steep_angle_degrees int default 0-- OR The angle has to be greather than this given value, This is used to avoid to touch all angles
)
RETURNS rog_clean_edge_type
  AS $$
DECLARE
  ect rog_clean_edge_type;
BEGIN
  ect = (
    _simplify_tolerance,
    _simplify_max_average_vertex_length,
    _simplify_attribute_values,
    _chaikins_nIterations,
    _chaikins_max_length,
    _chaikins_min_degrees,
    _chaikins_max_degrees,
    _chaikins_min_steep_angle_degrees,
    _chaikins_max_steep_angle_degrees
  );
  return ect;
END;
$$
LANGUAGE plpgsql;


-----------------------------------------------------------------------------------------------
-- resolve_overlap_data_clean_type ------------------------------------------------------------
-----------------------------------------------------------------------------------------------
-- TODO move to rog_clean_edge_type
CREATE TYPE resolve_overlap_data_clean_type AS (
  -- TODO move own type
  min_area_to_keep float, -- if this a polygon  is below this limit it will merge into a neighbour polygon. The area is sqare meter.

  split_input_lines_at_vertex_n int, -- split all lines at given vertex num

  -- if the evenlope of an input polygons is bigger than the content based grid cell used the code will break it up at the cell border.
  -- Like https://gitlab.com/nibioopensource/resolve-overlap-and-gap/uploads/2a4ce05e531ced49e8881b5051cf85ca/Screenshot_2024-04-02_at_21.10.37.png
  -- st_npoints | st_area  | mdr_area  | st_numinteriorrings
  -- 3627257 | 20311057 | 742819770 |               10586
  break_up_big_polygons boolean,

  resolve_based_on_attribute resolve_based_on_attribute_type, -- resolve_based_on_attribute_type for attributes that have equal values

  --TODO move this ones into rog_clean_edge_type

  simplify_max_average_vertex_length int, -- in meter both for utm and deegrees, this used to avoid running ST_simplifyPreserveTopology for long lines lines with few points

  -- here we will use chaikins togehter with simply to smooth lines
  -- The basic idea idea is to use smooth out sharp edges in another way than
  chaikins_nIterations int, -- -- IF 0 NO CHAKINS WILL BE DONE,  A big value here make no sense because the number of points will increaes exponential )
  chaikins_max_length int, --edge that are longer than this value will not be touched for chaikins_min_degrees or chaikins_max_degrees
  chaikins_min_degrees int, -- The angle has to be less this given value, This is used to avoid to touch all angles.
  chaikins_max_degrees int, -- OR rhe angle has to be greather than this given value, This is used to avoid to touch all angles

  -- This is used to round angles that are verry step and does not depend chaikins_max_length
  chaikins_min_steep_angle_degrees int, -- The angle has to be less this given value, This is used to avoid to touch all angles.
  chaikins_max_steep_angle_degrees int, -- OR the angle has to be greather than this given value, This is used to avoid to touch all angles

  --TODO move this ones into rog_clean_edge_type


  -- To limit and set as master area. This must be in the same coordsys as _new_sf_table_srid and must be polygon or a multipolgyon,
  -- This will added as the first geometry into postgis topology. NB! this not done now since it kills the performance
  -- Data outside the master area will not be used.
  -- If you send in a null geometry here this parameter will not have any effect.
  input_boundary geometry,

  -- ST_IsEmpty(ST_Buffer(face_geom,-min_st_is_empty_buffer))
  -- This is an approximate value min_st_is_empty_buffer in meter used remove sliver polygon,
  -- The formula used to convert from meter used for testing now
  -- SELECT ST_Length(ST_Boundary(ST_Envelope(((ST_Buffer('POINT (10.061995275172155 59.716094977883)'::geography,1,'quad_segs=1'))::geometry))))/9;
  min_st_is_empty_buffer_value float,


  -- This contains info about how clean edges from the input layers
  -- If thus NULL no edge simplification will be done
  -- TODO support an ARRAY ff you have two tables as input only want clean up second on kepp NULL in row number ser
  edge_input_tables_cleanup rog_clean_edge_type[]
  --(edge_input_tables_cleanup).simplify_tolerance float default 0, -- is this is more than zero simply will called with
  --(edge_input_tables_cleanup).simplify_max_average_vertex_length int default 0, -- in meter both for utm and deegrees, this used to avoid running ST_simplifyPreserveTopology for long lines lines with few points
  --(edge_input_tables_cleanup).chaikins_nIterations int default 0, -- IF 0 NO CHAKINS WILL BE DONE,  A big value here make no sense because the number of points will increaes exponential )
  --(edge_input_tables_cleanup).chaikins_max_length int default 0, --edge that are longer than this value will not be touched by _chaikins_min_degrees and _chaikins_max_degrees
  --(edge_input_tables_cleanup).chaikins_min_degrees int default 0, -- The angle has to be less this given value, This is used to avoid to touch all angles.
  --(edge_input_tables_cleanup).chaikins_max_degrees int default 0, -- OR the angle has to be greather than this given value, This is used to avoid to touch all angles
  --(edge_input_tables_cleanup).chaikins_min_steep_angle_degrees int default 0, -- The angle has to be less this given value, This is used to avoid to touch all angles.
  --(edge_input_tables_cleanup).chaikins_max_steep_angle_degrees int default 0-- OR The angle has to be greather than this given value, This is used to avoid to touch all angles
);


CREATE OR REPLACE FUNCTION resolve_overlap_data_clean_type_func(
_min_area_to_keep float default 0, -- if this a polygon  is below this limit it will merge into a neighbour polygon. The area is sqare meter.
_split_input_lines_at_vertex_n int default 0, -- split all lines at given vertex num

_break_up_big_polygons boolean default false, -- if the evenlope of an input polygons is bigger than the content based grid cell used the code will break it up at the cell border.

_resolve_based_on_attribute resolve_based_on_attribute_type default null, -- resolve_based_on_attribute_type for attributes that have equal values

--TODO BE REMOVED
_simplify_max_average_vertex_length int default 0, -- in meter both for utm and deegrees, this used to avoid running ST_simplifyPreserveTopology for long lines lines with few points
_chaikins_nIterations int default 0, -- IF 0 NO CHAKINS WILL BE DONE,  A big value here make no sense because the number of points will increaes exponential )
_chaikins_max_length int default 0, --edge that are longer than this value will not be touched by _chaikins_min_degrees and _chaikins_max_degrees
_chaikins_min_degrees int default 0, -- The angle has to be less this given value, This is used to avoid to touch all angles.
_chaikins_max_degrees int default 0, -- OR the angle has to be greather than this given value, This is used to avoid to touch all angles
_chaikins_min_steep_angle_degrees int default 0, -- The angle has to be less this given value, This is used to avoid to touch all angles.
_chaikins_max_steep_angle_degrees int default 0,-- OR The angle has to be greather than this given value, This is used to avoid to touch all angles
--TODO BE REMOVED


-- To limit and set as master area. This must be in the same coordsys as _new_sf_table_srid and must be polygon or a multipolgyon,
-- This will added as the first geometry into postgis topology. Data outside the master area will not be used.
-- If you send in a null geometry here this parameter will not have any effect.
_input_boundary geometry default NULL,

-- ST_IsEmpty(ST_Buffer(face_geom,-min_st_is_empty_buffer))
-- This is an approximate value min_st_is_empty_buffer in meter used remove sliver polygon,
-- The formula used to convert from meter used for testing now
-- SELECT ST_Length(ST_Boundary(ST_Envelope(((ST_Buffer('POINT (10.061995275172155 59.716094977883)'::geography,1,'quad_segs=1'))::geometry))))/9;
_min_st_is_empty_buffer_value float default NULL,

-- This contains info about how clean edges from the input layers
_edge_input_tables_cleanup rog_clean_edge_type[] DEFAULT NULL


)
RETURNS resolve_overlap_data_clean_type
  AS $$
DECLARE
  ct resolve_overlap_data_clean_type;
BEGIN
  ct = (
    _min_area_to_keep,
    _split_input_lines_at_vertex_n, -- split all lines at given vertex num
    _break_up_big_polygons,
    _resolve_based_on_attribute,
    _simplify_max_average_vertex_length,
    _chaikins_nIterations,
    _chaikins_max_length,
    _chaikins_min_degrees,
    _chaikins_max_degrees,
    _chaikins_min_steep_angle_degrees,
    _chaikins_max_steep_angle_degrees,
    _input_boundary,
    _min_st_is_empty_buffer_value,
    _edge_input_tables_cleanup
    );

  return ct;
END;
$$
LANGUAGE plpgsql;




CREATE TYPE resolve_overlap_data_debug_options_type AS (
contiune_after_stat_exception boolean, -- if set to false, it will do topology.ValidateTopology and stop to if the this call returns any rows
validate_topoplogy_for_each_run boolean, -- if set to true, it will do topology.ValidateTopology at each loop return if it's error
run_add_border_line_as_single_thread boolean, -- if set to false, it will in many cases generate topo errors beacuse of running in many parralell threads
pre_defined_metagrid_tables text[], --
start_at_job_type int, -- if set to more than 1 it will skip init procces and start at given job_type
start_at_loop_nr int, -- many of jobs are ran in loops beacuse because if get an exception or cell is not allowed handle because cell close to is also started to work , this cell will gandled in the next loop.
stop_at_job_type int, -- if set to more than 0 the job will stop  when this job type is reday to run and display a set sql to run
stop_at_loop_nr int -- if set to more than 0 the job will stop  when this job type is reday to run and display a set sql to run
);

CREATE OR REPLACE FUNCTION resolve_overlap_data_debug_options_func(
_contiune_after_stat_exception boolean default true, -- if set to false, it will do topology.ValidateTopology and stop to if the this call returns any rows
_validate_topoplogy_for_each_run boolean default false, -- if set to true, it will do topology.ValidateTopology at each loop return if it's error
_run_add_border_line_as_single_thread boolean default true, --  if set to false, it will in many cases generate topo errors beacuse of running in many parralell threads
_pre_defined_metagrid_tables text[] DEFAULT NULL, --
_start_at_job_type int default 1, -- if set to more than 1 it will skip init procces and start at given job_type
_start_at_loop_nr int default 1, -- many of jobs are ran in loops beacuse because if get an exception or cell is not allowed handle because cell close to is also started to work , this cell will gandled in the next loop.
_stop_at_job_type int default 0, -- if set to more than 0 the job will stop  when this job type is reday to run and display a set sql to run
_stop_at_loop_nr int default 0 -- if set to more than 0 the job will stop  when this job type is reday to run and display a set sql to run
)
RETURNS resolve_overlap_data_debug_options_type
  AS $$
DECLARE
  ct resolve_overlap_data_debug_options_type;
BEGIN
  ct = (
    _contiune_after_stat_exception,
    _validate_topoplogy_for_each_run,
    _run_add_border_line_as_single_thread,
    _pre_defined_metagrid_tables,
    _start_at_job_type,
    _start_at_loop_nr,
    _stop_at_job_type,
    _stop_at_loop_nr
    );

  return ct;
END;
$$
LANGUAGE plpgsql;

/**

This is a type so we can easy check and verify that input is parsed correctly

*/

CREATE TYPE rog_overlay_source_table_metainfo_type AS (
  table_input_order int, -- A counter used in ref and other places
    table_name text, -- The table to be used as input including schema
    table_pk_colum text, -- Primary column name
    table_pk_type text, -- Primary column type
    table_selected_alias text, -- A user selected alias
    table_geo_column text, -- The name of the geometry column to be used
    table_geo_type text, -- The geomnetry type
    table_geo_srid int, -- The geometry srid
    table_geo_is_utm boolean -- If false then we use function like  ST_Length(geom,true);
);



/**

This is a type that returns

*/

CREATE TYPE rog_overlay_add_border_failed_line AS (
  error_line geometry,
  v_state text,
  v_msg text,
  v_detail text,
  v_hint text,
  v_context text
);

CREATE TYPE add_border_lines_faces_to_remove AS (

  edge_ids int[], -- the list of edges around the face
  face_area float,
  face_id int,
  table_input_order int,
  edge_geoms geometry
);

/**

This is as type used function add_border_lines which holds edges to be removed

*/

CREATE TYPE rog_overlay_add_border_lines_type AS (

  ok_input_line_edges integer[],
  failed_line rog_overlay_add_border_failed_line,
  face_edges_to_remove add_border_lines_faces_to_remove[],
  table_input_order integer,
  src_table_pk_column_value text,
  input_line geometry,
  selected_edge_face_id int
);

/**

Computes boundary geoms, used compute cell splitting lines

*/

CREATE TYPE rog_bb_boundary_geoms AS (

  -- the original input bbox
  input_bb geometry,

  -- This a area where do not touch or move any points
  boundary_geom geometry,

  -- this is the inner part of boundary_geom
  inner_boundary_geom geometry,

  -- this is the outer part of boundary_geom
  outer_boundary_geom geometry

);


-- a line and input order from simple feature dataset and other sources
CREATE TYPE rog_input_line_order AS (
  geo geometry,
  input_order  int,
  -- TODU support an array of primary key values
  src_table_pk_column_value text
);

-- a line and input order from simple feature dataset and other sources
CREATE TYPE rog_input_line_cell_crossing_order AS (
  id int,
  geo geometry,
  input_order  int
);

/**

This is types's that hold the output result from calling topo_update.get_simplified_split_border_lines

*/
CREATE TYPE rog_get_simplified_split_border_lines_result AS (
  -- A set of valid lines to be added to Postgis Toplogy for this cell
  lines_to_add rog_input_line_order[],

  -- A set of in valid lines to be added to Postgis Toplogy for this cell
  invalid_lines_to_add rog_input_line_order[],

  -- Lines that intersects with ay of this point can not be changed before all lines are added
  fixed_point_set geometry
);


/**

This is types's that hold an internal structure used _find_slice_cutter_lines*

*/

CREATE TYPE rog_find_slice_cutter_lines_internal_type AS (
  -- A set of lines  that is need to remove or tiny slices remove spikes
  spike_lines rog_input_line_order[],

  -- When add new line there us no nened to check the results tha cuts the results
  inputline_left geometry

);




