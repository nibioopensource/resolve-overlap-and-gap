SET search_path TO "$user", public, topology, topo_rog_static;

DROP SCHEMA IF EXISTS test_data CASCADE;
DROP SCHEMA IF EXISTS test_ar5_web CASCADE;

\i ./src/test/sql/regress/overlap_gap_input_t1.sql
\i ./src/test/sql/regress/overlap_gap_input_t2.sql

-- Create data test case degrees
CREATE table test_data.overlap_gap_input_t2 AS (SELECT * from test_data.overlap_gap_input_t1 WHERE c1 in (633,1233,1231,834,1130));
ALTER TABLE test_data.overlap_gap_input_t2 ADD PRIMARY KEY (c1);
CREATE INDEX ON test_data.overlap_gap_input_t2 USING GIST (geom);

-- Create test data for ar
CREATE table test_ar5_web.flate_t1 AS SELECT f.* from test_ar5_web.flate f
WHERE ST_Intersects('0103000020A21000000100000005000000BAF34A7E4DD6174082EF7C7F078A4D40BAF34A7E4DD617406217819A2D8A4D40B443FDC569D717406217819A2D8A4D40B443FDC569D7174082EF7C7F078A4D40BAF34A7E4DD6174082EF7C7F078A4D40',
f.geo);
ALTER TABLE test_ar5_web.flate_t1 ADD PRIMARY KEY (qms_id_flate);
CREATE INDEX ON test_ar5_web.flate_t1 USING GIST (geo);

CALL topo_rog_static.rog_overlay(
ARRAY[
'test_ar5_web.flate_t1',
'test_data.overlap_gap_input_t2'
],
'test_topo_ar5_test3',
4258,
10, -- _max_parallel_jobs int, -- this is the max number of paralell jobs to run. There must be at least the same number of free connections
200, -- _max_rows_in_each_cell int, -- this is the max number rows that intersects with box before it's split into 4 new boxes
null
);


SELECT 'ValidateTopology', * FROM topology.ValidateTopology('test_topo_ar5_test3_topo');

SELECT 'TopologySummary', * FROM topology.TopologySummary('test_topo_ar5_test3_topo');

\d test_topo_ar5_test3_topo.multi_input_2;

\d test_topo_ar5_test3_topo.multi_input_2_v;



