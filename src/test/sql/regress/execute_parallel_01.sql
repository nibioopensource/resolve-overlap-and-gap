-- To avoid error since binary geo seems to vary from local systen and Travis
SET client_min_messages TO ERROR;

set execute_parallel.connstring = 'host=localhost dbname=nibio_reg user=postgres password=testp101';

-- Test calling with out wait
CALL do_execute_parallel('{"SELECT  pg_sleep(10)","SELECT  pg_sleep(1)","SELECT  pg_sleep(1)","SELECT  pg_sleep(1)","SELECT  pg_sleep(1)","SELECT  pg_sleep(1)","SELECT  pg_sleep(1)","SELECT  pg_sleep(1)","SELECT  pg_sleep(1)","SELECT  pg_sleep(1)","SELECT  pg_sleep(1)"}'::text[],3,true);

-- Test create table
CALL do_execute_parallel('{"create table test(c1 int)"}'::text[],3);

-- Test insert rows in table (one invalid sql commamd),  with out wait
/**
CALL do_execute_parallel('{"insert  into testfeil) values (2)","insert  into test(c1) values (1)","insert  into test(c1) values (1)","insert  into test(c1) values (1)","insert  into test(c1) values (1)","insert  into test(c1) values (1)","insert  into test(c1) values (1)"}'::text[],3);
SELECT  5, count(*) from test where c1=1;
SELECT  6, count(*) from test where c1=2;
*/

-- Test insert rows in table, with out wait
CALL do_execute_parallel('{"insert  into test(c1) values (1)","insert  into test(c1) values (1)","insert  into test(c1) values (1)","insert  into test(c1) values (1)","insert  into test(c1) values (1)","insert  into test(c1) values (1)"}'::text[],10);
SELECT  8, count(*) from test where c1=1;
SELECT  9, count(*) from test where c1=2;


-- Test insert rows in table, with  wait by using start_execute_parallel
DO $$
<<test_parallel>>
declare

  -- start used by start_execute_parallel
  connstr text; -- this will be buildt up inside start_execute_parallel at first call
  conntions_array text[]; -- this will be buildt up inside start_execute_parallel at first call
  conn_stmts text[]; -- this will be buildt up inside start_execute_parallel at first call
  all_stmts_done boolean = false; -- this will be buildt up inside start_execute_parallel at first call
  start_execute_parallel_counter int = 0;
  seconds_to_wait_before_check_result int = 300;
  -- done used by start_execute_parallel

  stmts text[];
  used_time float;
  start_time_delta_job timestamp WITH time zone;
  max_parallel_jobs int;
  contiune_after_stat_exception boolean;
  start_time timestamp WITH time zone;
  loop_number int = 1;


begin

  stmts = '{"insert  into test(c1) values (3)","insert  into test(c1) values (3)","insert  into test(c1) values (3)"}'::text[];
  max_parallel_jobs = 2;
  contiune_after_stat_exception = true;

  all_stmts_done = false;
  start_execute_parallel_counter = 0;
  connstr = NULL;
  conntions_array = NULL;
  conn_stmts = NULL;
  seconds_to_wait_before_check_result = 1;
  start_time := Clock_timestamp();

  WHILE all_stmts_done = FALSE LOOP
    CALL start_execute_parallel(
      stmts,
      connstr,
      conntions_array,
      conn_stmts,
      all_stmts_done,
      seconds_to_wait_before_check_result,
      max_parallel_jobs,
      true,
      contiune_after_stat_exception
    );
    start_execute_parallel_counter = start_execute_parallel_counter + 1;

    used_time := (Extract(EPOCH FROM (Clock_timestamp() - start_time_delta_job)));
    IF used_time > seconds_to_wait_before_check_result THEN
      start_time_delta_job = Clock_timestamp();
	    RAISE NOTICE 'Status job length % jobs running in % threads at loop_number % at % in % secs',
	    Array_length(stmts, 1), coalesce(Array_length(conntions_array, 1),0), loop_number, Clock_timestamp(), (Extract(EPOCH FROM (Clock_timestamp() - start_time)));
    END IF;

    RAISE NOTICE 'Status job length % jobs running in % threads at loop_number % at % in % secs',
    Array_length(stmts, 1), coalesce(Array_length(conntions_array, 1),0), loop_number, Clock_timestamp(), (Extract(EPOCH FROM (Clock_timestamp() - start_time)));

    IF all_stmts_done = FALSE THEN
      perform pg_sleep(seconds_to_wait_before_check_result/10);
      COMMIT;
    END IF;


  END LOOP;


end test_parallel $$ ;

SELECT  11, count(*) from test where c1=3;


-- Create a user user01_test_execute_parallel and grant access to table test
DO $$
BEGIN
CREATE ROLE user01_test_execute_parallel;
EXCEPTION WHEN duplicate_object THEN RAISE NOTICE '%, skipping', SQLERRM USING ERRCODE = SQLSTATE;
END
$$;
ALTER ROLE user01_test_execute_parallel WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION NOBYPASSRLS PASSWORD 'testp101';
GRANT ALL ON TABLE test TO user01_test_execute_parallel;

-- Set execute_parallel.connstring so user user01_test_execute_parallel can connect by using dblink 3 rows should be added
set execute_parallel.connstring = 'host=localhost dbname=nibio_reg user=user01_test_execute_parallel password=testp101';
CALL do_execute_parallel('{"insert into test(c1) values (4)","insert into test(c1) values (4)","insert into test(c1) values (4)"}'::text[]);
SELECT  12, count(*) from test where c1=4;

-- Set execute_parallel.connstring to a non exting user no rows should be added
set execute_parallel.connstring = 'host=localhost dbname=nibio_reg user=user01_not_exist password=testp101';
DO $$
BEGIN
CALL do_execute_parallel('{"insert into test(c1) values (5)","insert into test(c1) values (5)","insert into test(c1) values (5)","insert into test(c1) values (5)"}'::text[]);
EXCEPTION WHEN OTHERS THEN RAISE NOTICE '%, skipping', SQLERRM USING ERRCODE = SQLSTATE;
END
$$;
SELECT  13, count(*) from test where c1=5;

SET execute_parallel.connstring = '';

SET execute_parallel.password = 'testp101';
DO $$
BEGIN
CALL do_execute_parallel('{"insert into test(c1) values (5)","insert into test(c1) values (5)","insert into test(c1) values (5)","insert into test(c1) values (5)"}'::text[]);
EXCEPTION WHEN OTHERS THEN RAISE NOTICE '%, skipping', SQLERRM USING ERRCODE = SQLSTATE;
END
$$;
SELECT  14, count(*) from test where c1=5;

--drop table test;






