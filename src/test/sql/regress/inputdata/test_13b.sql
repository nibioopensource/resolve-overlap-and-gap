--
-- PostgreSQL database dump
--

-- Dumped from database version 14.12 (Ubuntu 14.12-0ubuntu0.22.04.1)
-- Dumped by pg_dump version 16.3 (Homebrew)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: test_13; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA test_13;


ALTER SCHEMA test_13 OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: bygg_arsvers_23_v2; Type: TABLE; Schema: test_13; Owner: postgres
--

CREATE TABLE test_13.bygg_arsvers_23_v2 (
    geom public.geometry(Polygon,25833),
    id integer NOT NULL
);


ALTER TABLE test_13.bygg_arsvers_23_v2 OWNER TO postgres;

--
-- Name: bygg_arsvers_23_v2_id_seq; Type: SEQUENCE; Schema: test_13; Owner: postgres
--

CREATE SEQUENCE test_13.bygg_arsvers_23_v2_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE test_13.bygg_arsvers_23_v2_id_seq OWNER TO postgres;

--
-- Name: bygg_arsvers_23_v2_id_seq; Type: SEQUENCE OWNED BY; Schema: test_13; Owner: postgres
--

ALTER SEQUENCE test_13.bygg_arsvers_23_v2_id_seq OWNED BY test_13.bygg_arsvers_23_v2.id;


--
-- Name: gsk_2023_segmenter_renska; Type: TABLE; Schema: test_13; Owner: postgres
--

CREATE TABLE test_13.gsk_2023_segmenter_renska (
    geo public.geometry(Polygon,25833),
    clname character varying(254),
    id integer NOT NULL
);


ALTER TABLE test_13.gsk_2023_segmenter_renska OWNER TO postgres;

--
-- Name: gsk_2023_segmenter_renska_id_seq; Type: SEQUENCE; Schema: test_13; Owner: postgres
--

CREATE SEQUENCE test_13.gsk_2023_segmenter_renska_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE test_13.gsk_2023_segmenter_renska_id_seq OWNER TO postgres;

--
-- Name: gsk_2023_segmenter_renska_id_seq; Type: SEQUENCE OWNED BY; Schema: test_13; Owner: postgres
--

ALTER SEQUENCE test_13.gsk_2023_segmenter_renska_id_seq OWNED BY test_13.gsk_2023_segmenter_renska.id;


--
-- Name: multi_input_2_grid; Type: TABLE; Schema: test_13; Owner: postgres
--

CREATE TABLE test_13.multi_input_2_grid (
    id integer NOT NULL,
    cell_bbox public.geometry(Geometry,4258),
    validate_topology_ok boolean,
    validate_topology_exception text,
    simple_feat_num integer,
    simple_feat_exception text,
    inside_cell boolean,
    grid_thread_cell integer,
    num_polygons integer,
    row_number integer
);


ALTER TABLE test_13.multi_input_2_grid OWNER TO postgres;

--
-- Name: multi_input_2_grid_metagrid_0001; Type: TABLE; Schema: test_13; Owner: postgres
--

CREATE TABLE test_13.multi_input_2_grid_metagrid_0001 (
    id integer NOT NULL,
    cell_bbox public.geometry(Geometry,4258)
);


ALTER TABLE test_13.multi_input_2_grid_metagrid_0001 OWNER TO postgres;

--
-- Name: multi_input_2_grid_metagrid_0001_lines; Type: TABLE; Schema: test_13; Owner: postgres
--

CREATE TABLE test_13.multi_input_2_grid_metagrid_0001_lines (
    id integer NOT NULL,
    cell_bbox public.geometry(Geometry,4258)
);


ALTER TABLE test_13.multi_input_2_grid_metagrid_0001_lines OWNER TO postgres;

--
-- Name: multi_input_2_grid_metagrid_0002; Type: TABLE; Schema: test_13; Owner: postgres
--

CREATE TABLE test_13.multi_input_2_grid_metagrid_0002 (
    id integer NOT NULL,
    cell_bbox public.geometry(Geometry,4258)
);


ALTER TABLE test_13.multi_input_2_grid_metagrid_0002 OWNER TO postgres;

--
-- Name: multi_input_2_grid_metagrid_0002_lines; Type: TABLE; Schema: test_13; Owner: postgres
--

CREATE TABLE test_13.multi_input_2_grid_metagrid_0002_lines (
    id integer NOT NULL,
    cell_bbox public.geometry(Geometry,4258)
);


ALTER TABLE test_13.multi_input_2_grid_metagrid_0002_lines OWNER TO postgres;

--
-- Name: multi_input_2_grid_metagrid_0003; Type: TABLE; Schema: test_13; Owner: postgres
--

CREATE TABLE test_13.multi_input_2_grid_metagrid_0003 (
    id integer NOT NULL,
    cell_bbox public.geometry(Geometry,4258)
);


ALTER TABLE test_13.multi_input_2_grid_metagrid_0003 OWNER TO postgres;

--
-- Name: multi_input_2_grid_metagrid_0003_lines; Type: TABLE; Schema: test_13; Owner: postgres
--

CREATE TABLE test_13.multi_input_2_grid_metagrid_0003_lines (
    id integer NOT NULL,
    cell_bbox public.geometry(Geometry,4258)
);


ALTER TABLE test_13.multi_input_2_grid_metagrid_0003_lines OWNER TO postgres;

--
-- Name: multi_input_2_grid_metagrid_0004; Type: TABLE; Schema: test_13; Owner: postgres
--

CREATE TABLE test_13.multi_input_2_grid_metagrid_0004 (
    id integer NOT NULL,
    cell_bbox public.geometry(Geometry,4258)
);


ALTER TABLE test_13.multi_input_2_grid_metagrid_0004 OWNER TO postgres;

--
-- Name: multi_input_2_grid_metagrid_0004_lines; Type: TABLE; Schema: test_13; Owner: postgres
--

CREATE TABLE test_13.multi_input_2_grid_metagrid_0004_lines (
    id integer NOT NULL,
    cell_bbox public.geometry(Geometry,4258)
);


ALTER TABLE test_13.multi_input_2_grid_metagrid_0004_lines OWNER TO postgres;

--
-- Name: multi_input_2_grid_metagrid_0005; Type: TABLE; Schema: test_13; Owner: postgres
--

CREATE TABLE test_13.multi_input_2_grid_metagrid_0005 (
    id integer NOT NULL,
    cell_bbox public.geometry(Geometry,4258)
);


ALTER TABLE test_13.multi_input_2_grid_metagrid_0005 OWNER TO postgres;

--
-- Name: multi_input_2_grid_metagrid_0005_lines; Type: TABLE; Schema: test_13; Owner: postgres
--

CREATE TABLE test_13.multi_input_2_grid_metagrid_0005_lines (
    id integer NOT NULL,
    cell_bbox public.geometry(Geometry,4258)
);


ALTER TABLE test_13.multi_input_2_grid_metagrid_0005_lines OWNER TO postgres;

--
-- Name: bygg_arsvers_23_v2 id; Type: DEFAULT; Schema: test_13; Owner: postgres
--

ALTER TABLE ONLY test_13.bygg_arsvers_23_v2 ALTER COLUMN id SET DEFAULT nextval('test_13.bygg_arsvers_23_v2_id_seq'::regclass);


--
-- Name: gsk_2023_segmenter_renska id; Type: DEFAULT; Schema: test_13; Owner: postgres
--

ALTER TABLE ONLY test_13.gsk_2023_segmenter_renska ALTER COLUMN id SET DEFAULT nextval('test_13.gsk_2023_segmenter_renska_id_seq'::regclass);


--
-- Data for Name: bygg_arsvers_23_v2; Type: TABLE DATA; Schema: test_13; Owner: postgres
--

COPY test_13.bygg_arsvers_23_v2 (geom, id) FROM stdin;
\.


--
-- Data for Name: gsk_2023_segmenter_renska; Type: TABLE DATA; Schema: test_13; Owner: postgres
--

COPY test_13.gsk_2023_segmenter_renska (geo, clname, id) FROM stdin;
0103000020E96400000100000009000000000000000093DFC00000004054B55941000000000093DFC00000000054B5594100000000C092DFC00000000054B5594100000000C092DFC00000008053B55941000000004093DFC00000008053B55941000000004093DFC0000000C053B55941000000008093DFC0000000C053B55941000000008093DFC00000004054B55941000000000093DFC00000004054B55941	treeLayer	1
0103000020E9640000010000000B00000000000000C094DFC00000000052B5594100000000C094DFC0000000C051B55941000000008094DFC0000000C051B55941000000008094DFC00000008051B5594100000000C094DFC00000008051B5594100000000C094DFC00000004051B55941000000004095DFC00000004051B55941000000004095DFC00000008051B55941000000000095DFC00000008051B55941000000000095DFC00000000052B5594100000000C094DFC00000000052B55941	trees	2
0103000020E9640000010000000A00000000000000C095DFC00000004050B5594100000000C095DFC0000000C050B55941000000000095DFC0000000C050B55941000000000095DFC00000004050B55941000000004095DFC00000004050B55941000000004095DFC00000000050B55941000000000095DFC00000000050B55941000000000095DFC0000000C04FB5594100000000C095DFC0000000C04FB5594100000000C095DFC00000004050B55941	trees	3
0103000020E9640000010000000B00000000000000C094DFC00000000052B5594100000000C094DFC00000004052B55941000000008094DFC00000004052B55941000000008094DFC00000008052B55941000000004094DFC00000008052B55941000000004094DFC0000000C052B55941000000000094DFC0000000C052B55941000000000094DFC0000000C051B55941000000004094DFC0000000C051B55941000000004094DFC00000000052B5594100000000C094DFC00000000052B55941	trees	4
\.


--
-- Data for Name: multi_input_2_grid; Type: TABLE DATA; Schema: test_13; Owner: postgres
--

COPY test_13.multi_input_2_grid (id, cell_bbox, validate_topology_ok, validate_topology_exception, simple_feat_num, simple_feat_exception, inside_cell, grid_thread_cell, num_polygons, row_number) FROM stdin;
4905	0103000020A2100000010000000500000080CEBDCEAB321540E8B4CFDB17374E4080CEBDCEAB3215406A7EAEECE8374E4043F432B5EE4215406A7EAEECE8374E4043F432B5EE421540E8B4CFDB17374E4080CEBDCEAB321540E8B4CFDB17374E40	\N	\N	\N	\N	t	24	0	91
\.


--
-- Data for Name: multi_input_2_grid_metagrid_0001; Type: TABLE DATA; Schema: test_13; Owner: postgres
--

COPY test_13.multi_input_2_grid_metagrid_0001 (id, cell_bbox) FROM stdin;
24	0103000020A21000000100000005000000B411744E5DE11440CAAFA0EFA82B4E40B411744E5DE11440EC478DFDB9384E40DB6DC6B58BE51540EC478DFDB9384E40DB6DC6B58BE51540CAAFA0EFA82B4E40B411744E5DE11440CAAFA0EFA82B4E40
\.


--
-- Data for Name: multi_input_2_grid_metagrid_0001_lines; Type: TABLE DATA; Schema: test_13; Owner: postgres
--

COPY test_13.multi_input_2_grid_metagrid_0001_lines (id, cell_bbox) FROM stdin;
\.


--
-- Data for Name: multi_input_2_grid_metagrid_0002; Type: TABLE DATA; Schema: test_13; Owner: postgres
--

COPY test_13.multi_input_2_grid_metagrid_0002 (id, cell_bbox) FROM stdin;
22	0103000020A21000000100000005000000B411744E5DE11440A817B4E1971E4E40B411744E5DE11440EC478DFDB9384E4002CA181DBAE91640EC478DFDB9384E4002CA181DBAE91640A817B4E1971E4E40B411744E5DE11440A817B4E1971E4E40
\.


--
-- Data for Name: multi_input_2_grid_metagrid_0002_lines; Type: TABLE DATA; Schema: test_13; Owner: postgres
--

COPY test_13.multi_input_2_grid_metagrid_0002_lines (id, cell_bbox) FROM stdin;
\.


--
-- Data for Name: multi_input_2_grid_metagrid_0003; Type: TABLE DATA; Schema: test_13; Owner: postgres
--

COPY test_13.multi_input_2_grid_metagrid_0003 (id, cell_bbox) FROM stdin;
10	0103000020A21000000100000005000000B411744E5DE11440A817B4E1971E4E40B411744E5DE114402F786619DC524E405082BDEB16F218402F786619DC524E405082BDEB16F21840A817B4E1971E4E40B411744E5DE11440A817B4E1971E4E40
\.


--
-- Data for Name: multi_input_2_grid_metagrid_0003_lines; Type: TABLE DATA; Schema: test_13; Owner: postgres
--

COPY test_13.multi_input_2_grid_metagrid_0003_lines (id, cell_bbox) FROM stdin;
\.


--
-- Data for Name: multi_input_2_grid_metagrid_0004; Type: TABLE DATA; Schema: test_13; Owner: postgres
--

COPY test_13.multi_input_2_grid_metagrid_0004 (id, cell_bbox) FROM stdin;
4	0103000020A21000000100000005000000B411744E5DE11440A817B4E1971E4E40B411744E5DE11440B6D8185120874E40EBF20689D0021D40B6D8185120874E40EBF20689D0021D40A817B4E1971E4E40B411744E5DE11440A817B4E1971E4E40
\.


--
-- Data for Name: multi_input_2_grid_metagrid_0004_lines; Type: TABLE DATA; Schema: test_13; Owner: postgres
--

COPY test_13.multi_input_2_grid_metagrid_0004_lines (id, cell_bbox) FROM stdin;
\.


--
-- Data for Name: multi_input_2_grid_metagrid_0005; Type: TABLE DATA; Schema: test_13; Owner: postgres
--

COPY test_13.multi_input_2_grid_metagrid_0005 (id, cell_bbox) FROM stdin;
1	0103000020A21000000100000005000000FC60C227D47F09409A564F720FB64D40FC60C227D47F0940B6D8185120874E40EBF20689D0021D40B6D8185120874E40EBF20689D0021D409A564F720FB64D40FC60C227D47F09409A564F720FB64D40
\.


--
-- Data for Name: multi_input_2_grid_metagrid_0005_lines; Type: TABLE DATA; Schema: test_13; Owner: postgres
--

COPY test_13.multi_input_2_grid_metagrid_0005_lines (id, cell_bbox) FROM stdin;
\.


--
-- Name: bygg_arsvers_23_v2_id_seq; Type: SEQUENCE SET; Schema: test_13; Owner: postgres
--

SELECT pg_catalog.setval('test_13.bygg_arsvers_23_v2_id_seq', 1, false);


--
-- Name: gsk_2023_segmenter_renska_id_seq; Type: SEQUENCE SET; Schema: test_13; Owner: postgres
--

SELECT pg_catalog.setval('test_13.gsk_2023_segmenter_renska_id_seq', 4, true);


--
-- Name: bygg_arsvers_23_v2 bygg_arsvers_23_v2_pkey; Type: CONSTRAINT; Schema: test_13; Owner: postgres
--

ALTER TABLE ONLY test_13.bygg_arsvers_23_v2
    ADD CONSTRAINT bygg_arsvers_23_v2_pkey PRIMARY KEY (id);


--
-- Name: gsk_2023_segmenter_renska gsk_2023_segmenter_renska_pkey; Type: CONSTRAINT; Schema: test_13; Owner: postgres
--

ALTER TABLE ONLY test_13.gsk_2023_segmenter_renska
    ADD CONSTRAINT gsk_2023_segmenter_renska_pkey PRIMARY KEY (id);


--
-- Name: multi_input_2_grid_metagrid_0001_lines multi_input_2_grid_metagrid_0001_lines_pkey; Type: CONSTRAINT; Schema: test_13; Owner: postgres
--

ALTER TABLE ONLY test_13.multi_input_2_grid_metagrid_0001_lines
    ADD CONSTRAINT multi_input_2_grid_metagrid_0001_lines_pkey PRIMARY KEY (id);


--
-- Name: multi_input_2_grid_metagrid_0001 multi_input_2_grid_metagrid_0001_pkey; Type: CONSTRAINT; Schema: test_13; Owner: postgres
--

ALTER TABLE ONLY test_13.multi_input_2_grid_metagrid_0001
    ADD CONSTRAINT multi_input_2_grid_metagrid_0001_pkey PRIMARY KEY (id);


--
-- Name: multi_input_2_grid_metagrid_0002_lines multi_input_2_grid_metagrid_0002_lines_pkey; Type: CONSTRAINT; Schema: test_13; Owner: postgres
--

ALTER TABLE ONLY test_13.multi_input_2_grid_metagrid_0002_lines
    ADD CONSTRAINT multi_input_2_grid_metagrid_0002_lines_pkey PRIMARY KEY (id);


--
-- Name: multi_input_2_grid_metagrid_0002 multi_input_2_grid_metagrid_0002_pkey; Type: CONSTRAINT; Schema: test_13; Owner: postgres
--

ALTER TABLE ONLY test_13.multi_input_2_grid_metagrid_0002
    ADD CONSTRAINT multi_input_2_grid_metagrid_0002_pkey PRIMARY KEY (id);


--
-- Name: multi_input_2_grid_metagrid_0003_lines multi_input_2_grid_metagrid_0003_lines_pkey; Type: CONSTRAINT; Schema: test_13; Owner: postgres
--

ALTER TABLE ONLY test_13.multi_input_2_grid_metagrid_0003_lines
    ADD CONSTRAINT multi_input_2_grid_metagrid_0003_lines_pkey PRIMARY KEY (id);


--
-- Name: multi_input_2_grid_metagrid_0003 multi_input_2_grid_metagrid_0003_pkey; Type: CONSTRAINT; Schema: test_13; Owner: postgres
--

ALTER TABLE ONLY test_13.multi_input_2_grid_metagrid_0003
    ADD CONSTRAINT multi_input_2_grid_metagrid_0003_pkey PRIMARY KEY (id);


--
-- Name: multi_input_2_grid_metagrid_0004_lines multi_input_2_grid_metagrid_0004_lines_pkey; Type: CONSTRAINT; Schema: test_13; Owner: postgres
--

ALTER TABLE ONLY test_13.multi_input_2_grid_metagrid_0004_lines
    ADD CONSTRAINT multi_input_2_grid_metagrid_0004_lines_pkey PRIMARY KEY (id);


--
-- Name: multi_input_2_grid_metagrid_0004 multi_input_2_grid_metagrid_0004_pkey; Type: CONSTRAINT; Schema: test_13; Owner: postgres
--

ALTER TABLE ONLY test_13.multi_input_2_grid_metagrid_0004
    ADD CONSTRAINT multi_input_2_grid_metagrid_0004_pkey PRIMARY KEY (id);


--
-- Name: multi_input_2_grid_metagrid_0005_lines multi_input_2_grid_metagrid_0005_lines_pkey; Type: CONSTRAINT; Schema: test_13; Owner: postgres
--

ALTER TABLE ONLY test_13.multi_input_2_grid_metagrid_0005_lines
    ADD CONSTRAINT multi_input_2_grid_metagrid_0005_lines_pkey PRIMARY KEY (id);


--
-- Name: multi_input_2_grid_metagrid_0005 multi_input_2_grid_metagrid_0005_pkey; Type: CONSTRAINT; Schema: test_13; Owner: postgres
--

ALTER TABLE ONLY test_13.multi_input_2_grid_metagrid_0005
    ADD CONSTRAINT multi_input_2_grid_metagrid_0005_pkey PRIMARY KEY (id);


--
-- Name: multi_input_2_grid multi_input_2_grid_pkey; Type: CONSTRAINT; Schema: test_13; Owner: postgres
--

ALTER TABLE ONLY test_13.multi_input_2_grid
    ADD CONSTRAINT multi_input_2_grid_pkey PRIMARY KEY (id);


--
-- Name: bygg_arsvers_23_v2_geom_idx; Type: INDEX; Schema: test_13; Owner: postgres
--

CREATE INDEX bygg_arsvers_23_v2_geom_idx ON test_13.bygg_arsvers_23_v2 USING gist (geom);


--
-- Name: gsk_2023_segmenter_renska_geo_idx; Type: INDEX; Schema: test_13; Owner: postgres
--

CREATE INDEX gsk_2023_segmenter_renska_geo_idx ON test_13.gsk_2023_segmenter_renska USING gist (geo);


--
-- Name: multi_input_2_grid_cell_bbox_idx; Type: INDEX; Schema: test_13; Owner: postgres
--

CREATE INDEX multi_input_2_grid_cell_bbox_idx ON test_13.multi_input_2_grid USING gist (cell_bbox);


--
-- Name: multi_input_2_grid_metagrid_0001_cell_bbox_idx; Type: INDEX; Schema: test_13; Owner: postgres
--

CREATE INDEX multi_input_2_grid_metagrid_0001_cell_bbox_idx ON test_13.multi_input_2_grid_metagrid_0001 USING gist (cell_bbox);


--
-- Name: multi_input_2_grid_metagrid_0001_lines_cell_bbox_idx; Type: INDEX; Schema: test_13; Owner: postgres
--

CREATE INDEX multi_input_2_grid_metagrid_0001_lines_cell_bbox_idx ON test_13.multi_input_2_grid_metagrid_0001_lines USING gist (cell_bbox);


--
-- Name: multi_input_2_grid_metagrid_0002_cell_bbox_idx; Type: INDEX; Schema: test_13; Owner: postgres
--

CREATE INDEX multi_input_2_grid_metagrid_0002_cell_bbox_idx ON test_13.multi_input_2_grid_metagrid_0002 USING gist (cell_bbox);


--
-- Name: multi_input_2_grid_metagrid_0002_lines_cell_bbox_idx; Type: INDEX; Schema: test_13; Owner: postgres
--

CREATE INDEX multi_input_2_grid_metagrid_0002_lines_cell_bbox_idx ON test_13.multi_input_2_grid_metagrid_0002_lines USING gist (cell_bbox);


--
-- Name: multi_input_2_grid_metagrid_0003_cell_bbox_idx; Type: INDEX; Schema: test_13; Owner: postgres
--

CREATE INDEX multi_input_2_grid_metagrid_0003_cell_bbox_idx ON test_13.multi_input_2_grid_metagrid_0003 USING gist (cell_bbox);


--
-- Name: multi_input_2_grid_metagrid_0003_lines_cell_bbox_idx; Type: INDEX; Schema: test_13; Owner: postgres
--

CREATE INDEX multi_input_2_grid_metagrid_0003_lines_cell_bbox_idx ON test_13.multi_input_2_grid_metagrid_0003_lines USING gist (cell_bbox);


--
-- Name: multi_input_2_grid_metagrid_0004_cell_bbox_idx; Type: INDEX; Schema: test_13; Owner: postgres
--

CREATE INDEX multi_input_2_grid_metagrid_0004_cell_bbox_idx ON test_13.multi_input_2_grid_metagrid_0004 USING gist (cell_bbox);


--
-- Name: multi_input_2_grid_metagrid_0004_lines_cell_bbox_idx; Type: INDEX; Schema: test_13; Owner: postgres
--

CREATE INDEX multi_input_2_grid_metagrid_0004_lines_cell_bbox_idx ON test_13.multi_input_2_grid_metagrid_0004_lines USING gist (cell_bbox);


--
-- Name: multi_input_2_grid_metagrid_0005_cell_bbox_idx; Type: INDEX; Schema: test_13; Owner: postgres
--

CREATE INDEX multi_input_2_grid_metagrid_0005_cell_bbox_idx ON test_13.multi_input_2_grid_metagrid_0005 USING gist (cell_bbox);


--
-- Name: multi_input_2_grid_metagrid_0005_lines_cell_bbox_idx; Type: INDEX; Schema: test_13; Owner: postgres
--

CREATE INDEX multi_input_2_grid_metagrid_0005_lines_cell_bbox_idx ON test_13.multi_input_2_grid_metagrid_0005_lines USING gist (cell_bbox);


--
-- PostgreSQL database dump complete
--

