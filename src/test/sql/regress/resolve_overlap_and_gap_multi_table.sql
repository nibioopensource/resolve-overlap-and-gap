-- To avoid error since binary geo seems to vary from local systen and Travis
SET client_min_messages TO ERROR;

-- Create data test case degrees
CREATE table test_data.overlap_gap_input_t2 AS (SELECT * from test_data.overlap_gap_input_t1 WHERE c1 in (633,1233,1231,834,1130));
ALTER TABLE test_data.overlap_gap_input_t2 ADD PRIMARY KEY (c1);
CREATE INDEX ON test_data.overlap_gap_input_t2 USING GIST (geom);


-- Create test data for ar
CREATE table test_ar5_web.flate_t1 AS SELECT f.* from test_ar5_web.flate f
WHERE ST_Intersects('0103000020A21000000100000005000000BAF34A7E4DD6174082EF7C7F078A4D40BAF34A7E4DD617406217819A2D8A4D40B443FDC569D717406217819A2D8A4D40B443FDC569D7174082EF7C7F078A4D40BAF34A7E4DD6174082EF7C7F078A4D40',
f.geo);
ALTER TABLE test_ar5_web.flate_t1 ADD PRIMARY KEY (qms_id_flate);
CREATE INDEX ON test_ar5_web.flate_t1 USING GIST (geo);

select 'each geo before geo test_ar5_web.flate_t1 ', round(ST_Area(geo,true)) FROM test_ar5_web.flate_t1 ORDER BY round(ST_Area(geo,true));
select 'Sum geo before geo test_ar5_web.flate_t1 ', round(sum(ST_Area(geo,true))) as area, count(*) as antall FROM test_ar5_web.flate_t1;

select 'count test_data.overlap_gap_input_t2 ', count(*) FROM test_data.overlap_gap_input_t2;


-- Call function to resolve overlap and gap in the function in test_data.overlap_gap_
CALL resolve_overlap_gap_run(
ARRAY[(null,null,null, -- The simple line feature info with attributtes
'test_ar5_web.flate_t1','qms_id_flate','geo' -- The simple polygons feature info with attributtes
,4258,false, -- info about srid and utm or not
null,null,null,null
)::resolve_overlap_data_input_type,
(null,null,null, -- The simple line feature info with attributtes
'test_data.overlap_gap_input_t2','c1','geom' -- The simple polygons feature info with attributtes
,4258,false, -- info about srid and utm or not
null,null,null,null
)::resolve_overlap_data_input_type],
('test_topo_ar5_test3',4258,false,0.0,true,null,null,
true, -- use_temp_topology
false, -- do_qualitycheck_on_final_reseult
1 --  Method 2 topology.TopoGeo_LoadGeometry
), -- TYPE resolve_overlap_data_topology
  resolve_overlap_data_clean_type_func(  -- TYPE resolve_overlap_data_clean
  0,  -- if this a polygon  is below this limit it will merge into a neighbour polygon. The area is sqare meter.
  NULL, -- split all lines at given vertex num,
  false, -- _break_up_big_polygons
  resolve_based_on_attribute_type_func(),
  null, -- _max_average_vertex_length, in meter both for utm and deegrees, this used to avoid running ST_simplifyPreserveTopology for long lines lines with few points
  0, -- IF 0 NO CHAKINS WILL BE DONE A big value here make no sense because the number of points will increaes exponential )
  10000, --edge that are longer than this value will not be touched by _chaikins_min_degrees and _chaikins_max_degrees
  120, -- The angle has to be less this given value, This is used to avoid to touch all angles.
  240, -- OR the angle has to be greather than this given value, This is used to avoid to touch all angles
  40, -- The angle has to be less this given value, This is used to avoid to touch all angles.
  320 -- OR The angle has to be greather than this given value, This is used to avoid to touch all angles
)
,5 -- this is the max number of paralell jobs to run. There must be at least the same number of free connections
,3 -- this is the max number rows that intersects with box before it's split into 4 new boxes, default is 5000
,resolve_overlap_data_debug_options_func(
  false --if set to false, it will do topology.ValidateTopology and stop to if the this call returns any rows
  )
);

SELECT 'degrees_check_added_faces areas', round(sum(ST_Area(mbr,true))), count(*) from test_topo_ar5_test3.face;

select 'each geo after geo test_ar5_web.flate_t1 ', round(ST_Area(geom,true)) , "1-pk-qms_id_flate-test_ar5_web.flate_t1"
FROM test_topo_ar5_test3.multi_input_2 where "1-pk-qms_id_flate-test_ar5_web.flate_t1" IS NOT NULL ORDER BY round(ST_Area(geom,true));

select 'Sum geo after geo test_ar5_web.flate_t1 ', round(sum(ST_Area(geom,true))) as area, count(*) as antall FROM
test_topo_ar5_test3.multi_input_2 where "1-pk-qms_id_flate-test_ar5_web.flate_t1" IS NOT NULL;

SELECT 'degrees_check_failed_lines failed', count(geo) from test_topo_ar5_test3.multi_input_2_no_cut_line_failed;

SELECT 'degrees_check_border_lines distinct', count(distinct ST_AsBinary(geom)) from test_topo_ar5_test3.multi_input_2_border_line_segments_final;

SELECT 'degrees_check_border_lines distinct', count(geom), round(Sum(ST_Length(geom,true))) from test_topo_ar5_test3.multi_input_2_border_line_segments_final;

--SELECT 'degrees_check_added_lines', count(geom) from test_topo_ar5_test3.edge;

SELECT 'degrees_check_added_faces count', count(mbr) from test_topo_ar5_test3.face;

SELECT 'degrees_check_added_face_attributes_all', count(*) from test_topo_ar5_test3.face_attributes;

SELECT 'degrees_check_added_edge_attributes_all', count(*) from test_topo_ar5_test3.edge_attributes;

SELECT 'degrees_check_added_simple_feature_geo', count(*) from test_topo_ar5_test3.multi_input_2 where geom is not null;

-- Get area from the original data for test_ar5_web.flate_t1
select 'degrees_check sum geo before test 1 input test_ar5_web.flate_t1 group by artype ', *
FROM
  (
    SELECT round(sum(ST_Area(geo,true))) as m2, count(*) as antall, artype
    FROM test_ar5_web.flate_t1
    group by artype
  ) r
ORDER BY artype,m2;

-- Get area from the data produced by Postgis Topology for test_ar5_web.flate_t1
CREATE TABLE test_topo_ar5_test3.test_ar5_web_flate_t1_from_rog AS (
SELECT * FROM
  (
  SELECT geom, t01_artype AS artype
  FROM test_topo_ar5_test3.multi_input_2_v
  WHERE t01_artype IS NOT NULL
  ) r
);
select 'degrees_check sum geo after input 2 test_ar5_web.flate_t1 group by artype ', *
FROM
  (
    SELECT round(sum(ST_Area(geom,true))) as m2, count(*) as antall, artype
    FROM test_topo_ar5_test3.test_ar5_web_flate_t1_from_rog
    group by artype
  ) r
ORDER BY artype,m2;


-- Get area from the data produced by Postgis Topology for overlap_gap_input_t2
CREATE TABLE test_topo_ar5_test3.overlap_gap_input_t2_from_rog AS (
SELECT * FROM
  (
  SELECT geom, t02_c2 AS c2
  FROM test_topo_ar5_test3.multi_input_2_v
  UNION ALL
  SELECT r.geom, ir.c2
  FROM test_topo_ar5_test3.multi_input_2 r,
  test_data.overlap_gap_input_t2 ir
  WHERE  ir.c1 = ANY(r."2-pk_list-c1-test_data.overlap_gap_input_t2")
  ) r
);
select 'degrees_check sum geo before input test_data.overlap_gap_input_t2 group by name ', *
FROM
  (
    SELECT round(sum(ST_Area(geom,true))) as m2, count(*) as antall, c2
    FROM test_data.overlap_gap_input_t2
    group by c2
  ) r
ORDER BY c2,m2;


SELECT 'validation', * FROM topology.ValidateTopology('test_topo_ar5_test3');

SELECT 'drop test_topo_ar5_test3', topology.droptopology('test_topo_ar5_test3');


-- Create data test case meter and new column names, new srid based test_data.overlap_gap_input_t2
CREATE table test_data.overlap_gap_input_t3 AS
(SELECT distinct c1 as c1t3, c2 as c2t3, c3, ST_transform(geom,25833)::Geometry(Polygon,25833) as geo from test_data.overlap_gap_input_t2);
ALTER TABLE test_data.overlap_gap_input_t3 ADD PRIMARY KEY (c1t3);


-- Call function to resolve overlap and gap in the function in test_data.overlap_gap_
CALL resolve_overlap_gap_run(
ARRAY[(null,null,null, -- The simple line feature info with attributtes
'test_ar5_web.flate_t1','qms_id_flate','geo' -- The simple polygons feature info with attributtes
,4258,false, -- info about srid and utm or not
null,null,null,null
)::resolve_overlap_data_input_type,
(null,null,null, -- The simple line feature info with attributtes
'test_data.overlap_gap_input_t3','c1t3','geo' -- The simple polygons feature info with attributtes
,25833,true, -- info about srid and utm or not
null,null,null,null
)::resolve_overlap_data_input_type],
('test_topo_ar5_test4',4258,false,0.0,true,null,null,
true, -- use_temp_topology
false, -- do_qualitycheck_on_final_reseult
1 --  Method 2 topology.TopoGeo_LoadGeometry
), -- TYPE resolve_overlap_data_topology
  resolve_overlap_data_clean_type_func(  -- TYPE resolve_overlap_data_clean
  0,  -- if this a polygon  is below this limit it will merge into a neighbour polygon. The area is sqare meter.
  NULL, -- split all lines at given vertex num
  false, -- _break_up_big_polygons
  resolve_based_on_attribute_type_func(),
  null, -- _max_average_vertex_length, in meter both for utm and deegrees, this used to avoid running ST_simplifyPreserveTopology for long lines lines with few points
  0, -- IF 0 NO CHAKINS WILL BE DONE A big value here make no sense because the number of points will increaes exponential )
  10000, --edge that are longer than this value will not be touched by _chaikins_min_degrees and _chaikins_max_degrees
  120, -- The angle has to be less this given value, This is used to avoid to touch all angles.
  240, -- OR the angle has to be greather than this given value, This is used to avoid to touch all angles
  40, -- The angle has to be less this given value, This is used to avoid to touch all angles.
  320 -- OR The angle has to be greather than this given value, This is used to avoid to touch all angles
)
,5 -- this is the max number of paralell jobs to run. There must be at least the same number of free connections
,3 -- this is the max number rows that intersects with box before it's split into 4 new boxes, default is 5000
,resolve_overlap_data_debug_options_func(
  false --if set to false, it will do topology.ValidateTopology and stop to if the this call returns any rows
  )
);

select 'Sum geo by artype test_topo_ar5_test4.multi_input_2_v ', *
FROM
  (
    SELECT round(sum(ST_Area(geom,true))/10) as m2_div_10, count(*) as antall, t01_artype, t02_c2t3
    FROM test_topo_ar5_test4.multi_input_2_v group by t01_artype, t02_c2t3
  ) r
ORDER BY t01_artype,t02_c2t3,m2_div_10;




SELECT 't01_gaps_test_ar5_web.flate_t1', count(*) from test_topo_ar5_test4.multi_input_2_v where "t01_gaps_test_ar5_web.flate_t1" ;
SELECT 't01_overlaps_test_ar5_web.flate_t1', count(*) from test_topo_ar5_test4.multi_input_2_v where "t01_overlaps_test_ar5_web.flate_t1" ;
SELECT 't02_gaps_test_data.overlap_gap_input_t3', count(*) from test_topo_ar5_test4.multi_input_2_v where "t02_gaps_test_data.overlap_gap_input_t3" ;
SELECT 't02_overlaps_test_data.overlap_gap_input_t3', count(*) from test_topo_ar5_test4.multi_input_2_v where "t02_overlaps_test_data.overlap_gap_input_t3" ;


SELECT
    indexname,
    indexdef,
    tablename
FROM
    pg_indexes
WHERE
    tablename = 'multi_input_2'
ORDER BY indexname;

SELECT 'validation', * FROM topology.ValidateTopology('test_topo_ar5_test4');

SELECT 'drop test_topo_ar5_test4', topology.droptopology('test_topo_ar5_test4');


drop schema test_data cascade;
drop schema test_ar5_web cascade;

