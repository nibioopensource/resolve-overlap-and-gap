set client_min_messages to WARNING;
set timezone to utc;


-- the issue is here
-- https://gitlab.com/nibioopensource/resolve-overlap-and-gap/-/issues/24

-- find interesting areas
-- SELECT f.* from org_rmpkart_v2.rmp_helling_ar5_foi_flate_table f, sl_lop.feil_1665039396019 feil  where ST_Intersects(overlay_geo,geo) AND ST_area(f.geo,true) < 1;
-- SELECT ST_Envelope(overlay_geo) FROM sl_lop.feil_1665039396019 feil  where overlay_id in  (3698239);

--CREATE TABLE d01_rmp_helling_ar5_foi_flate_table AS TABLE org_rmpkart_v2.rmp_helling_ar5_foi_flate_table WITH NO DATA;
--INSERT INTO d01_rmp_helling_ar5_foi_flate_table SELECT * FROM (SELECT f.* FROM org_rmpkart_v2.rmp_helling_ar5_foi_flate_table f WHERE ST_Intersects(f.geo, '0103000020A21000000100000005000000434283B47FD319408250203E34DE4E40434283B47FD31940B0B930C65EDE4E400CB14A5A80D51940B0B930C65EDE4E400CB14A5A80D519408250203E34DE4E40434283B47FD319408250203E34DE4E40')) AS r;
--pg_dump -h db09test.nibio.no -U postgres sl -t d01_rmp_helling_ar5_foi_flate_table > test/regress/rmp/inputdata/d01_rmp_helling_ar5_foi_flate_table.sql

--CREATE TABLE d01_ar5_gpi2_arealklasser_flate AS TABLE org_ar5august2022versjon.ar5_flate WITH NO DATA;
--INSERT INTO d01_ar5_gpi2_arealklasser_flate SELECT * FROM (SELECT f.* FROM org_ar5august2022versjon.ar5_flate f WHERE ST_Intersects(f.geo, '0103000020A21000000100000005000000434283B47FD319408250203E34DE4E40434283B47FD31940B0B930C65EDE4E400CB14A5A80D51940B0B930C65EDE4E400CB14A5A80D519408250203E34DE4E40434283B47FD319408250203E34DE4E40')) AS r;
--pg_dump -h db09test.nibio.no -U postgres sl -t d01_ar5_gpi2_arealklasser_flate > test/regress/rmp/inputdata/d01_ar5_gpi2_arealklasser_flate.sql

--CREATE TABLE d01_org_helling AS TABLE org_helling.helling_rmp_geo WITH NO DATA;
--INSERT INTO d01_org_helling SELECT * FROM (SELECT f.* FROM org_helling.helling_rmp_geo f WHERE ST_Intersects(f.geo, '0103000020A21000000100000005000000434283B47FD319408250203E34DE4E40434283B47FD31940B0B930C65EDE4E400CB14A5A80D51940B0B930C65EDE4E400CB14A5A80D519408250203E34DE4E40434283B47FD319408250203E34DE4E40')) AS r;
--pg_dump -h db09test.nibio.no -U postgres sl -t d01_org_helling > test/regress/rmp/inputdata/d01_org_helling.sql

-- SET datadir_input directory
SELECT :'regdir' || '/rmp/inputdata' as datadir_input \gset

-- Read in d01_rmp_helling_ar5_foi_flate_table.sql that is a subset of ar5_gpi2_arealklasser_flate
SELECT :'datadir_input'||'/d01_ar5_gpi2_arealklasser_flate.sql' as x \gset
\i :x
SELECT '--a1--', count(*) AS num_below_1_m2 FROM d01_ar5_gpi2_arealklasser_flate WHERE ST_Area(geo,true) < 1;

-- Read in d01_rmp_helling_ar5_foi_flate_table.sql thats is a that is a subset of org_helling
SELECT :'datadir_input'||'/d01_org_helling.sql' as x \gset
\i :x
SELECT '--a2--', count(*) AS num_below_1_m2 FROM d01_org_helling WHERE ST_Area(geo,true) < 1;

-- Read in d01_rmp_helling_ar5_foi_flate_table.sql this is intersection of
-- ar5_gpi2_arealklasser_flate and org_helling created in the original database
SELECT :'datadir_input'||'/d01_rmp_helling_ar5_foi_flate_table.sql' as x \gset
\i :x

-- We here have many tiny polygons that is causing noise and errors
SELECT '--a3--', count(*) AS num_below_1_m2 FROM d01_rmp_helling_ar5_foi_flate_table WHERE ST_Area(geo,true) < 1;

-- A full test for activity 1.1.82 that depend on layers above based on simple feature

-- 1) Set input get drawn by user
SELECT 'SRID=4258;POLYGON((6.455099311734943 61.73582332915254,6.455099311734943 61.73870990791557,6.458860426587608 61.73870990791557,6.458860426587608 61.73582332915254,6.455099311734943 61.73582332915254))' as input_geo \gset
SELECT '--b1-- area input', round(ST_Area(:'input_geo',true));

-- 2) Find valid area
-- To get valid area we check klippe_layer_type_border
-- SELECT id, klippe_layer_type_border from org_rmpkart_v2.rmp_tiltak WHERE miljotema_id = 1 AND aktivitetsomraade_id = 1 AND tiltaknavn_id = 82;
-- id | klippe_layer_type_border
-- 59 | {8}
-- which is 8 that and refers to org_helling where hellingGpiklasse/h_klasse = 1 in {1,2};

-- The plan is to also to calculate this up front
-- If we calculate this up front we also have decide how we tag this area.
-- As today we related this to a internal number 8, which not make any meaning for others than a couple persons in NIBIO,
-- but if mark this as 1.1.82 and use this to get description for this key then this valid area make meaning for all the users off the system.
-- In many cases this valid areas may be used many different activities with different codes so we also need a internal
-- key like "8" to say something about how this are is calculated, but thats only for internal usage.



-- This valid area is a just default
-- 1) May not be valid for all municipalities
-- 2) May be reduced if the user already has a activity in the area
-- 3) Some user may override and apply any place insdie the municipality .

-- There are different reasons for calculating this up front
-- 1) We get to test that we do get any Topology errors for area in Norway
-- 2) Performance since we have some properties that consist off more than 1000 polygons.
-- 3) We can easily make map that that can show where on your farms you can apply
-- If we do this up front we must able to handle changes in the under laying data

-- If we decide to this on demand using simple feature then we may wrap this into a function postgreSQL function called_valid_area_08
-- that takes geometry as input and and runs a function like the below.

SELECT '--b2-- test valid area before union', round(ST_Area(valid_geo,true)) area_m2, ST_GeometryType(valid_geo) FROM (SELECT (ST_Dump(ST_Intersection(:'input_geo',geo))).geom AS valid_geo FROM d01_org_helling WHERE h_klasse in (1,2)) AS r ORDER BY round(ST_Area(valid_geo,true));

SELECT ST_Union(ST_Intersection(:'input_geo',geo)) AS valid_area FROM d01_org_helling WHERE h_klasse in (1,2) \gset
SELECT '--b2-- area valid ', round(ST_Area(:'valid_area',true)), 'of type ' , ST_GeometryType(:'valid_area'::geometry);
SELECT '--b2-- small geom', count(*) FROM ( SELECT (ST_Dump(:'valid_area')).geom AS geo ) AS r WHERE ST_Area(geo,true) < 1;
SELECT '--b2-- invalid geom', count(*) FROM ( SELECT (ST_Dump(:'valid_area')).geom AS geo ) AS r WHERE NOT ST_IsValid(geo);


-- Here we we see problem with holes after union IS, this will cause problems when we check the difference
SELECT '--b2-- area of holes inside polyongs',
round(ST_Area(r.geom,true)) as area_m2 FROM ( SELECT (ST_DumpRings(geo)).* AS dump_data FROM ( SELECT (ST_Dump(:'valid_area')).geom AS geo ) AS r) AS r
WHERE r.path[1] > 0 ORDER BY round(ST_Area(r.geom,true));


-- 3) Use valid area to compute overlay for the valid area left
-- To get overlay type
-- SELECT id, overlay_layer_type FROM org_rmpkart_v2.rmp_tiltak WHERE miljotema_id = 1 AND aktivitetsomraade_id = 1 AND tiltaknavn_id = 82;
-- 59 |                  4
-- which is 4 and that refers to HellingAr5_FOI(4, "1=112,2=113,3=114,4=115,*=500",null)
-- This could be function overlay_area_04 ??
-- where 112 .. 115, 500 is the computed overlay based on the intersection

-- This could also be calculated up front then we would get keys like 112 .. 115 for each valid area(may need to split valid area in smaller pieces ) of type 1.1.82 .
-- The keys like 112 .. 115 should be visible for the farmers .

-- The 500 is causing a problem here because then valid area is is every where so need do some thinking here.


CREATE TABLE test_result_b2 AS SELECT
CASE
    WHEN h_klasse = 1 and artype in (21,22) THEN 112
    WHEN h_klasse = 2 and artype in (21,22) THEN 113
    WHEN h_klasse = 1 and artype = 23 THEN 114
    WHEN h_klasse = 2 and artype = 23 THEN 115
    ELSE 115
END AS class_value,
r.geo::geometry(Polygon,4258 ) as geo
from (
SELECT
po.artype,
e.h_klasse,
(ST_Dump(ST_intersection(ST_intersection(po.geo,e.geo),:'valid_area'))).geom  as geo
FROM
d01_ar5_gpi2_arealklasser_flate po,
d01_org_helling e
where po.artype in (21,22,23) and
ST_intersects(po.geo,e.geo) AND
ST_intersects(po.geo,:'valid_area') AND
ST_intersects(e.geo,:'valid_area')
) as r where ST_Area(geo) > 0;

-- We also need to compute the difference
INSERT INTO test_result_b2(geo,class_value)
SELECT * FROM
( SELECT (ST_Dump(ST_Difference(:'valid_area',ST_Union(geo)))).geom::geometry(Polygon,4258 ) AS GEO, 500 AS class_value FROM test_result_b2) r
WHERE ST_Area(geo) > 0;



-- Test the result table
-- Result pr class
SELECT '--b3-- result', class_value, round(ST_Area(ST_Union(geo),true)) FROM test_result_b2 GROUP BY class_value ORDER BY class_value;

--
-- Number polygons under 1 m2
-- This is problem but if many small parts can be grouped together to a bigger area then it may be OK to use.
SELECT '--b3-- small geoms', class_value, count(*) FROM test_result_b2 WHERE ST_Area(geo,true) < 1 GROUP BY class_value ORDER BY class_value;

-- Number polygons under 1 m2 after union pr. class_value
-- This shows more the real problem with tiny small areas that can not be grouped to bigger ones.
SELECT '--b3-- result small geoms after group by',
class_value, count(*) FROM
(SELECT (ST_Dump(ST_Union(geo))).geom , class_value FROM test_result_b2 GROUP BY class_value ORDER BY class_value) AS R
WHERE ST_Area(geom,true) < 1
GROUP BY class_value ORDER BY class_value;




DROP TABLE test_result_b2;

DROP TABLE d01_rmp_helling_ar5_foi_flate_table;

DROP TABLE d01_ar5_gpi2_arealklasser_flate;

DROP TABLE d01_org_helling;


