set client_min_messages to WARNING;
set timezone to utc;

-- the issue is here
-- https://gitlab.com/nibioopensource/resolve-overlap-and-gap/-/issues/24

-- find interesting areas
-- SELECT f.* from org_rmpkart_v2.rmp_helling_ar5_foi_flate_table f, sl_lop.feil_1665039396019 feil  where ST_Intersects(overlay_geo,geo) AND ST_area(f.geo,true) < 1;
-- SELECT ST_Envelope(overlay_geo) FROM sl_lop.feil_1665039396019 feil  where overlay_id in  (3698239);

--CREATE TABLE d01_rmp_helling_ar5_foi_flate_table AS TABLE org_rmpkart_v2.rmp_helling_ar5_foi_flate_table WITH NO DATA;
--INSERT INTO d01_rmp_helling_ar5_foi_flate_table SELECT * FROM (SELECT f.* FROM org_rmpkart_v2.rmp_helling_ar5_foi_flate_table f WHERE ST_Intersects(f.geo, '0103000020A21000000100000005000000434283B47FD319408250203E34DE4E40434283B47FD31940B0B930C65EDE4E400CB14A5A80D51940B0B930C65EDE4E400CB14A5A80D519408250203E34DE4E40434283B47FD319408250203E34DE4E40')) AS r;
--pg_dump -h db09test.nibio.no -U postgres sl -t d01_rmp_helling_ar5_foi_flate_table > test/regress/rmp/inputdata/d01_rmp_helling_ar5_foi_flate_table.sql

--CREATE TABLE d01_ar5_gpi2_arealklasser_flate AS TABLE org_ar5august2022versjon.ar5_flate WITH NO DATA;
--INSERT INTO d01_ar5_gpi2_arealklasser_flate SELECT * FROM (SELECT f.* FROM org_ar5august2022versjon.ar5_flate f WHERE ST_Intersects(f.geo, '0103000020A21000000100000005000000434283B47FD319408250203E34DE4E40434283B47FD31940B0B930C65EDE4E400CB14A5A80D51940B0B930C65EDE4E400CB14A5A80D519408250203E34DE4E40434283B47FD319408250203E34DE4E40')) AS r;
--pg_dump -h db09test.nibio.no -U postgres sl -t d01_ar5_gpi2_arealklasser_flate > test/regress/rmp/inputdata/d01_ar5_gpi2_arealklasser_flate.sql

--CREATE TABLE d01_org_helling AS TABLE org_helling.helling_rmp_geo WITH NO DATA;
--INSERT INTO d01_org_helling SELECT * FROM (SELECT f.* FROM org_helling.helling_rmp_geo f WHERE ST_Intersects(f.geo, '0103000020A21000000100000005000000434283B47FD319408250203E34DE4E40434283B47FD31940B0B930C65EDE4E400CB14A5A80D51940B0B930C65EDE4E400CB14A5A80D519408250203E34DE4E40434283B47FD319408250203E34DE4E40')) AS r;
--pg_dump -h db09test.nibio.no -U postgres sl -t d01_org_helling > test/regress/rmp/inputdata/d01_org_helling.sql

-- SET datadir_input directory
SELECT :'regdir' || '/rmp/inputdata' as datadir_input \gset

-- Read in d01_rmp_helling_ar5_foi_flate_table.sql that is a subset of ar5_gpi2_arealklasser_flate
SELECT :'datadir_input'||'/d01_ar5_gpi2_arealklasser_flate.sql' as x \gset
\i :x
SELECT '--a1-- num_below_1_m2', count(*) AS num_below_1_m2 FROM d01_ar5_gpi2_arealklasser_flate WHERE ST_Area(geo,true) < 1;

-- Read in d01_rmp_helling_ar5_foi_flate_table.sql thats is a that is a subset of org_helling
SELECT :'datadir_input'||'/d01_org_helling.sql' as x \gset
\i :x
SELECT '--a2-- num_below_1_m2', count(*) AS num_below_1_m2 FROM d01_org_helling WHERE ST_Area(geo,true) < 1;

-- Read in d01_rmp_helling_ar5_foi_flate_table.sql this is intersection of
-- ar5_gpi2_arealklasser_flate and org_helling created in the original database
SELECT :'datadir_input'||'/d01_rmp_helling_ar5_foi_flate_table.sql' as x \gset
\i :x

SELECT '--a3-- num_polygons', count(*) AS num_polygons FROM d01_rmp_helling_ar5_foi_flate_table;

-- We here have many tiny polygons that is causing noise and errors
SELECT '--a3-- num_below_1_m2', count(*) AS num_below_1_m2 FROM d01_rmp_helling_ar5_foi_flate_table WHERE ST_Area(geo,true) < 1;

-- Call function to resolve overlap and gap in the function in test_data.overlap_gap_input_t1 which we just testet for overlap
CALL rmp.rmp_resolve_overlap_gap_run(
	'public.d01_rmp_helling_ar5_foi_flate_table',
	'id',
	'geo',
	'topotest1',
	1, -- _max_parallel_jobs int, -- this is the max number of paralell jobs to run. There must be at least the same number of free connections
	200, -- _max_rows_in_each_cell int, -- this is the max number rows that intersects with box before it's split into 4 new boxes
	1.0
);

SELECT '--t3-- failed_lines', count(geo) as failed_lines from topotest1.d01_rmp_helling_ar5_foi_flate_table_no_cut_line_failed;

SELECT '--t3-- border_line_segments', count(geo) as lines from topotest1.d01_rmp_helling_ar5_foi_flate_table_border_line_segments;

SELECT '--t3-- edges', count(geom) as edges from topotest1.edge;

SELECT '--t3-- not used edges', count(geom) as edges from topotest1.edge where left_face = right_face;

SELECT '--t3-- faces', count(mbr) as faces from topotest1.face;

SELECT '--t3-- num_polygons', count(*) as num_polygons from topotest1.d01_rmp_helling_ar5_foi_flate_table_result where geo is not null;

SELECT '--t3-- num_below_1_m2', count(*) as num_below_1_m2 from topotest1.d01_rmp_helling_ar5_foi_flate_table_result where ST_Area(geo,true) < 1;

SELECT 'validation topotest1', * FROM topology.ValidateTopology('topotest1');

DROP TABLE d01_rmp_helling_ar5_foi_flate_table;

SELECT 'drop topotest1', topology.droptopology('topotest1');


-- test merge of lines from d01_ar5_gpi2_arealklasser_flate and d01_org_helling

CREATE TABLE merge_ar5_gpi_and_d0_helling AS (
SELECT row_number() over ( ORDER BY ST_Area(geo,true) ) id, * FROM (
	SELECT
		--format('%s.%s', 'd01_ar5_gpi2_arealklasser_flate'::regclass::oid::text, sl_sdeid) as id,
		'd01_ar5_gpi2_arealklasser_flate' AS src_table,
		to_jsonb(v1)::jsonb - 'geo' AS sf_org_columns,
		geo
	FROM d01_ar5_gpi2_arealklasser_flate v1
		UNION ALL
	SELECT
		--format('%s.%s', 'd01_org_helling'::regclass::oid::text, gid) as id,
		'd01_org_helling' AS src_table,
		to_jsonb(v2)::jsonb - 'geo' as sf_org_columns,
		geo
	FROM d01_org_helling AS v2
) AS r ORDER by ST_Area(geo,true)
);

--ALTER TABLE merge_ar5_gpi_and_d0_helling ADD COLUMN id serial primary key;

SELECT '--a4-- num_polygons', src_table, count(*) AS num_polygons FROM merge_ar5_gpi_and_d0_helling GROUP BY src_table ORDER BY src_table;

SELECT '--a4-- num_below_1_m2', src_table, count(*) AS num_below_1_m2 FROM merge_ar5_gpi_and_d0_helling WHERE ST_Area(geo,true) < 1 GROUP BY src_table ORDER BY src_table;

-- Call function to resolve overlap and gap in the function in test_data.overlap_gap_input_t1 which we just testet for overlap
CALL rmp.rmp_resolve_overlap_gap_run(
	'public.merge_ar5_gpi_and_d0_helling',
	'id',
	'geo',
	'topotest2'
);

SELECT '--t4-- failed_lines', count(geo) as failed_lines from topotest2.merge_ar5_gpi_and_d0_helling_no_cut_line_failed;

SELECT '--t4-- border_line_segments', count(geo) as lines from topotest2.merge_ar5_gpi_and_d0_helling_border_line_segments;

SELECT '--t4-- edges', count(geom) as edges from topotest2.edge;

SELECT '--t4-- not used edges', count(geom) as edges from topotest2.edge where left_face = right_face;

SELECT '--t4-- faces', count(mbr) as faces from topotest2.face;

SELECT '--t4-- num_polygons', src_table, count(*) as num_polygons from topotest2.merge_ar5_gpi_and_d0_helling_result where geo is not null GROUP BY src_table;

SELECT '--t4-- num_below_1_m2', count(*) as num_below_1_m2 from topotest2.merge_ar5_gpi_and_d0_helling_result where ST_Area(geo,true) < 1;

SELECT '--t4-- _other_intersect_id_list', _other_intersect_id_list from topotest2.merge_ar5_gpi_and_d0_helling_result where ST_Area(geo,true) > 1 order by ST_area(geo::geometry,true);

SELECT '--t4-- _all_intersect_id_list', id, _all_intersect_id_list from topotest2.merge_ar5_gpi_and_d0_helling_result where ST_Area(geo,true) > 1 order by ST_area(geo::geometry,true);

SELECT '--t4-- src_table', src_table, round(ST_area(geo::geometry,true)) from topotest2.face_attributes order by round(ST_area(geo::geometry,true));

-- Find all areas that are newly created, which means that did not exists in d01_ar5_gpi2_arealklasser_flate or d01_org_helling,
-- but is created because of a hole in a polygon or that two or more polygons forms a new face
SELECT '--t4-- new area', round(ST_Area(geo,true)) AS area FROM topotest2.merge_ar5_gpi_and_d0_helling_result r
WHERE r._all_intersect_id_list IS NULL ORDER BY ST_Area(geo,true);

-- Find all old areas , which means that did exists in d01_ar5_gpi2_arealklasser_flate or d01_org_helling,
-- but is created because of a hole in a polygon or that two or more polygons forms a new face
SELECT '--t4-- old area', round(ST_Area(geo,true)) AS area FROM topotest2.merge_ar5_gpi_and_d0_helling_result r
WHERE r._all_intersect_id_list IS NOT NULL ORDER BY ST_Area(geo,true);

-- Find all areas that is created ONLY from d01_ar5_gpi2_arealklasser_flate
CREATE TABLE only_d01_ar5_gpi2_arealklasser_flate AS
SELECT distinct r1.id, t1.src_table, t1.sf_org_columns, r1.geo::geometry(Polygon,4258) FROM
public.merge_ar5_gpi_and_d0_helling t1,
topotest2.merge_ar5_gpi_and_d0_helling_result r1
WHERE array_length(r1._all_intersect_id_list,1) = 1 AND t1.id = r1._all_intersect_id_list[1] AND
t1.src_table = 'd01_ar5_gpi2_arealklasser_flate';

SELECT '--t4-- only_d01_ar5_gpi2_arealklasser_flate area', sf_org_columns, round(sum(ST_Area(geo,true))) AS area FROM only_d01_ar5_gpi2_arealklasser_flate r
GROUP BY sf_org_columns ORDER BY sf_org_columns;

-- Find all areas that is ONLY created from d01_org_helling
CREATE TABLE only_d01_org_helling AS
SELECT distinct r1.id, t1.src_table, t1.sf_org_columns, r1.geo::geometry(Polygon,4258) FROM
public.merge_ar5_gpi_and_d0_helling t1,
topotest2.merge_ar5_gpi_and_d0_helling_result r1
WHERE array_length(r1._all_intersect_id_list,1) = 1 AND t1.id = r1._all_intersect_id_list[1] AND
t1.src_table = 'd01_org_helling';

SELECT '--t4-- only_d01_org_helling area', sf_org_columns, round(sum(ST_Area(geo,true))) AS area FROM only_d01_org_helling r
GROUP BY sf_org_columns ORDER BY sf_org_columns;


-- Find all areas that is created from both d01_ar5_gpi2_arealklasser_flate and d01_org_helling
CREATE TABLE both_d01_ar5_gpi2_arealklasser_flate_d01_org_helling AS
SELECT distinct r1.id, t1.src_table, t1.sf_org_columns, r1.geo::geometry(Polygon,4258) FROM
public.merge_ar5_gpi_and_d0_helling t1,
public.merge_ar5_gpi_and_d0_helling t2,
topotest2.merge_ar5_gpi_and_d0_helling_result r1
WHERE array_length(r1._all_intersect_id_list,1) = 2 AND
(t1.id = r1._all_intersect_id_list[1] OR t1.id = r1._all_intersect_id_list[2]) AND t1.src_table = 'd01_org_helling' AND
(t2.id = r1._all_intersect_id_list[1] OR t2.id = r1._all_intersect_id_list[2]) AND t2.src_table = 'd01_ar5_gpi2_arealklasser_flate';

SELECT '--t4-- both_d01_ar5_gpi2_arealklasser_flate_d01_org_helling area', sf_org_columns, round(sum(ST_Area(geo,true))) AS area FROM both_d01_ar5_gpi2_arealklasser_flate_d01_org_helling r
GROUP BY sf_org_columns ORDER BY sf_org_columns;


-- Find all areas that is created from both d01_ar5_gpi2_arealklasser_flate and d01_org_helling
-- and  d01_org_helling.h_klasse = 2
CREATE TABLE both_gpi2_arealklasser_flate_helling_h_klasse_2  AS
SELECT distinct r1.id,
t2.src_table AS gpi_src_table, t2.sf_org_columns AS gpi_columns,
t1.src_table AS helling_src_table, t1.sf_org_columns AS helling_columns ,
r1.geo::geometry(Polygon,4258)
FROM
public.merge_ar5_gpi_and_d0_helling t1,
public.merge_ar5_gpi_and_d0_helling t2,
topotest2.merge_ar5_gpi_and_d0_helling_result r1
WHERE array_length(r1._all_intersect_id_list,1) = 2 AND
(t1.id = r1._all_intersect_id_list[1] OR t1.id = r1._all_intersect_id_list[2]) AND t1.src_table = 'd01_org_helling' AND
t1.sf_org_columns->>'h_klasse' = '2' AND
(t2.id = r1._all_intersect_id_list[1] OR t2.id = r1._all_intersect_id_list[2]) AND t2.src_table = 'd01_ar5_gpi2_arealklasser_flate'
;

SELECT '--t4-- both_gpi2_arealklasser_flate_helling_h_klasse_2 area', gpi_columns, gpi_columns, round(sum(ST_Area(geo,true))) AS area FROM both_gpi2_arealklasser_flate_helling_h_klasse_2 r
GROUP BY gpi_columns, gpi_columns ORDER BY gpi_columns, gpi_columns;



SELECT 'validation topotest2', * FROM topology.ValidateTopology('topotest2');

DROP TABLE merge_ar5_gpi_and_d0_helling;

DROP TABLE d01_ar5_gpi2_arealklasser_flate;

DROP TABLE d01_org_helling;

DROP TABLE only_d01_ar5_gpi2_arealklasser_flate;

DROP TABLE only_d01_org_helling;

DROP TABLE both_d01_ar5_gpi2_arealklasser_flate_d01_org_helling;

DROP TABLE both_gpi2_arealklasser_flate_helling_h_klasse_2;

SELECT 'drop topotest2', topology.droptopology('topotest2');
