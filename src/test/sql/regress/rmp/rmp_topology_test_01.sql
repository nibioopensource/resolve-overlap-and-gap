set client_min_messages to WARNING;
set timezone to utc;

set execute_parallel.connstring = 'host=localhost dbname=nibio_reg user=postgres password=testp101';

-- SET datadir_input directory
SELECT :'regdir' || '/rmp/inputdata' as datadir_input \gset

-- Read in d01_rmp_helling_ar5_foi_flate_table.sql this is intersection of
-- ar5_gpi2_arealklasser_flate and org_helling created in the original database
SELECT :'datadir_input'||'/d01_rmp_helling_ar5_foi_flate_table.sql' as x \gset
\i :x

SELECT '--a3-- num_polygons', count(*) AS num_polygons FROM d01_rmp_helling_ar5_foi_flate_table;

-- We here have many tiny polygons that is causing noise and errors
SELECT '--a3-- num_below_1_m2', count(*) AS num_below_1_m2 FROM d01_rmp_helling_ar5_foi_flate_table WHERE ST_Area(geo,true) < 1;

-- Call function to resolve overlap and gap in the function in test_data.overlap_gap_input_t1 which we just testet for overlap
CALL rmp.rmp_resolve_overlap_gap_run(
	'public.d01_rmp_helling_ar5_foi_flate_table',
	'id',
	'geo',
	'rmp_test_01',
	1, -- _max_parallel_jobs int, -- this is the max number of paralell jobs to run. There must be at least the same number of free connections
	200, -- _max_rows_in_each_cell int, -- this is the max number rows that intersects with box before it's split into 4 new boxes
	0.00001
);

-- SELECT '--t3-- failed_lines', count(geo) as failed_lines from rmp_test_01.d01_rmp_helling_ar5_foi_flate_table_no_cut_line_failed;

-- this seems to vari between 30 and 31
-- SELECT '--t3-- border_line_segments', count(geom) as lines from rmp_test_01.d01_rmp_helling_ar5_foi_flate_table_border_line_segments_final;

-- this seems to vari between 62 and 63
-- SELECT '--t3-- edges', count(geom) as edges from rmp_test_01.edge;

--SELECT '--t3-- faces', count(mbr) as faces from rmp_test_01.face;

--SELECT '--t3-- num_polygons', count(*) as num_polygons from rmp_test_01.d01_rmp_helling_ar5_foi_flate_table_result where geo is not null;

--SELECT '--t3-- num_below_1_m2', count(*) as num_below_1_m2 from rmp_test_01.d01_rmp_helling_ar5_foi_flate_table_result where ST_Area(geo,true) < 1;

SELECT 'validation rmp_test_01', * FROM topology.ValidateTopology('rmp_test_01');

SELECT 'drop rmp_test_01', topology.droptopology('rmp_test_01');

DROP TABLE d01_rmp_helling_ar5_foi_flate_table;
