#!/bin/sh

REGDIR=$(dirname $0)
TOPDIR=${REGDIR}/../../../../..

export POSTGIS_REGRESS_DB=nibio_reg
export PGTZ=utc

echo ARGS: $@

if echo $@ | grep -q -- '--nocreate'; then
	:
else
	OPTS="${OPTS} --after-create-script ${REGDIR}/schema/rmp.sql"
fi

if echo $@ | grep -q -- '--nodrop'; then
	:
else
	OPTS="${OPTS} --before-uninstall-script ${REGDIR}/schema/rmp_uninstall.sql"
fi

${REGDIR}/../run_resolve_test.sh ${OPTS} $@

