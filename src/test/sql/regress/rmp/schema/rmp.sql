DROP SCHEMA IF EXISTS rmp CASCADE;
CREATE SCHEMA rmp;

CREATE PROCEDURE rmp.rmp_resolve_overlap_gap_run(
  input_table regclass,
  input_id_column varchar, -- TODO: use name
  input_geom_column varchar, -- TODO: use name
  output_schema varchar,
  -- this is the max number of paralell jobs to run. There must
  -- be at least the same number of free connections
  max_parallel_jobs int = 1,
  -- this is the max number rows that intersects with box before
  -- it's split into 4 new boxes
  max_rows_in_each_cell int = 5000,

  --(_topology_info).topology_snap_tolerance float, -- this is tolerance used as base when creating the the postgis topolayer
  topology_snap_tolerance float default 0
)
AS $BODY$
DECLARE

  -- TODO: expose create_attribute_table as parameter ?
  create_attribute_table boolean := true;

  srid int; -- Will be initialized from input
  is_utm boolean := false; -- TODO: find out from intput tables

  cla pg_catalog.pg_class;
  nsp pg_catalog.pg_namespace;
BEGIN

  SELECT * FROM pg_catalog.pg_class
  WHERE oid = input_table
  INTO cla; -- should raise an exception if class it not found

  SELECT * FROM pg_catalog.pg_namespace
  WHERE oid = cla.relnamespace
  INTO nsp; -- should raise an exception if class it not found

  srid := find_srid(nsp.nspname::varchar, cla.relname::varchar, input_geom_column::varchar);
  IF srid IS NULL THEN
    RAISE EXCEPTION 'Cannot determine srid from input table %', input_table;
  END IF;
  IF srid = 0 THEN
    RAISE EXCEPTION 'Cannot use unknown srid in input table %', input_table;
  END IF;

  CALL resolve_overlap_gap_run(
    (null,null,null, -- The simple line feature info with attributtes
      format('%I.%I', nsp.nspname, cla.relname),
      NULL,
      input_id_column,
      input_geom_column,
      srid,
      is_utm,
      null,null,null,null
    ), -- TYPE resolve_overlap_data_input
    (output_schema, output_schema, srid, is_utm, topology_snap_tolerance, create_attribute_table, null, null,
    true, -- use_temp_topology
    false, -- do_qualitycheck_on_final_reseult
    1 --  Method 2 topology.TopoGeo_LoadGeometry

    ), -- TYPE resolve_overlap_data_topology
    resolve_overlap_data_clean_type_func(  -- TYPE resolve_overlap_data_clean
      1,  -- if this a polygon  is below this limit it will merge into a neighbour polygon. The area is sqare meter.
      NULL, -- split all lines at given vertex num
      false, -- _break_up_big_polygons
      resolve_based_on_attribute_type_func(), -- resolve_based_on_attribute_type for attributes that have equal values
      null, -- _max_average_vertex_length, in meter both for utm and deegrees, this used to avoid running ST_simplifyPreserveTopology for long lines lines with few points
      0, -- IF 0 NO CHAKINS WILL BE DONE A big value here make no sense because the number of points will increaes exponential )
      10000, --edge that are longer than this value will not be touched by _chaikins_min_degrees and _chaikins_max_degrees
      120, -- The angle has to be less this given value, This is used to avoid to touch all angles.
      240, -- OR the angle has to be greather than this given value, This is used to avoid to touch all angles
      40, -- The angle has to be less this given value, This is used to avoid to touch all angles.
      320, -- OR The angle has to be greather than this given value, This is used to avoid to touch all angles
      NULL,
      0.00001
    )
    , max_parallel_jobs -- _max_parallel_jobs int, -- this is the max number of paralell jobs to run. There must be at least the same number of free connections
    , max_rows_in_each_cell -- _max_rows_in_each_cell int, -- this is the max number rows that intersects with box before it's split into 4 new boxes, default is 5000
    , resolve_overlap_data_debug_options_func(
      false --if set to false, it will do topology.ValidateTopology and stop to if the this call returns any rows
    )
  );

END;
$BODY$ LANGUAGE 'plpgsql';
