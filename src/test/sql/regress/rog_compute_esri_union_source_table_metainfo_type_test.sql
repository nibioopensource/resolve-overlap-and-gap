-- To avoid error since binary geo seems to vary from local systen and Travis
SET client_min_messages TO ERROR;

-- Create data test case degrees
CREATE table test_data.overlap_gap_input_t2 AS (SELECT * from test_data.overlap_gap_input_t1 WHERE c1 in (633,1233,1231,834));
-- Set primary key
ALTER TABLE test_data.overlap_gap_input_t2 ADD PRIMARY KEY (c1);

-- Create data test case meter and new column names
CREATE table test_data.overlap_gap_input_t3 AS (SELECT distinct c1 as c1t3, c2 as c2t3, c3, ST_transform(geom,4258)::Geometry(Polygon,4258) as geo from test_data.overlap_gap_input_t1 WHERE c1 in (633,1233,1231,834));
-- Set primary key
ALTER TABLE test_data.overlap_gap_input_t3 ADD PRIMARY KEY (c1t3);

CALL rog_compute_esri_union_source_table_metainfo_type(ARRAY['test_data.overlap_gap_input_t2 c1,c2,c3','test_data.overlap_gap_input_t3 c1t3,c2t3,c3'],NULL);


drop SCHEMA test_data CASCADE;

drop SCHEMA test_ar5_web CASCADE;
