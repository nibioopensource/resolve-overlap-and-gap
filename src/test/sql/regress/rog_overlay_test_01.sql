-- To avoid error since binary geo seems to vary from local systen and Travis
SET client_min_messages TO ERROR;

set execute_parallel.connstring = 'host=localhost dbname=nibio_reg user=postgres password=testp101';

--\i :regdir/../../../main/sql/function_rog_overlay.sql
--tmp_rmp_input_data.rmp_erosjonsrisiko_harmonisert_flate_v_to_2023
-- test_data.overlap_gap_input_t2_long_long_long_long_table_____name_long_long_long_long_table_____name
-- Create data test case degrees
CREATE table test_data.overlap_gap_input_t2_long_long_long_long_table_____name AS (SELECT * from test_data.overlap_gap_input_t1 WHERE c1 in (633,1233,1231,834));
-- Set primary key
ALTER TABLE test_data.overlap_gap_input_t2_long_long_long_long_table_____name ADD PRIMARY KEY (c1);
CREATE INDEX ON test_data.overlap_gap_input_t2_long_long_long_long_table_____name USING GIST (geom);

-- Create data test case meter and new column names
CREATE table test_data.overlap_gap_input_t3 AS (SELECT distinct c1 as c1t3, c2 as c2t3, c3, ST_transform(geom,25833)::Geometry(Polygon,25833) as geo from test_data.overlap_gap_input_t1 WHERE c1 in (633,1233,1231,834));
-- Set primary key
ALTER TABLE test_data.overlap_gap_input_t3 ADD PRIMARY KEY (c1t3);
CREATE INDEX ON test_data.overlap_gap_input_t3 USING GIST (geo);



CALL topo_rog_static.rog_overlay(
ARRAY['test_data.overlap_gap_input_t2_long_long_long_long_table_____name short_alias_4_long','test_data.overlap_gap_input_t3'],
'test_topo_t2.test2_res',
--4258,
10, -- _max_parallel_jobs int, -- this is the max number of paralell jobs to run. There must be at least the same number of free connections
200 -- _max_rows_in_each_cell int, -- this is the max number rows that intersects with box before it's split into 4 new boxes
);


\d test_topo_t2.test2_res_v;

--SELECT 'tables created ' , table_schema||'.'||table_name
--FROM information_schema.tables WHERE table_schema != 'information_schema' and table_schema != 'pg_catalog'
--ORDER BY table_schema, table_name;

--\d+ test_topo_t2.test2_res;

--\d+ test_data.overlap_gap_input_t2_long_long_long_long_table_____name;

--\d+ test_data.overlap_gap_input_t3;

SELECT 'degrees_check_failed_lines', count(geo) from test_topo_t2_topo.test2_res_no_cut_line_failed;

--SELECT 'length in meter degrees_check_border_lines', round(Sum(ST_Length(geo,true))) from test_topo_t2.test2_res_border_line_segments;

SELECT 'length in meter degrees_check_no_cut_line_failed', round(Sum(ST_Length(geo,true))) from test_topo_t2_topo.test2_res_no_cut_line_failed;

SELECT 'length in meter degrees_check_added_lines', round(Sum(ST_Length(geom,true))) from test_topo_t2_topo.edge;

SELECT 'degrees_check_added_lines not used', count(geom) from test_topo_t2_topo.edge where left_face = right_face;

SELECT 'degrees_check_added_faces', count(mbr) from test_topo_t2_topo.face;

SELECT 'degrees_check_added_simple_feature_geom', count(*) from test_topo_t2.test2_res_v where geom is not null;

SELECT 'degrees_check_added_simple_feature_c1', count(*) from test_topo_t2.test2_res_v where short_alias_4_long_c2 is not null;

SELECT 'degrees_check_added_simple_feature_c2', count(*) from test_topo_t2.test2_res_v where short_alias_4_long_c3 is not null;

SELECT 'degrees_check_added_simple_feature_c3', count(*) from test_topo_t2.test2_res_v where t02_c3 is not null;

SELECT 'check new primary default key', "short_alias_4_long__selected_pk", "2-pk-c1t3-test_data.overlap_gap_input_t3" FROM test_topo_t2_topo.test2_res ORDER BY ST_Area(geom,true);

select 'obj1 test_topo_t2.test2_res:',
substring(obj_description('test_topo_t2_topo.test2_res'::regclass),
0,
(POSITION('Done proccesed at' in obj_description('test_topo_t2_topo.test2_res'::regclass))+18)
);

SELECT 'test2_res', co.column_name, col_description(pc.oid, co.ordinal_position::int)
FROM pg_catalog.pg_class pc,information_schema.columns co
WHERE pc.oid = (co.table_schema||'.'||co.table_name)::regclass::oid
AND co.table_schema = 'test_topo_t2_topo' AND co.table_name = 'test2_res'
ORDER BY co.ordinal_position;



select 'obj1 test_topo_t2_topo.test2_res_v:',
substring(obj_description('test_topo_t2.test2_res_v'::regclass),
0,
(POSITION('Done proccesed at' in obj_description('test_topo_t2_topo.test2_res_v'::regclass))+18)
);


SELECT 'test2_res_v', co.column_name, col_description(pc.oid, co.ordinal_position::int)
FROM pg_catalog.pg_class pc,information_schema.columns co
WHERE pc.oid = (co.table_schema||'.'||co.table_name)::regclass::oid
AND co.table_schema = 'test_topo_t2_topo' AND co.table_name = 'test2_res_v'
ORDER BY co.ordinal_position;



-- Records from this function would mean an invalid topology was created
SELECT 'validation', * FROM topology.ValidateTopology('test_topo_t2_topo');

--By default this checks are now turned off in the simple siganture for topo_rog_static.rog_overlay
--SELECT 'degrees_check_added_simple_feature_c3 gaps', count(*) from test_topo_t2.test2_res_rogtest_gap;
--SELECT 'degrees_check_added_simple_feature_c3 overlap', count(*) from test_topo_t2.test2_res_rogtest_overlap;
--SELECT 'degrees_check_added_simple_feature_c3 validation', validate_result  from test_topo_t2_topo.test2_res_grid_validate_topology;

SELECT 'degrees', topology.droptopology('test_topo_t2_topo');

drop SCHEMA test_topo_t2 CASCADE;
drop SCHEMA test_data CASCADE;
drop SCHEMA test_ar5_web CASCADE;

