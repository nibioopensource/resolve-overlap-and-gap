-- Here we test overlay betweem two almost datasetts by running ST_Buffer(f.geo,0.0000001) on secomd input

SET client_min_messages TO ERROR;

--\i :regdir/../../../main/sql/function_rog_overlay.sql

-- Create test data for ar5
CREATE table test_data.test_02_tablet_01 AS SELECT
f.qms_id_flate,
f.artype,
f.arskogbon,
f.artreslag,
f.argrunnf,
f.geo::geometry(Polygon,4258)AS geo
FROM test_ar5_web.flate f
WHERE ST_Intersects('0103000020A21000000100000005000000BAF34A7E4DD6174082EF7C7F078A4D40BAF34A7E4DD617406217819A2D8A4D40B443FDC569D717406217819A2D8A4D40B443FDC569D7174082EF7C7F078A4D40BAF34A7E4DD6174082EF7C7F078A4D40',
f.geo);
ALTER TABLE test_data.test_02_tablet_01 ADD PRIMARY KEY (qms_id_flate);
CREATE INDEX ON test_data.test_02_tablet_01 USING GIST (geo);


-- Create test data for ar5
CREATE table test_data.test_02_tablet_02 AS SELECT
f.qms_id_flate,
-1 artype,
f.arskogbon,
f.artreslag,
f.argrunnf,
ST_Buffer(f.geo,0.0000001)::geometry(Polygon,4258) AS geo
FROM test_ar5_web.flate f
WHERE ST_Intersects('0103000020A21000000100000005000000BAF34A7E4DD6174082EF7C7F078A4D40BAF34A7E4DD617406217819A2D8A4D40B443FDC569D717406217819A2D8A4D40B443FDC569D7174082EF7C7F078A4D40BAF34A7E4DD6174082EF7C7F078A4D40',
f.geo);
ALTER TABLE test_data.test_02_tablet_02 ADD PRIMARY KEY (qms_id_flate);
CREATE INDEX ON test_data.test_02_tablet_02 USING GIST (geo);



CALL topo_rog_static.rog_overlay(
ARRAY['test_data.test_02_tablet_01','test_data.test_02_tablet_02'],
'test_topo_t2',
4258,
10, -- _max_parallel_jobs int, -- this is the max number of paralell jobs to run. There must be at least the same number of free connections
200, -- _max_rows_in_each_cell int, -- this is the max number rows that intersects with box before it's split into 4 new boxes
null
);


SELECT 'degrees_check_added_simple_feature_geom count', count(*), 'area:', round(sum(ST_Area(geom,true)))
from test_topo_t2.multi_input_2_v
where geom is not null AND ST_Area(geom,true) > 1.0;

SELECT 'degrees_check_added_simple_feature_t01_artype count', count(*), 'area:', round(sum(ST_Area(geom,true)))
from test_topo_t2.multi_input_2_v
where t01_artype is not null AND ST_Area(geom,true) > 1.0;

SELECT 'degrees_check_added_simple_feature_t02_artype count', count(*), 'area:', round(sum(ST_Area(geom,true)))
from test_topo_t2.multi_input_2_v
where t02_artype is not null AND ST_Area(geom,true) > 1.0;

SELECT 'degrees_check_added_simple_feature_geom count small', count(*), 'area:', round(sum(ST_Area(geom,true)))
from test_topo_t2.multi_input_2_v
where geom is not null AND ST_Area(geom,true) <= 1.0;

SELECT 'degrees_check_added_simple_feature_t01_artype count small', count(*), 'area:', round(sum(ST_Area(geom,true)))
from test_topo_t2.multi_input_2_v
where t01_artype is not null AND ST_Area(geom,true) <= 1.0;

SELECT 'degrees_check_added_simple_feature_t02_artype count small', count(*), 'area:', round(sum(ST_Area(geom,true)))
from test_topo_t2.multi_input_2_v
where t02_artype is not null AND ST_Area(geom,true) <= 1.0;

--SELECT 'degrees_check_added_lines not used', count(geom) from test_topo_t2_topo.edge where left_face = right_face;

SELECT 'degrees_check_added_faces', count(mbr) from test_topo_t2_topo.face;


SELECT 'degrees_check_failed_lines', count(geo) from test_topo_t2_topo.multi_input_2_no_cut_line_failed;


SELECT 'length in meter degrees_check_no_cut_line_failed', round(Sum(ST_Length(geo,true))) from test_topo_t2_topo.multi_input_2_no_cut_line_failed;

SELECT 'length in meter degrees_check_added_lines', round(Sum(ST_Length(geom,true))) from test_topo_t2_topo.edge;


-- Records from this function would mean an invalid topology was created
SELECT 'validation', * FROM topology.ValidateTopology('test_topo_t2_topo');

--SELECT 'degrees_check_added_multi_input_2_ogtest_ gaps', count(*) from test_topo_t2.multi_input_2_ogtest_gap;
--SELECT 'degrees_check_added_multi_input_2_ogtest overlap', count(*) from test_topo_t2.multi_input_2_ogtest_overlap;

SELECT 'degrees_check_added_multi_input_2_ogtest_gap validation', validate_result  from test_topo_t2_topo.multi_input_2_grid_validate_topology;


-- Test for area for each type based on old old types from new test_data.test_02_tablet_01
SELECT 'old mapping table, test_data.test_02_tablet_01',
org.id, org.area, org.num_geom, org.num_points,
overlay.id overlay_id, overlay.area overlay_area, overlay.num_geom overlay_num_geom, overlay.num_points overlay_num_points
FROM (
	SELECT qms_id_flate id, ROUND(SUM(ST_Area(geo,true))) area, count(*) num_geom, sum(ST_Npoints(geo)) num_points
	FROM test_data.test_02_tablet_01
	GROUP BY id ORDER BY id
) org
LEFT JOIN
(
	SELECT qms_id_flate id, ROUND(SUM(ST_Area(geo,true))) area, count(*) num_geom, sum(ST_Npoints(geo)) num_points
	FROM (
	SELECT ST_GetFaceGeometry('test_topo_t2_topo',face.face_id) geo, old.qms_id_flate
	FROM
	test_topo_t2_topo.face face
	LEFT JOIN
	(
		SELECT * FROM (
		SELECT unnest("1-pk_list-qms_id_flate-test_data.test_02_tablet_01") id, face_id
		FROM test_topo_t2.multi_input_2_v

		UNION
		SELECT "1-pk-qms_id_flate-test_data.test_02_tablet_01" id, face_id
		FROM test_topo_t2.multi_input_2_v
		) s
	) new ON new.face_id = face.face_id
	LEFT JOIN test_data.test_02_tablet_01 old ON old.qms_id_flate = new.id
	WHERE face.face_id > 0
	) r
	GROUP BY id ORDER BY id
) overlay ON org.id = overlay.id
ORDER BY (overlay.id) DESC; -- to get null value in the top


-- Test for area for each type based on old old types from new test_data.test_02_tablet_02
SELECT 'old mapping table, test_data.test_02_tablet_02',
org.id, org.area, org.num_geom, org.num_points,
overlay.id overlay_id, overlay.area overlay_area, overlay.num_geom overlay_num_geom, overlay.num_points overlay_num_points
FROM (
	SELECT qms_id_flate id, ROUND(SUM(ST_Area(geo,true))) area, count(*) num_geom, sum(ST_Npoints(geo)) num_points
	FROM test_data.test_02_tablet_02
	GROUP BY id ORDER BY id
) org
LEFT JOIN
(
	SELECT qms_id_flate id, ROUND(SUM(ST_Area(geo,true))) area, count(*) num_geom, sum(ST_Npoints(geo)) num_points
	FROM (
	SELECT ST_GetFaceGeometry('test_topo_t2_topo',face.face_id) geo, old.qms_id_flate
	FROM
	test_topo_t2_topo.face face
	LEFT JOIN
	(
		SELECT * FROM (
		SELECT unnest("2-pk_list-qms_id_flate-test_data.test_02_tablet_02") id, face_id
		FROM test_topo_t2.multi_input_2_v

		UNION
		SELECT "2-pk-qms_id_flate-test_data.test_02_tablet_02" id, face_id
		FROM test_topo_t2.multi_input_2_v
		) s
	) new ON new.face_id = face.face_id
	LEFT JOIN test_data.test_02_tablet_02 old ON old.qms_id_flate = new.id
	WHERE face.face_id > 0
	) r
	GROUP BY id ORDER BY id
) overlay ON org.id = overlay.id
ORDER BY (overlay.id) DESC; -- to get null value in the top

-- Test for missing values

SELECT 'Values missing for test_data.test_02_tablet_01', qms_id_flate
FROM
test_data.test_02_tablet_01 org
WHERE
NOT EXISTS
(
	SELECT qms_id_flate id, ROUND(SUM(ST_Area(geo,true))) area, count(*) num_geom, sum(ST_Npoints(geo)) num_points
	FROM (
	SELECT ST_GetFaceGeometry('test_topo_t2_topo',face.face_id) geo, old.qms_id_flate
	FROM
	test_topo_t2_topo.face face
	LEFT JOIN
	(
		SELECT * FROM (
		SELECT unnest("1-pk_list-qms_id_flate-test_data.test_02_tablet_01") id, face_id
		FROM test_topo_t2.multi_input_2_v

		UNION
		SELECT "1-pk-qms_id_flate-test_data.test_02_tablet_01" id, face_id
		FROM test_topo_t2.multi_input_2_v
		) s
	) new ON new.face_id = face.face_id
	LEFT JOIN test_data.test_02_tablet_01 old ON old.qms_id_flate = new.id
	WHERE face.face_id > 0
	) r
	GROUP BY id ORDER BY id
)
ORDER BY org.qms_id_flate;


-- Test for missing values

SELECT 'Values missing for test_data.test_02_tablet_02', qms_id_flate
FROM
test_data.test_02_tablet_01 org
WHERE
NOT EXISTS
(
	SELECT qms_id_flate id, ROUND(SUM(ST_Area(geo,true))) area, count(*) num_geom, sum(ST_Npoints(geo)) num_points
	FROM (
	SELECT ST_GetFaceGeometry('test_topo_t2_topo',face.face_id) geo, old.qms_id_flate
	FROM
	test_topo_t2_topo.face face
	LEFT JOIN
	(
		SELECT * FROM (
		SELECT unnest("2-pk_list-qms_id_flate-test_data.test_02_tablet_02") id, face_id
		FROM test_topo_t2.multi_input_2_v

		UNION
		SELECT "2-pk-qms_id_flate-test_data.test_02_tablet_02" id, face_id
		FROM test_topo_t2.multi_input_2_v
		) s
	) new ON new.face_id = face.face_id
	LEFT JOIN test_data.test_02_tablet_01 old ON old.qms_id_flate = new.id
	WHERE face.face_id > 0
	) r
	GROUP BY id ORDER BY id
)
ORDER BY org.qms_id_flate;

/**


-- Test for area for each type based on old old types from new test_data.test_02_tablet_01
SELECT
'new mapping table-test_data.test_02_tablet_01',
org.id, org.area, org.num_geom, org.num_points,
overlay.id overlay_id, overlay.area overlay_area, overlay.num_geom overlay_num_geom, overlay.num_points overlay_num_points
FROM (
	SELECT qms_id_flate id, ROUND(SUM(ST_Area(geo,true))) area, count(*) num_geom, sum(ST_Npoints(geo)) num_points
	FROM test_data.test_02_tablet_01
	GROUP BY id ORDER BY id
) org
LEFT JOIN
(
	SELECT qms_id_flate id, ROUND(SUM(ST_Area(geo,true))) area, count(*) num_geom, sum(ST_Npoints(geo)) num_points
	FROM (
	SELECT ST_GetFaceGeometry('test_topo_t2_topo',face.face_id) geo, old.qms_id_flate
	FROM
	test_topo_t2_topo.face face,
	test_topo_t2_topo.multi_input_2_org_pk_value_to_face_id new
	LEFT JOIN test_data.test_02_tablet_01 old ON old.qms_id_flate = new.src_table_pk_column_value
	WHERE face.face_id > 0 AND new.pk_value_set AND new.table_input_order = 1 AND new.face_id = face.face_id
	) r
	GROUP BY id ORDER BY ID
) overlay ON org.id = overlay.id
ORDER BY overlay.id DESC; -- to get null value in the top

-- Test for area for each type based on old old types from new test_data.test_02_tablet_02
SELECT
'new mapping table-test_data.test_02_tablet_02',
org.id, org.area, org.num_geom, org.num_points,
overlay.id overlay_id, overlay.area overlay_area, overlay.num_geom overlay_num_geom, overlay.num_points overlay_num_points
FROM (
	SELECT qms_id_flate id, ROUND(SUM(ST_Area(geo,true))) area, count(*) num_geom, sum(ST_Npoints(geo)) num_points
	FROM test_data.test_02_tablet_02
	GROUP BY id ORDER BY id
) org
LEFT JOIN
(
	SELECT qms_id_flate id, ROUND(SUM(ST_Area(geo,true))) area, count(*) num_geom, sum(ST_Npoints(geo)) num_points
	FROM (
	SELECT ST_GetFaceGeometry('test_topo_t2_topo',face.face_id) geo, old.qms_id_flate
	FROM
	test_topo_t2_topo.face face,
	test_topo_t2_topo.multi_input_2_org_pk_value_to_face_id new
	LEFT JOIN test_data.test_02_tablet_02 old ON old.qms_id_flate = new.src_table_pk_column_value
	WHERE face.face_id > 0 AND new.pk_value_set AND new.table_input_order = 2 AND new.face_id = face.face_id
	) r
	GROUP BY id ORDER BY ID
) overlay ON org.id = overlay.id
ORDER BY overlay.id DESC; -- to get null value in the top

*/

SELECT 'degrees', topology.droptopology('test_topo_t2_topo');

drop SCHEMA test_data CASCADE;
drop SCHEMA test_ar5_web CASCADE;

