SET client_min_messages TO ERROR;

-- This is a test with invalid input data in table test_data.input_03_b

ALTER TABLE test_data.input_03_a ADD PRIMARY KEY (c1);
CREATE INDEX ON test_data.input_03_a USING GIST (geom);

ALTER TABLE test_data.input_03_b ADD PRIMARY KEY (objectid);
CREATE INDEX ON test_data.input_03_b USING GIST (geom);


CALL topo_rog_static.rog_overlay(
ARRAY['test_data.input_03_a','test_data.input_03_b'],
'test_topo_t2',
4258, 
10, -- _max_parallel_jobs int, -- this is the max number of paralell jobs to run. There must be at least the same number of free connections
4, -- _max_rows_in_each_cell int, -- this is the max number rows that intersects with box before it's split into 4 new boxes
null
);



SELECT 'degrees_check_failed_lines', count(geo) from test_topo_t2_topo.multi_input_2_no_cut_line_failed;


SELECT 'length in meter degrees_check_no_cut_line_failed', round(Sum(ST_Length(geo,true))) from test_topo_t2_topo.multi_input_2_no_cut_line_failed;

SELECT 'length in meter degrees_check_added_lines', round(Sum(ST_Length(geom,true))) from test_topo_t2_topo.edge;

SELECT 'degrees_check_added_lines not used', count(geom) from test_topo_t2_topo.edge where left_face = right_face;

SELECT 'degrees_check_added_faces', count(mbr) from test_topo_t2_topo.face;

SELECT 'sum count, area for t02_naturtype in orignal table, before make valid', count(*) num_rows, round(sum(ST_Area(geom,true))) area FROM test_data.input_03_b WHERE naturtype IS NOT NULL;

SELECT 'sum count, area for t02_naturtype in orignal table, after make valid', count(*) num_rows, round(sum(ST_Area(ST_MakeValid(geom),true))) area FROM test_data.input_03_b WHERE naturtype IS NOT NULL;

SELECT 'sum count, area for t02_naturtype in topology', count(*) num_rows, round(sum(ST_Area(geom,true))) area FROM test_topo_t2.multi_input_2_v WHERE t02_naturtype IS NOT NULL;

SELECT 'check add num, area for t02_naturtype in topology', t02_naturtype, round(ST_Area(geom,true)) area FROM test_topo_t2.multi_input_2_v ORDER BY ST_Area(geom,true) desc;


-- \d+ test_topo_t2.multi_input_2_v;

-- Records from this function would mean an invalid topology was created
SELECT 'validation', * FROM topology.ValidateTopology('test_topo_t2_topo');



--SELECT 'degrees', topology.droptopology('test_topo_t2_topo');

--drop SCHEMA test_data CASCADE;
