set client_min_messages to WARNING;
set timezone to utc;

/**

select ST_union(cell_bbox) FROM topo_t1_3049_dyrkbarjord.multi_input_5_grid where id in ( 39, 40, 45 ,25)

create schema test_05;

CREATE TABLE test_05.n5_forvaltning2022_flate AS
select a.objid, a.kommunenummer, a.geo  FROM sk_grl.n5_forvaltning2022_flate a
WHERE ST_Intersects(geo,
'0103000020A2100000010000000900000070C16AA6D19824400E83A51145EE4D4025C191AABA8E24400E83A51145EE4D40DAC0B8AEA38424400E83A51145EE4D40DAC0B8AEA3842440EC13EF6856F04D40DAC0B8AEA3842440CAA438C067F24D4025C191AABA8E2440CAA438C067F24D4070C16AA6D1982440CAA438C067F24D4070C16AA6D1982440EC13EF6856F04D4070C16AA6D19824400E83A51145EE4D40'::geometry
);
ALTER TABLE test_05.n5_forvaltning2022_flate ADD PRIMARY KEY (objid);
CREATE INDEX ON test_05.n5_forvaltning2022_flate USING GIST (geo);


CREATE TABLE test_05.snapshot_ar5_flate_utvalg AS
select a.id_ar5, a.artype, a.geo::geometry(Polygon,3035) FROM tmp_prod_dyrkbarjord.snapshot_ar5_flate_utvalg a
WHERE ST_Intersects(geo,
ST_Transform(
'0103000020A2100000010000000900000070C16AA6D19824400E83A51145EE4D4025C191AABA8E24400E83A51145EE4D40DAC0B8AEA38424400E83A51145EE4D40DAC0B8AEA3842440EC13EF6856F04D40DAC0B8AEA3842440CAA438C067F24D4025C191AABA8E2440CAA438C067F24D4070C16AA6D1982440CAA438C067F24D4070C16AA6D1982440EC13EF6856F04D4070C16AA6D19824400E83A51145EE4D40'::geometry,
3035)
);
ALTER TABLE test_05.snapshot_ar5_flate_utvalg ADD PRIMARY KEY (id_ar5);
CREATE INDEX ON test_05.snapshot_ar5_flate_utvalg USING GIST (geo);


CREATE TABLE test_05.snapshot_dmk_flate AS
select a.id_markslag, a.atil, a.geo::geometry(Polygon,3035) FROM tmp_prod_dyrkbarjord.snapshot_dmk_flate a
WHERE ST_Intersects(geo,
ST_Transform(
'0103000020A2100000010000000900000070C16AA6D19824400E83A51145EE4D4025C191AABA8E24400E83A51145EE4D40DAC0B8AEA38424400E83A51145EE4D40DAC0B8AEA3842440EC13EF6856F04D40DAC0B8AEA3842440CAA438C067F24D4025C191AABA8E2440CAA438C067F24D4070C16AA6D1982440CAA438C067F24D4070C16AA6D1982440EC13EF6856F04D4070C16AA6D19824400E83A51145EE4D40'::geometry,
3035)
);
ALTER TABLE test_05.snapshot_dmk_flate ADD PRIMARY KEY (id_markslag);
CREATE INDEX ON test_05.snapshot_dmk_flate USING GIST (geo);


CREATE TABLE test_05.snapshot_mdir_flate AS
select a.id_mdir, a.verneform, a.geo::geometry(Polygon,3035) FROM tmp_prod_dyrkbarjord.snapshot_mdir_flate a
WHERE ST_Intersects(geo,
ST_Transform(
'0103000020A2100000010000000900000070C16AA6D19824400E83A51145EE4D4025C191AABA8E24400E83A51145EE4D40DAC0B8AEA38424400E83A51145EE4D40DAC0B8AEA3842440EC13EF6856F04D40DAC0B8AEA3842440CAA438C067F24D4025C191AABA8E2440CAA438C067F24D4070C16AA6D1982440CAA438C067F24D4070C16AA6D1982440EC13EF6856F04D4070C16AA6D19824400E83A51145EE4D40'::geometry,
3035)
);
ALTER TABLE test_05.snapshot_mdir_flate ADD PRIMARY KEY (id_mdir);
CREATE INDEX ON test_05.snapshot_mdir_flate USING GIST (geo);


CREATE TABLE test_05.snapshot_ssb_flate_utvalg AS
select a.id_ssb, a.ssbarealbrukhovedklasse, a.geo::geometry(Polygon,3035) FROM tmp_prod_dyrkbarjord.snapshot_ssb_flate_utvalg a
WHERE ST_Intersects(geo,
ST_Transform(
'0103000020A2100000010000000900000070C16AA6D19824400E83A51145EE4D4025C191AABA8E24400E83A51145EE4D40DAC0B8AEA38424400E83A51145EE4D40DAC0B8AEA3842440EC13EF6856F04D40DAC0B8AEA3842440CAA438C067F24D4025C191AABA8E2440CAA438C067F24D4070C16AA6D1982440CAA438C067F24D4070C16AA6D1982440EC13EF6856F04D4070C16AA6D19824400E83A51145EE4D40'::geometry,
3035)
);
ALTER TABLE test_05.snapshot_ssb_flate_utvalg ADD PRIMARY KEY (id_ssb);
CREATE INDEX ON test_05.snapshot_ssb_flate_utvalg USING GIST (geo);


pg_dump -h vroom5.int.nibio.no -U postgres rog_01 -n test_05 > /Users/lop/dev/github/resolve-overlap-and-gap/src/test/sql/regress/inputdata/test_05.sql

*/


/**

-- SET datadir_input directory
SELECT :'regdir' || '/inputdata' as datadir_input \gset

-- Read
SELECT :'datadir_input'||'/test_05.sql' as x \gset
\i :x

*/


SELECT 'n5_forvaltning2022_flate num', count(*) AS num_below_1_m2 FROM test_05.n5_forvaltning2022_flate;

SELECT 'snapshot_ar5_flate_utvalg num', count(*) AS num_below_1_m2 FROM test_05.snapshot_ar5_flate_utvalg;


CALL topo_rog_static.rog_overlay(
ARRAY[
'test_05.n5_forvaltning2022_flate',
'test_05.snapshot_ar5_flate_utvalg',
'test_05.snapshot_dmk_flate',
'test_05.snapshot_mdir_flate',
'test_05.snapshot_ssb_flate_utvalg'
],
'topo_test_05_3049',
4258,
0.0,
1.0, -- min area
NULL, -- split all lines at given vertex num
false, -- _break_up_big_polygons
2.0, -- min negative buffer value
NULL,
true,
20, -- _max_parallel_jobs int, -- this is the max number of paralell jobs to run. There must be at least the same number of free connections
10, -- _max_rows_in_each_cell int, -- this is the max number rows that intersects with box before it's split into 4 new boxes
null
);

SELECT 'topo_test_05_3049 < 1m2 ', count(*), round(sum(ST_area(geom,true))) from topo_test_05_3049.multi_input_5_v where ST_Area(geom,true) < 1.0;

SELECT 'topo_test_05_3049 > 1m2 ', count(*), round(sum(ST_area(geom,true))) from topo_test_05_3049.multi_input_5_v where ST_Area(geom,true) > 1.0;

SELECT 'degrees', topology.droptopology('topo_test_05_3049_topo');

drop SCHEMA test_05 CASCADE;
