set client_min_messages to ERROR;
set timezone to utc;

/**


'test_06.n5_forvaltning2022_flate',
'test_06.snapshot_ar5_flate_utvalg',
'test_06.snapshot_ssb_flate_utvalg',
'test_06.snapshot_mdir_flate',
'test_06.snapshot_dmk_flate',
'test_06.raviner_fra_dted'


select ST_union(cell_bbox) FROM topo_t5_3049_dyrkbarjord.multi_input_6_grid where id in ( 71 )

create schema test_06;

CREATE TABLE test_06.n5_forvaltning2022_flate AS
select a.objid, a.kommunenummer, a.geo  FROM test_06.n5_forvaltning2022_flate a
WHERE ST_Intersects(geo,
'0103000020A2100000010000000500000043C006B7757024400E83A51145EE4D4043C006B775702440EC13EF6856F04D408EC0DFB28C7A2440EC13EF6856F04D408EC0DFB28C7A24400E83A51145EE4D4043C006B7757024400E83A51145EE4D40'::geometry
);
ALTER TABLE test_06.n5_forvaltning2022_flate ADD PRIMARY KEY (objid);
CREATE INDEX ON test_06.n5_forvaltning2022_flate USING GIST (geo);


CREATE TABLE test_06.snapshot_ar5_flate_utvalg AS
select a.id_ar5, a.artype, a.geo::geometry(Polygon,3035) FROM test_06.snapshot_ar5_flate_utvalg a
WHERE ST_Intersects(geo,
ST_Transform(
'0103000020A2100000010000000500000043C006B7757024400E83A51145EE4D4043C006B775702440EC13EF6856F04D408EC0DFB28C7A2440EC13EF6856F04D408EC0DFB28C7A24400E83A51145EE4D4043C006B7757024400E83A51145EE4D40'::geometry,
3035)
);
ALTER TABLE test_06.snapshot_ar5_flate_utvalg ADD PRIMARY KEY (id_ar5);
CREATE INDEX ON test_06.snapshot_ar5_flate_utvalg USING GIST (geo);


CREATE TABLE test_06.snapshot_dmk_flate AS
select a.id_markslag, a.atil, a.geo::geometry(Polygon,3035) FROM test_06.snapshot_dmk_flate a
WHERE ST_Intersects(geo,
ST_Transform(
'0103000020A2100000010000000500000043C006B7757024400E83A51145EE4D4043C006B775702440EC13EF6856F04D408EC0DFB28C7A2440EC13EF6856F04D408EC0DFB28C7A24400E83A51145EE4D4043C006B7757024400E83A51145EE4D40'::geometry,
3035)
);
ALTER TABLE test_06.snapshot_dmk_flate ADD PRIMARY KEY (id_markslag);
CREATE INDEX ON test_06.snapshot_dmk_flate USING GIST (geo);


CREATE TABLE test_06.snapshot_mdir_flate AS
select a.id_mdir, a.verneform, a.geo::geometry(Polygon,3035) FROM test_06.snapshot_mdir_flate a
WHERE ST_Intersects(geo,
ST_Transform(
'0103000020A2100000010000000500000043C006B7757024400E83A51145EE4D4043C006B775702440EC13EF6856F04D408EC0DFB28C7A2440EC13EF6856F04D408EC0DFB28C7A24400E83A51145EE4D4043C006B7757024400E83A51145EE4D40'::geometry,
3035)
);
ALTER TABLE test_06.snapshot_mdir_flate ADD PRIMARY KEY (id_mdir);
CREATE INDEX ON test_06.snapshot_mdir_flate USING GIST (geo);


CREATE TABLE test_06.snapshot_ssb_flate_utvalg AS
select a.id_ssb, a.ssbarealbrukhovedklasse, a.geo::geometry(Polygon,3035) FROM test_06.snapshot_ssb_flate_utvalg a
WHERE ST_Intersects(geo,
ST_Transform(
'0103000020A2100000010000000500000043C006B7757024400E83A51145EE4D4043C006B775702440EC13EF6856F04D408EC0DFB28C7A2440EC13EF6856F04D408EC0DFB28C7A24400E83A51145EE4D4043C006B7757024400E83A51145EE4D40'::geometry,
3035)
);
ALTER TABLE test_06.snapshot_ssb_flate_utvalg ADD PRIMARY KEY (id_ssb);
CREATE INDEX ON test_06.snapshot_ssb_flate_utvalg USING GIST (geo);


CREATE TABLE test_06.raviner_fra_dted AS
select a.id, a.geo::geometry(Polygon,4258) FROM test_06.raviner_fra_dted a
WHERE ST_Intersects(geo,
'0103000020A2100000010000000500000043C006B7757024400E83A51145EE4D4043C006B775702440EC13EF6856F04D408EC0DFB28C7A2440EC13EF6856F04D408EC0DFB28C7A24400E83A51145EE4D4043C006B7757024400E83A51145EE4D40'::geometry
);
ALTER TABLE test_06.raviner_fra_dted ADD PRIMARY KEY (id);
CREATE INDEX ON test_06.raviner_fra_dted USING GIST (geo);


pg_dump -h vroom5.int.nibio.no -U postgres rog_01 -n test_06 > /Users/lop/dev/github/resolve-overlap-and-gap/src/test/sql/regress/inputdata/test_06.sql

*/


/**

SET search_path TO topo_rog_static,public,topology;

SELECT round(ST_Area(ST_Union(r.cell_bbox),true)) FROM
     (SELECT ST_transform(q_grid.cell,4258)::Geometry(geometry,4258) as cell_bbox
     FROM (
      SELECT (st_dump(
      cbg_content_based_balanced_grid(
      '{"test_06.n5_forvaltning2022_flate geo","test_06.snapshot_ar5_flate_utvalg geo","test_06.snapshot_ssb_flate_utvalg geo","test_06.snapshot_mdir_flate geo","test_06.snapshot_dmk_flate geo","test_06.raviner_fra_dted geo"}',
      3,
      NULL)
      )
      ).geom as cell) as q_grid
     ) r
     WHERE NULL IS NULL OR (NULL && r.cell_bbox AND ST_Area(ST_Intersection(NULL,r.cell_bbox)) > 0.0)

*/


SELECT 'n5_forvaltning2022_flate num', count(*) AS num_below_1_m2 FROM test_06.n5_forvaltning2022_flate;

SELECT 'snapshot_ar5_flate_utvalg num', count(*) AS num_below_1_m2 FROM test_06.snapshot_ar5_flate_utvalg;


CALL topo_rog_static.rog_overlay(
ARRAY[
'test_06.n5_forvaltning2022_flate',
'test_06.snapshot_ar5_flate_utvalg',
'test_06.snapshot_ssb_flate_utvalg',
'test_06.snapshot_mdir_flate',
'test_06.snapshot_dmk_flate',
'test_06.raviner_fra_dted'
],
'topo_test_06_3049',
4258,
0.0,
0.0, -- min_area
NULL, -- split all lines at given vertex num
false, -- _break_up_big_polygons
NULL,
true,
1, -- _max_parallel_jobs int, -- this is the max number of paralell jobs to run. There must be at least the same number of free connections
1000, -- _max_rows_in_each_cell int, -- this is the max number rows that intersects with box before it's split into 4 new boxes
null
);

SELECT 'topo_test_06_3049 < 1m2 ', count(*), round(sum(ST_area(geom,true))) from topo_test_06_3049.multi_input_6_v where ST_Area(geom,true) < 1.0;

SELECT 'topo_test_06_3049 > 1m2 ', count(*), round(sum(ST_area(geom,true))) from topo_test_06_3049.multi_input_6_v where ST_Area(geom,true) > 1.0;

SELECT 'topo_test_06_3049 grid', count(*), round(sum(ST_area(cell_bbox,true))) from topo_test_06_3049_topo.multi_input_6_grid;

SELECT 'topo_test_06_3049 invalid cells', count(*) from topo_test_06_3049_topo.multi_input_6_grid_validate_topology  ;

SELECT * FROM topology.ValidateTopology('topo_test_06_3049_topo');


/**

SELECT 'degrees', topology.droptopology('topo_test_06_3049');

drop SCHEMA test_06 CASCADE;

SELECT  'SELECT max(topogeo_addlinestring) FROM ( SELECT topogeo_addlinestring FROM topology.TopoGeo_addLinestring('||quote_literal('test_topo')||','||quote_literal(r.geom::text)||',0)) as r; SELECT '||edge_id||', * FROM topology.ValidateTopology('||quote_literal('test_topo')||');' FROM
       (SELECT edge_id, geom, leqr FROM topo_test_06_3049_topo.edge_data_tmp_1
       WHERE geom && '0103000020A21000000100000025000000F41A823333772440114F540181EE4D40EA9E97BE1E7724401DF2468281EE4D402095ED120B772440B6462A0083EE4D40F38808F2F87624406E4F516C85EE4D402A4E410EE9762440FA06E7AE88EE4D40F66FEA03DC762440ADD5D8A78CEE4D40194D4E53D2762440B818123091EE4D40B5FAC05BCC7624402B9BFC1A96EE4D40836EF657CA7624402D3A37389BEE4D40836EF657CA762440C5E2B5FFEDEE4D40B5FAC05BCC762440C781F01CF3EE4D40194D4E53D27624403A04DB07F8EE4D40F66FEA03DC76244045471490FCEE4D402A4E410EE9762440F815068900EF4D40F38808F2F876244084CD9BCB03EF4D402095ED120B7724403CD6C23706EF4D40EA9E97BE1E772440D52AA6B507EF4D40F41A823333772440E1CD983608EF4D4097EAE2E984782440E1CD983608EF4D40A166CD5E99782440D52AA6B507EF4D406B70770AAD7824403CD6C23706EF4D40987C5C2BBF78244084CD9BCB03EF4D4061B7230FCF782440F815068900EF4D4095957A19DC78244045471490FCEE4D4072B816CAE57824403A04DB07F8EE4D40D60AA4C1EB782440C781F01CF3EE4D4008976EC5ED782440C5E2B5FFEDEE4D4008976EC5ED7824402D3A37389BEE4D40D60AA4C1EB7824402B9BFC1A96EE4D4072B816CAE5782440B818123091EE4D4095957A19DC782440ADD5D8A78CEE4D4061B7230FCF782440FA06E7AE88EE4D40987C5C2BBF7824406E4F516C85EE4D406B70770AAD782440B6462A0083EE4D40A166CD5E997824401DF2468281EE4D4097EAE2E984782440114F540181EE4D40F41A823333772440114F540181EE4D40' ) AS r
       ORDER BY leqr, ST_X(ST_Centroid(r.geom)), ST_Y(ST_Centroid(r.geom));

*/
