set client_min_messages to ERROR;
set timezone to utc;

set execute_parallel.connstring = 'host=localhost dbname=nibio_reg user=postgres password=testp101';

/**

'sk_grl.n5_forvaltning2022_flate', *
'tmp_prod_dyrkbarjord.snapshot_ar5_flate_utvalg',
'tmp_prod_dyrkbarjord.snapshot_ssb_flate_utvalg',
'tmp_prod_dyrkbarjord.snapshot_mdir_flate',
'tmp_prod_dyrkbarjord.snapshot_dmk_flate', *
'tmp_prod_dyrkbarjord.raviner_fra_dted_norge_flate',
'tmp_prod_dyrkbarjord.raviner_fra_statsforvalter_flate' *



select ST_union(cell_bbox) FROM topo_t4_dyrkbarjord_test_01.multi_input_7_grid where id in (44);

create schema test_07;

CREATE TABLE test_07.n5_forvaltning2022_flate AS
select a.objid, a.kommunenummer, a.geo  FROM sk_grl.n5_forvaltning2022_flate a
WHERE ST_Intersects(geo,
'0103000020A21000000100000005000000DAC0B8AEA3842440655715C69BF84D40DAC0B8AEA38424402279A874BEFC4D4070C16AA6D19824402279A874BEFC4D4070C16AA6D1982440655715C69BF84D40DAC0B8AEA3842440655715C69BF84D40'::geometry
);
ALTER TABLE test_07.n5_forvaltning2022_flate ADD PRIMARY KEY (objid);
CREATE INDEX ON test_07.n5_forvaltning2022_flate USING GIST (geo);


CREATE TABLE test_07.snapshot_ar5_flate_utvalg AS
select a.id_ar5, a.artype, a.geo::geometry(Polygon,3035) FROM tmp_prod_dyrkbarjord.snapshot_ar5_flate_utvalg a
WHERE ST_Intersects(geo,
ST_Transform(
'0103000020A21000000100000005000000DAC0B8AEA3842440655715C69BF84D40DAC0B8AEA38424402279A874BEFC4D4070C16AA6D19824402279A874BEFC4D4070C16AA6D1982440655715C69BF84D40DAC0B8AEA3842440655715C69BF84D40'::geometry,
3035)
);
ALTER TABLE test_07.snapshot_ar5_flate_utvalg ADD PRIMARY KEY (id_ar5);
CREATE INDEX ON test_07.snapshot_ar5_flate_utvalg USING GIST (geo);


CREATE TABLE test_07.snapshot_dmk_flate AS
select a.id_markslag, a.atil, a.geo::geometry(Polygon,3035) FROM tmp_prod_dyrkbarjord.snapshot_dmk_flate a
WHERE ST_Intersects(geo,
ST_Transform(
'0103000020A21000000100000005000000DAC0B8AEA3842440655715C69BF84D40DAC0B8AEA38424402279A874BEFC4D4070C16AA6D19824402279A874BEFC4D4070C16AA6D1982440655715C69BF84D40DAC0B8AEA3842440655715C69BF84D40'::geometry,
3035)
);
ALTER TABLE test_07.snapshot_dmk_flate ADD PRIMARY KEY (id_markslag);
CREATE INDEX ON test_07.snapshot_dmk_flate USING GIST (geo);


CREATE TABLE test_07.snapshot_mdir_flate AS
select a.id_mdir, a.verneform, a.geo::geometry(Polygon,3035) FROM test_07.snapshot_mdir_flate a
WHERE ST_Intersects(geo,
ST_Transform(
'0103000020A21000000100000005000000DAC0B8AEA3842440655715C69BF84D40DAC0B8AEA38424402279A874BEFC4D4070C16AA6D19824402279A874BEFC4D4070C16AA6D1982440655715C69BF84D40DAC0B8AEA3842440655715C69BF84D40'::geometry,
3035)
);
ALTER TABLE test_07.snapshot_mdir_flate ADD PRIMARY KEY (id_mdir);
CREATE INDEX ON test_07.snapshot_mdir_flate USING GIST (geo);


CREATE TABLE test_07.snapshot_ssb_flate_utvalg AS
select a.id_ssb, a.ssbarealbrukhovedklasse, a.geo::geometry(Polygon,3035) FROM test_07.snapshot_ssb_flate_utvalg a
WHERE ST_Intersects(geo,
ST_Transform(
'0103000020A21000000100000005000000DAC0B8AEA3842440655715C69BF84D40DAC0B8AEA38424402279A874BEFC4D4070C16AA6D19824402279A874BEFC4D4070C16AA6D1982440655715C69BF84D40DAC0B8AEA3842440655715C69BF84D40'::geometry,
3035)
);
ALTER TABLE test_07.snapshot_ssb_flate_utvalg ADD PRIMARY KEY (id_ssb);
CREATE INDEX ON test_07.snapshot_ssb_flate_utvalg USING GIST (geo);


CREATE TABLE test_07.raviner_fra_dted_norge_flate AS
select a.id, a.geo::geometry(Polygon,3035) FROM tmp_prod_dyrkbarjord.raviner_fra_dted_norge_flate a
WHERE ST_Intersects(geo,
ST_Transform(
'0103000020A21000000100000005000000DAC0B8AEA3842440655715C69BF84D40DAC0B8AEA38424402279A874BEFC4D4070C16AA6D19824402279A874BEFC4D4070C16AA6D1982440655715C69BF84D40DAC0B8AEA3842440655715C69BF84D40'::geometry,
3035)
);
ALTER TABLE test_07.raviner_fra_dted_norge_flate ADD PRIMARY KEY (id);
CREATE INDEX ON test_07.raviner_fra_dted_norge_flate USING GIST (geo);


CREATE TABLE test_07.raviner_fra_statsforvalter_flate AS
select a.id, a.geo::geometry(Polygon,3035) FROM tmp_prod_dyrkbarjord.raviner_fra_statsforvalter_flate a
WHERE ST_Intersects(geo,
ST_Transform(
'0103000020A21000000100000005000000DAC0B8AEA3842440655715C69BF84D40DAC0B8AEA38424402279A874BEFC4D4070C16AA6D19824402279A874BEFC4D4070C16AA6D1982440655715C69BF84D40DAC0B8AEA3842440655715C69BF84D40'::geometry,
3035)
);
ALTER TABLE test_07.raviner_fra_statsforvalter_flate ADD PRIMARY KEY (id);
CREATE INDEX ON test_07.raviner_fra_statsforvalter_flate USING GIST (geo);

pg_dump -h db01tempdata.nibio.no -U postgres rog_02 -n test_07 > /Users/lop/dev/github/resolve-overlap-and-gap/src/test/sql/regress/inputdata/test_07.sql


*/


/**

SET search_path TO topo_rog_static,public,topology;

*/


SELECT 'n5_forvaltning2022_flate num', count(*) AS num_below_1_m2 FROM test_07.n5_forvaltning2022_flate;
SELECT 'snapshot_dmk_flate num', count(*) AS num_below_1_m2 FROM test_07.snapshot_dmk_flate;
SELECT 'raviner_fra_statsforvalter_flate num', count(*) AS num_below_1_m2 FROM test_07.raviner_fra_statsforvalter_flate;


CALL topo_rog_static.rog_overlay(
ARRAY[
'test_07.n5_forvaltning2022_flate n5',
'test_07.snapshot_ar5_flate_utvalg ar5' ,
'test_07.snapshot_dmk_flate dmk',
'test_07.raviner_fra_statsforvalter_flate ravine'
],
'topo_test_07.result',
4258,
0.0, -- no snapto
1.0, -- min area
200, --max vertx length
false, -- _break_up_big_polygons
NULL, -- split all lines at given vertex num
NULL, -- alarea
    true, -- validate the results
    3,--  Method 1 topology.TopoGeo_addLinestring
  --  Method 2 topology.TopoGeo_LoadGeometry
  --  Method 3 select from tmp toplogy into master topology
1, -- _max_parallel_jobs int, -- this is the max number of paralell jobs to run. There must be at least the same number of free connections
50, -- _max_rows_in_each_cell int, -- this is the max number rows that intersects with box before it's split into 4 new boxes
null
);

SELECT 'topo_test_07 < 1m2 ', count(*), round(sum(ST_area(geom,true))) from topo_test_07.result_v  where ST_Area(geom,true) < 1.0;

SELECT 'topo_test_07 > 1m2 ', count(*), round(sum(ST_area(geom,true))) from topo_test_07.result_v  where ST_Area(geom,true) > 1.0;

SELECT 'topo_test_07 grid', count(*), round(sum(ST_area(cell_bbox,true))) from topo_test_07_topo.result_grid;

SELECT 'topo_test_07 gaps', count(*), round(sum(ST_area(geom,true))) from topo_test_07.result_rogtest_gap ;



SELECT 'topo_test_07 invalid cells', count(*) from topo_test_07.result_grid_validate_topology  ;

SELECT * FROM topology.ValidateTopology('topo_test_07_topo');


/**

SELECT 'degrees', topology.droptopology('topo_test_07');

drop SCHEMA test_07 CASCADE;

*/

/**

SELECT count(*), round(sum(ST_area(geom,true))) from topo_test_07.result_v WHERE (ST_MaximumInscribedCircle(geom)).radius < 1.0e-05;
 count | round
-------+-------
   215 |  2357
(1 row)

Time: 667.414 ms

SELECT count(*), round(sum(ST_area(geom,true))) from topo_test_07.result_v WHERE ST_IsEmpty(ST_Buffer(geom,-1.0e-05));
 count | round
-------+-------
   215 |  2357
(1 row)

Time: 90.701 ms


SELECT count(*), round(sum(ST_area(geom,true))) from topo_test_07.result_v WHERE ST_IsEmpty(ST_Buffer(geom,-1.0e-05,'quad_segs=1'));
 count | round
-------+-------
   215 |  2357
(1 row)

Time: 87.202 ms


SELECT count(*), round(sum(ST_area(geom,true))) from topo_test_07.result_v WHERE ST_IsEmpty(ST_Buffer(geom::geography,-1,'quad_segs=1')::geometry);
count | round
-------+-------
   234 |  2697
(1 row)

Time: 182.644 ms


SELECT ST_AsText(ST_Buffer('POINT (10.061995275172155 59.716094977883)'::geography,1,'quad_segs=4'))

SELECT ST_AsText(ST_Expand('POINT (10.061995275172155 59.716094977883)'::geography,1));

SELECT ST_Length(ST_Boundary(ST_Envelope(((ST_Buffer('POINT (10.061995275172155 59.716094977883)'::geography,200,'quad_segs=1'))::geometry))),true)/9;


SELECT ST_Area(((ST_Buffer('POINT (10.061995275172155 59.716094977883)'::geography,2,'quad_segs=1'))::geometry),true)/4.0;

SELECT count(*), round(sum(ST_area(geom,true))) , ST_IsEmpty(ST_Buffer(geom,-1.0e-05))  is_empty
from topo_test_07.result_v
WHERE ST_Perimeter(geom) <= 0.008043847290534064
GROUP BY is_empty;

WHERE ST_Perimeter(geom) <= 0.008043847290534064
GROUP BY is_empty;
 count | round  | is_empty
-------+--------+----------
   460 | 698258 | f
   215 |   2357 | t
(2 rows)

Time: 55.527 ms


SELECT count(*), round(sum(ST_area(geom,true))) , ST_IsEmpty(ST_Buffer(geom,-1.0e-05))  is_empty
from topo_test_07.result_v
WHERE ST_Perimeter(geom) <= 0.008043847290534064 AND (ST_MaximumInscribedCircle(geom)).radius < 1.0e-05
GROUP BY is_empty;

GROUP BY is_empty;
 count | round | is_empty
-------+-------+----------
   215 |  2357 | t
(1 row)

Time: 577.770 ms

0103000020A21000000100000005000000EABD3ED8BD1F24403C6B0F00A9DB4D40EABD3ED8BD1F24402279A874BEFC4D409CC2CE952DC124402279A874BEFC4D409CC2CE952DC124403C6B0F00A9DB4D40EABD3ED8BD1F24403C6B0F00A9DB4D40

select ST_Buffer('0103000020A21000000100000005000000EABD3ED8BD1F24403C6B0F00A9DB4D40EABD3ED8BD1F24402279A874BEFC4D409CC2CE952DC124402279A874BEFC4D409CC2CE952DC124403C6B0F00A9DB4D40EABD3ED8BD1F24403C6B0F00A9DB4D40',-1.0e-05)

*/