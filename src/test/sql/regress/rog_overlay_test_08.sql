set client_min_messages to ERROR;
set timezone to utc;

/**

'prosj_arealregnskap.gsk_2022_flate_utenoverlapp'

select ST_union(cell_bbox) FROM topo_arealregnskap_t2_test_01.gsk_2022_flate_utenoverlapp_grid where id in (422);

create schema test_08;

CREATE TABLE test_08.n5_forvaltning2022_flate AS
select a.objid, a.kommunenummer, a.geo  FROM sk_grl.n5_forvaltning2022_flate a
WHERE ST_Intersects(geo,
ST_Buffer('0103000020A210000001000000050000004C869992877024401A05DDA849DD4D404C86999287702440D4535D1D6CDD4D40833CC6F37D712440D4535D1D6CDD4D40833CC6F37D7124401A05DDA849DD4D404C869992877024401A05DDA849DD4D40',0.001)::geometry
);
ALTER TABLE test_08.n5_forvaltning2022_flate ADD PRIMARY KEY (objid);
CREATE INDEX ON test_08.n5_forvaltning2022_flate USING GIST (geo);


CREATE TABLE test_08.gsk_2022_flate_utenoverlapp AS
select a.gid, a.class_number, a.geo::geometry(Polygon,4258) FROM prosj_arealregnskap.gsk_2022_flate_utenoverlapp a
WHERE ST_CoveredBy(geo,
ST_Buffer('0103000020A210000001000000050000004C869992877024401A05DDA849DD4D404C86999287702440D4535D1D6CDD4D40833CC6F37D712440D4535D1D6CDD4D40833CC6F37D7124401A05DDA849DD4D404C869992877024401A05DDA849DD4D40',0.001)::geometry
);

ALTER TABLE test_08.gsk_2022_flate_utenoverlapp ADD PRIMARY KEY (gid);
CREATE INDEX ON test_08.gsk_2022_flate_utenoverlapp USING GIST (geo);


SET search_path TO topo_rog_static,public,topology;

pg_dump -h vroom5.int.nibio.no -U postgres rog_01 -n test_08 > /Users/lop/dev/github/resolve-overlap-and-gap/src/test/sql/regress/inputdata/test_08.sql

*/



SELECT 'test_08.gsk_2022_flate_utenoverlapp num', count(*) AS num_below_1_m2 FROM test_08.gsk_2022_flate_utenoverlapp;


CALL topo_rog_static.rog_overlay(
ARRAY[
'test_08.n5_forvaltning2022_flate',
'test_08.gsk_2022_flate_utenoverlapp'
],
'topo_test_08',
4258,
0.0,  -- tolerance
1.0, -- min area
NULL, -- split all lines at given vertex num
true, -- _break_up_big_polygons
1.0, -- min negative buffer value
ARRAY[(NULL,'topo')::rog_input_boundary],
true,
1, -- _max_parallel_jobs int, -- this is the max number of paralell jobs to run. There must be at least the same number of free connections
1000, -- _max_rows_in_each_cell int, -- this is the max number rows that intersects with box before it's split into 4 new boxes
null
);

-- No rows returned on on local mac
-- POSTGIS="3.3.3 2355e8e" [EXTENSION] PGSQL="140" GEOS="3.12.0-CAPI-1.18.0" PROJ="9.2.1" LIBXML="2.11.4" LIBJSON="0.16" LIBPROTOBUF="1.4.1" WAGYU="0.5.0 (Internal)" TOPOLOGY
-- But one row returned in CI https://gitlab.com/nibioopensource/resolve-overlap-and-gap/-/jobs/5529190672
-- SELECT 'topo_test_08 < 1m2 ', count(*), round(sum(ST_area(geom,true))) from topo_test_08.multi_input_2_v  where ST_Area(geom,true) < 1.0;

SELECT 'topo_test_08 < 1m2 ', t02_class_number, count(*), round(sum(ST_area(geom,true))) from topo_test_08.multi_input_2_v
where ST_Area(geom,true) < 1.0
group by t02_class_number
order by t02_class_number;

SELECT 'topo_test_08 > 1m2 ', t02_class_number, count(*), round(sum(ST_area(geom,true))) from topo_test_08.multi_input_2_v
where ST_Area(geom,true) > 1.0
group by t02_class_number
order by t02_class_number;


SELECT 'topo_test_08 grid', count(*), round(sum(ST_area(cell_bbox,true))) from topo_test_08_topo.multi_input_2_grid ;

SELECT 'topo_test_08 invalid cells', count(*) from topo_test_08_topo.multi_input_2_grid_validate_topology  ;

SELECT * FROM topology.ValidateTopology('topo_test_08_topo');



-- Test a single layer as input

CALL topo_rog_static.rog_overlay(
ARRAY[
'test_08.gsk_2022_flate_utenoverlapp'
],
'topo_test_08_b',
4258,
0.0,  -- tolerance
1.0, -- min area
NULL, -- split all lines at given vertex num
true, -- _break_up_big_polygons
1.0, -- min negative buffer value
ARRAY[(NULL,'topo')::rog_input_boundary],
true,
1, -- _max_parallel_jobs int, -- this is the max number of paralell jobs to run. There must be at least the same number of free connections
1000, -- _max_rows_in_each_cell int, -- this is the max number rows that intersects with box before it's split into 4 new boxes
null
);

SELECT 'topo_test_08_b < 1m2 ', count(*), t01_class_number, round(sum(ST_area(geom,true))) from topo_test_08_b.multi_input_1_v
where ST_Area(geom,true) < 1.0
group by t01_class_number
order by t01_class_number;


SELECT 'topo_test_08_b > 1m2 ', count(*), t01_class_number, round(sum(ST_area(geom,true))) from topo_test_08_b.multi_input_1_v
where ST_Area(geom,true) > 1.0
group by t01_class_number
order by t01_class_number;

SELECT 'topo_test_08_b grid', count(*), round(sum(ST_area(cell_bbox,true))) from topo_test_08_b_topo.multi_input_1_grid  ;

SELECT 'topo_test_08_b invalid cells', count(*) from topo_test_08_b_topo.multi_input_1_grid_validate_topology  ;

SELECT * FROM topology.ValidateTopology('topo_test_08_b_topo');


SELECT 'drop top topo_test_08_topo', topology.droptopology('topo_test_08_topo');

Drop schema topo_test_08 cascade ;


SELECT 'drop top topo_test_08_b_topo', topology.droptopology('topo_test_08_b_topo');

Drop schema topo_test_08_b cascade ;

Drop schema test_08 cascade ;


