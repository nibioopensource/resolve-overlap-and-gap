set client_min_messages to ERROR;
set timezone to utc;

/**

select ST_Envelope(ST_Union(geom)) FROM tmp_prod_dyrkbarjord_02.overlay_utvalg_for_union_grp_table_v2 where id in (440197,438770,438853,438773);

select ST_Envelope(ST_Union(geom)) FROM tmp_prod_dyrkbarjord_02.overlay_utvalg_for_union_grp_table_v2 where id in (520858,514727,514726);

select ST_Envelope(ST_Union(geom)) FROM tmp_prod_dyrkbarjord_02.overlay_utvalg_for_union_grp_table_v2 where id in (1125810,1131367,1125807);

ST_CoveredBy(geom,
ST_Buffer('0103000020A21000000100000005000000A3447139845928405A755AF87C6A4E40A3447139845928400D928BADE26B4E402D5235B2AA6428400D928BADE26B4E402D5235B2AA6428405A755AF87C6A4E40A3447139845928405A755AF87C6A4E40',0.001)::geometry


0103000020A21000000100000005000000F8B7C30E39A42540250F58A65C5C4E40F8B7C30E39A425407187BF24A1F94F40608568E5DD9728407187BF24A1F94F40608568E5DD972840250F58A65C5C4E40F8B7C30E39A42540250F58A65C5C4E40

select ST_Envelope(ST_Buffer(ST_Envelope(ST_Union(geom)),0.1))
FROM tmp_prod_dyrkbarjord_02.overlay_utvalg_for_union_grp_table_v2
WHERE id in
(440197,438770,438853,438773,520858,514727,514726,1125810,1131367,1125807)

create schema test_09;


CREATE TABLE test_09.overlay_utvalg_grouped AS
select t1.id, t1.t01_kommunenummer,t1.overflatedyrka_klasse, t1.vernform_klasse, t1.jord_klasse, t1.myr_klasse, t1.ravine_klasse, t1.endret_etter_2008,
t1.geom::geometry(Polygon,4258) geom
FROM tmp_prod_dyrkbarjord_02.overlay_utvalg_for_union_grp_table_v2 t1
WHERE geom &&
ST_Buffer('0103000020A21000000100000005000000999A5E527F4C2640F2DB247329694E40999A5E527F4C2640DCBFBCBE576A4E40268BE1310C562640DCBFBCBE576A4E40268BE1310C562640F2DB247329694E40999A5E527F4C2640F2DB247329694E40',0.001)::geometry
OR
ST_CoveredBy(geom,
ST_Buffer('0103000020A21000000100000005000000A3447139845928405A755AF87C6A4E40A3447139845928400D928BADE26B4E402D5235B2AA6428400D928BADE26B4E402D5235B2AA6428405A755AF87C6A4E40A3447139845928405A755AF87C6A4E40',0.001)::geometry
)OR
ST_CoveredBy(geom,
ST_Buffer('0103000020A210000001000000050000002BEBF6416CD725400025B3AE83EB4F402BEBF6416CD72540A4BAF257D4EC4F402977338DE3E52540A4BAF257D4EC4F402977338DE3E525400025B3AE83EB4F402BEBF6416CD725400025B3AE83EB4F40',0.001)::geometry
);



ALTER TABLE test_09.overlay_utvalg_grouped ADD PRIMARY KEY (id);
CREATE INDEX ON test_09.overlay_utvalg_grouped USING GIST (geom);

pg_dump -h vroom5.int.nibio.no -U postgres rog_01 -n test_09 > /Users/lop/dev/github/resolve-overlap-and-gap/src/test/sql/regress/inputdata/test_09.sql

sh src/test/sql/regress/run_resolve_test.sh  ./src/test/sql/regress/rog_overlay_test_09.sql; \
cat /tmp/pgis_reg/test_3_diff

CREATE database rog_01 TEMPLATE rmp_build_tiltak_layers

*/


--DELETE FROM test_09.overlay_utvalg_grouped WHERE id NOT IN (1125809,1126675);

SELECT 'test_09.overlay_utvalg_grouped num', count(*) AS num_below_1_m2 FROM test_09.overlay_utvalg_grouped;


CALL topo_rog_static.rog_overlay(
ARRAY[
'test_09.overlay_utvalg_grouped'
],
'topo_test_09',
4258,
0.0,  -- tolerance
10.0, -- min area
NULL, -- split all lines at given vertex num
false, -- _break_up_big_polygons
1.0, -- min negative buffer value
ARRAY[('0103000020A21000000100000005000000F8B7C30E39A42540250F58A65C5C4E40F8B7C30E39A425407187BF24A1F94F40608568E5DD9728407187BF24A1F94F40608568E5DD972840250F58A65C5C4E40F8B7C30E39A42540250F58A65C5C4E40','topo')::rog_input_boundary],
true,
1, -- _max_parallel_jobs int, -- this is the max number of paralell jobs to run. There must be at least the same number of free connections
100, -- _max_rows_in_each_cell int, -- this is the max number rows that intersects with box before it's split into 4 new boxes
null
);

SELECT 'topo_test_09 < 1m2 ', "1-pk-id-test_09.overlay_utvalg_grouped", count(*), round(sum(ST_area(geom,true))) from topo_test_09.multi_input_1_v
where ST_Area(geom,true) < 1.0
group by "1-pk-id-test_09.overlay_utvalg_grouped"
order by "1-pk-id-test_09.overlay_utvalg_grouped";

SELECT 'topo_test_09 > 1m2 ', "1-pk-id-test_09.overlay_utvalg_grouped", count(*), round(sum(ST_area(geom,true))) from topo_test_09.multi_input_1_v
where ST_Area(geom,true) > 1.0
group by "1-pk-id-test_09.overlay_utvalg_grouped"
order by "1-pk-id-test_09.overlay_utvalg_grouped";


SELECT * FROM topology.ValidateTopology('topo_test_09_topo');

--0103000020A21000000100000005000000999A5E527F4C2640F2DB247329694E40999A5E527F4C2640DCBFBCBE576A4E40268BE1310C562640DCBFBCBE576A4E40268BE1310C562640F2DB247329694E40999A5E527F4C2640F2DB247329694E40

/**
SELECT 'drop top topo_test_09_topo', topology.droptopology('topo_test_09_topo');

Drop schema topo_test_09 cascade ;

Drop schema test_09 cascade ;
*/

/**

brew services restart postgresql@14; \
rm -fr /tmp/pgis_reg; \
sleep 1; \
echo '' > /opt/homebrew/var/log/postgresql@14.log; \
make clean; \
make; \
sh src/test/sql/regress/run_resolve_test.sh --nodrop  ./src/test/sql/regress/rog_overlay_test_09.sql; \
/opt/homebrew/bin/psql postgres -c"drop database if exists rog_t1;" -c"CREATE database rog_t1 TEMPLATE nibio_reg;"; \
cat /tmp/pgis_reg/test_3_diff;

*/



