set client_min_messages to ERROR;
set timezone to utc;

/**


'org_gronnstruktur_fkb.bygg_arsvers_23',
'org_gronnstruktur_fkb.gsk_2023_test_segmenter_renska'



select ST_Envelope(ST_Buffer(ST_Envelope(ST_Union(geo)),60)) FROM org_gronnstruktur_fkb.gsk_2023_test_segmenter_renska where id in (4690599);


create schema test_10;

CREATE TABLE test_10.gsk_testrun_segmenter_renska AS
select t1.id, t1.frafil, t1.fkb, t1.clname,
t1.geo::geometry(Polygon,25833) geo
FROM org_gronnstruktur_fkb.gsk_2023_test_segmenter_renska t1
WHERE ST_Intersects(geo,
'0103000020E96400000100000005000000000000000872084100000000F04159410000000008720841000000002542594100000000007A0841000000002542594100000000007A084100000000F0415941000000000872084100000000F0415941'::geometry
);

ALTER TABLE test_10.gsk_testrun_segmenter_renska ADD PRIMARY KEY (id);
CREATE INDEX ON test_10.gsk_testrun_segmenter_renska USING GIST (geo);


CREATE TABLE test_10.bygg_arsvers_23 AS
select t1.id, t1.komid,
t1.geo::geometry(Polygon,25833) geo
FROM org_gronnstruktur_fkb.bygg_arsvers_23 t1
WHERE ST_Intersects(geo,
'0103000020E96400000100000005000000000000000872084100000000F04159410000000008720841000000002542594100000000007A0841000000002542594100000000007A084100000000F0415941000000000872084100000000F0415941'::geometry
);

ALTER TABLE test_10.bygg_arsvers_23 ADD PRIMARY KEY (id);
CREATE INDEX ON test_10.bygg_arsvers_23 USING GIST (geo);



pg_dump -h vroom5.int.nibio.no -U postgres rog_01 -n test_10 > /Users/lop/dev/github/resolve-overlap-and-gap/src/test/sql/regress/inputdata/test_10.sql

sh src/test/sql/regress/run_resolve_test.sh  ./src/test/sql/regress/rog_overlay_test_10.sql; \
cat /tmp/pgis_reg/test_3_diff

CREATE database rog_01 TEMPLATE rmp_build_tiltak_layers

*/


SELECT 'test_10.gsk_testrun_segmenter_renska num', count(*) AS num_below_1_m2 FROM test_10.gsk_testrun_segmenter_renska;


CALL topo_rog_static.rog_overlay(
ARRAY[
'test_10.bygg_arsvers_23',
'test_10.gsk_testrun_segmenter_renska'
],
'topo_test_10',
4258,
0.0,  -- tolerance
10.0, -- min area
200, -- split all lines at given vertex num
true, -- _break_up_big_polygons
NULL, -- min negative buffer value
NULL,
true,
1, -- _max_parallel_jobs int, -- this is the max number of paralell jobs to run. There must be at least the same number of free connections
100, -- _max_rows_in_each_cell int, -- this is the max number rows that intersects with box before it's split into 4 new boxes
null,
ARRAY[
  NULL -- no cleanup for test_10.bygg_arsvers_23
  ,
  rog_clean_edge_type_func(
    0.00003,
    NULL,
    ('clname','{treeLayer,fieldLayer,trees,bushLayer,FKB_bygg,greyArea}', '{0.00001,0.00001,0.00001,0.00001,0.00001,0.000025}')::rog_clean_simplify_attribute_values
  )
]
);

/**
          'clname', --09
          '{treeLayer,fieldLayer,greyArea,trees,bushLayer,FKB_bygg}',
          '{0.00001,0.00001,0.00001,0.00001,0.00001,0.00002}'

          CREATE TYPE rog_clean_simplify_attribute_values AS (
  attribute_name text,
  attribute_values text[],
  simplify_tolerances float[]
);

*/



SELECT 'topo_test_10 < 1m2 ', "2-pk-id-test_10.gsk_testrun_segmenter_renska", count(*), round(sum(ST_area(geom,true))) from topo_test_10.multi_input_2_v
where ST_Area(geom,true) < 1.0
group by "2-pk-id-test_10.gsk_testrun_segmenter_renska"
order by "2-pk-id-test_10.gsk_testrun_segmenter_renska";

SELECT 'topo_test_10 > 1m2  sum', count(*), round(sum(ST_area(geom,true))) from topo_test_10.multi_input_2_v
WHERE "2-pk-id-test_10.gsk_testrun_segmenter_renska" IS NOT NULL AND ST_Area(geom,true) > 1.0;

SELECT 'topo_test_10 > 1m2 ', "2-pk-id-test_10.gsk_testrun_segmenter_renska", count(*), round(sum(ST_area(geom,true))) from topo_test_10.multi_input_2_v
where ST_Area(geom,true) > 1.0
group by "2-pk-id-test_10.gsk_testrun_segmenter_renska"
order by "2-pk-id-test_10.gsk_testrun_segmenter_renska";


SELECT 'input simple feature num polygons:'||count(*), ' num vertexes:'||sum(st_npoints(geo)) FROM test_10.gsk_testrun_segmenter_renska;
SELECT 'output simple feature num polygons:'||count(*), ' num vertexes:'||sum(st_npoints(geom)) FROM topo_test_10.multi_input_2_v;


SELECT * FROM topology.ValidateTopology('topo_test_10_topo');


/**
SELECT 'drop top topo_test_10_topo', topology.droptopology('topo_test_10_topo');

Drop schema topo_test_10 cascade ;

Drop schema test_10 cascade ;
*/

/**

brew services restart postgresql@14; \
rm -fr /tmp/pgis_reg; \
sleep 1; \
echo '' > /opt/homebrew/var/log/postgresql@14.log; \
make clean; \
make; \
sh src/test/sql/regress/run_resolve_test.sh --nodrop  ./src/test/sql/regress/rog_overlay_test_10.sql; \
/opt/homebrew/bin/psql postgres -c"drop database if exists rog_t1;" -c"CREATE database rog_t1 TEMPLATE nibio_reg;"; \
cat /tmp/pgis_reg/test_3_diff;



*/

