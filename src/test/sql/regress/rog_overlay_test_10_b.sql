set client_min_messages to ERROR;
set timezone to utc;


-- a test file with https://gitlab.com/nibioopensource/resolve-overlap-and-gap/uploads/352d900a4f5e66baccc00407a14d85e6/test_10_b.sql.gz
alter table test_10.gsk_2023_test_segmenter_renska rename to gsk_testrun_segmenter_renska;

SELECT 'test_10.gsk_testrun_segmenter_renska num', count(*) AS num_below_1_m2 FROM test_10.gsk_testrun_segmenter_renska;

SELECT 'topo_test_10 input > 1m2  sum', id, round(sum(ST_area(geo))) from test_10.gsk_testrun_segmenter_renska
WHERE ST_Area(geo) > 10
group by id
order by id;


CALL topo_rog_static.rog_overlay(
ARRAY[
'test_10.gsk_testrun_segmenter_renska'
],
'topo_test_10',
4258,
0.0,  -- tolerance
10.0, -- min area
200, -- split all lines at given vertex num
true, -- _break_up_big_polygons
NULL, -- min negative buffer value
NULL,
true,
4, -- _max_parallel_jobs int, -- this is the max number of paralell jobs to run. There must be at least the same number of free connections
10, -- _max_rows_in_each_cell int, -- this is the max number rows that intersects with box before it's split into 4 new boxes
null,
null
);


/**
Some tests from a mac pro

1, -- _max_parallel_jobs int, -- this is the max number of paralell jobs to run. There must be at least the same number of free connections
100, -- _max_rows_in_each_cell int, -- this is the max number rows that intersects with box before it's split into 4 new boxes
**********

NULL, -- split all lines at given vertex num
false, -- _break_up_big_polygons
sh src/test/sql/regress/run_resolve_test.sh --nodrop   0.22s user 0.25s system 0% cpu 21:59.73 total

200, -- split all lines at given vertex num
false, -- _break_up_big_polygons
sh src/test/sql/regress/run_resolve_test.sh --nodrop   0.22s user 0.26s system 0% cpu 1:36.56 total

200, -- split all lines at given vertex num
true, -- _break_up_big_polygons
sh src/test/sql/regress/run_resolve_test.sh --nodrop   0.22s user 0.26s system 0% cpu 1:37.49 total

TRUE, -- split all lines at given vertex num
true, -- _break_up_big_polygons
sh src/test/sql/regress/run_resolve_test.sh --nodrop   0.22s user 0.27s system 0% cpu 22:18.91 total
-------------

1, -- _max_parallel_jobs int, -- this is the max number of paralell jobs to run. There must be at least the same number of free connections
25, -- _max_rows_in_each_cell int, -- this is the max number rows that intersects with box before it's split into 4 new boxes
***********@

NULL, -- split all lines at given vertex num
false, -- _break_up_big_polygons
sh src/test/sql/regress/run_resolve_test.sh --nodrop   0.22s user 0.25s system 0% cpu 8:19.68 total

200, -- split all lines at given vertex num
false, -- _break_up_big_polygons
sh src/test/sql/regress/run_resolve_test.sh --nodrop   0.22s user 0.26s system 0% cpu 1:19.64 total

200, -- split all lines at given vertex num
true, -- _break_up_big_polygons
sh src/test/sql/regress/run_resolve_test.sh --nodrop   0.23s user 0.27s system 0% cpu 52.099 total

NULL, -- split all lines at given vertex num
true, -- _break_up_big_polygons
sh src/test/sql/regress/run_resolve_test.sh --nodrop   0.22s user 0.25s system 0% cpu 2:29.12 total
-------------


4, -- _max_parallel_jobs int, -- this is the max number of paralell jobs to run. There must be at least the same number of free connections
25, -- _max_rows_in_each_cell int, -- this is the max number rows that intersects with box before it's split into 4 new boxes
***********@

200, -- split all lines at given vertex num
true, -- _break_up_big_polygons
sh src/test/sql/regress/run_resolve_test.sh --nodrop   0.22s user 0.28s system 1% cpu 45.075 total
-------------

*/

SELECT 'topo_test_10 < 1m2 ', "1-pk-id-test_10.gsk_testrun_segmenter_renska", count(*), round(sum(ST_area(geom,true))) from topo_test_10.multi_input_1_v
where ST_Area(geom,true) < 1.0
group by "1-pk-id-test_10.gsk_testrun_segmenter_renska"
order by "1-pk-id-test_10.gsk_testrun_segmenter_renska";

SELECT 'topo_test_10 > 1m2  sum', "1-pk-id-test_10.gsk_testrun_segmenter_renska", round(sum(ST_area(geom,true))) from topo_test_10.multi_input_1_v
WHERE "1-pk-id-test_10.gsk_testrun_segmenter_renska" IS NOT NULL AND ST_Area(geom,true) >= 1.00
group by "1-pk-id-test_10.gsk_testrun_segmenter_renska"
order by "1-pk-id-test_10.gsk_testrun_segmenter_renska";


/**
SELECT 'topo_test_10 > 1m2 ', "1-pk-id-test_10.gsk_testrun_segmenter_renska", count(*), round(sum(ST_area(geom,true))) from topo_test_10.multi_input_1_v
where ST_Area(geom,true) > 1.0
group by "1-pk-id-test_10.gsk_testrun_segmenter_renska"
order by "1-pk-id-test_10.gsk_testrun_segmenter_renska";


SELECT 'input simple feature num polygons:'||count(*), ' num vertexes:'||sum(st_npoints(geo)) FROM test_10.gsk_testrun_segmenter_renska;
SELECT 'output simple feature num polygons:'||count(*), ' num vertexes:'||sum(st_npoints(geom)) FROM topo_test_10.multi_input_1_v;
*/


SELECT * FROM topology.ValidateTopology('topo_test_10_topo');


/**
SELECT 'drop top topo_test_10_topo', topology.droptopology('topo_test_10_topo');

Drop schema topo_test_10 cascade ;

Drop schema test_10 cascade ;
*/

/**

brew services restart postgresql@14; \
rm -fr /tmp/pgis_reg; \
sleep 1; \
echo '' > /opt/homebrew/var/log/postgresql@14.log; \
make clean; \
make; \
sh src/test/sql/regress/run_resolve_test.sh --nodrop  ./src/test/sql/regress/rog_overlay_test_10.sql; \
/opt/homebrew/bin/psql postgres -c"drop database if exists rog_t1;" -c"CREATE database rog_t1 TEMPLATE nibio_reg;"; \
cat /tmp/pgis_reg/test_3_diff;



*/

