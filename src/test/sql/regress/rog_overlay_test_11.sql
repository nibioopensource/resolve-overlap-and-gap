set client_min_messages to ERROR;
set timezone to utc;

/**


DO
$$DECLARE
input_tables text[] = ARRAY[
  'sk_grl.n5_forvaltning_flate',
  'prod_dyrkbarjord_04.snapshot_ar5_flate',
  'prod_dyrkbarjord_04.snapshot_ssb_flate',
  'prod_dyrkbarjord_04.snapshot_mdir_flate',
  'prod_dyrkbarjord_04.snapshot_dmk_flate',
  'prod_dyrkbarjord_04.snapshot_ravine_dtm10_dmk_flate',
  'prod_dyrkbarjord_04.snapshot_ravine_statsforvalter_flate'
  ];
schema_table_name text;
schema_table_name_srid int;
schema_table_name_geom text = 'geo';
schema_name text;
table_name text;
test_schema_name text = 'test_11';
bbox_grader geometry = 'SRID=4258; POLYGON((15.79665471008581 69.05705819329019,15.79665471008581 69.05735526027637,15.79762019616626 69.05735526027637,15.79762019616626 69.05705819329019,15.79665471008581 69.05705819329019))'::geometry;

BEGIN

  EXECUTE Format($fmt$
  DROP schema %1$s CASCADE;
  CREATE schema %1$s;
  $fmt$,
  test_schema_name
  );

  FOREACH schema_table_name IN ARRAY input_tables LOOP
    schema_name = (string_to_array(schema_table_name, '.')::text[])[1];
    table_name = (string_to_array(schema_table_name, '.')::text[])[2];
    RAISE NOTICE 'table_name %' , schema_table_name;

    EXECUTE Format($fmt$ SELECT Find_SRID(%1$L,%2$L,%3$L) $fmt$,
    schema_name, table_name, schema_table_name_geom) INTO schema_table_name_srid;

    RAISE NOTICE '% schema_table_name_srid %', (Format($fmt$ SELECT Find_SRID(%1$L,%2$L,%3$L) $fmt$,
    schema_name, table_name, schema_table_name_geom)), schema_table_name_srid;


    EXECUTE Format($fmt$
    CREATE TABLE %1$s AS
    select t1.%5$s::geometry(Polygon,%4$s) %5$s
    FROM %2$s t1
    WHERE geo && %3$L;

    ALTER TABLE %1$s ADD column id serial primary key;
    CREATE INDEX ON %1$s USING GIST (%5$s);

    $fmt$,
    test_schema_name||'.'||table_name,
    schema_table_name,
    ST_Transform(bbox_grader,schema_table_name_srid),
    schema_table_name_srid,
    schema_table_name_geom
    );


  END LOOP;
END$$;

pg_dump -h vroom5.int.nibio.no -U postgres rog_01 -n test_11 > /Users/lop/dev/github/resolve-overlap-and-gap/src/test/sql/regress/inputdata/test_11.sql

sh src/test/sql/regress/run_resolve_test.sh  ./src/test/sql/regress/rog_overlay_test_11.sql; \
cat /tmp/pgis_reg/test_3_diff

*/



CALL topo_rog_static.rog_overlay(
ARRAY[
  'test_11.n5_forvaltning_flate',
  'test_11.snapshot_ar5_flate',
  'test_11.snapshot_ssb_flate',
  'test_11.snapshot_mdir_flate',
  'test_11.snapshot_dmk_flate',
  'test_11.snapshot_ravine_dtm10_dmk_flate',
  'test_11.snapshot_ravine_statsforvalter_flate'
],
'topo_test_11',
4258,
0.0,  -- tolerance
1.0, -- min area
NULL, -- split all lines at given vertex num
false, -- _break_up_big_polygons
1.0, -- min negative buffer value
NULL,
true,
1, -- _max_parallel_jobs int, -- this is the max number of paralell jobs to run. There must be at least the same number of free connections
100, -- _max_rows_in_each_cell int, -- this is the max number rows that intersects with box before it's split into 4 new boxes
null
);

SELECT * FROM topology.ValidateTopology('topo_test_11_topo');

SELECT 'num surfaces', count(*) from topo_test_11.multi_input_7_v ;

/**
SELECT 'topo_test_11 < 1m2 ', "1-pk-id-test_11.overlay_utvalg_grouped", count(*), round(sum(ST_area(geom,true))) from topo_test_11.multi_input_1_v
where ST_Area(geom,true) < 1.0
group by "1-pk-id-test_11.overlay_utvalg_grouped"
order by "1-pk-id-test_11.overlay_utvalg_grouped";

SELECT 'topo_test_11 > 1m2 ', "1-pk-id-test_11.overlay_utvalg_grouped", count(*), round(sum(ST_area(geom,true))) from topo_test_11.multi_input_1_v
where ST_Area(geom,true) > 1.0
group by "1-pk-id-test_11.overlay_utvalg_grouped"
order by "1-pk-id-test_11.overlay_utvalg_grouped";
*/


--0103000020A21000000100000005000000999A5E527F4C2640F2DB247329694E40999A5E527F4C2640DCBFBCBE576A4E40268BE1310C562640DCBFBCBE576A4E40268BE1310C562640F2DB247329694E40999A5E527F4C2640F2DB247329694E40

/**
SELECT 'drop top topo_test_11_topo', topology.droptopology('topo_test_11_topo');

Drop schema topo_test_11 cascade ;

Drop schema test_11 cascade ;
*/

/**

brew services restart postgresql@14; \
rm -fr /tmp/pgis_reg; \
sleep 1; \
echo '' > /opt/homebrew/var/log/postgresql@14.log; \
make clean; \
make; \
sh src/test/sql/regress/run_resolve_test.sh --nodrop  ./src/test/sql/regress/rog_overlay_test_11.sql; \
/opt/homebrew/bin/psql postgres -c"drop database if exists rog_t1;" -c"CREATE database rog_t1 TEMPLATE nibio_reg;"; \
cat /tmp/pgis_reg/test_3_diff;

*/



