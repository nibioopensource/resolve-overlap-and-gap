set client_min_messages to ERROR;
set timezone to utc;

set execute_parallel.connstring = 'host=localhost dbname=nibio_reg user=postgres password=testp101';

/**



https://gitlab.com/nibioopensource/resolve-overlap-and-gap/-/merge_requests/65


select ST_envelope(ST_Union(cell_bbox)) from tmp_dyrkbarjord_12_037.dyrkbarjord_05_grid where id in (1078,1097,1077,1096);
--0103000020A21000000100000005000000D26081274FD424406BB4C8C41AA94F40D26081274FD42440485118D382AC4F407AF1452D04F52440485118D382AC4F407AF1452D04F524406BB4C8C41AA94F40D26081274FD424406BB4C8C41AA94F40

metagrid_tables text[] = ARRAY[
  'tmp_dyrkbarjord_12_037.dyrkbarjord_05_grid',
  'tmp_dyrkbarjord_12_037.dyrkbarjord_05_grid_metagrid_0001',
  'tmp_dyrkbarjord_12_037.dyrkbarjord_05_grid_metagrid_0001_lines',
  'tmp_dyrkbarjord_12_037.dyrkbarjord_05_grid_metagrid_0002',
  'tmp_dyrkbarjord_12_037.dyrkbarjord_05_grid_metagrid_0002_lines',
  'tmp_dyrkbarjord_12_037.dyrkbarjord_05_grid_metagrid_0003',
  'tmp_dyrkbarjord_12_037.dyrkbarjord_05_grid_metagrid_0003_lines',
  'tmp_dyrkbarjord_12_037.dyrkbarjord_05_grid_metagrid_0004',
  'tmp_dyrkbarjord_12_037.dyrkbarjord_05_grid_metagrid_0004_lines'
];

DO
$$DECLARE
input_tables text[] = ARRAY[
'sk_grl,n5_forvaltning_flate,geo,;komid',
'prod_dyrkbarjord_05,snapshot_ar5_flate,geo,;artype',
'prod_dyrkbarjord_05,snapshot_ssb_flate,geo,;ssbarealbrukhovedklasse',
'prod_dyrkbarjord_05,snapshot_mdir_flate,geo,;verneform',
'prod_dyrkbarjord_05,snapshot_dmk_flate,geo,;atil',
'prod_dyrkbarjord_05,snapshot_ravine_dtm10_dmk_flate,geo,;ravine',
'prod_dyrkbarjord_05,snapshot_ravine_statsforvalter_flate,geo,;ravine'
];
schema_table_name text;
schema_table_name_srid int;
schema_table_name_geom text;
schema_table_name_columns text;
schema_name text;
table_name text;
test_schema_name text = 'test_12';

bbox_grader geometry = ST_buffer('0103000020A21000000100000005000000D26081274FD424406BB4C8C41AA94F40D26081274FD42440485118D382AC4F407AF1452D04F52440485118D382AC4F407AF1452D04F524406BB4C8C41AA94F40D26081274FD424406BB4C8C41AA94F40'::geometry,0.000001);

grid_metagrid_grader geometry = bbox_grader;
grid_metagrid_grader_tmp geometry;

metagrid_tables text[];

BEGIN

  EXECUTE Format($fmt$
  DROP schema IF EXISTS %1$s CASCADE;
  CREATE schema %1$s;
  $fmt$,
  test_schema_name
  );

  FOREACH schema_table_name IN ARRAY input_tables LOOP
    schema_name = (string_to_array(schema_table_name, ',')::text[])[1];
    table_name = (string_to_array(schema_table_name, ',')::text[])[2];
    schema_table_name_geom = (string_to_array(schema_table_name, ',')::text[])[3];
    schema_table_name_columns = (string_to_array(schema_table_name, ',')::text[])[4];

	schema_table_name_columns =  REPLACE(schema_table_name_columns,';',',');

    RAISE NOTICE 'table_name %' , schema_table_name;
    RAISE NOTICE 'schema_table_name_columns %', schema_table_name_columns;

    EXECUTE Format($fmt$ SELECT Find_SRID(%1$L,%2$L,%3$L) $fmt$,
    schema_name, table_name, schema_table_name_geom) INTO schema_table_name_srid;

    RAISE NOTICE '% schema_table_name_srid %', (Format($fmt$ SELECT Find_SRID(%1$L,%2$L,%3$L) $fmt$,
    schema_name, table_name, schema_table_name_geom)), schema_table_name_srid;

    EXECUTE Format($fmt$
    CREATE TABLE %1$s AS
    select t1.%5$s::geometry(Polygon,%4$s) %5$s %6$s
    FROM %2$s t1
    WHERE ST_CoveredBy(%5$s,%3$L);
    --WHERE ST_Intersects(%5$s,%3$L);

    ALTER TABLE %1$s ADD column id serial primary key;
    CREATE INDEX ON %1$s USING GIST (%5$s);

    $fmt$,
    test_schema_name||'.'||table_name,
    schema_name||'.'||table_name,
    ST_Transform(bbox_grader,schema_table_name_srid),
    schema_table_name_srid,
    schema_table_name_geom,
    schema_table_name_columns
    );

    EXECUTE Format($fmt$
    SELECT ST_transform(ST_Union(%2$s),4258) FROM %1$s
    $fmt$,
    test_schema_name||'.'||table_name,
    schema_table_name_geom
    ) INTO grid_metagrid_grader_tmp;

    IF grid_metagrid_grader_tmp IS NOT NULL THEN
    	grid_metagrid_grader = ST_Union(grid_metagrid_grader,grid_metagrid_grader_tmp);
    END IF;

  END LOOP;

  IF metagrid_tables IS NOT NULL THEN
    FOREACH schema_table_name IN ARRAY metagrid_tables LOOP
    schema_name = (string_to_array(schema_table_name, '.')::text[])[1];
    table_name = (string_to_array(schema_table_name, '.')::text[])[2];
    RAISE NOTICE 'table_name %' , schema_table_name;


    EXECUTE Format($fmt$

    CREATE TABLE %1$s ( like  %2$s);
    INSERT INTO %1$s SELECT * FROM %2$s WHERE ST_Intersects(cell_bbox,%3$L);
    ALTER TABLE %1$s ADD primary key(id);
    CREATE INDEX ON %1$s USING GIST (cell_bbox);

    $fmt$,
    test_schema_name||'.'||table_name,
    schema_table_name,
    bbox_grader
    );
    END LOOP;
  END IF;

END$$;

pg_dump -h cpu11.int.nibio.no -U postgres rog_02 -n test_12 > /Users/lop/dev/github/resolve-overlap-and-gap/src/test/sql/regress/inputdata/test_12.sql

sh src/test/sql/regress/run_resolve_test.sh  ./src/test/sql/regress/rog_overlay_test_12.sql; \
cat /tmp/pgis_reg/test_3_diff

*/

CALL topo_rog_static.rog_overlay(
   ARRAY[
'test_12.n5_forvaltning_flate n5',
'test_12.snapshot_ar5_flate ar5',
'test_12.snapshot_ssb_flate ssb',
'test_12.snapshot_mdir_flate mdir',
'test_12.snapshot_dmk_flate dmk',
'test_12.snapshot_ravine_dtm10_dmk_flate dtm10',
'test_12.snapshot_ravine_statsforvalter_flate ravine'
   ],
   'tmp_dyrkbarjord_12.dyrkbarjord_05',
   4258, --srid
   0, -- no snapto
   10.0, -- min m2 area to keep
   0, --max vertx length
   false, -- _break_up_big_polygons
   1.0, -- min negative buffer value null, remove sliver polygons
   ARRAY[
   ('0103000020A21000000100000005000000D26081274FD424406BB4C8C41AA94F40D26081274FD42440485118D382AC4F407AF1452D04F52440485118D382AC4F407AF1452D04F524406BB4C8C41AA94F40D26081274FD424406BB4C8C41AA94F40','037')::rog_input_boundary::rog_input_boundary
   ]
   , -- run all the area blocks
   true, -- do_qualitycheck_on_final_reseult
   --  Method 1 topology.TopoGeo_addLinestring
   --  Method 2 topology.TopoGeo_LoadGeometry
   --  Method 3 select from tmp toplogy into master topology
   3,
   60, -- _max_parallel_jobs int, -- this is the max number of paralell jobs to run. There must be at least the same number of free connections
   15, -- _max_rows_in_each_cell int, -- this is the max number rows that intersects with box before it's split into 4 new boxes
   NULL
);

/**

-- Will also vary from system to system

SELECT 'topo_test_12 < 10m2 ', count(*), round(sum(ST_area(geom,true)))
FROM tmp_dyrkbarjord_12.dyrkbarjord_05_v
WHERE ST_Area(geom,true) < 10.0;

*/

SELECT 'ar5_artype ',
-- count(*), will vary from system to system
round(sum(ST_area(geom,true))/1000)
FROM tmp_dyrkbarjord_12.dyrkbarjord_05_v
WHERE ar5_artype IS NOT NULL;


-- should be 0
SELECT 'missing_area', st_area(geom,true) from tmp_dyrkbarjord_12_037.dyrkbarjord_05_rogtest_gap;

SELECT 'line_segments_short', count(*) from tmp_dyrkbarjord_12_037.dyrkbarjord_05_border_line_segments_short ;

-- SELECT 'line_segments_final', count(*) from tmp_dyrkbarjord_12_037.dyrkbarjord_05_border_line_segments_final ;

-- find all st_buffer(center) of edges where left_face eq right_face that intersects grid lines
DROP table IF EXISTS tmp_dyrkbarjord_12_037.area_to_check;
CREATE table tmp_dyrkbarjord_12_037.area_to_check AS
SELECT distinct r.small_area area_to_check, r.edge_id
FROM (
SELECT  ST_Buffer(ST_LineInterpolatePoint(ed.geom,0.5),0.000001) small_area, ed.edge_id FROM
tmp_dyrkbarjord_12_037.dyrkbarjord_05_grid_lines grid,
tmp_dyrkbarjord_12_037.edge_data ed
WHERE ed.left_face > 0 AND ST_Intersects(grid.cell_bbox,ed.geom)
AND ed.left_face = ed.right_face
) r;
ALTER table tmp_dyrkbarjord_12_037.area_to_check add column id serial primary key;
CREATE INDEX ON tmp_dyrkbarjord_12_037.area_to_check USING GIST(area_to_check);
--SELECT 'left_face_eq_right_face_to_check', count(*) from tmp_dyrkbarjord_12_037.area_to_check ;

-- find all st_buffer(center) of edges where left_face eq right_face that intersects grid lines
-- that are not havig a left face eq right face close by
-- or are not related to a dropped edge because for instance a small area
DROP table IF EXISTS tmp_dyrkbarjord_12_037.error_line_fleft_eq_right;
CREATE table tmp_dyrkbarjord_12_037.error_line_fleft_eq_right AS
SELECT distinct r.small_area error_line_fleft_eq_right, r.edge_id, r.edge_geom
FROM (
SELECT  ST_Buffer(ST_LineInterpolatePoint(ed.geom,0.5),0.000001) small_area, ed.edge_id, ed.geom edge_geom FROM
tmp_dyrkbarjord_12_037.dyrkbarjord_05_grid_lines grid,
tmp_dyrkbarjord_12_037.edge_data ed
WHERE ed.left_face > 0 AND ST_Intersects(grid.cell_bbox,ed.geom)
AND ed.left_face = ed.right_face
) r
WHERE NOT EXISTS (SELECT TRUE FROM tmp_dyrkbarjord_12_037.edge_data e WHERE ST_Intersects(r.small_area,e.geom) AND e.edge_id != r.edge_id AND e.left_face != e.right_face AND e.left_face > 0 AND e.right_face > 0)
AND NOT EXISTS (SELECT TRUE FROM tmp_dyrkbarjord_12_037.dyrkbarjord_05_edges_to_remove er WHERE  ST_Intersects(r.edge_geom,er.geo));
ALTER table tmp_dyrkbarjord_12_037.error_line_fleft_eq_right add column id serial primary key;
CREATE INDEX ON tmp_dyrkbarjord_12_037.error_line_fleft_eq_right USING GIST(error_line_fleft_eq_right);
SELECT 'left_face_eq_right_face_error', count(*) from tmp_dyrkbarjord_12_037.error_line_fleft_eq_right ;

DROP table IF EXISTS tmp_dyrkbarjord_12_037.border_line_segments_final;
CREATE table tmp_dyrkbarjord_12_037.border_line_segments_final AS
SELECT
cluster, array_agg(table_input_order) table_input_order_list,
array_agg(src_table_pk_column_value) src_table_pk_column_value_list,
array_agg(id) id_list, ST_LineMerge(ST_Union(geom)) geos_list
  FROM (
    SELECT id, table_input_order, src_table_pk_column_value, geom,
    ST_ClusterIntersectingWin(geom) OVER () AS cluster
    FROM tmp_dyrkbarjord_12_037.dyrkbarjord_05_border_line_segments_final AS b
) r  group by cluster;

ALTER table tmp_dyrkbarjord_12_037.border_line_segments_final add column id serial primary key;
CREATE INDEX ON tmp_dyrkbarjord_12_037.border_line_segments_final USING GIST(geos_list);
SELECT 'border_line_segments_final', count(*) from tmp_dyrkbarjord_12_037.border_line_segments_final ;





-- but sometime it fails like this
/**

missing_area|50.6710274991092
missing_area|15700.3700019417
missing_area|33039.5121226062
missing_area|185428.64694124
missing_area|1089.35502443065
missing_area|1675.15169605278
missing_area|70894.4020513969


*/


/**

SELECT * FROM topology.ValidateTopology('tmp_dyrkbarjord_12_037');

SELECT 'num surfaces total', count(*) from tmp_dyrkbarjord_12_037.multi_input_1 ;

SELECT 'null id', count(*) from tmp_dyrkbarjord_12_037.multi_input_1 WHERE "1-pk-id-test_12.nyesegmenter_testomrader" IS NULL;

SELECT 'num surfaces with value from simple feature', count(*) from skog_maska_01.multi_input_1_v ;

SELECT 'num rows in primary key face mapping table', count(*) from tmp_dyrkbarjord_12_037.multi_input_1_org_pk_value_to_face_id ;


*/

--clean up

SELECT 'degrees', topology.droptopology('tmp_dyrkbarjord_12_037');
drop SCHEMA tmp_dyrkbarjord_12 CASCADE;
drop SCHEMA test_12 CASCADE;
