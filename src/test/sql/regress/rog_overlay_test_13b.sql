set client_min_messages to ERROR;
set timezone to utc;

/**


https://gitlab.com/nibioopensource/resolve-overlap-and-gap/-/issues/77



select ST_envelope(ST_Union(geom)) from gronn_2023_v9_area4_004.multi_input_2 where face_id in ('4442585')
-- 0103000020A2100000010000000500000004E1F3EDF93715402E48E4ECCA374E4004E1F3EDF93715408DADEE15CC374E40C7B338B6083815408DADEE15CC374E40C7B338B6083815402E48E4ECCA374E4004E1F3EDF93715402E48E4ECCA374E40

DO
$$DECLARE
input_tables text[] = ARRAY[
'org_gronnstruktur_fkb,bygg_arsvers_23_v2,geom,',
'org_gronnstruktur_fkb,gsk_2023_segmenter_renska,geo,;clname'
];
schema_table_name text;
schema_table_name_srid int;
schema_table_name_geom text;
schema_table_name_columns text;
schema_name text;
table_name text;
test_schema_name text = 'test_13';

grid_metagrid_grader geometry = '0103000020A2100000010000000500000004E1F3EDF93715402E48E4ECCA374E4004E1F3EDF93715408DADEE15CC374E40C7B338B6083815408DADEE15CC374E40C7B338B6083815402E48E4ECCA374E4004E1F3EDF93715402E48E4ECCA374E40'::geometry;

test_data_grader geometry = ST_buffer(grid_metagrid_grader::geometry,0.0001);





metagrid_tables text[] = ARRAY[
  'gronn_2023_v9_004.multi_input_2_grid',
  'gronn_2023_v9_004.multi_input_2_grid_metagrid_0001',
  'gronn_2023_v9_004.multi_input_2_grid_metagrid_0001_lines',
  'gronn_2023_v9_004.multi_input_2_grid_metagrid_0002',
  'gronn_2023_v9_004.multi_input_2_grid_metagrid_0002_lines',
  'gronn_2023_v9_004.multi_input_2_grid_metagrid_0003',
  'gronn_2023_v9_004.multi_input_2_grid_metagrid_0003_lines',
  'gronn_2023_v9_004.multi_input_2_grid_metagrid_0004',
  'gronn_2023_v9_004.multi_input_2_grid_metagrid_0004_lines',
  'gronn_2023_v9_004.multi_input_2_grid_metagrid_0005',
  'gronn_2023_v9_004.multi_input_2_grid_metagrid_0005_lines'
];

BEGIN

  EXECUTE Format($fmt$
  DROP schema IF EXISTS %1$s CASCADE;
  CREATE schema %1$s;
  $fmt$,
  test_schema_name
  );

  FOREACH schema_table_name IN ARRAY input_tables LOOP
    schema_name = (string_to_array(schema_table_name, ',')::text[])[1];
    table_name = (string_to_array(schema_table_name, ',')::text[])[2];
    schema_table_name_geom = (string_to_array(schema_table_name, ',')::text[])[3];
    schema_table_name_columns = (string_to_array(schema_table_name, ',')::text[])[4];

		schema_table_name_columns =  REPLACE(schema_table_name_columns,';',',');

    RAISE NOTICE 'table_name %' , schema_table_name;
    RAISE NOTICE 'schema_table_name_columns %', schema_table_name_columns;

    EXECUTE Format($fmt$ SELECT Find_SRID(%1$L,%2$L,%3$L) $fmt$,
    schema_name, table_name, schema_table_name_geom) INTO schema_table_name_srid;

    RAISE NOTICE '% schema_table_name_srid %', (Format($fmt$ SELECT Find_SRID(%1$L,%2$L,%3$L) $fmt$,
    schema_name, table_name, schema_table_name_geom)), schema_table_name_srid;


    EXECUTE Format($fmt$
    CREATE TABLE %1$s AS
    select t1.%5$s::geometry(Polygon,%4$s) %5$s %6$s
    FROM %2$s t1
    WHERE ST_CoveredBy(%5$s,%3$L);

    ALTER TABLE %1$s ADD column id serial primary key;
    CREATE INDEX ON %1$s USING GIST (%5$s);

    $fmt$,
    test_schema_name||'.'||table_name,
    schema_name||'.'||table_name,
    ST_Transform(test_data_grader,schema_table_name_srid),
    schema_table_name_srid,
    schema_table_name_geom,
    schema_table_name_columns
    );
  END LOOP;


  FOREACH schema_table_name IN ARRAY metagrid_tables LOOP
    schema_name = (string_to_array(schema_table_name, '.')::text[])[1];
    table_name = (string_to_array(schema_table_name, '.')::text[])[2];
    RAISE NOTICE 'table_name %' , schema_table_name;


    EXECUTE Format($fmt$

    CREATE TABLE %1$s ( like  %2$s);
    INSERT INTO %1$s SELECT * FROM %2$s WHERE ST_Intersects(cell_bbox,%3$L);
    ALTER TABLE %1$s ADD primary key(id);
    CREATE INDEX ON %1$s USING GIST (cell_bbox);

    $fmt$,
    test_schema_name||'.'||table_name,
    schema_table_name,
    grid_metagrid_grader
    );


  END LOOP;



END$$;















pg_dump -h cpu11.int.nibio.no -U postgres rog_02 -n test_13 > /Users/lop/dev/github/resolve-overlap-and-gap/src/test/sql/regress/inputdata/test_13b.sql

sh src/test/sql/regress/run_resolve_test.sh  ./src/test/sql/regress/rog_overlay_test_13.sql; \
cat /tmp/pgis_reg/test_3_diff

*/

--CREATE EXTENSION pg_stat_statements ;

--select pg_stat_statements_reset();



CALL topo_rog_static.rog_overlay(
ARRAY[
'test_13.bygg_arsvers_23_v2',
'test_13.gsk_2023_segmenter_renska'
],
'gronn_2023_v9',
4258, --srid
0.0, -- no snapto
5.0, -- min m2 area to keep
200, -- split all lines at given vertex num
true, -- _break_up_big_polygons
NULL,
ARRAY[
('0103000020A21000000100000005000000FC60C227D47F09409A564F720FB64D40FC60C227D47F0940B6D8185120874E40EBF20689D0021D40B6D8185120874E40EBF20689D0021D409A564F720FB64D40FC60C227D47F09409A564F720FB64D40','004')::rog_input_boundary
] , -- run IN 22 area blocks
false,
10, -- _max_parallel_jobs int, -- this is the max number of paralell jobs to run. There must be at least the same number of free connections
100, -- _max_rows_in_each_cell int, -- this is the max number rows that intersects with box before it's split into 4 new boxes
NULL, --rog_overlay_source_table_metainfo_type[],
ARRAY[
	  NULL, -- no cleanup for org_gronnstruktur_fkb.bygg_arsvers_23
	  rog_clean_edge_type_func(
	    0.00003,
	    NULL,
	    ('clname','{treeLayer,fieldLayer,trees,bushLayer,FKB_bygg,greyArea}', '{0.00001,0.00001,0.00001,0.00001,0.00001,0.000025}')::rog_clean_simplify_attribute_values
	  )
],
resolve_overlap_data_debug_options_func(
  false, --if set to false, it will do topology.ValidateTopology and stop to if the this call returns any rows
  false, -- if set to true, it will do topology.ValidateTopology at each loop return if it's error
  true, --  if set to false, it will in many cases generate topo errors beacuse of running in many parralell threads
  ARRAY[
  'test_13.multi_input_2_grid',
  'test_13.multi_input_2_grid_metagrid_0001',
  'test_13.multi_input_2_grid_metagrid_0001_lines',
  'test_13.multi_input_2_grid_metagrid_0002',
  'test_13.multi_input_2_grid_metagrid_0002_lines',
  'test_13.multi_input_2_grid_metagrid_0003',
  'test_13.multi_input_2_grid_metagrid_0003_lines',
  'test_13.multi_input_2_grid_metagrid_0004',
  'test_13.multi_input_2_grid_metagrid_0004_lines'
  ]::text[]
)
);


SELECT * FROM topology.ValidateTopology('gronn_2023_v9_004');

SELECT 'num surfaces total', count(*) from gronn_2023_v9.multi_input_2_v ;

SELECT 'num surfaces total below 5.0', count(*) from gronn_2023_v9.multi_input_2_v WHERE ST_Area(geom,true) < 5.0;

SELECT 'surfaces total below 5.0', ST_area(geom,true) from gronn_2023_v9.multi_input_2_v WHERE ST_Area(geom,true) < 5.0;


