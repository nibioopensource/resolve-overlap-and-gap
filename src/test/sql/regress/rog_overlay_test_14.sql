set client_min_messages to ERROR;
set timezone to utc;

set execute_parallel.connstring = 'host=localhost dbname=nibio_reg user=postgres password=testp101';

/**



https://gitlab.com/nibioopensource/resolve-overlap-and-gap/-/issues/82

skog_maska_01_topo.multi_input_1_gri

-- picture 02
select ST_envelope(ST_Union(geom)) from skog_maska_01_topo.multi_input_1 where face_id in ('171134');
-- 0103000020A21000000100000005000000019758A2DB7D274036DB66AA0EF44E40019758A2DB7D2740DC991CFA2AF44E40E79280C8DF7E2740DC991CFA2AF44E40E79280C8DF7E274036DB66AA0EF44E40019758A2DB7D274036DB66AA0EF44E40

-- picture 01
select ST_envelope(ST_Union(geom)) from skog_maska_01_topo.multi_input_1 where face_id in ('168877');
-- 0103000020A21000000100000005000000BE23BAECC0A227404C2A73609DF24E40BE23BAECC0A227400A914E6ED0F24E409A19DFDA60A427400A914E6ED0F24E409A19DFDA60A427404C2A73609DF24E40BE23BAECC0A227404C2A73609DF24E40

-- picture 03
select ST_envelope(ST_Union(geom)) from skog_maska_01_topo.multi_input_1 where face_id in ('182974');
-- 0103000020A21000000100000005000000ADE8B529AF4C28400683F36AACF24E40ADE8B529AF4C28401D20212ADAF24E4089CAF15A214F28401D20212ADAF24E4089CAF15A214F28400683F36AACF24E40ADE8B529AF4C28400683F36AACF24E40

-- picture 04
select ST_envelope(ST_Union(geom)) from skog_maska_01_topo.multi_input_1 where face_id in ('115997');
-- 0103000020A2100000010000000500000073722D9E929F1E402DBD46B9A8154D4073722D9E929F1E404B0D3190A9154D408742D6F0B69F1E404B0D3190A9154D408742D6F0B69F1E402DBD46B9A8154D4073722D9E929F1E402DBD46B9A8154D40


DO
$$DECLARE
input_tables text[] = ARRAY[
'prosj_skogmaske,nyesegmenter_testomrader,geo,;class'
];
schema_table_name text;
schema_table_name_srid int;
schema_table_name_geom text;
schema_table_name_columns text;
schema_name text;
table_name text;
test_schema_name text = 'test_14';

bbox_grader geometry = ST_buffer('0103000020A2100000010000000500000073722D9E929F1E402DBD46B9A8154D4073722D9E929F1E404B0D3190A9154D408742D6F0B69F1E404B0D3190A9154D408742D6F0B69F1E402DBD46B9A8154D4073722D9E929F1E402DBD46B9A8154D40'::geometry,0.001);

grid_metagrid_grader geometry = bbox_grader;
grid_metagrid_grader_tmp geometry;

metagrid_tables text[] = ARRAY[
  'skog_maska_01_topo.multi_input_1_grid',
  'skog_maska_01_topo.multi_input_1_grid_metagrid_0001',
  'skog_maska_01_topo.multi_input_1_grid_metagrid_0001_lines',
  'skog_maska_01_topo.multi_input_1_grid_metagrid_0002',
  'skog_maska_01_topo.multi_input_1_grid_metagrid_0002_lines',
  'skog_maska_01_topo.multi_input_1_grid_metagrid_0003',
  'skog_maska_01_topo.multi_input_1_grid_metagrid_0003_lines',
  'skog_maska_01_topo.multi_input_1_grid_metagrid_0004',
  'skog_maska_01_topo.multi_input_1_grid_metagrid_0004_lines',
  'skog_maska_01_topo.multi_input_1_grid_metagrid_0005',
  'skog_maska_01_topo.multi_input_1_grid_metagrid_0005_lines',
  'skog_maska_01_topo.multi_input_1_grid_metagrid_0006',
  'skog_maska_01_topo.multi_input_1_grid_metagrid_0006_lines',
  'skog_maska_01_topo.multi_input_1_grid_metagrid_0007',
  'skog_maska_01_topo.multi_input_1_grid_metagrid_0007_lines',
  'skog_maska_01_topo.multi_input_1_grid_metagrid_0008',
  'skog_maska_01_topo.multi_input_1_grid_metagrid_0008_lines'
];

BEGIN

  EXECUTE Format($fmt$
  DROP schema IF EXISTS %1$s CASCADE;
  CREATE schema %1$s;
  $fmt$,
  test_schema_name
  );

  FOREACH schema_table_name IN ARRAY input_tables LOOP
    schema_name = (string_to_array(schema_table_name, ',')::text[])[1];
    table_name = (string_to_array(schema_table_name, ',')::text[])[2];
    schema_table_name_geom = (string_to_array(schema_table_name, ',')::text[])[3];
    schema_table_name_columns = (string_to_array(schema_table_name, ',')::text[])[4];

	schema_table_name_columns =  REPLACE(schema_table_name_columns,';',',');

    RAISE NOTICE 'table_name %' , schema_table_name;
    RAISE NOTICE 'schema_table_name_columns %', schema_table_name_columns;

    EXECUTE Format($fmt$ SELECT Find_SRID(%1$L,%2$L,%3$L) $fmt$,
    schema_name, table_name, schema_table_name_geom) INTO schema_table_name_srid;

    RAISE NOTICE '% schema_table_name_srid %', (Format($fmt$ SELECT Find_SRID(%1$L,%2$L,%3$L) $fmt$,
    schema_name, table_name, schema_table_name_geom)), schema_table_name_srid;


    EXECUTE Format($fmt$
    CREATE TABLE %1$s AS
    select t1.%5$s::geometry(Polygon,%4$s) %5$s %6$s
    FROM %2$s t1
    WHERE ST_Intersects(%5$s,%3$L);


    ALTER TABLE %1$s ADD column id serial primary key;
    CREATE INDEX ON %1$s USING GIST (%5$s);

    $fmt$,
    test_schema_name||'.'||table_name,
    schema_name||'.'||table_name,
    ST_Transform(bbox_grader,schema_table_name_srid),
    schema_table_name_srid,
    schema_table_name_geom,
    schema_table_name_columns
    );

    EXECUTE Format($fmt$
    SELECT ST_transform(ST_Union(%2$s),4258) FROM %1$s
    $fmt$,
    test_schema_name||'.'||table_name,
    schema_table_name_geom
    ) INTO grid_metagrid_grader_tmp;

    IF grid_metagrid_grader_tmp IS NOT NULL THEN
    	grid_metagrid_grader = ST_Union(grid_metagrid_grader,grid_metagrid_grader_tmp);
    END IF;

  END LOOP;


  FOREACH schema_table_name IN ARRAY metagrid_tables LOOP
    schema_name = (string_to_array(schema_table_name, '.')::text[])[1];
    table_name = (string_to_array(schema_table_name, '.')::text[])[2];
    RAISE NOTICE 'table_name %' , schema_table_name;


    EXECUTE Format($fmt$

    CREATE TABLE %1$s ( like  %2$s);
    INSERT INTO %1$s SELECT * FROM %2$s WHERE ST_Intersects(cell_bbox,%3$L);
    ALTER TABLE %1$s ADD primary key(id);
    CREATE INDEX ON %1$s USING GIST (cell_bbox);

    $fmt$,
    test_schema_name||'.'||table_name,
    schema_table_name,
    grid_metagrid_grader
    );


  END LOOP;

END$$;














pg_dump -h laop-test-pg01 -U postgres rog_03 -n test_14 > /Users/lop/dev/github/resolve-overlap-and-gap/src/test/sql/regress/inputdata/test_14.sql

sh src/test/sql/regress/run_resolve_test.sh  ./src/test/sql/regress/rog_overlay_test_14.sql; \
cat /tmp/pgis_reg/test_3_diff

*/

--CREATE EXTENSION pg_stat_statements ;

--select pg_stat_statements_reset();


-- complicate the picture by adding two eual values
INSERT INTO test_14.nyesegmenter_testomrader(geo, class)
SELECT geo, 'equal class' class FROM test_14.nyesegmenter_testomrader where id = 1;

-- complicate the picture by adding squal
INSERT INTO test_14.nyesegmenter_testomrader(geo, class)
SELECT ST_envelope(ST_buffer(ST_Centroid(geo),5)) geo, 'retangle inside' class FROM test_14.nyesegmenter_testomrader where id = 1;

INSERT INTO test_14.nyesegmenter_testomrader(geo, class)
SELECT ST_envelope(ST_buffer(ST_Centroid(geo),10)) geo, 'retangle inside' class FROM test_14.nyesegmenter_testomrader where id = 1;

CALL topo_rog_static.rog_overlay(
ARRAY[
'test_14.nyesegmenter_testomrader'
],
    'skog_maska_01',
    4258, --srid
    0, -- no snapto
    0, -- min m2 area to keep
    200, --max vertx length
    true, -- _break_up_big_polygons
    NULL, -- min negative buffer value null, remove sliver polygons
    NULL,
--    ARRAY[('0103000020A2100000010000000500000022ADD13DA6C72740C81B7A166FC44E4022ADD13DA6C727406E784C361D1E4F40E0BCF2C5C1DA2A406E784C361D1E4F40E0BCF2C5C1DA2A40C81B7A166FC44E4022ADD13DA6C72740C81B7A166FC44E40','topo')::rog_input_boundary] ,
    false, -- validate the results
    3,--  Method 1 topology.TopoGeo_addLinestring
  --  Method 2 topology.TopoGeo_LoadGeometry
  --  Method 3 select from tmp toplogy into master topology
    10, -- _max_parallel_jobs int, -- this is the max number of paralell jobs to run. There must be at least the same number of free connections
    500, -- _max_rows_in_each_cell int, -- this is the max number rows that intersects with box before it's split into 4 new boxes
    NULL,
	ARRAY[
	  rog_clean_edge_type_func(
	    0.00001,
	    NULL,
	    ('class','{}', '{}')::rog_clean_simplify_attribute_values

	  )
	],
  resolve_overlap_data_debug_options_func(
  false, --if set to false, it will do topology.ValidateTopology and stop to if the this call returns any rows
  false, -- if set to true, it will do topology.ValidateTopology at each loop return if it's error
  true, --  if set to false, it will in many cases generate topo errors beacuse of running in many parralell threads
  ARRAY[
  'test_14.multi_input_1_grid',
  'test_14.multi_input_1_grid_metagrid_0001',
  'test_14.multi_input_1_grid_metagrid_0001_lines',
  'test_14.multi_input_1_grid_metagrid_0002',
  'test_14.multi_input_1_grid_metagrid_0002_lines',
  'test_14.multi_input_1_grid_metagrid_0003',
  'test_14.multi_input_1_grid_metagrid_0003_lines',
  'test_14.multi_input_1_grid_metagrid_0004',
  'test_14.multi_input_1_grid_metagrid_0004_lines',
  'test_14.multi_input_1_grid_metagrid_0005',
  'test_14.multi_input_1_grid_metagrid_0005_lines',
  'test_14.multi_input_1_grid_metagrid_0006',
  'test_14.multi_input_1_grid_metagrid_0006_lines',
  'test_14.multi_input_1_grid_metagrid_0007',
  'test_14.multi_input_1_grid_metagrid_0007_lines',
  'test_14.multi_input_1_grid_metagrid_0008',
  'test_14.multi_input_1_grid_metagrid_0008_lines'
  ]::text[]
  )
);


SELECT * FROM topology.ValidateTopology('skog_maska_01_topo');

SELECT 'num surfaces total', count(*) from skog_maska_01_topo.multi_input_1 ;

SELECT 'null id', count(*) from skog_maska_01_topo.multi_input_1 WHERE "1-pk-id-test_14.nyesegmenter_testomrader" IS NULL;

SELECT 'num surfaces with value from simple feature', count(*) from skog_maska_01.multi_input_1_v ;

SELECT 'num rows in primary key face mapping table', count(*) from skog_maska_01_topo.multi_input_1_org_pk_value_to_face_id ;


-- For each face check that value in the mappiong table check what old id's we are poining back to;
SELECT 'new_face_id before check left/right face:' || face.face_id as face_id,
ARRAY_AGG(new.src_table_pk_column_value ORDER BY (src_table_pk_column_value::int)) id_ref_list
FROM
skog_maska_01_topo.face face
LEFT JOIN skog_maska_01_topo.multi_input_1_org_pk_value_to_face_id new ON new.face_id = face.face_id
LEFT JOIN test_14.nyesegmenter_testomrader old ON old.id = new.src_table_pk_column_value::int
AND ST_Covers(
        ST_Transform(old.geo,4258),
		(ST_MaximumInscribedCircle(ST_GetFaceGeometry('skog_maska_01_topo',face.face_id))).center
	)
WHERE face.face_id > 0 AND new.pk_value_set AND new.table_input_order = 1
GROUP BY face.face_id
ORDER BY face.face_id;


-- Test for area for each type based on old old types from new test_14.nyesegmenter_testomrader

SELECT 'old mapping table',
org.id, org.area, org.num_geom, org.num_points,
overlay.id overlay_id, overlay.area overlay_area, overlay.num_geom overlay_num_geom, overlay.num_points overlay_num_points
FROM (
	SELECT id, ROUND(SUM(ST_Area(geo))) area, count(*) num_geom, sum(ST_Npoints(geo)) num_points
	FROM test_14.nyesegmenter_testomrader
	GROUP BY id ORDER BY ID
) org
LEFT JOIN
(
	SELECT id, ROUND(SUM(ST_Area(geo,true))) area, count(*) num_geom, sum(ST_Npoints(geo)) num_points
	FROM (
	SELECT ST_GetFaceGeometry('skog_maska_01_topo',face.face_id) geo, old.id
	FROM
	skog_maska_01_topo.face face
	LEFT JOIN
	(
		SELECT * FROM (
		SELECT unnest("1-pk_list-id-test_14.nyesegmenter_testomrader") id, face_id
		FROM skog_maska_01.multi_input_1_v
		UNION
		SELECT "1-pk-id-test_14.nyesegmenter_testomrader" id, face_id
		FROM skog_maska_01.multi_input_1_v
		) s
	) new ON new.face_id = face.face_id
	LEFT JOIN test_14.nyesegmenter_testomrader old ON old.id = new.id::int
	WHERE face.face_id > 0
	) r
	GROUP BY id ORDER BY id
) overlay ON org.id = overlay.id
ORDER BY (overlay.id::int) DESC; -- to get null value in the top

-- Value 12 is missing because of simplify and wrong mapping back based on ST_MaximumInscribedCircle
-- Value 5 is missing because of simplify
SELECT 'Values missing based on spatial check from test_14.nyesegmenter_testomrader', org.id
FROM
test_14.nyesegmenter_testomrader org
WHERE
NOT EXISTS
(SELECT
TRUE
FROM
(
	SELECT id, ROUND(SUM(ST_Area(geo,true))) area, count(*) num_geom, sum(ST_Npoints(geo)) num_points
	FROM (
	SELECT ST_GetFaceGeometry('skog_maska_01_topo',face.face_id) geo, old.id
	FROM
	skog_maska_01_topo.face face
	LEFT JOIN
	(
		SELECT * FROM (
		SELECT unnest("1-pk_list-id-test_14.nyesegmenter_testomrader") id, face_id
		FROM skog_maska_01.multi_input_1_v
		UNION
		SELECT "1-pk-id-test_14.nyesegmenter_testomrader" id, face_id
		FROM skog_maska_01.multi_input_1_v
		) s
	) new ON new.face_id = face.face_id
	LEFT JOIN test_14.nyesegmenter_testomrader old ON old.id = new.id::int
	WHERE face.face_id > 0
	) r
	GROUP BY id ORDER BY id
) new
WHERE org.id = new.id::int
)

ORDER BY org.id;

-- Value 12 is missing because of simplify and wrong mapping back based on ST_MaximumInscribedCircle
-- Value 5 is missing because of simplify
SELECT 'Values missing based on id check only from test_14.nyesegmenter_testomrader', org.id
FROM
test_14.nyesegmenter_testomrader org
WHERE
NOT EXISTS
(SELECT
TRUE
FROM
(
	SELECT id
	FROM (
	SELECT old.id
	FROM
	skog_maska_01_topo.face face
	LEFT JOIN
	(
		SELECT * FROM (
		SELECT unnest("1-pk_list-id-test_14.nyesegmenter_testomrader") id, face_id
		FROM skog_maska_01.multi_input_1_v
		UNION
		SELECT "1-pk-id-test_14.nyesegmenter_testomrader" id, face_id
		FROM skog_maska_01.multi_input_1_v
		) s
	) new ON new.face_id = face.face_id
	LEFT JOIN test_14.nyesegmenter_testomrader old ON old.id = new.id::int
	) r
	GROUP BY id ORDER BY id
) new
WHERE org.id = new.id::int
)

ORDER BY org.id;

/**
-- Test for area for each type based on old old types from new test_14.nyesegmenter_testomrader
SELECT
'new mapping table',
org.id, org.area, org.num_geom, org.num_points,
overlay.id overlay_id, overlay.area overlay_area, overlay.num_geom overlay_num_geom, overlay.num_points overlay_num_points
FROM (
	SELECT id, ROUND(SUM(ST_Area(geo))) area, count(*) num_geom, sum(ST_Npoints(geo)) num_points
	FROM test_14.nyesegmenter_testomrader
	GROUP BY id ORDER BY ID
) org
LEFT JOIN
(
	SELECT id, ROUND(SUM(ST_Area(geo,true))) area, count(*) num_geom, sum(ST_Npoints(geo)) num_points
	FROM (
	SELECT ST_GetFaceGeometry('skog_maska_01_topo',face.face_id) geo, old.id
	FROM
	skog_maska_01_topo.face face
	LEFT JOIN skog_maska_01_topo.multi_input_1_org_pk_value_to_face_id new ON new.face_id = face.face_id
	LEFT JOIN test_14.nyesegmenter_testomrader old ON old.id = new.src_table_pk_column_value::int
	WHERE face.face_id > 0 AND new.pk_value_set AND new.table_input_order = 1
	) r
	GROUP BY id ORDER BY ID
) overlay ON org.id = overlay.id
ORDER BY (overlay.id::int) DESC; -- to get null value in the top


-- Test for missing id's in new result
SELECT 'Values missing from test_14.nyesegmenter_testomrader', org.id
FROM
test_14.nyesegmenter_testomrader org
WHERE
NOT EXISTS (SELECT TRUE
FROM skog_maska_01_topo.multi_input_1_org_pk_value_to_face_id new
WHERE org.id = new.src_table_pk_column_value::int AND new.table_input_order = 1)
ORDER BY org.id;


*/

--clean up
SELECT 'degrees', topology.droptopology('skog_maska_01_topo');
drop SCHEMA skog_maska_01 CASCADE;
drop SCHEMA test_14 CASCADE;
