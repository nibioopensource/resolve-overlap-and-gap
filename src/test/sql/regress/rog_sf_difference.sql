-- To avoid error since binary geo seems to vary from local systen and Travis

/**

CREATE table all_area AS TABLE sl_lop.la_t1;
ALTER TABLE all_area ADD PRIMARY KEY (id);
ALTER TABLE all_area ALTER COLUMN geo type geometry(MultiPolygon, 4258) USING ST_SetSRID(ST_Multi(geo),4258);
ALTER TABLE all_area ADD COLUMN row_type char default 'a';

CREATE table used_area AS TABLE sl_lop.lb_t1;
ALTER TABLE used_area ADD PRIMARY KEY (id);
ALTER TABLE used_area ALTER COLUMN geo type geometry(MultiPolygon, 4258) USING ST_SetSRID(ST_Multi(geo),4258);
ALTER TABLE used_area ADD COLUMN row_type char default 'b';

CREATE ROLE <role> LOGIN SUPERUSER;

*/

SET client_min_messages TO ERROR;

-- This is all the area that are available to be used for selected acivities 
SELECT 'all_area input sum', COUNT(*), round(Sum(ST_area(geo,true)))  FROM all_area;

-- This is the area already used by an activty (in some cases the used area may extend the input area) 
SELECT 'used_area input sum', COUNT(*), round(Sum(ST_area(geo,true)))  FROM used_area;
SELECT 'used_area detail', ST_GeometryType(r.geom), count(*), round(ST_Area(r.geom,true)), ST_ISValid(r.geom), ST_NPoints(r.geom)
FROM (SELECT (ST_Dump(u.geo)).geom FROM used_area u) r 
GROUP BY r.geom ORDER BY ST_Area(r.geom,true);

-- This is a test where a user want's find the rest off available area for new activities area, so we need to remove the area already used.
-- So it's really a plain ST_Difference
-- In this test use ST_Collect because the user should be selected free area from a map after this operation

/**

CREATE TABLE difference_test_01 AS 
SELECT (ST_Dump((ST_Difference(pt.geo,u.geo)))).geom AS geom FROM 
( SELECT ST_Collect(distinct pt.geo) AS geo FROM all_area pt WHERE ST_IsValid(pt.geo)) AS pt, 
( SELECT ST_Union(geo) AS geo FROM used_area u WHERE ST_IsValid(geo) ) AS u;

*/

-- The code below was suggested by Sandro and that works and gives the correct result 

CREATE TABLE difference_test_01 AS
SELECT
  ST_Difference(
    a.geo,
    ( SELECT ST_Union(u.geo) FROM used_area u )
  )
AS geom
FROM all_area a;


-- Just add to make easy view in QGIS
ALTER TABLE difference_test_01 ADD COLUMN id SERIAL PRIMARY KEY;

--SELECT 'difference_test_01 detail', ST_GeometryType(r.geom), count(*), round(ST_Area(r.geom,true)), ST_ISValid(r.geom), ST_NPoints(r.geom)
--FROM difference_test_01 r GROUP BY r.geom ORDER BY ST_Area(r.geom,true);

SELECT 'difference_test_01 sum', COUNT(*), sum(round(ST_Area(r.geom,true))), sum(ST_NPoints(r.geom)) FROM difference_test_01 r;

-- The result differ from postgis version to postgis version as see below

--Wrong result db09test
--POSTGIS="3.2.0 c3e3cc0" [EXTENSION] PGSQL="120" GEOS="3.10.1-CAPI-1.16.0" SFCGAL="1.3.7" PROJ="8.2.0" GDAL="GDAL 3.4.0, released 2021/11/04" LIBXML="2.9.10" LIBJSON="0.13.1" LIBPROTOBUF="1.3.3" WAGYU="0.5.0 (Internal)" TOPOLOGY RASTER
--ON Postgresql 12
--result = difference_test_01 sum|9|12374747|72

--Wrong result localhost
--POSTGIS="3.2.3 2f97b6c" [EXTENSION] PGSQL="140" GEOS="3.11.0-CAPI-1.17.0" PROJ="9.0.1" LIBXML="2.9.14" LIBJSON="0.16" LIBPROTOBUF="1.4.1" WAGYU="0.5.0 (Internal)" TOPOLOGY
--ON Postgresql 14
--result = difference_test_01 sum|9|12374747|72

--Wrong result CI
--POSTGIS="3.3.3 2355e8e" [EXTENSION] PGSQL="120" GEOS="3.10.2-CAPI-1.16.0" PROJ="8.2.1" LIBXML="2.9.13" LIBJSON="0.15" LIBPROTOBUF="1.3.3" WAGYU="0.5.0 (Internal)" TOPOLOGY
--ON Postgresql 12, 13 and 14
--result = difference_test_01 sum|8|11848089|67

--But it seems to work on a local server by Sandro with lates code from source (db01tempdb)
--POSTGIS="3.4.0dev 3.3.0rc2-958-g4c776d418" [EXTENSION] PGSQL="120" GEOS="3.9.1-CAPI-1.14.2" SFCGAL="1.3.7" PROJ="7.2.1" LIBXML="2.9.10" LIBJSON="0.13.1" LIBPROTOBUF="1.3.3" WAGYU="0.5.0 (Internal)" TOPOLOGY
--ON Postgresql 12
--result = difference_test_01 sum|1551|15799749|412840

-- Test by doing ST_union
CREATE TABLE difference_test_02 AS 
SELECT (ST_Dump((ST_Difference(pt.geo,u.geo)))).geom AS geom FROM 
( SELECT ST_Union(distinct pt.geo) AS geo FROM all_area pt WHERE ST_IsValid(pt.geo)) AS pt, 
( SELECT ST_Union(distinct u.geo) AS geo FROM used_area u) AS u;

-- Just add to make easy view in QGIS
ALTER TABLE difference_test_02 ADD COLUMN id SERIAL PRIMARY KEY;

SELECT 'difference_test_02 sum', COUNT(*), sum(round(ST_Area(r.geom,true))), sum(ST_NPoints(r.geom)) FROM difference_test_02 r;


--Let's try to make the case simpler by only focusing on the area that intersects the used area
ALTER TABLE all_area ADD COLUMN intersets_used_area boolean default false;

UPDATE all_area a 
SET intersets_used_area = TRUE
FROM used_area u WHERE ST_Intersects(u.geo,a.geo); 

-- In difference_test_03 when then only check those that intersects
CREATE TABLE difference_test_03 AS 
SELECT (ST_Dump((ST_Difference(pt.geo,u.geo)))).geom AS geom FROM 
( SELECT ST_Collect(distinct pt.geo) AS geo FROM all_area pt WHERE intersets_used_area AND ST_IsValid(pt.geo)) AS pt, 
used_area u;
-- Just add to make easy view in QGIS
ALTER TABLE difference_test_03 ADD COLUMN id SERIAL PRIMARY KEY;
SELECT 'difference_test_03 sum', COUNT(*), sum(round(ST_Area(r.geom,true))), sum(ST_NPoints(r.geom)) FROM difference_test_03 r;


--Wrong result db09test
--POSTGIS="3.2.0 c3e3cc0" [EXTENSION] PGSQL="120" GEOS="3.10.1-CAPI-1.16.0" SFCGAL="1.3.7" PROJ="8.2.0" GDAL="GDAL 3.4.0, released 2021/11/04" LIBXML="2.9.10" LIBJSON="0.13.1" LIBPROTOBUF="1.3.3" WAGYU="0.5.0 (Internal)" TOPOLOGY RASTER
--ON Postgresql 12
--result 

--Wrong result localhost
--POSTGIS="3.2.3 2f97b6c" [EXTENSION] PGSQL="140" GEOS="3.11.0-CAPI-1.17.0" PROJ="9.0.1" LIBXML="2.9.14" LIBJSON="0.16" LIBPROTOBUF="1.4.1" WAGYU="0.5.0 (Internal)" TOPOLOGY
--ON Postgresql 14
--result =  difference_test_03 sum |     0 | [NULL] | [NULL]

--Wrong result CI
--POSTGIS="3.3.3 2355e8e" [EXTENSION] PGSQL="120" GEOS="3.10.2-CAPI-1.16.0" PROJ="8.2.1" LIBXML="2.9.13" LIBJSON="0.15" LIBPROTOBUF="1.3.3" WAGYU="0.5.0 (Internal)" TOPOLOGY
--ON Postgresql 12, 13 and 14
--result = 

--Wrong result localhost (db01tempdb)
--POSTGIS="3.4.0dev 3.3.0rc2-958-g4c776d418" [EXTENSION] PGSQL="120" GEOS="3.9.1-CAPI-1.14.2" SFCGAL="1.3.7" PROJ="7.2.1" LIBXML="2.9.10" LIBJSON="0.13.1" LIBPROTOBUF="1.3.3" WAGYU="0.5.0 (Internal)" TOPOLOGY
--ON Postgresql 12
--result = difference_test_03 sum |     1 | 2633 |   4


--Let's try to make the case simpler by only focusing on the area that are the used area
ALTER TABLE all_area ADD COLUMN close_to_used_area boolean default false;

UPDATE all_area a 
SET close_to_used_area = TRUE
FROM used_area u WHERE ST_IsValid(u.geo) AND ST_Distance(a.geo,u.geo,true) < 10;

-- In difference_test_04 when then only check those that intersects
CREATE TABLE difference_test_04 AS 
SELECT (ST_Dump((ST_Difference(pt.geo,u.geo)))).geom AS geom FROM 
( SELECT ST_Collect(distinct pt.geo) AS geo FROM all_area pt WHERE close_to_used_area AND ST_IsValid(pt.geo)) AS pt, 
used_area u;
-- Just add to make easy view in QGIS
ALTER TABLE difference_test_04 ADD COLUMN id SERIAL PRIMARY KEY;
SELECT 'difference_test_04 sum', COUNT(*), sum(round(ST_Area(r.geom,true))), sum(ST_NPoints(r.geom)) FROM difference_test_04 r;


DROP TABLE difference_test_01;
DROP TABLE difference_test_02;
DROP TABLE difference_test_03;
DROP TABLE difference_test_04;
DROP TABLE all_area;
DROP TABLE used_area;
