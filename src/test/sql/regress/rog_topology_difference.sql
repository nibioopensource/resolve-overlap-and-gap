
SET client_min_messages TO ERROR;

-- This is all the area that are available to be used for selected acivities
SELECT 'all_area input sum', COUNT(*), round(Sum(ST_area(geo,true)))  FROM all_area;

-- This is the area already used by an activty (in some cases the used area may extend the input area)
SELECT 'used_area input sum', COUNT(*), round(Sum(ST_area(geo,true)))  FROM used_area;
SELECT 'used_area detail', ST_GeometryType(r.geom), count(*), round(ST_Area(r.geom,true)), ST_ISValid(r.geom), ST_NPoints(r.geom)
FROM (SELECT (ST_Dump(u.geo)).geom FROM used_area u) r
GROUP BY r.geom ORDER BY ST_Area(r.geom,true);

-- This is a test where a user want's find the rest off available area for new activities area, so we need to remove the area already used.
-- So it's really a plain ST_Difference
-- In this test use ST_Collect because the user should be selected free area from a map after this operation

CALL topo_rog_static.rog_overlay(
ARRAY['public.all_area','public.used_area'],
'topo_result',
4258,
0.0,-- topology_snap_tolerance
1, --min area in m2
NULL, -- split all lines at given vertex num,
false, -- _break_up_big_polygons
1, -- This is an approximate value min_st_is_empty_buffer in meter used remove sliver polygon
ARRAY[
('SRID=4258;MULTIPOLYGON(((10.630033998040961 64.95128849863016,10.630033998040961 64.99207986978575,10.712862998539457 64.99207986978575,10.712862998539457 64.95128849863016,10.630033998040961 64.95128849863016)),((10.754277498788703 64.95128849863016,10.754277498788703 64.96148634141906,10.774984748913326 64.96148634141906,10.774984748913326 64.95128849863016,10.754277498788703 64.95128849863016)))'::geometry,
'test_01')::rog_input_boundary
],

  -- a flag to run checks like topology.ValidateTopology and https://github.com/larsop/resolve-overlap-and-gap
  -- To run this does take time, spesially when working on verry very datasets so we trun this when needed.
  -- In the final result there should be no Topology errors in the topology structure
  -- In the simple feature result produced from Postgis Topology there should be overlaps
true,

10, -- _max_parallel_jobs int, -- this is the max number of paralell jobs to run. There must be at least the same number of free connections
200, -- _max_rows_in_each_cell int, -- this is the max number rows that intersects with box before it's split into 4 new boxes
null
);

--SELECT ST_AsEwkt(ST_Collect(cell_bbox)) from topo_result_test_01.multi_input_2_grid  where id in( 19, 17);

-- Create a simple feature with the difference result
CREATE TABLE topo_difference_test_01 AS SELECT geom FROM topo_result_test_01.multi_input_2_v
WHERE  "1-pk-id-public.all_area" IS NOT NULL AND "2-pk-id-public.used_area"  IS NULL;

-- Just add to make easy view in QGIS
ALTER TABLE topo_difference_test_01 ADD COLUMN id SERIAL PRIMARY KEY;

--SELECT 'topo_difference_test_01 detail', ST_GeometryType(r.geom), count(*), round(ST_Area(r.geom,true)), ST_ISValid(r.geom), ST_NPoints(r.geom)
--FROM topo_difference_test_01 r GROUP BY r.geom ORDER BY ST_Area(r.geom,true);

SELECT 'topo_difference_test_01 sum', COUNT(*), sum(round(ST_Area(r.geom,true))), sum(ST_NPoints(r.geom)) FROM topo_difference_test_01 r;
--result from  POSTGIS="3.2.3 2f97b6c" [EXTENSION] PGSQL="140" GEOS="3.11.0-CAPI-1.17.0" PROJ="9.0.1" LIBXML="2.9.14" LIBJSON="0.16" LIBPROTOBUF="1.4.1" WAGYU="0.5.0 (Internal)" TOPOLOGY
-- sinple st__difference           difference_test_01 sum|1551|15799749|412840
-- one big job                topo_difference_test_01 sum|1734|15799747|442667
-- split in smaller parts     topo_difference_test_01 sum|1734|15799747|440021
-- let topology fix the edges topo_difference_test_01 sum|1734|15799746|441133

-- test with new code for remove for tiny poygons topo_difference_test_01 sum|1732|15799746|443755

SELECT 'count all multi_input_2_border_line_segments', count(geom) from topo_result_test_01.multi_input_2_border_line_segments_final ;

SELECT 'count distinct on geo multi_input_2_border_line_segments', count(distinct ST_AsBinary(geom)) from topo_result_test_01.multi_input_2_border_line_segments_final ;

-- Records from this function would mean an invalid topology was created
SELECT 'topo validation, topo_result_test_01', * FROM topology.ValidateTopology('topo_result_test_01');

SELECT topology.droptopology('topo_result_test_01');

DROP TABLE topo_difference_test_01 CASCADE;
DROP TABLE all_area;
DROP TABLE used_area;

drop SCHEMA topo_result CASCADE;



-- Fails on POSTGIS="3.2.3 2f97b6c" [EXTENSION] PGSQL="140" GEOS="3.11.0-CAPI-1.17.0" PROJ="9.0.1" LIBXML="2.9.14" LIBJSON="0.16" LIBPROTOBUF="1.4.1" WAGYU="0.5.0 (Internal)" TOPOLOGY
-- topo validation, topo_result_test_01|invalid next_left_edge|4477|4478
-- topo validation, topo_result_test_01|invalid next_right_edge|4478|-4559
-- topo validation, topo_result_test_01|invalid next_right_edge|4476|-4477

-- NO topo error on
-- POSTGIS="3.4.0dev 3.3.0rc2-958-g4c776d418" [EXTENSION] PGSQL="120" GEOS="3.9.1-CAPI-1.14.2" SFCGAL="1.3.7" PROJ="7.2.1" LIBXML="2.9.10" LIBJSON="0.13.1" LIBPROTOBUF="1.3.3" WAGYU="0.5.0 (Internal)" TOPOLOGY


