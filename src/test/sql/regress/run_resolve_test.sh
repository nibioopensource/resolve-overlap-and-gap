#!/bin/sh

REGDIR=$(dirname $0)
TOPDIR=${REGDIR}/../../../..

export POSTGIS_REGRESS_DB=nibio_reg
export PGTZ=utc

echo ARGS: $@

if echo $@ | grep -q -- '--nocreate'; then
	:
else
	OPTS="--topology --extension"
	OPTS="${OPTS} --after-create-script ${TOPDIR}/resolve_overlap_and_gap-static.sql"
	OPTS="${OPTS} --after-create-script ${TOPDIR}/install-runtime-deps.sql"
fi

if echo $@ | grep -q -- '--nodrop'; then
	:
else
	OPTS="${OPTS} --before-uninstall-script ${TOPDIR}/resolve_overlap_and_gap-static-uninstall.sql"
	OPTS="${OPTS} --before-uninstall-script ${REGDIR}/hooks/uninstall-test-deps.sql"
fi

${REGDIR}/run_test.pl ${OPTS} $@

