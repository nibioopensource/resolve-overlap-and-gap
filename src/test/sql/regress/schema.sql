--set client_min_messages to warning;

select 'functions_count', n.nspname, count(p.*)
from pg_proc p, pg_namespace n
where p.pronamespace = n.oid
and n.nspname = 'topo_rog_static'
GROUP BY nspname;

show search_path;
